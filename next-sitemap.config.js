module.exports = {
  siteUrl: process.env.HARISENIN_SCHOOL_URL || "https://www.harisenin.com/school",
  generateRobotsTxt: true,
  exclude: ["/checkout", "/proclass/checkout", "/server-sitemap.xml"],
  additionalPaths: async (config) => {
    const result = []
    result.push(await config.transform(config, "/bootcamp"))
    result.push(await config.transform(config, "/proclass"))
    return result
  },
  robotsTxtOptions: {
    additionalSitemaps: [
      `${
        process.env.HARISENIN_SCHOOL_URL || "https://www.harisenin.com/school"
      }/server-sitemap.xml`,
    ],
  },
}
