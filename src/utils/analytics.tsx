interface itemProps {
  item_id: string | undefined
  item_name: string | undefined
  item_category?: string
  item_variant?: string
  item_brand?: string
  item_list_name?: string
  item_list_id?: string
  index?: number
  item_location_id?: string
  price: number | undefined
  currency: 'IDR'
  quantity: number
  item_url?: string
  item_image_url?: string
}

interface checkoutParamsProps {
  currency: 'IDR'
  value?: number
  coupon?: string
  items: itemProps[]
}

interface addPaymentInfoProps extends checkoutParamsProps {
  payment_type: "BANK_TRANSFER" | "EWALLET" | "CREDIT_CARD" | "PAY_LATER",
  payment_method?: string
}


export const trackViewItem = (params: itemProps) => {
  window?.gtag("event", "hms_view_item", params)
}

export const trackAddToCart = (params: itemProps) => {
  window?.gtag("event", "hms_add_to_cart", params)
}

export const trackRemoveFromCart = (params: itemProps) => {
  window?.gtag("event", "hms_remove_from_cart", params)
}

export const trackBeginCheckout = (params: checkoutParamsProps) => {
  window?.gtag("event", "hms_begin_checkout", params)
}

export const trackAddPaymentInfo = (params: addPaymentInfoProps) => {
  window?.gtag("event", "hms_add_payment_info", params)
}