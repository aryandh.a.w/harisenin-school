<<<<<<< HEAD
import { GetServerSideProps } from "next"
import CheckoutRepo from "@lib/api/server-side/checkout.repo"
import { serverTokenChecker } from "@lib/functions"
import PaymentSuccessPage from "@modules/invoice/page/PaymentSuccessPage"

// @ts-ignore

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const checkout = new CheckoutRepo()
  const token = serverTokenChecker(ctx)
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  const { orderId } = ctx.params!

  if (!token) {
    return {
      redirect: {
        destination: "/",
      },
      props: {
        data: {},
      },
    }
  }

  try {
    const res = await checkout.getRawCheckoutDetail(token, orderId as string)

    const body = JSON.parse(res.body)

    return {
      props: {
        invoice: body ?? {},
      },
    }
  } catch (error) {
    return {
      redirect: {
        destination: "/",
      },
      props: {
        invoice: {},
      },
    }
  }
}

export default PaymentSuccessPage
=======
import { GetServerSideProps } from "next"
import { PaymentDetail } from "../../../constants/interfaces/order"
import CheckoutRepo from "../../../lib/apis/checkout.repo"
import { serverTokenChecker } from "../../../lib/utils/method"
import PaymentSuccessPage from "../../../ui/components/invoice/PaymentSuccessPage"

// @ts-ignore
const InvoiceSuccess = ({ checkoutData }: { checkoutData: PaymentDetail }) =>
  PaymentSuccessPage({ invoice: checkoutData })

// @ts-ignore
export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const checkoutId = ctx.params?.orderId as string

  const { res } = ctx
  const token = serverTokenChecker(ctx)
  const repo = new CheckoutRepo()

  if (!token) {
    return {
      redirect: {
        destination: "/",
      },
      props: {
        data: {},
      },
    }
  }

  try {
    const response = await repo.getRawPayments(checkoutId, token)

    if (response.statusCode !== 200) {
      return {
        notFound: true,
      }
    } else {
      const data = JSON.parse(response.body)
      if (data.status) {
        res.statusCode = 500
        return {
          props: {
            error: 500,
          },
        }
      } else {
        return {
          props: {
            checkoutData: data.result,
          },
        }
      }
    }
  } catch (e) {
    console.log(e)
    res.statusCode = 500
    return {
      props: {
        error: 500,
      },
    }
  }
}

export default InvoiceSuccess
>>>>>>> 94ccec2 (setup fix)
