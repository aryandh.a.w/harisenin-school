<<<<<<< HEAD
import CheckoutRepo from "@lib/api/server-side/checkout.repo"
import { GetServerSideProps } from "next"
import { serverTokenChecker } from "@lib/functions/checker"
import InvoicePage from "@modules/invoice/page/InvoiceUnpaidPage"

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const checkout = new CheckoutRepo()
  const token = serverTokenChecker(ctx)
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  const { orderId } = ctx.params!

  if (!token) {
    return {
      redirect: {
        destination: "/",
      },
      props: {
        data: {},
      },
    }
  }

  try {
    const res = await checkout.getRawInvoice(token, orderId as string)

    const body = JSON.parse(res.body)
    const result = body?.result

    return {
      props: {
        data: result ?? {},
      },
    }
  } catch (error) {
    console.log(error)
    return {
      redirect: {
        destination: "/",
      },
      props: {
        data: {},
      },
    }
  }
}

export default InvoicePage
=======
import { GetServerSideProps } from "next"
import { serverTokenChecker } from "../../../lib/utils/method/checker"
import InvoicePage from "../../../ui/components/invoice/InvoiceUnpaidPage"

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const token = serverTokenChecker(ctx)

  if (!token) {
    return {
      redirect: {
        destination: "/",
      },
      props: {
        data: {},
      },
    }
  } else {
    return {
      props: {},
    }
  }
}

export default InvoicePage
>>>>>>> 94ccec2 (setup fix)
