<<<<<<< HEAD
import "regenerator-runtime/runtime"
import NProgress from "nprogress"
import Router from "next/router"

// import Bugsnag from "../lib/functions/bugsnag"
import Error from "./_error"

import "../styles/index.scss"
import "react-multi-carousel/lib/styles.css"
import "react-phone-number-input/style.css"
import "nprogress/nprogress.css"
import { useGoogleOneTapLogin } from "@hooks/useGoogleOneTapLogin"
import dynamic from "next/dynamic"

// const ErrorBoundary = Bugsnag.getPlugin("react")

const FloatingWhatsApp = dynamic(() => import("@harisenin/react-floating-whatsapp"), { ssr: false })

function App({ Component, pageProps }) {
  // Progress bar on top of screen
  Router.events.on("routeChangeStart", () => {
    NProgress.start()
  })
  Router.events.on("routeChangeComplete", () => {
    NProgress.done()
  })
  Router.events.on("routeChangeError", () => {
    NProgress.done()
  })

  useGoogleOneTapLogin()

  if (pageProps.error) {
    return <Error />
  }

  return (
    // @ts-ignore
    // Error boundary wrapper for bugsnag
    <>
    <>
      <FloatingWhatsApp
        phoneNumber="+6281312117711"
        accountName="Customer Services"
        allowClickAway
        notification
        notificationDelay={30000}
      />
      <Component {...pageProps} />
    </>
    </>
  )
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
async function getInitialProps({ Component, router, ctx }) {
  let pageProps = {}

  if (Component.getInitialProps) {
    pageProps = await Component.getInitialProps(ctx)
  }

  return { pageProps }
}

export default App
=======
import NProgress from "nprogress"
import Router, { useRouter } from "next/router"

import Bugsnag from "../lib/utils/method/bugsnag"
import Error from "./_error"

import "../styles/index.scss"
import "../styles/vendors/bootstrap.min.css"
import "react-multi-carousel/lib/styles.css"
import "react-phone-number-input/style.css"
import "nprogress/nprogress.css"
import { useGoogleOneTapLogin } from "../lib/utils/hooks/useGoogleOneTapLogin"
import { useEffect } from "react"
import dynamic from "next/dynamic"
import Script from "next/script"
import { GA_TRACKING_ID } from "../lib/utils/method/gtag"
import { GTMId } from "../constants/const"

const ErrorBoundary = Bugsnag.getPlugin("react")

const FloatingWhatsApp = dynamic(() => import("@harisenin/react-floating-whatsapp"), { ssr: false })

function MyApp({ Component, pageProps }) {
  // Progress bar on top of screen
  Router.events.on("routeChangeStart", () => {
    NProgress.start()
  })
  Router.events.on("routeChangeComplete", () => {
    NProgress.done()
  })
  Router.events.on("routeChangeError", () => {
    NProgress.done()
  })

  const router = useRouter()

  useEffect(() => {
    import("react-facebook-pixel")
      .then((module) => module.default)
      .then((ReactPixel) => {
        ReactPixel.init(`${process.env.FACEBOOK_PIXEL}`) // facebookPixelId
        router.events.on("routeChangeComplete", () => {
          ReactPixel.pageView()
        })
      })
  }, [router.events])

  useGoogleOneTapLogin()

  if (pageProps.error) {
    return <Error />
  }

  return (
    // @ts-ignore
    // Error boundary wrapper for bugsnag
    <ErrorBoundary FallbackComponent={Error}>
      {/* Handling Script */}
      {/* Global Site Tag (gtag.js) - Google Analytics */}
      <Script src={`https://www.googletagmanager.com/gtag/js?id=${GA_TRACKING_ID}`} />
      <Script
        dangerouslySetInnerHTML={{
          __html: `
                    window.dataLayer = window.dataLayer || [];
                    function gtag(){dataLayer.push(arguments);}
                    gtag('js', new Date());

                    gtag('config', '${GA_TRACKING_ID}', {
                      page_path: window.location.pathname,
                    });
                  `,
        }}
      />

      <Script
        type="application/ld+json"
        dangerouslySetInnerHTML={{
          __html: JSON.stringify({
            "@context": "http://schema.org",
            "@type": "Marketplace",
            name: "Harisenin",
            url: "https://www.harisenin.com",
            sameAs: [
              "https://twitter.com/harisenincom",
              "https://www.facebook.com/harisenincom",
              "https://www.instagram.com/harisenincom/?hl=en",
              "https://www.linkedin.com/company/harisenin-com/about/",
            ],
            address: {
              "@type": "PostalAddress",
              streetAddress:
                "JL. Cilandak Kko, Kel. Cilandak Timur, Kec. Pasar Minggu, Kota Adm. Jakarta Selatan, Prov. DKI Jakarta",
              addressRegion: "DKI Jakarta",
              postalCode: "12560",
              addressCountry: "ID",
            },
          }),
        }}
      />

      {/* Google Tag Manager */}
      <Script
        dangerouslySetInnerHTML={{
          __html: `
                    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                    })(window,document,'script','dataLayer','${GTMId}');
                  `,
        }}
      />
      {/* End Google Tag Manager */}

      {/* Google Sign in script */}
      <Script strategy="lazyOnload" src="https://accounts.google.com/gsi/client" />

      {/* Tiktok Pixel */}
      <Script
        dangerouslySetInnerHTML={{
          __html: `
                    !function (w, d, t) {
                    w.TiktokAnalyticsObject=t;var ttq=w[t]=w[t]||[];ttq.methods=["page","track","identify","instances","debug","on","off","once","ready","alias","group","enableCookie","disableCookie"],ttq.setAndDefer=function(t,e){t[e]=function(){t.push([e].concat(Array.prototype.slice.call(arguments,0)))}};for(var i=0;i<ttq.methods.length;i++)ttq.setAndDefer(ttq,ttq.methods[i]);ttq.instance=function(t){for(var e=ttq._i[t]||[],n=0;n<ttq.methods.length;n++)ttq.setAndDefer(e,ttq.methods[n]);return e},ttq.load=function(e,n){var i="https://analytics.tiktok.com/i18n/pixel/events.js";ttq._i=ttq._i||{},ttq._i[e]=[],ttq._i[e]._u=i,ttq._t=ttq._t||{},ttq._t[e]=+new Date,ttq._o=ttq._o||{},ttq._o[e]=n||{};var o=document.createElement("script");o.type="text/javascript",o.async=!0,o.src=i+"?sdkid="+e+"&lib="+t;var a=document.getElementsByTagName("script")[0];a.parentNode.insertBefore(o,a)};
                    ttq.load('C9FQKRRC77U37L800T5G');
                    ttq.page();
                   }(window, document, 'ttq');
                  `,
        }}
      />

      {/* Twitter universal website tag code */}
      <Script
        dangerouslySetInnerHTML={{
          __html: `
                    !function(e,t,n,s,u,a){e.twq||(s=e.twq=function(){s.exe?s.exe.apply(s,arguments):s.queue.push(arguments);
                    },s.version='1.1',s.queue=[],u=t.createElement(n),u.async=!0,u.src='//static.ads-twitter.com/uwt.js',
                    a=t.getElementsByTagName(n)[0],a.parentNode.insertBefore(u,a))}(window,document,'script');
                    // Insert Twitter Pixel ID and Standard Event data below
                    twq('init','o8qsw');
                    twq('track','PageView');
                  `,
        }}
      />

      <FloatingWhatsApp
        phoneNumber="+6281312117711"
        accountName="Customer Services"
        allowClickAway
        notification
        notificationDelay={30000}
      />
      <Component {...pageProps} />
    </ErrorBoundary>
  )
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
async function getInitialProps({ Component, router, ctx }) {
  let pageProps = {}

  if (Component.getInitialProps) {
    pageProps = await Component.getInitialProps(ctx)
  }

  return { pageProps }
}

export default MyApp
>>>>>>> 94ccec2 (setup fix)
