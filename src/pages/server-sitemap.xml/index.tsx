<<<<<<< HEAD
import { getServerSideSitemapLegacy, ISitemapField } from "next-sitemap"
import { GetServerSideProps } from "next"
import got from "got"

interface SitemapResponse {
  message: string
  result: ISitemapField[]
}

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  // Method to source urls from cms

  const { result }: SitemapResponse = await got
    .get(process.env.HARISENIN_API_ENDPOINT + "/v2/schools?sitemap=true")
    .json()

  return getServerSideSitemapLegacy(ctx, result)
}

// Default export to prevent next.js errors
// eslint-disable-next-line @typescript-eslint/no-empty-function
export default function ServerSitemap() {}
=======
import { getServerSideSitemap, ISitemapField } from "next-sitemap"
import { GetServerSideProps } from "next"
import got from "got"

interface SitemapResponse {
  message: string
  result: ISitemapField[]
}

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  // Method to source urls from cms

  const { result }: SitemapResponse = await got
    .get(process.env.HARISENIN_API_ENDPOINT + "/v2/schools?sitemap=true")
    .json()

  return getServerSideSitemap(ctx, result)
}

// Default export to prevent next.js errors
// eslint-disable-next-line @typescript-eslint/no-empty-function
export default function ServerSitemap() {}
>>>>>>> 94ccec2 (setup fix)
