<<<<<<< HEAD
import { GetServerSideProps } from "next"

import SchoolRepo from "@lib/api/server-side/school.repo"
import ProgramLandingPage, { ProgramLandingPageProps } from "@modules/program/ProgramLandingPage"

export const getServerSideProps: GetServerSideProps<ProgramLandingPageProps> = async (ctx) => {
  const { slug } = ctx.params
  const res = ctx.res
  const repo = new SchoolRepo()

  const convertToReviewFormat = (testimonies: any) => {
    const reviews = []

    if (!testimonies) {
      return reviews
    }

    testimonies.map((testimony) => {
      const review = {
        "@type": "Review",
        name: testimony?.testimony_username,
        reviewBody: testimony?.testimony,
        reviewRating: {
          "@type": "Rating",
          ratingValue: testimony?.testimony_rating?.toString(),
          bestRating: "5",
          worstRating: "1",
        },
        datePublished: testimony?.date_published,
        author: {
          "@type": "Person",
          name: testimony?.testimony_username,
        },
        publisher: {
          "@type": "Organization",
          name: testimony?.company?.company_name,
          logo: {
            "@type": "ImageObject",
            url: testimony?.company?.company_picture,
          },
        },
      }

      reviews.push(review)
    })

    return reviews
  }

  try {
    const response = await repo.schoolDetail(slug as string, "school")

    if (response.statusCode !== 200) {
      return {
        notFound: true,
      }
    }

    const body = JSON.parse(response.body)
    const result = body.result

    const schema = {
      "@context": "https://schema.org/",
      "@type": "Product",
      name: result?.product_name,
      image: result?.school_banner.banner_image,
      description: result?.school_meta_description,
      brand: {
        "@type": "Brand",
        name: "Harisenin.com",
      },
      aggregateRating: {
        "@type": "AggregateRating",
        ratingValue: result?.review_rating?.rating_value,
        bestRating: 5,
        worstRating: 1,
        ratingCount: result?.review_rating?.rating_count,
        reviewCount: result?.review_rating?.rating_count,
      },
      review: convertToReviewFormat(result?.school_testimonies),
    }

    if (!result) {
      return {
        notFound: true,
      }
    } else {
      return {
        props: {
          schoolDetail: result,
          schema,
        },
      }
    }
  } catch (e) {
    console.log({ e })
    res.setHeader("Location", "/")
    res.statusCode = 301

    return {
      notFound: true,
    }
  }
}

export default ProgramLandingPage
=======
import { FC } from "react"
import { GetServerSideProps } from "next"

import ProgramLandingPage, {
  ProgramLandingPageProps,
} from "../../ui/components/program/ProgramLandingPage"
import SchoolRepo from "../../lib/apis/school.repo"

const ProgramPage: FC<ProgramLandingPageProps> = ({ schoolDetail }) =>
  ProgramLandingPage({ schoolDetail })

export const getServerSideProps: GetServerSideProps<ProgramLandingPageProps> = async (ctx) => {
  const { slug } = ctx.params
  const res = ctx.res
  const repo = new SchoolRepo()

  try {
    const response = await repo.schoolDetail(slug as string)

    if (response.statusCode !== 200) {
      return {
        notFound: true,
      }
    }

    const body = JSON.parse(response.body)
    const result = body.result

    if (!result) {
      return {
        notFound: true,
      }
    } else {
      return {
        props: {
          schoolDetail: result,
        },
      }
    }
  } catch (e) {
    console.log(e)
    res.setHeader("Location", "/")
    res.statusCode = 301

    return {
      notFound: true,
    }
  }
}

export default ProgramPage
>>>>>>> 94ccec2 (setup fix)
