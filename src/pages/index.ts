<<<<<<< HEAD
import "regenerator-runtime/runtime"
import { GetServerSideProps } from "next"
import MiscRepo from "../lib/api/server-side/misc.repo"
import SchoolRepo from "../lib/api/server-side/school.repo"
import LandingPage from "@modules/landing-page/pages/LandingPage"
import { DynamicHelmet } from "@interfaces/misc"

export const getServerSideProps: GetServerSideProps = async () => {
  const common = new MiscRepo()
  const school = new SchoolRepo()

  try {
    const res = common.getMetaTag("hms")
    const res2 = school.schoolList("BOOTCAMP")
    const res3 = school.schoolList("PRO_CLASS")
    let meta: DynamicHelmet | null = null
    let bootcamps = []
    let proClasses = []

    const [mt, sch, pc] = await Promise.all([res, res2, res3])

    if (mt.statusCode === 200) {
      const result = JSON.parse(mt.body).result

      if (result) {
        meta = {
          title: result.meta_title,
          description: result.meta_description,
        }
      }
    }

    if (sch.statusCode === 200) {
      const result = JSON.parse(sch.body).result

      if (result) {
        bootcamps = result.data
      }
    }

    if (pc.statusCode === 200) {
      const result = JSON.parse(pc.body).result

      if (result) {
        proClasses = result.data
      }
    }

    return {
      props: {
        meta,
        bootcamps,
        proClasses,
      },
    }
  } catch (e) {
    console.log(e)

    return {
      props: {
        meta: null,
        bootcamps: null,
        proClasses: null,
      },
    }
  }
}

export default LandingPage
=======
import { GetServerSideProps } from "next"
import LandingPage, { LandingPageProps } from "../ui/components/landing-page/LandingPage"
import { DynamicHelmet } from "../constants/interfaces/misc"
import MiscRepo from "../lib/apis/misc.repo"
import SchoolRepo from "../lib/apis/school.repo"

const Homepage = ({ meta, bootcamps, proClasses }: LandingPageProps) =>
  LandingPage({ meta, bootcamps, proClasses })

export const getServerSideProps: GetServerSideProps = async () => {
  const common = new MiscRepo()
  const school = new SchoolRepo()

  try {
    const res = common.getMetaTag("hms")
    const res2 = school.schoolList("BOOTCAMP")
    const res3 = school.schoolList("PRO_CLASS")
    let meta: DynamicHelmet | null = null
    let bootcamps = []
    let proClasses = []

    const [mt, sch, pc] = await Promise.all([res, res2, res3])

    if (mt.statusCode === 200) {
      const result = JSON.parse(mt.body).result

      if (result) {
        meta = {
          title: result.meta_title,
          description: result.meta_description,
        }
      }
    }

    if (sch.statusCode === 200) {
      const result = JSON.parse(sch.body).result

      if (result) {
        bootcamps = result.data
      }
    }

    if (pc.statusCode === 200) {
      const result = JSON.parse(pc.body).result

      if (result) {
        proClasses = result.data
      }
    }

    return {
      props: {
        meta,
        bootcamps,
        proClasses,
      },
    }
  } catch (e) {
    console.log(e)

    return {
      props: {
        meta: null,
        bootcamps: null,
        proClasses: null,
      },
    }
  }
}

export default Homepage
>>>>>>> 94ccec2 (setup fix)
