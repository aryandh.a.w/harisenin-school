<<<<<<< HEAD
import { GetServerSideProps } from "next"
import ProgramListPage from "@modules/program-list/pages/ProgramListPage"
import SchoolRepo from "@lib/api/server-side/school.repo"
import { SchoolType } from "@interfaces/school"

const ProgamList = ({ bootcamps }) =>
  ProgramListPage({
    data: bootcamps,
    metaTitle: "harisenin.com: Semua Program - ProClass",
    metaDescription:
      "Tingkatkan skill profesional yang kamu butuhkan untuk persiapan kariermu bersama harisenin ProClass, hanya dalam 1-2 bulan",
  })

export const getServerSideProps: GetServerSideProps = async ({ query }) => {
  const school = new SchoolRepo()

  try {
    const res = await school.schoolList(query.type ? (query.type as SchoolType) : "BOOTCAMP")
    let bootcamps = []

    if (res.statusCode === 200) {
      const result = JSON.parse(res.body).result

      if (result) {
        bootcamps = result.data
      }
    }

    return {
      props: {
        bootcamps,
      },
    }
  } catch (e) {
    console.log(e)

    return {
      props: {
        bootcamps: [],
      },
    }
  }
}

export default ProgamList
=======
import { GetServerSideProps } from "next"
import ProgramListPage from "../../ui/components/program-list/ProgramListPage"
import SchoolRepo from "../../lib/apis/school.repo"
import { SchoolType } from "../../constants/interfaces/school"

const ProgamList = ({ bootcamps }) =>
  ProgramListPage({
    data: bootcamps,
    metaTitle: "harisenin.com: Semua Program - ProClass",
    metaDescription:
      "Tingkatkan skill profesional yang kamu butuhkan untuk persiapan kariermu bersama harisenin ProClass, hanya dalam 1-2 bulan",
  })

export const getServerSideProps: GetServerSideProps = async ({ query }) => {
  const school = new SchoolRepo()

  try {
    const res = await school.schoolList(query.type ? (query.type as SchoolType) : "BOOTCAMP")
    let bootcamps = []

    if (res.statusCode === 200) {
      const result = JSON.parse(res.body).result

      if (result) {
        bootcamps = result.data
      }
    }

    return {
      props: {
        bootcamps,
      },
    }
  } catch (e) {
    console.log(e)

    return {
      props: {
        bootcamps: [],
      },
    }
  }
}

export default ProgamList
>>>>>>> 94ccec2 (setup fix)
