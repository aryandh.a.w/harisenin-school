<<<<<<< HEAD
import { GetServerSideProps } from "next"

import ProgramLandingPage, { ProgramLandingPageProps } from "@modules/program/ProgramLandingPage"
import SchoolRepo from "../../lib/api/server-side/school.repo"

export const getServerSideProps: GetServerSideProps<ProgramLandingPageProps> = async (ctx) => {
  const { slug } = ctx.params
  const res = ctx.res
  const repo = new SchoolRepo()

  try {
    const response = await repo.schoolDetail(slug as string, "school")

    if (response.statusCode === 200) {
      const body = JSON.parse(response.body)
      const result = body.result

      return {
        props: {
          schoolDetail: result,
        },
      }
    } else {
      return {
        notFound: true,
      }
    }
  } catch (e) {
    console.log(e)
    res.setHeader("Location", "/")
    res.statusCode = 301

    return {
      notFound: true,
    }
  }
}

export default ProgramLandingPage
=======
import { FC } from "react"
import { GetServerSideProps } from "next"

import ProgramLandingPage, {
  ProgramLandingPageProps,
} from "../../ui/components/program/ProgramLandingPage"
import SchoolRepo from "../../lib/apis/school.repo"

const ProgramPage: FC<ProgramLandingPageProps> = ({ schoolDetail }) =>
  ProgramLandingPage({ schoolDetail })

export const getServerSideProps: GetServerSideProps<ProgramLandingPageProps> = async (ctx) => {
  const { slug } = ctx.params
  const res = ctx.res
  const repo = new SchoolRepo()

  try {
    const response = await repo.schoolDetail(slug as string)

    if (response.statusCode === 200) {
      const body = JSON.parse(response.body)
      const result = body.result

      return {
        props: {
          schoolDetail: result,
        },
      }
    } else {
      return {
        notFound: true,
      }
    }
  } catch (e) {
    console.log(e)
    res.setHeader("Location", "/")
    res.statusCode = 301

    return {
      notFound: true,
    }
  }
}

export default ProgramPage
>>>>>>> 94ccec2 (setup fix)
