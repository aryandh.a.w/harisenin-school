export type ProgramCompareType = "yes" | "no" | "normal"

export interface ProgramCompareValue {
  type: ProgramCompareType
  text?: string
}

export interface ProgramCompareList {
  label: string
  fullcourse: ProgramCompareValue
  guarantee: ProgramCompareValue
}

export const PROGRAM_COMPARE_LIST: ProgramCompareList[] = [
  {
    label: "Live Course",
    fullcourse: {
      type: "yes",
    },
    guarantee: {
      type: "yes",
    },
  },
  {
    label: "Sertifikat",
    fullcourse: {
      type: "yes",
    },
    guarantee: {
      type: "yes",
    },
  },
  {
    label: "Final Project & Assignment",
    fullcourse: {
      type: "yes",
    },
    guarantee: {
      type: "yes",
    },
  },
  {
    label: "1-on-1 Career coaching & consultation",
    fullcourse: {
      type: "no",
    },
    guarantee: {
      type: "yes",
    },
  },
  {
    label: "Job Connect",
    fullcourse: {
      type: "no",
    },
    guarantee: {
      type: "yes",
    },
  },
  {
    label: "Durasi Waktu",
    fullcourse: {
      type: "normal",
      text: "1 - 3 Bulan"
    },
    guarantee: {
      type: "normal",
      text: "2 - 6 Bulan"
    },
  }
]
