import {
  HARISENIN_PUBLIC_ICON,
  HARISENIN_PUBLIC_PAGE_ASSETS,
  HARISENIN_PUBLIC_S3,
} from "./pictures"

export const COMPANY = [
  { url: `${HARISENIN_PUBLIC_S3}/logos/logo_ruang-guru.png`, alt: "air-asia" },
  { url: `${HARISENIN_PUBLIC_S3}/logos/logo_tokopedia-3.png`, alt: "tokopedia" },
  { url: `${HARISENIN_PUBLIC_S3}/logos/logo_traveloka.png`, alt: "traveloka" },
  { url: `${HARISENIN_PUBLIC_S3}/logos/logo_link-aja-2.png`, alt: "link-aja" },
  { url: `${HARISENIN_PUBLIC_S3}/logos/logo_wings.png`, alt: "wings" },
  { url: `${HARISENIN_PUBLIC_S3}/logos/logo_gojek-3.png`, alt: "gojek" },
  { url: `${HARISENIN_PUBLIC_S3}/logos/logo_p-g.png`, alt: "p&g" },
  { url: `${HARISENIN_PUBLIC_S3}/logos/logo_blibli.png`, alt: "blibli" },
  { url: `${HARISENIN_PUBLIC_S3}/logos/logo_sirclo.png`, alt: "sirclo" },
  { url: `${HARISENIN_PUBLIC_S3}/logos/logo_bukalapak-2.png`, alt: "bukalapak" },
  { url: `${HARISENIN_PUBLIC_S3}/logos/logo_kpmg-3.png`, alt: "kpmg" },
  { url: `${HARISENIN_PUBLIC_S3}/logos/logo_lg.png`, alt: "lg" },
  { url: `${HARISENIN_PUBLIC_S3}/logos/logo_vosfoyer.png`, alt: "vosfoyer" },
  { url: `${HARISENIN_PUBLIC_S3}/logos/logo_ey-3.png`, alt: "ey" },
  { url: `${HARISENIN_PUBLIC_S3}/logos/logo_mnc-media.png`, alt: "mnc-media" },
]

export const REASON = [
  {
    icon: `${HARISENIN_PUBLIC_ICON}/icon-reason_1.png`,
    title: "Kuasai keterampilan yang paling banyak diminati",
    description:
      "Kuasai technical & soft skills yang paling dibutuhkan untuk mempercepat jenjang karirmu.",
  },
  {
    icon: `${HARISENIN_PUBLIC_ICON}/icon-reason_2.png`,
    title: 'Dapatkan formula rahasia untuk menjadi "rock star"',
    description:
      "Dapatkan tips sukses langsung dari  top instruktur yang berpengalaman dan dibantu cari kerja setelah lulus.",
  },
  {
    icon: `${HARISENIN_PUBLIC_ICON}/icon-reason_3.png`,
    title: "Dapatkan pengalaman kerja secara nyata",
    description: "Menjadi salah satu partner di harisenin.com dan terlibat dalam real project.",
  },
  {
    icon: `${HARISENIN_PUBLIC_ICON}/icon-reason_4.png`,
    title: "Berbagi dan memberi dampak kepada sekitar",
    description:
      "2.5% dari pendapatan akan didonasikan kepada organisasi yang mendukung pendidikan anak kurang mampu.",
  },
]

export const FAQs = [
  {
    title: "Seberapa besar komitmen waktu yang harus saya berikan?",
    answer:
      "Program ini berjalan selama 4 minggu dengan total 12 pertemuan. Setiap minggu terdapat 3 pertemuan, 2 pertemuan pada hari kerja malam hari dan 1 pertemuan pada Sabtu siang. ",
  },
  {
    title: "Bagaimana bentuk konkrit pertemuan dalam program ini?",
    answer: "Pertemuan berbentuk kelas online, menggunakan media konferensi video.",
  },
  {
    title: "Apa syarat untuk dapat bergabung di program ini?",
    answer: "Kamu cukup mengisi form pendaftaran dan menyediakan scan ijazah",
  },
  {
    title: "Siapa saja yang dapat mengikuti program ini?",
    answer: "Siapapun dengan berbagai latar belakang pendidikan dapat mengikuti program ini",
  },
  {
    title: "Apa syarat kelulusan dari program ini?",
    answer: "Kamu harus menghadiri minimal 8 pertemuan dan mengerjakan final project",
  },
  {
    title: "Bagaimana cara mendapatkan slot di program ini?",
    answer:
      "Bayar secara penuh saat sudah mendaftar atau dengan menggunakan program cicilan sebanyak 3x",
  },
]

export const SOLUTIONS = [
  {
    picture: `${HARISENIN_PUBLIC_PAGE_ASSETS}/solution_slide-1.png`,
    link: `${process.env.HOME_URL}/cari-jasa`,
    title: "Consulting & Digital Services",
    subtitle:
      "On demand services untuk fundraising, tax, website, mobile apps, & digital marketing",
  },
  {
    picture: `${HARISENIN_PUBLIC_PAGE_ASSETS}/solution_slide-2.png`,
    link: `${process.env.HOME_URL}/enterprise`,
    title: "Talent Services",
    subtitle: "Temukan talent yang tepat dan cepat. Full-time, Part-time atau freelance.",
  },
  {
    picture: `${HARISENIN_PUBLIC_PAGE_ASSETS}/solution_slide-3.png`,
    link: null,
    title: "Corporate Training (Coming Soon)",
    subtitle: "Kembangkan skill talent Anda. Buat bisnismu jadi lebih maju.",
  },
]

export const CORPORATE = [
  { url: `${HARISENIN_PUBLIC_S3}/logos/logo_air-asia.png`, alt: "air-asia" },
  { url: `${HARISENIN_PUBLIC_S3}/logos/logo_bahamify.png`, alt: "ambiz" },
  {
    url: `${HARISENIN_PUBLIC_S3}/logos/logo_kompas.png`,
    alt: "mandiri-sekuritas",
  },
  { url: `${HARISENIN_PUBLIC_S3}/logos/logo_jubelio.png`, alt: "jubelio" },
  { url: `${HARISENIN_PUBLIC_S3}/logos/logo_moselio.png`, alt: "moselio" },
  {
    url: `${HARISENIN_PUBLIC_S3}/logos/logo_foresthree-2.png`,
    alt: "foresthree",
  },
  { url: `${HARISENIN_PUBLIC_S3}/logos/logo_room-me.png`, alt: "room-me" },
  { url: `${HARISENIN_PUBLIC_S3}/logos/logo_meridian.svg`, alt: "meridian" },
]
