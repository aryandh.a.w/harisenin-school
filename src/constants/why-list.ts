export type WhyType = "yes" | "no" | "warning" | "normal"

export interface WhyValue {
  type: WhyType
  text?: string
}

export interface WhyList {
  label: string
  hms: WhyValue
  other: WhyValue
  university: WhyValue
}

export const WHY_LIST: WhyList[] = [
  {
    label: "Technical skill",
    hms: {
      type: "yes",
    },
    other: {
      type: "yes",
    },
    university: {
      type: "yes",
    },
  },
  {
    label: "Soft skill",
    hms: {
      type: "yes",
    },
    other: {
      type: "warning",
      text: "Not All",
    },
    university: {
      type: "no",
    },
  },
  {
    label: "Real work experience",
    hms: {
      type: "yes",
    },
    other: {
      type: "yes",
    },
    university: {
      type: "no",
    },
  },
  {
    label: "Career coaching acceleration",
    hms: {
      type: "yes",
    },
    other: {
      type: "yes",
    },
    university: {
      type: "no",
    },
  },
  {
    label: "Free All Access Q&A",
    hms: {
      type: "yes",
    },
    other: {
      type: "yes",
    },
    university: {
      type: "no",
    },
  },
  {
    label: "Job Guarantee",
    hms: {
      type: "yes",
    },
    other: {
      type: "warning",
      text: "Not All"
    },
    university: {
      type: "no",
    },
  },
  {
    label: "Money back guarantee",
    hms: {
      type: "normal",
      text: "100%"
    },
    other: {
      type: "normal",
      text: "0% - 100%"
    },
    university: {
      type: "normal",
      text: "0%"
    },
  },
  {
    label: "Duration",
    hms: {
      type: "normal",
      text: "1 - 3 Bulan"
    },
    other: {
      type: "normal",
      text: "2 - 6 Bulan"
    },
    university: {
      type: "normal",
      text: "1 Tahun"
    },
  },
  {
    label: "Tuition",
    hms: {
      type: "normal",
      text: "Rp 500K - 2.5 Jt"
    },
    other: {
      type: "normal",
      text: "Rp 5 - 50 Jt"
    },
    university: {
      type: "normal",
      text: "Rp 50 - 200 Jt"
    },
  },
  {
    label: "Installment Program (Cicilan)",
    hms: {
      type: "yes",
    },
    other: {
      type: "yes",
    },
    university: {
      type: "no",
    },
  },
  {
    label: "Personal finance literacy",
    hms: {
      type: "yes",
    },
    other: {
      type: "no",
    },
    university: {
      type: "no",
    },
  },
  {
    label: "Pro Partner - Corporate Project",
    hms: {
      type: "yes",
    },
    other: {
      type: "no",
    },
    university: {
      type: "no",
    },
  },
  {
    label: "Teach you how to freelance",
    hms: {
      type: "yes",
    },
    other: {
      type: "no",
    },
    university: {
      type: "no",
    },
  },
  {
    label: "Give a class to others in need",
    hms: {
      type: "yes",
    },
    other: {
      type: "no",
    },
    university: {
      type: "no",
    },
  },
  {
    label: "Stress-free learning method",
    hms: {
      type: "yes",
    },
    other: {
      type: "no",
    },
    university: {
      type: "no",
    },
  },
  {
    label: "Mental Health education",
    hms: {
      type: "yes",
    },
    other: {
      type: "no",
    },
    university: {
      type: "no",
    },
  },
]
