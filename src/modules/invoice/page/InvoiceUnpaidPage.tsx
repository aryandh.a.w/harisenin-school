import React, { useEffect, useState } from "react"
import { useRouter } from "next/router"
import { HARISENIN_PUBLIC_LOGO } from "@constants/pictures"
import Helmet from "@helpers/Helmet"
import { Header } from "@components/header"

import { RegularButton, LinkButton } from "@components/buttons"
import { CountdownCard } from "@modules/invoice/components/CountdownCard"
import { useBasicWebSocket } from "@hooks/useWebsocket"
import { InvoiceDetail, OrderStatus } from "@interfaces/order"
import { BCAMethod, BNIMethod, BRIMethod, MandiriMethod, OtherBank } from "@constants/const"
import { HeaderOrder } from "@components/header/HeaderOrder"
import InvoiceService from "@services/invoice.services"
import Image from "next/image"
import clsx from "clsx"
import { FlexBox } from "@components/wrapper"
import { Disclosure, Transition } from "@headlessui/react"
import { AiOutlineDown } from "react-icons/ai"
import { Modal } from "@components/modal"

const InvoicePage = () => {
  const [invoiceDetail, setInvoiceDetail] = useState<InvoiceDetail | null>(null)
  const [transactionId, setTransactionId] = useState("")
  const [transferAmount, setTransferAmount] = useState(0)
  const [bank, setBank] = useState("")
  const [method, setMethod] = useState(OtherBank)
  const [status, setStatus] = useState<OrderStatus>("PENDING")

  const [modal, setModal] = useState(false)
  const [checkoutLink, setCheckoutLink] = useState("")

  const router = useRouter()
  const { orderId, productId } = router.query

  const invoice = new InvoiceService()

  useEffect(() => {
    const getInvoice = async () => {
      try {
        const res = await invoice.getInvoiceDetail(orderId as string)
        if (res.isSuccess) {
          const data = res.getValue()
          setInvoiceDetail(data)
          setBank(data.bank_code)
          setTransferAmount(data.transfer_amount)
          setTransactionId(data.invoice_number)
          setStatus(data.status)
          setCheckoutLink(data.checkout_url)

          const banks = data.bank_code
          switch (banks) {
            case "BCA":
              setMethod(BCAMethod)
              break
            case "BNI":
              setMethod(BNIMethod)
              break
            case "BRI":
              setMethod(BRIMethod)
              break
            case "MANDIRI":
              setMethod(MandiriMethod)
              break
            default:
              break
          }
        }
      } catch (error) {
        console.log(error)
      }
    }
    if (orderId) {
      getInvoice()
    }
  }, [orderId, productId])

  const redirectToSuccessPage = async () => {
    window.location.assign(`/school/invoice/${orderId}/success?productId=${productId}`)
  }

  useBasicWebSocket({
    isSocketStart: process.env.NODE_ENV === "production",
    channel: `invoices-${orderId}`,
    fetcher: redirectToSuccessPage,
  })

  return (
    <>
      <Helmet title="Harisenin.com: Konfirmasi Pembayaran" />
      <Header />
      <HeaderOrder step={2} className="sm:!w-1/2 mx-auto " />

      <div className="bg-primary-grey sm:min-h-[90vh] min-h-[96vh]">
        <div className={clsx("sm:py-[30px] sm:w-1/2 mx-auto", "sm-only:px-4 w-full py-6")}>
          <h1 className={clsx("sm:text-2xl mb-3 font-semibold", "text-lg")}>
            Konfirmasi Pembayaran
          </h1>

          <div className={clsx("bg-white rounded-10 sm:py-10 sm-only:p-4 ")}>
            <div className={clsx("text-center sm:text-base", "text-sm")}>
              Mohon selesaikan pembayaranmu dengan rincian sebagai berikut:
            </div>

            <div className="sm:mb-3 mb-2 mx-auto  ">
              <CountdownCard data={invoiceDetail} />
            </div>

            <FlexBox
              className={clsx(
                "sm:w-4/5 mx-auto sm:mb-3 justify-between ",
                "sm-only:flex-wrap sm-only:text-xs sm-only:mb-5"
              )}
            >
              <div className="sm-only:w-3/5">
                <div className="mb-1">Nomor Tagihan</div>
                <p>{transactionId}</p>
              </div>

              <div className="sm-only:w-2/5">
                <div className="mb-1">Jumlah tagihan kamu</div>
                <p className="font-semibold">
                  Rp {Intl.NumberFormat("id-ID").format(transferAmount)}
                </p>
              </div>

              {bank && (
                <div className="sm-only:pt-6 sm-only:mx-auto">
                  <Image
                    src={`${HARISENIN_PUBLIC_LOGO}/logo_${bank?.toLowerCase()}.png`}
                    alt={bank?.toLowerCase()}
                    className="h-5 mb-1 w-auto"
                    height={20}
                    width={200}
                  />
                  <div>{bank} Virtual Account</div>
                </div>
              )}
            </FlexBox>

            {status !== "SETTLED" && (
              <FlexBox
                className={clsx(
                  "mx-auto sm:gap-4 sm:mb-5 sm:w-4/5",
                  "sm-only:flex-col gap-2.5 sm-only:mb-5"
                )}
                justify="center"
              >
                <RegularButton
                  className="sm:min-w-[250px] sm:w-1/2 w-full"
                  customPadding="sm:py-3 py-2.5"
                  onClick={() => setModal(true)}
                >
                  Bayar Sekarang
                </RegularButton>
                <LinkButton
                  variant="transparent"
                  customPadding="sm:py-3 py-2.5"
                  className="sm:min-w-[250px] sm:w-1/2 w-full !border-2"
                  href={`/checkout/${orderId}`}
                  id="btn-change-payment"
                >
                  Ganti Metode Pembayaran
                </LinkButton>
              </FlexBox>
            )}

            <FlexBox>
              <div className={clsx("sm:w-4/5 mx-auto responsive-font", "w-full mb-2")}>
                <h5 className="text-center mb-3 responsive-font">Metode Pembayaran</h5>
                {method.map((item, i) => (
                  <Disclosure key={i}>
                    {({ open }) => (
                      <>
                        <Disclosure.Button
                          className={clsx(
                            "w-full my-2 font-semibold sm:py-3 sm:px-5 border border-grey-c4 flex items-center justify-between rounded",
                            "py-2 px-6"
                          )}
                        >
                          {item.method}
                          <AiOutlineDown
                            className={`${open ? "rotate-180 transform" : ""} font-semibold`}
                          />
                        </Disclosure.Button>
                        <Transition
                          enter="transition duration-100 ease-out"
                          enterFrom="transform scale-95 opacity-0"
                          enterTo="transform scale-100 opacity-100"
                          leave="transition duration-75 ease-out"
                          leaveFrom="transform scale-100 opacity-100"
                          leaveTo="transform scale-95 opacity-0"
                        >
                          {item.step.map((list) => (
                            <li key={list} className="custom-list">
                              {list}
                            </li>
                          ))}
                          {/* {value.child.map((item, i) => (
                            <Disclosure.Panel key={i}>
                              <a href={item.url} className="my-2">
                                {item.label}
                              </a>
                            </Disclosure.Panel>
                          ))} */}
                        </Transition>
                      </>
                    )}
                  </Disclosure>
                ))}
              </div>
            </FlexBox>

            <p
              className={clsx(
                "sm:w-4/5 m-auto text-center mt-8",
                "w-full sm-only:text-sm sm-only:leading-5"
              )}
            >
              Pembelian akan otomatis dibatalkan apabila kamu tidak melakukan pembayaran lebih dari
              3 (tiga) jam setelah orderan ini dibuat
            </p>
          </div>
        </div>
      </div>

      <Modal show={modal} onClose={() => setModal(false)}>
        <iframe src={checkoutLink} height={500} className="w-full" />
      </Modal>
    </>
  )
}

export default InvoicePage
