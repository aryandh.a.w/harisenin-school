import React, { useEffect, useState } from "react"
import { FaCheck } from "react-icons/fa"
import { useRouter } from "next/router"
import dayjs from "dayjs"

import Helmet from "@helpers/Helmet"
import { Header } from "@components/header"
import { HeaderOrder } from "@components/header/HeaderOrder"
import BasicCard from "@components/card/BasicCard"
import { LinkButton } from "@components/buttons"

import "dayjs/locale/id"
import { PaymentDetail } from "@interfaces/order"

import CheckoutService from "@services/checkout.services"
import clsx from "clsx"
import { FlexBox } from "@components/wrapper"

const PaymentSuccessPage = ({ invoice }: { invoice: PaymentDetail }) => {
  // const [paidDate, setPaidDate] = useState(new Date())
  const [orderNumber, setOrderNumber] = useState("")
  const [programTitle, setProgramTitle] = useState("")

  const router = useRouter()
  const checkout = new CheckoutService()
  const { orderId } = router.query
  dayjs().locale("id").format()

  useEffect(() => {
    const getInvoice = async () => {
      setOrderNumber(invoice.order_number)

      const res = await checkout.getCheckoutDetail(orderId as string)

      if (res.isSuccess) {
        const data = res.getValue()
        setProgramTitle(data.order_cart[0].school_title)
      }
    }

    if (orderId && invoice) {
      getInvoice()
    }
  }, [orderNumber, invoice])

  return (
    <>
      <Helmet title="Harisenin.com: Pembayaran Berhasil" />
      <Header />
      <HeaderOrder step={3} className="sm:!w-1/2 mx-auto" />

      <div className="grey-background sm-only:px-4">
        <div className={clsx("py-8 px-0 w-1/2 m-auto", "sm-only:w-full sm-only:py-6 sm-only:px-4")}>
          <BasicCard
            className={clsx(
              "w-full my-0 mx-auto py-10 px-8",
              "sm-only:mb-10 sm-only:py-8 sm-only:px-5 sm-only:mx-5"
            )}
            isBordered
          >
            <FlexBox align="center" direction="col" className="sm:mb-5 mb-3">
              <FlexBox
                justify="center"
                align="center"
                className={clsx("sm:w-24 sm:h-24 bg-green rounded-full", "w-12 h-12")}
              >
                <div className={clsx("flex items-center justify-center w-1/2 h-1/2")}>
                  <FaCheck className="text-white h-full w-full" />
                </div>
              </FlexBox>
            </FlexBox>

            <div className={clsx("text-center")}>
              <div className={clsx("mb-5")}>
                <div className={clsx("sm:text-2xl")}>Pembayaran Berhasil !</div>
                <div className={clsx("mb-8")}>
                  Terima kasih telah melakukan pembelian <b>{programTitle}</b> harisenin.com! <br />
                  <br />
                  Silahkan cek e-mail kamu untuk pemberitahuan lebih lanjut. Dalam waktu 3 x 24 jam,
                  kamu akan di hubungi dan diundang ke whatsapp group oleh admin harisenin.com atau{" "}
                  <a href="https://api.whatsapp.com/send?phone=6281312117711">hubungi kami</a> jika
                  ada kendala.
                </div>
                <FlexBox
                  justify="between"
                  className={clsx("w-4/5 m-auto", "sm-only:flex-col sm-only:w-full sm-only:gap-2")}
                >
                  <LinkButton href="/" isExternal id="btn-back-to-homepage">
                    Kembali ke Homepage
                  </LinkButton>
                  <LinkButton
                    href="/dashboard/order?type=school"
                    isExternal
                    id="btn-see-purchase-list"
                  >
                    Lihat Detail Pembelian
                  </LinkButton>
                </FlexBox>
              </div>
            </div>
          </BasicCard>
        </div>
      </div>
    </>
  )
}

export default PaymentSuccessPage
