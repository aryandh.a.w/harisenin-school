import { FC } from "react"
import dayjs from "dayjs"
import "dayjs/locale/id"
import clsx from "clsx"
import { InvoiceDetail } from "@interfaces/order"
import { CountdownTimer } from "@lib/functions"
import { BasicCard, FlexBox } from "@components/wrapper"
import { pad } from "@lib/functions"

interface CountdownProps {
  data: InvoiceDetail | null
}

export const CountdownCard: FC<CountdownProps> = ({ data }) => {
  dayjs.locale("id")

  if (!data) {
    return <></>
  }

  const countdownTime = CountdownTimer(data.expiry_date)

  return (
    <BasicCard
      className={clsx(
        "px-2.5 py-5 mx-auto sm:w-4/5 border border-green sm:mx-auto",
        "my-3 mx-0 w-full"
      )}
    >
      <FlexBox
        className={clsx(
          "sm:justify-center sm:gap-4 mb-2.5 sm:w-full",
          "justify-between w-3/5 mx-auto"
        )}
      >
        <CountDownWrapper count={countdownTime.hours} label="Jam" />
        <CountDownWrapper count={countdownTime.minutes} label="Menit" />
        <CountDownWrapper count={countdownTime.seconds} label="Detik" />
      </FlexBox>

      <div className="text-center sm:text-base text-sm">
        (Sebelum <b>{dayjs(data.expiry_date).format("dddd, DD MMMM YYYY HH:mm")} WIB</b>)
      </div>
    </BasicCard>
  )
}

interface CountDownWrapperProps {
  count: number
  label: string
}

const CountDownWrapper: FC<CountDownWrapperProps> = ({ count, label }) => {
  return (
    <div>
      <FlexBox
        justify="center"
        align="center"
        className={clsx(
          "sm:w-[57px] sm:h-[57px] rounded sm:text-xl font-semibold bg-[#e7f7f4] sm:mb-4",
          "wh-10 mb-1.5"
        )}
      >
        {pad(count)}
      </FlexBox>
      <div className={clsx("text-center sm:text-base", "text-sm")}>{label}</div>
    </div>
  )
}
