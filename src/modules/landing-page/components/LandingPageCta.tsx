import { LinkButton } from "@components/buttons"
import { container, FlexBox } from "@components/wrapper"
import { HARISENIN_PUBLIC_PAGE_ASSETS } from "@constants/pictures"
import { useUserData } from "@lib/context/ComponentContext"
import clsx from "clsx"
import Image from "next/image"

export function LandingPageCta() {
  const { isLogin } = useUserData()

  if (isLogin) {
    return <div />
  }

  return (
    <section className={clsx(container, "sm:pt-10 sm:pb-[60px]", "pt-5 pb-10 px-4")}>
      <FlexBox
        className={clsx(
          "bg-[#07401c] rounded px-[60px] sm:flex-row sm:item-start",
          "flex-col items-center"
        )}
        justify="between"
      >
        <FlexBox
          className={clsx(
            "sm:w-1/2 sm:items-start sm:pt-0 sm:mb-0",
            "w-full items-center pt-10 mb-10"
          )}
          direction="col"
          justify="center"
        >
          <h2
            className={clsx(
              "text-white sm:text-3xl font-semibold sm:mb-10 sm:leading-[50px] sm:text-left",
              "text-md mb-4  sm-only:text-center"
            )}
          >
            Masih bingung dengan pilihan kariermu? Konsultasikan dengan expert kami
          </h2>

          <LinkButton
            href="https://api.whatsapp.com/send?phone=6281312117711"
            className={clsx("sm:text-md mx-0", "text-xs sm-only:mx-auto")}
            customPadding="sm:py-[14px] sm:px-[50px] py-2.5 px-5"
          >
            Hubungi Kami
          </LinkButton>
        </FlexBox>

        <Image
          src={`${HARISENIN_PUBLIC_PAGE_ASSETS}/webapp_cta.png`}
          alt="Call to action image"
          className="sm:w-2/5 w-[90%]"
          width={1080}
          height={1080}
        />
      </FlexBox>
    </section>
  )
}
