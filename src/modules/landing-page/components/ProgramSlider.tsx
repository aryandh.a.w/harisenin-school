<<<<<<< HEAD:src/modules/landing-page/components/ProgramSlider.tsx
import { ProgramCard } from "@components/card/SchoolCard"
import { Spinner } from "@components/misc"
import { SliderArrow } from "@components/slider"
import { Container, FlexBox } from "@components/wrapper"
import { School } from "@interfaces/school"
import { FC } from "react"
import Carousel from "react-multi-carousel"

export interface ProgramSliderProps {
  width: number
  data: School[]
  isLoading: boolean
}

const ProgramSlider: FC<ProgramSliderProps> = ({ width, data, isLoading }) => {
  const responsive = {
    superLargeDesktop: {
      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: 6,
      slidesToSlide: 6,
      partialVisibilityGutter: 30,
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 3,
      slidesToSlide: 1,
      partialVisibilityGutter: 30,
    },
    tablet: {
      breakpoint: { max: 768, min: 464 },
      items: 1,
      slidesToSlide: 3,
    },
  }

  if (isLoading) {
    return (
      <Container
        className="d-flex justify-content-center align-items-center"
        style={{ height: "40vh" }}
      >
        <Spinner />
      </Container>
    )
  }

  if (!data) {
    return <></>
  }

  return (
    <>
      {width > 576 ? (
        <Carousel
          responsive={responsive}
          ssr={true} // means to render carousel on server-side.
          arrows={false}
          containerClass="flex relative -mx-3 mb-5"
          itemClass="py-0 hover:text-inherit px-[0.5vw]"
          customButtonGroup={
            <SliderArrow
              length={data.length}
              displayedSlide={3}
              leftClassName="top-[45%] w-[42px] h-[42px] -left-[0.6vw]"
              rightClassName="top-[45%] w-[42px] h-[42px] -right-[0.8vw]"
            />
          }
          renderButtonGroupOutside
        >
          {data?.map((value, index) => (
            <ProgramCard item={value} key={index} index={index} screenWidth={width} />
          ))}
        </Carousel>
      ) : (
        <FlexBox className="gap-4 mb-4 px-4" direction="col">
          {data.map((value, index) => (
            <ProgramCard item={value} key={index} index={index} screenWidth={width} />
          ))}
        </FlexBox>
      )}
    </>
  )
}

export default ProgramSlider
=======
import React, { FC } from "react"
import Carousel from "react-multi-carousel"
import SliderArrow from "./SliderArrow"
import { Container, Spinner } from "react-bootstrap"
import { School } from "../../../constants/interfaces/school"
import ProgramCard from "../card/ProgramCard"

import style from "../../components/landing-page/styles/landing-page.module.scss"

export interface ProgramSliderProps {
  width: number
  data: School[]
  isLoading: boolean
}

const ProgramSlider: FC<ProgramSliderProps> = ({ width, data, isLoading }) => {
  const responsive = {
    superLargeDesktop: {
      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: 6,
      slidesToSlide: 6,
      partialVisibilityGutter: 30,
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 3,
      slidesToSlide: 1,
      partialVisibilityGutter: 30,
    },
    tablet: {
      breakpoint: { max: 768, min: 464 },
      items: 1,
      slidesToSlide: 3,
    },
  }

  if (isLoading) {
    return (
      <Container
        className="d-flex justify-content-center align-items-center"
        style={{ height: "40vh" }}
      >
        <Spinner animation={"border"} variant={"info"} />
      </Container>
    )
  }

  if (!data) {
    return <></>
  }

  return (
    <>
      {width > 576 ? (
        <Carousel
          responsive={responsive}
          ssr={true} // means to render carousel on server-side.
          arrows={false}
          containerClass={style.landing_page__program__row}
          itemClass={style.landing_page__program__item}
          customButtonGroup={
            <SliderArrow
              length={data.length}
              displayedSlide={3}
              className={style.landing_page__program__arrow}
            />
          }
          renderButtonGroupOutside
        >
          {data?.map((value, index) => (
            <ProgramCard data={value} key={index} index={index} />
          ))}
        </Carousel>
      ) : (
        <div className={style.landing_page__program__container}>
          <div className={style.landing_page__program__row}>
            {data.map((value, index) => (
              <ProgramCard data={value} key={index} index={index} />
            ))}
          </div>
        </div>
      )}
    </>
  )
}

export default ProgramSlider
>>>>>>> 94ccec2 (setup fix):src/ui/modules/sliders/ProgramSlider.tsx
