import { COMPANY } from "@constants/dummy-data"
import { NextImage } from "@components/misc"
import clsx from "clsx"

import { container, FlexBox } from "@components/wrapper"
import { Heading } from "@components/typography"
import { useEffect, useState } from "react"
import { chunk } from "@lib/functions/array"
import { useIsMounted } from "@lib/hooks/useIsMounted"

export function CompanyRow({ screenWidth }) {
  const [companies, setCompanies] = useState<any[][]>([])

  const isMounted = useIsMounted()

  useEffect(() => {
    if (!isMounted) {
      setCompanies([])
    }

    if (screenWidth > 576) {
      setCompanies(chunk(COMPANY, 5))
    } else {
      setCompanies(chunk(COMPANY, 4))
    }
  }, [screenWidth, isMounted])

  if (!screenWidth) {
    return <></>
  }

  return (
    <div className={clsx("bg-[#f6f7f7] sm:px-[30px] sm:mt-[60px] mt-5")}>
      <div className={clsx(container)}>
        <Heading headingLevel="h2" className="lg:mb-[60px] text-center mb-5 pt-4">
          Mentor kami berpengalaman di <br />
          startup teratas & perusahaan multinasional
        </Heading>

        <FlexBox direction="col" justify="center">
          {screenWidth > 576
            ? companies.map((com, idx) => (
                <FlexBox className="w-4/5 mx-auto mb-7" justify="between" align="center" key={idx}>
                  {com.map((value, index) => (
                    <div key={index}>
                      <NextImage
                        className="max-h-[4vw] w-auto "
                        src={value.url}
                        alt={value.alt}
                        size={[80, 100]}
                      />
                    </div>
                  ))}
                </FlexBox>
              ))
            : companies.map((com, idx) => (
                <FlexBox
                  className={clsx("w-full mx-auto mb-7", idx === 3 && "flex-wrap")}
                  justify="between"
                  align="center"
                  key={idx}
                >
                  {com.map((value, index) => (
                    <div key={index} className="mx-auto">
                      <NextImage
                        className="w-auto max-h-[8vw]"
                        src={value.url}
                        alt={value.alt}
                        size={[80, 100]}
                      />
                    </div>
                  ))}
                </FlexBox>
              ))}
        </FlexBox>
      </div>
    </div>
  )
}
