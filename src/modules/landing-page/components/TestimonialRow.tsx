import { Avatar, NextImage } from "@components/misc"
import { SliderArrow } from "@components/slider"
import { Heading } from "@components/typography"
import { container, FlexBox } from "@components/wrapper"
import { StudentTestimonial } from "@interfaces/school"
import { useIsMounted } from "@lib/hooks/useIsMounted"
import SchoolServices from "@services/school.services"
import clsx from "clsx"
import { useEffect, useState } from "react"
import Carousel from "react-multi-carousel"

const TestimonialCard = ({ value }: { value: StudentTestimonial }) => {
  if (!value) {
    return <></>
  }

  return (
    <FlexBox
      direction="col"
      justify="between"
      className={clsx(
        "relative rounded-10 py-[18px] lg:px-[26px] bg-white shadow-[0_2px_15px_0_#9aaacf33] lg:w-auto lg:mr-0 sm:h-[20em]",
        "w-[70vw] mr-[5vw] px-[18px]"
      )}
    >
      <div>
        <FlexBox align="center" className="mb-6 gap-2.5">
          <div className="rounded w-[60px]">
            <Avatar src={value.testimony_user_picture} alt={value.testimony_username} size={60} />
          </div>
          <div className="sm-only:text-sm">
            <div className="font-bold">{value.testimony_username}</div>
            <div className="text-[#909090]">Alumni Harisenin.com</div>
          </div>
        </FlexBox>

        <div
          className={clsx(
            "sm-only:text-sm lg:leading-6 lg:h-[9em] sm:line-clamp-[6] overflow-hidden",
            "h-full leading-[22px]"
          )}
        >
          {value.testimony}
        </div>
      </div>

      <FlexBox className="mt-4 w-full" justify="between" align="center">
        <div className="w-[70%] text-sm">
          <div className="text-grey-90">Profesi</div>
          <div className="font-semibold">{value.testimony_user_as}</div>
        </div>

        <div className="max-w-[35%] h-auto object-contain">
          {value.company ? (
            <NextImage
              src={value.company.company_picture}
              alt={value.company.company_name}
              size={[40, 60]}
              className="h-auto max-w-[60px] max-h-[40px] object-contain"
            />
          ) : null}
        </div>
      </FlexBox>
    </FlexBox>
  )
}

export function TestimonialRow({ screenWidth }: { screenWidth: number }) {
  // const [isLoading, setIsLoading] = useState(true)
  const [testimonials, setTestimonials] = useState<StudentTestimonial[]>([])

  const school = new SchoolServices()
  const isMounted = useIsMounted()

  useEffect(() => {
    const getData = async () => {
      try {
        const res = await school.getHomepageTestimonials()

        if (res.isSuccess) {
          const data = res.getValue()
          setTestimonials(data)
        }

        // setIsLoading(false)
      } catch (e: any) {
        // school.bugsnagNotifier(e)
        // setIsLoading(false)
      }
    }

    if (isMounted) {
      getData()
    }
  }, [isMounted])

  const responsive = {
    superLargeDesktop: {
      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: 3,
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 3,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 1,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
    },
  }

  if (!testimonials.length) {
    return <></>
  }

  return (
    <section className={clsx("relative lg:py-10 py-5", screenWidth > 576 && container)}>
      <div className={clsx(screenWidth < 576 && container)}>
        <Heading headingLevel="h2" className="text-center mb-5">
          Apa yang alumni kami katakan?
        </Heading>
      </div>

      {screenWidth > 576 ? (
        <Carousel
          responsive={responsive}
          ssr={true} // means to render carousel on server-side.
          arrows={false}
          customButtonGroup={
            <SliderArrow
              length={testimonials.length}
              displayedSlide={3}
              leftClassName="-left-[1vw] top-[55%] w-[42px] h-[42px]"
              rightClassName="-right-[1vw] top-[55%] w-[42px] h-[42px]"
            />
          }
          renderButtonGroupOutside
          itemClass="px-[1.35vw]"
          containerClass="pb-5 -mx-[1.35vw]"
        >
          {testimonials.map((value, index) => (
            <TestimonialCard value={value} key={index} />
          ))}
        </Carousel>
      ) : (
        <div className="slider-mobile pl-4 w-full pb-1">
          <div className="flex w-max">
            {testimonials.map((value, index) => (
              <TestimonialCard value={value} key={index} />
            ))}
          </div>
        </div>
      )}
    </section>
  )
}
