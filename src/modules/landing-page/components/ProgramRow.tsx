<<<<<<< HEAD:src/modules/landing-page/components/ProgramRow.tsx
import { FC, useEffect, useState } from "react"

import clsx from "clsx"
import { School, SchoolType } from "@interfaces/school"
import SchoolServices from "@services/school.services"
import { container, FlexBox } from "@components/wrapper"
import { LinkButton } from "@components/buttons"
import ProgramSlider from "./ProgramSlider"
import { useIsMounted } from "@lib/hooks/useIsMounted"

export interface ProgramRowProps {
  screenWidth: number
  type: SchoolType
  initialData: School[]
}

const ProgramRow: FC<ProgramRowProps> = ({ screenWidth, type, initialData }) => {
  const [isLoading, setIsLoading] = useState(false)

  const [programs, setPrograms] = useState<School[]>(initialData)

  const [copyText, setCopyText] = useState({
    title: "",
    subtitle: "",
  })

  const school = new SchoolServices()
  const isMounted = useIsMounted()

  useEffect(() => {
    const getData = async () => {
      try {
        const res = await school.getSchoolListV2(type, { page: 1, limit: 12 })

        if (res.isSuccess) {
          const result = res.getValue()
          setPrograms(result.data)
        }

        setIsLoading(false)
      } catch (e) {
        setIsLoading(false)
      }
    }

    const getCopyText = () => {
      if (type === "BOOTCAMP") {
        setCopyText({
          title: "Jelajahi Program Bootcamp",
          subtitle:
            "Belajar intensif dengan career coaching dan job-guarantee, hanya dalam 3-5 bulan",
        })
      } else {
        setCopyText({
          title: "Jelajahi Program ProClass",
          subtitle:
            "Tingkatkan skill profesional yang kamu butuhkan untuk persiapan karier, hanya dalam 1-2 bulan",
        })
      }
    }

    if (isMounted) {
      getData()
      getCopyText()
    }
  }, [isMounted])

  if (isLoading) {
    return (
      <div className={clsx("relative", screenWidth > 576 && container)}>
        <ProgramSlider width={screenWidth} data={programs} isLoading={isLoading} />
      </div>
    )
  }

  if (!isLoading && (!programs || !programs.length)) {
    return <></>
  }

  return (
    <div className="sm:pt-[60px] pt-[30px]">
      <div className="sm-only:flex sm-only:flex-col">
        <FlexBox className={clsx("sm:mb-5 mb-4", container)} direction="col" align="center">
          <h2
            className={clsx(
              "sm:text-2xl font-semibold text-secondary-black text-center mb-2.5",
              "text-lg"
            )}
          >
            {copyText.title}
          </h2>
          <div className={clsx("text-secondary-black text-center sm-only:text-xs")}>
            {copyText.subtitle}
          </div>
        </FlexBox>
        <div className={clsx("relative", screenWidth > 576 && container)}>
          <ProgramSlider width={screenWidth} data={programs} isLoading={isLoading} />
        </div>

        <FlexBox className={clsx(screenWidth < 576 && container)} justify="center">
          <LinkButton
            variant="custom"
            href={`/${type === "BOOTCAMP" ? "bootcamp" : "proclass"}`}
            className={clsx(
              "sm:w-[35%] rounded bg-[#00295f] text-white hover:text-white",
              "w-full"
            )}
            customPadding="p-3"
            id={type === "BOOTCAMP" ? "btn-all-program-bootcamp" : "btn-all-program-proclass"}
          >
            Lihat Semua Program
          </LinkButton>
        </FlexBox>
      </div>
    </div>
  )
}

export default ProgramRow
=======
import { FC, useEffect, useState } from "react"

import { School, SchoolType } from "../../../constants/interfaces/school"

import ProgramSlider from "../../modules/sliders/ProgramSlider"
import { LinkButton } from "../../modules/buttons"
import SchoolServices from "../../../lib/services/school.services"

import style from "./styles/landing-page.module.scss"
import clsx from "clsx"

export interface ProgramRowProps {
  screenWidth: number
  type: SchoolType
  initialData: School[]
}

const ProgramRow: FC<ProgramRowProps> = ({ screenWidth, type, initialData }) => {
  const [isLoading, setIsLoading] = useState(false)

  const [programs, setPrograms] = useState<School[]>(initialData)

  const [copyText, setCopyText] = useState({
    title: "",
    subtitle: "",
  })

  const school = new SchoolServices()

  useEffect(() => {
    const getData = async () => {
      try {
        const res = await school.getSchoolListV2(type, { page: 1, limit: 12 })

        if (res.isSuccess) {
          const result = res.getValue()
          setPrograms(result.data)
        }

        setIsLoading(false)
      } catch (e) {
        setIsLoading(false)
      }
    }

    const getCopyText = () => {
      if (type === "BOOTCAMP") {
        setCopyText({
          title: "Jelajahi Program Bootcamp",
          subtitle:
            "Belajar intensif dengan career coaching dan job-guarantee, hanya dalam 3-6 bulan",
        })
      } else {
        setCopyText({
          title: "Jelajahi Program ProClass",
          subtitle:
            "Tingkatkan skill profesional yang kamu butuhkan untuk persiapan karier, hanya dalam 1-2 bulan",
        })
      }
    }

    getData()
    getCopyText()
  }, [])

  if (isLoading) {
    return (
      <div
        className={clsx(
          style.landing_page__program__content,
          "position-relative",
          screenWidth > 576 && "container"
        )}
      >
        <ProgramSlider width={screenWidth} data={programs} isLoading={isLoading} />
      </div>
    )
  }

  if (!isLoading && (!programs || !programs.length)) {
    return <></>
  }

  return (
    <div className={clsx(style.landing_page__program)}>
      <div className={style.landing_page__program__column}>
        <div className={clsx(style.landing_page__program__header, "container")}>
          <h2 className={style.landing_page__program__header__title}>{copyText.title}</h2>
          <div className={style.landing_page__program__header__subtitle}>{copyText.subtitle}</div>
        </div>
        <div
          className={clsx(
            style.landing_page__program__content,
            "position-relative",
            screenWidth > 576 && "container"
          )}
        >
          <ProgramSlider width={screenWidth} data={programs} isLoading={isLoading} />
        </div>
        <div className={clsx(screenWidth < 576 && "container")}>
          <LinkButton
            color="custom"
            href={`/${type === "BOOTCAMP" ? "bootcamp" : "proclass"}`}
            className={clsx(style.landing_page__program__link, `mx-auto`)}
            id={type === "BOOTCAMP" ? "btn-all-program-bootcamp" : "btn-all-program-proclass"}
          >
            Lihat Semua Program
          </LinkButton>
        </div>
      </div>
    </div>
  )
}

export default ProgramRow
>>>>>>> 94ccec2 (setup fix):src/ui/components/landing-page/ProgramRow.tsx
