import { HARISENIN_PUBLIC_PAGE_ASSETS } from "@constants/pictures"

import clsx from "clsx"
import { NextImage } from "@components/misc"
import { container } from "@components/wrapper"

export const LandingPageBanner = () => {
  return (
    <div className={clsx("relative sm:h-[48vw]")}>
      {/* Background Image */}
      <NextImage
        alt="harisenin-millennial-school"
        src={`${HARISENIN_PUBLIC_PAGE_ASSETS}/school_banner.png`}
        size={[1440, 807]}
        className={clsx("w-full h-full object-cover")}
      />

      {/* Content */}
      <div className={clsx("sm:pt-[60px] absolute w-full h-full bg-[#00295fb2] top-0 left-0")}>
        <div className={clsx("flex flex-col h-full m-auto justify-center", container)}>
          <h1
            id="header2"
            className={clsx(
              "mb-[18px] text-white sm:text-4xl font-semibold sm:leading-[52px] sm:w-[45%] ",
              "text-2xl"
            )}
          >
            Sekolah online yang membantumu menjadi
            <span className="text-green"> Top 7%</span> Talenta Indonesia
          </h1>
          <div className={clsx("text-white sm:leading-[174%] sm:text-lg text-sm leading-normal")}>
            Pelajari keahlian dan rahasia sukses untuk <br />
            mewujudkan karir dan masa depan impianmu
          </div>
        </div>
      </div>
    </div>
  )
}
