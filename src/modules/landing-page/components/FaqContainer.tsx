import { useEffect, useState } from "react"
import { FAQ } from "@interfaces/school"
import SchoolServices from "@services/school.services"
import { Heading } from "@components/typography"
import clsx from "clsx"
import { container } from "@components/wrapper"
import { Disclosure, Transition } from "@headlessui/react"
import { AiOutlineRight } from "react-icons/ai"
import { useIsMounted } from "@lib/hooks/useIsMounted"

const FAQs: FAQ[] = [
  {
    faq_question: "Seberapa besar komitmen waktu yang harus saya berikan?",
    faq_answer:
      "Program ini berjalan selama 4 minggu dengan total 12 pertemuan. Setiap minggu terdapat 3 pertemuan, 2 pertemuan pada hari kerja malam hari dan 1 pertemuan pada Sabtu siang. ",
  },
  {
    faq_question: "Bagaimana bentuk konkrit pertemuan dalam program ini?",
    faq_answer: "Pertemuan berbentuk kelas online, menggunakan media konferensi video.",
  },
  {
    faq_question: "Apa syarat untuk dapat bergabung di program ini?",
    faq_answer: "Kamu cukup mengisi form pendaftaran dan menyediakan scan ijazah",
  },
  {
    faq_question: "Siapa saja yang dapat mengikuti program ini?",
    faq_answer: "Siapapun dengan berbagai latar belakang pendidikan dapat mengikuti program ini",
  },
  {
    faq_question: "Apa syarat kelulusan dari program ini?",
    faq_answer: "Kamu harus menghadiri minimal 8 pertemuan dan mengerjakan final project",
  },
  {
    faq_question: "Bagaimana cara mendapatkan slot di program ini?",
    faq_answer:
      "Bayar secara penuh saat sudah mendaftar atau dengan menggunakan program cicilan sebanyak 3x",
  },
]

export const FaqContainer = () => {
  const [faqs, setFaqs] = useState<FAQ[]>(FAQs)
  const [middleSlice, setMiddleSlice] = useState(3)
  const [faqLength, setFaqLength] = useState(6)

  const school = new SchoolServices()
  const isMounted = useIsMounted()

  useEffect(() => {
    const getData = async () => {
      try {
        const res = await school.getHomepageFaqs()
        if (res.isSuccess) {
          const result = res.getValue()

          if (result.length) {
            setFaqs(result)

            const length = result.length
            setFaqLength(length)
            const modulus = length % 2

            if (modulus) {
              setMiddleSlice(length / 2 + 1)
            } else {
              setMiddleSlice(length / 2)
            }
          }
        }
      } catch (e) {
        if (process.env.NODE_ENV === "development") {
          console.log({ e })
        }
      }
    }

    if (isMounted) {
      getData()
    }
  }, [isMounted])

  return (
    <div className="py-[30px]">
      <Heading headingLevel="h2" className="text-center mb-5">
        Pertanyaan yang Sering Ditanyakan
      </Heading>
      <div className={clsx(container, "flex flex-col sm:flex-row justify-between px-4")}>
        {/* Left Accordion */}
        <div className="flex flex-col sm:w-[45%]">
          {faqs.slice(0, middleSlice).map((value, index) => (
            <Disclosure key={index}>
              {({ open }) => (
                <>
                  <Disclosure.Button
                    className={clsx(
                      "flex  justify-between bg-[#f7f9fa] p-4 font-medium border-l-4 sm:text-base border-green text-left mt-4",
                      "text-sm"
                    )}
                  >
                    {value.faq_question}
                    <AiOutlineRight className={open ? "rotate-90 transform" : ""} />
                  </Disclosure.Button>
                  <Transition
                    show={open}
                    enter="transition duration-100 ease-out"
                    enterFrom="transform scale-95 opacity-0"
                    enterTo="transform scale-100 opacity-100"
                    leave="transition duration-75 ease-out"
                    leaveFrom="transform scale-100 opacity-100"
                    leaveTo="transform scale-95 opacity-0"
                  >
                    <Disclosure.Panel static>
                      <div
                        className={clsx(
                          "bg-[#f7f9fa] px-4 py-2 border-l-4 sm:text-base border-green text-light-grey",
                          "text-sm"
                        )}
                      >
                        {value.faq_answer}
                      </div>
                    </Disclosure.Panel>
                  </Transition>
                </>
              )}
            </Disclosure>
          ))}
        </div>

        <div className="flex flex-col sm:w-[45%]">
          {faqs.slice(middleSlice, faqLength).map((value, index) => (
            <Disclosure key={index}>
              {({ open }) => (
                <>
                  <Disclosure.Button
                    className={clsx(
                      "flex  justify-between bg-[#f7f9fa] p-4 font-medium border-l-4 sm:text-base border-green text-left mt-4",
                      "text-sm"
                    )}
                  >
                    {value.faq_question}
                    <AiOutlineRight className={open ? "rotate-90 transform" : ""} />
                  </Disclosure.Button>
                  <Transition
                    show={open}
                    enter="transition duration-100 ease-out"
                    enterFrom="transform scale-95 opacity-0"
                    enterTo="transform scale-100 opacity-100"
                    leave="transition duration-75 ease-out"
                    leaveFrom="transform scale-100 opacity-100"
                    leaveTo="transform scale-95 opacity-0"
                  >
                    <Disclosure.Panel static>
                      <div
                        className={clsx(
                          "bg-[#f7f9fa] px-4 py-2 border-l-4 sm:text-base border-green text-light-grey",
                          "text-sm"
                        )}
                      >
                        {value.faq_answer}
                      </div>
                    </Disclosure.Panel>
                  </Transition>
                </>
              )}
            </Disclosure>
          ))}
        </div>
      </div>
    </div>
  )
}
