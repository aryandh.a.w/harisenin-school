<<<<<<< HEAD:src/modules/landing-page/pages/LandingPage.tsx
import { useEffect, useState } from "react"
import Helmet from "@helpers/Helmet"

import { HARISENIN_PUBLIC_PAGE_ASSETS } from "@constants/pictures"

import { Header } from "@components/header"
import {
  LandingPageBanner,
  FaqContainer,
  LandingPageCta,
  CompanyRow,
  TestimonialRow,
} from "@modules/landing-page/components"

import { DynamicHelmet } from "@interfaces/misc"
import { School } from "@interfaces/school"
import { HariseninFooter } from "@components/harisenin/HariseninFooter"
import { useScreenSize } from "@lib/hooks/useScreenSize"
import ProgramRow from "../components/ProgramRow"

export interface LandingPageProps {
  meta: DynamicHelmet
  bootcamps: School[]
  proClasses: School[]
  codingCamps: School[]
}

export default function LandingPage({
  bootcamps,
  proClasses,
  codingCamps,
  meta,
}: LandingPageProps) {
  const [isTransparent, setIsTransparent] = useState(true)

  const { screenWidth } = useScreenSize()

  useEffect(() => {
    window.addEventListener("scroll", trackScrolling)

    return () => {
      window.removeEventListener("scroll", trackScrolling)
    }
  }, [])

  function isBottom(el: HTMLElement) {
    const eleTop = el.getBoundingClientRect().top

    return eleTop < 60
  }

  const trackScrolling = () => {
    const wrappedElement = document.getElementById("header2")
    const isBtm = isBottom(wrappedElement)

    if (isBtm) {
      setIsTransparent(false)
    } else {
      setIsTransparent(true)
    }
  }

  return (
    <>
      <Helmet
        isIndexed
        image={`${HARISENIN_PUBLIC_PAGE_ASSETS}/homepage_banner_2.png`}
        title={meta?.title ?? "Sekolah online yang  membantu mewujudkan karir dan masa depanmu."}
        description={
          meta?.description ??
          "Bersama harisenin.com, pelajari keahlian dan rahasia sukses untuk memulai perjalanan karier dan masa depan impianmu."
        }
      />
      <Header transparent={isTransparent} isNotSticky />

      <main>
        <LandingPageBanner />
        <ProgramRow screenWidth={screenWidth} type="BOOTCAMP" initialData={bootcamps} />
        <ProgramRow screenWidth={screenWidth} type="CODING_CAMP" initialData={codingCamps} />
        <ProgramRow screenWidth={screenWidth} type="PRO_CLASS" initialData={proClasses} />
        <CompanyRow screenWidth={screenWidth} />

        <TestimonialRow screenWidth={screenWidth} />

        <FaqContainer />

        <LandingPageCta />
      </main>

      <HariseninFooter />
    </>
  )
}
=======
import { useEffect, useState } from "react"
import Helmet from "../../../lib/utils/Helmet"
import useScreenSize from "../../../lib/utils/hooks/useScreenSize"

import { HARISENIN_PUBLIC_PAGE_ASSETS } from "../../../constants/pictures"

import HeaderHome from "../../../ui/modules/header/HeaderHome"

import LandingPageBanner from "./LandingPageBanner"
import CompanyRow from "./CompanyRow"
import ProgramRow from "./ProgramRow"
import TestimonialRow from "./TestimonialRow"
import FaqContainer from "./FaqContainer"

import style from "./styles/landing-page.module.scss"
import LandingPageCta from "./LandingPageCta"
import { DynamicHelmet } from "../../../constants/interfaces/misc"
import { School } from "../../../constants/interfaces/school"
import { HariseninFooter } from "../../modules/harisenin/HariseninFooter"

export interface LandingPageProps {
  meta: DynamicHelmet
  bootcamps: School[]
  proClasses: School[]
}

export default function LandingPage({ meta, bootcamps, proClasses }: LandingPageProps) {
  const [isTransparent, setIsTransparent] = useState(true)

  const { screenWidth } = useScreenSize()

  useEffect(() => {
    window.addEventListener("scroll", trackScrolling)

    return () => {
      window.removeEventListener("scroll", trackScrolling)
    }
  }, [])

  function isBottom(el: HTMLElement) {
    const eleTop = el.getBoundingClientRect().top

    return eleTop < 60
  }

  const trackScrolling = () => {
    const wrappedElement = document.getElementById("header2")
    const isBtm = isBottom(wrappedElement)

    if (isBtm) {
      setIsTransparent(false)
    } else {
      setIsTransparent(true)
    }
  }

  return (
    <>
      <Helmet
        isIndexed
        image={`${HARISENIN_PUBLIC_PAGE_ASSETS}/homepage_banner_2.png`}
        // title={meta?.title}
        // description={meta?.description}
        title="Sekolah online yang  membantu mewujudkan karir dan masa depanmu."
        description="Bersama harisenin.com, pelajari keahlian dan rahasia sukses untuk memulai perjalanan karier dan masa depan impianmu."
      />
      <HeaderHome transparent={isTransparent} isNotSticky />

      <div className={style.landing_page}>
        <LandingPageBanner />
        <ProgramRow screenWidth={screenWidth} type="BOOTCAMP" initialData={bootcamps} />
        <ProgramRow screenWidth={screenWidth} type="PRO_CLASS" initialData={proClasses} />
        <CompanyRow screenWidth={screenWidth} />

        <TestimonialRow screenWidth={screenWidth} />

        <FaqContainer />

        <LandingPageCta />
      </div>

      <HariseninFooter />
    </>
  )
}
>>>>>>> 94ccec2 (setup fix):src/ui/components/landing-page/LandingPage.tsx
