import { textShortener } from "@lib/functions"
import clsx from "clsx"
import { FC, useEffect, useState } from "react"

import { SchoolDetail } from "@interfaces/school"

import Helmet from "@helpers/Helmet"
import {
  ProgramBanner,
  ProgramBatch,
  ProgramBenefit,
  ProgramColumn,
  ProgramCta,
  ProgramCurriculum,
  ProgramExperience,
  ProgramFaqContainer,
  ProgramMethod,
  ProgramNavigator,
  ProgramPortfolio,
  ProgramStudent,
  ProgramTools,
  ProgramTutor,
  RelatedProgram,
} from "./components"

import { HariseninFooter } from "@components/harisenin/HariseninFooter"
import { container } from "@components/wrapper"
import { useScreenSize } from "@lib/hooks/useScreenSize"
import ProgramRegisterFlow from "./components/ProgramRegisterFlow"
import ProgramSchedules from "./components/ProgramSchedules"
import ProgramVideoPreview from "./components/ProgramVideoPreview"
import { Header } from "@components/header"

export interface ProgramLandingPageProps {
  schoolDetail?: SchoolDetail
  schema?: any
}

const ProgramLandingPage: FC<ProgramLandingPageProps> = ({ schoolDetail, schema }) => {
  const [isTransparent, setIsTransparent] = useState(true)

  const { screenWidth, screenHeight } = useScreenSize()

  useEffect(() => {
    window.addEventListener("scroll", trackScrolling)

    return () => {
      window.removeEventListener("scroll", trackScrolling)
    }
  }, [])

  function isBottom(el: HTMLElement) {
    const cursorPos = document.documentElement.scrollTop

    return cursorPos + 60 >= el.offsetTop
  }

  const trackScrolling = () => {
    const wrappedElement = document.getElementById("header1")
    isBottom(wrappedElement)

    if (isBottom(wrappedElement)) {
      setIsTransparent(false)
    } else {
      setIsTransparent(true)
    }
  }

  const executeScroll = (id: string) => () => {
    const ele = document.getElementById(id)

    if (!ele) {
      return
    }

    ele.scrollIntoView({ behavior: "smooth" })
  }

  return (
    <>
      <Helmet
        isIndexed
        title={schoolDetail.school_meta_title || `${schoolDetail?.product_name} - Harisenin.com`}
        description={
          schoolDetail.school_meta_description ||
          textShortener(schoolDetail?.product_description, 280)
        }
        schemaMarkup={schema}
      />
      <div className="relative">
        <Header transparent={isTransparent} isNotSticky />

        <div className="relative scroll-smooth">
          {/* Floating button for scrolling*/}
          {/* <button className="program_page__scroll" onClick={scrollToTop}>
          <FaChevronUp />
        </button> */}

          <ProgramBanner
            schoolDetail={schoolDetail}
            scroll={executeScroll("fee")}
            screenWidth={screenWidth}
          />

          {schoolDetail && (
            <div
              className={clsx(
                "sm:flex justify-between sm:py-10 sm:px-0 relative",
                "pb-4 px-4",
                container
              )}
            >
              <ProgramNavigator
                screenWidth={screenWidth}
                schoolDetail={schoolDetail}
                screenHeight={screenHeight}
              />

              <div className={clsx("w-3/4 h-auto", "sm-only:w-full")}>
                <ProgramSchedules
                  data={schoolDetail.school_batch}
                  scroll={executeScroll}
                  schoolType={schoolDetail.school_type}
                />

              <ProgramVideoPreview
                data={schoolDetail.school_video_previews}
                screenWidth={screenWidth}
              />

                <ProgramBenefit detail={schoolDetail} screenWidth={screenWidth} />

                <ProgramCurriculum
                  learnMaterial={schoolDetail.school_courses}
                  schoolName={schoolDetail.product_name}
                  schoolId={schoolDetail.product_id}
                />

                <ProgramTools screenWidth={screenWidth} schoolTools={schoolDetail?.school_tools} />

                <ProgramTutor screenWidth={screenWidth} tutors={schoolDetail.school_tutors} />

              <ProgramExperience
                screenWidth={screenWidth}
                experiences={schoolDetail.school_learning_experiences}
                schoolType={schoolDetail.school_type}
              />

                {schoolDetail.school_type === "BOOTCAMP" && (
                  <ProgramMethod screenWidth={screenWidth} />
                )}

                <ProgramBatch data={schoolDetail.school_batch} screenWidth={screenWidth} />

                <ProgramColumn
                  programs={schoolDetail.school_program_type}
                  schoolDetail={schoolDetail}
                  screenWidth={screenWidth}
                  requirement={schoolDetail.school_program_requirement}
                />

                <ProgramRegisterFlow />

                <ProgramStudent data={schoolDetail.school_testimonies} screenWidth={screenWidth} />

                <ProgramPortfolio
                  screenWidth={screenWidth}
                  portfolio={schoolDetail.school_portfolio}
                />

              {Boolean(schoolDetail.school_faqs.length) && (
                <ProgramFaqContainer
                  faqs={schoolDetail.school_faqs}
                  schoolType={schoolDetail.school_type}
                />
              )}
            </div>
          </div>
        )}

          <RelatedProgram screenWidth={screenWidth} />

          <ProgramCta />
        </div>
      </div>

      <HariseninFooter />
    </>
  )
}

export default ProgramLandingPage
