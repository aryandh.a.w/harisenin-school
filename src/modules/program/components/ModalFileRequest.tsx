import { RegularButton, TextButton } from "@components/buttons"
import { TokenData } from "@interfaces/user"
import { Result } from "@services/response"
import SchoolServices from "@services/school.services"
import { Form, Formik } from "formik"
import { FC, useEffect, useState } from "react"
import { FaTimes } from "react-icons/fa"
import * as Yup from "yup"

import MaterialFormInput from "@components/input/MaterialFormInput"
import MaterialTextAreaInput from "@components/input/MaterialTextAreaInput"
import { Modal } from "@components/modal"
import { FlexBox } from "@components/wrapper"
import { COUNTRY_CODE } from "@constants/country-code"
import { FileRequestForm } from "@interfaces/school"
import clsx from "clsx"
import Select from "react-select"

export interface ModalFileRequestProps {
  data?: TokenData
  schoolId?: string
  mode: "syllabus" | "portfolio"
  show: boolean
  onHide: () => void
}

const ModalFileRequest: FC<ModalFileRequestProps> = ({ data, schoolId, mode, show, onHide }) => {
  const [initialValues, setInitialValues] = useState<FileRequestForm>({
    user_fullname: "",
    user_email: "",
    user_phone_number: "",
    reason_of_download: "",
  })

  const [validation, setValidation] = useState("")
  const [isLoading, setIsLoading] = useState(false)
  const [validationRequired, setValidationRequired] = useState(false)
  const [selectedCode, setSelectedCode] = useState("+62")

  const school = new SchoolServices()

  useEffect(() => {
    if (!data) {
      return
    }

    let codeIndex = 0
    let newPhoneNumber = ""
    let currentCode = ""

    while (newPhoneNumber.length < 1) {
      if (data.user_phone_number.includes(COUNTRY_CODE[codeIndex]?.dial_code)) {
        newPhoneNumber = data.user_phone_number.replace(COUNTRY_CODE[codeIndex]?.dial_code, "")
        currentCode = COUNTRY_CODE[codeIndex]?.dial_code
      } else {
        codeIndex++
      }
    }

    setInitialValues({
      user_fullname: data.fullname,
      user_email: data.user_email,
      user_phone_number: newPhoneNumber,
      reason_of_download: "",
    })
    setSelectedCode(currentCode)
  }, [data])

  useEffect(() => {
    if (!show) {
      setValidation("")
    }
  }, [show])

  const validationSchema = Yup.object({
    user_fullname: Yup.string()
      .required("*Wajib diisi")
      .matches(/^[A-Za-z\s]+$/, "Hanya boleh huruf"),
    user_email: Yup.string().email("Email tidak valid").required("*Wajib diisi"),
    user_phone_number: Yup.string()
      .min(10, "*Nomor telepon minimal 11 nomor")
      .max(12, "*Nomor telepon maksimal 13 nomor")
      .required("*Wajib diisi"),
    reason_of_download: Yup.string().min(20).max(300).required("*Wajib diisi"),
  })

  const DialCodeSelector = () => {
    return (
      <div className={clsx("relative")}>
        <Select
          options={COUNTRY_CODE}
          key={`my_unique_select_key__${JSON.stringify(selectedCode)}`}
          getOptionLabel={(option) => `${option.name}`}
          getOptionValue={(opt) => `${opt?.dial_code}`}
          formatOptionLabel={(option) => `${option.flag} ${option?.dial_code}`}
          onChange={(opt) => setSelectedCode(opt?.dial_code)}
          value={COUNTRY_CODE.find((c) => c?.dial_code === selectedCode)}
          styles={{
            control: (props) => ({
              ...props,
              border: "1px solid rgba(58, 53, 65, 0.23)",
              borderColor: "rgba(58, 53, 65, 0.23)",
              boxShadow: "0 0 0 rgba(58, 53, 65, 0.23)",
              borderRadius: "2px",
              minWidth: "120px",
              minHeight: 0,
              padding: "16px",
              "&:hover": {
                border: "1px solid rgba(58, 53, 65, 0.23)",
                boxShadow: "0 0 0 rgba(58, 53, 65, 0.23)",
              },
              "&:focus": {
                border: "2px solid rgba(58, 53, 65, 0.23)",
                boxShadow: "0 0 0 rgba(58, 53, 65, 0.23)",
              },
              "@media(max-width: 576px)": {
                fontSize: "12px",
              },
            }),
            input: (props) => ({
              ...props,
              margin: 0,
              paddingTop: 0,
              paddingBottom: 0,
            }),
            placeholder: (props) => ({
              ...props,
              color: "#979797",
              marginLeft: 0,
              marginRight: 0,
            }),
            indicatorSeparator: () => ({
              display: "none",
            }),
            dropdownIndicator: (props) => ({
              ...props,
              padding: 0,
            }),
            valueContainer: (props) => ({
              ...props,
              padding: "0 5px 0 0",
            }),
            option: (props, { isSelected }) => ({
              ...props,
              backgroundColor: isSelected ? "#0DAD8E" : "white",
              "&:active": {
                backgroundColor: "rgba(13, 173, 142, 0.5)",
              },
              "@media(max-width: 576px)": {
                fontSize: "12px",
              },
            }),
          }}
        />
      </div>
    )
  }

  return (
    <Modal
      show={show}
      onClose={onHide}
      contentClassName="sm:min-w-[50%] min-w-[90%] !rounded-none max-h-[80vh] overflow-scroll"
    >
      <div className="sm:p-5 p-3">
        <FlexBox justify="between" align="center" className="mb-5">
          <h2 className="text-xl">
            Yuk, Isi Data Dirimu untuk Akses {mode === "syllabus" ? "Materinya" : "Portofolionya"}!
          </h2>

          <TextButton onClick={onHide}>
            <FaTimes className="text-xl lg:flex hidden cursor-pointer" />
          </TextButton>
        </FlexBox>

        <Formik
          enableReinitialize
          initialValues={initialValues}
          validationSchema={validationSchema}
          validateOnChange={validationRequired}
          validateOnBlur={validationRequired}
          onSubmit={async (value) => {
            setIsLoading(true)
            try {
              let res: Result<any>

              const form = {
                ...value,
                user_phone_number: selectedCode + value.user_phone_number,
              }

              if (mode === "syllabus") {
                res = await school.getSyllabusForm(schoolId, form)
              } else {
                res = await school.getPortfolioForm(schoolId, form)
              }

              if (res && res.isSuccess) {
                const result = res.getValue()
                window.open(result as string, "_blank")
                // props.onHide()
              } else {
                setValidation(res ? res.error.message : "Terjadi kesalahan. Silahkan coba lagi")
              }
            } catch (e: any) {
              if (process.env.NODE_ENV === "development") {
                console.log(e)
              }
              setValidation("Terjadi kesalahan. Silahkan coba lagi")
            }
            setIsLoading(false)
          }}
        >
          {() => {
            return (
              <div className="p-0">
                <Form className="grid gap-6">
                  <MaterialFormInput label="Nama Depan" name="user_fullname" />

                  <MaterialFormInput label="Email" name="user_email" />

                  <FlexBox className={clsx("gap-3")}>
                    <DialCodeSelector />
                    <MaterialFormInput
                      label="Nomor Telepon"
                      name="user_phone_number"
                      className="w-full"
                    />
                  </FlexBox>

                  <MaterialTextAreaInput
                    label={`Kenapa nih mau download ${
                      mode === "syllabus" ? "kurikulum" : "portfolio"
                    }nya?`}
                    name="reason_of_download"
                    rows={3}
                    maxCharacter={300}
                    minCharacter={20}
                  />

                  <div className="text-red">{validation}</div>

                  <RegularButton
                    variant="solid-blue"
                    isSubmitting={isLoading}
                    onClick={() => {
                      setValidationRequired(true)
                    }}
                    isFormik
                    className="w-full !rounded-none"
                    customPadding="p-3"
                  >
                    Download {mode === "syllabus" ? "Kurikulum" : "Portofolio"}
                  </RegularButton>
                </Form>
              </div>
            )
          }}
        </Formik>
      </div>
    </Modal>
  )
}

export default ModalFileRequest
