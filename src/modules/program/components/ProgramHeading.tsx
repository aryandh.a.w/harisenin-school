import clsx from "clsx"
import React from "react"

interface ProgramHeadingProps {
  id: string
  title: string
  subTitle: string
}

export const ProgramHeading = ({ id, title, subTitle }: ProgramHeadingProps) => {
  return (
    <>
      <div className={clsx("absolute -top-24")} id={id} />

      <h2
        className={clsx(
          "sm:text-lg sm:mb-0 font-medium text-green uppercase tracking-widest",
          "mb-1"
        )}
      >
        {title}
      </h2>
      <h3
        className={clsx(
          "sm:text-[32px] sm:mb-9 sm:leading-9 font-medium",
          "text-xl mb-[18px] leading-7"
        )}
      >
        {subTitle}
      </h3>
    </>
  )
}
