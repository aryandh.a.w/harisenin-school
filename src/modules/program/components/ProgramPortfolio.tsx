<<<<<<< HEAD:src/modules/program/components/ProgramPortfolio.tsx
import React, { FC, useState } from "react"
import { SchoolPortfolio } from "@interfaces/school"
import Carousel from "react-multi-carousel"
import { SliderArrow } from "@components/slider/SliderArrow"
import { FaArrowRight } from "react-icons/fa"
import { TextButton } from "@components/buttons"
import { useUserData } from "@hooks/useUserData"
import ModalFileRequest from "./ModalFileRequest"
import { LoginModal } from "@components/modal/LoginModal"
import { HARISENIN_PUBLIC_PAGE_ASSETS } from "@constants/pictures"
import clsx from "clsx"
import { ProgramHeading } from "./ProgramHeading"
import Image from "next/image"

export interface ProgramPortfolioProps {
  screenWidth: number
  portfolio: SchoolPortfolio[]
}

export function ProgramPortfolio({ screenWidth, portfolio }: ProgramPortfolioProps) {
  // Slider breakpoint
  const responsive = {
    superLargeDesktop: {
      breakpoint: { max: 4000, min: 3000 },
      items: 1,
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 2,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 1,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
    },
  }

  const { isLogin, tokenData } = useUserData()

  const [openLogin, setOpenLogin] = useState(false)
  const [openSyllabus, setOpenSyllabus] = useState(false)
  const [portfolioId, setPortfolioId] = useState(-1)

  const openModal = (id: number) => () => {
    if (isLogin) {
      setOpenSyllabus(true)
      setPortfolioId(id)
    } else {
      setOpenLogin(true)
    }
  }

  const PortfolioCard: FC<{ data: SchoolPortfolio; index: number }> = ({ data, index }) => {
    return (
      <div
        className={clsx("sm:w-[calc(100%-30px)] m-auto rounded", "sm-only:w-[70vw] sm-only:ml-4")}
        id={`portfolio-card-${index}`}
        style={{ boxShadow: "0 10px 20px 0 #9aaacf40" }}
      >
        <div className={clsx("sm:h-[20vw] rounded-t", "h-[42vw]")}>
          <Image
            width={860}
            height={428}
            src={
              data.portfolio_thumbnail.length
                ? data.portfolio_thumbnail
                : HARISENIN_PUBLIC_PAGE_ASSETS + "/program_portfolio-dummy.png"
            }
            alt={data.portfolio_title}
            className={clsx("rounded-t w-full h-full object-cover")}
          />
        </div>
        <div className={clsx("sm:pt-4 sm:px-6 sm:pb-8", "py-3 px-4")}>
          <div
            className={clsx("sm:text-xl mb-3 font-semibold line-clamp-1", "text-sm")}
            id={`portfolio-${index}`}
          >
            {data.portfolio_title}
          </div>
          <div
            className={clsx(
              "sm:text-sm sm:h-36 sm:leading-6 mb-4 line-clamp-6",
              "text-xs leading-4 h-20"
            )}
          >
            {data.portfolio_description}
          </div>
          <TextButton
            className={clsx(
              "flex items-center gap-2 text-blue font-semibold",
              "sm-only:text-sm sm-only:mb-4"
            )}
            onClick={openModal(data.portfolio_id)}
            id={`btn-download-portfolio-${index}`}
          >
            Lihat Portofolio <FaArrowRight />
          </TextButton>
        </div>
      </div>
    )
  }

  if (!screenWidth || !portfolio?.length) {
    return <></>
  }

  return (
    <>
      {screenWidth > 576 ? (
        <div className={clsx("relative mb-15 mt-12")} id="portfolio1">
          <ProgramHeading
            id="portfolio"
            title="Portofolio Alumni"
            subTitle="Yuk, Intip Hasil Karya dari Para Alumni!"
          />

          <Carousel
            responsive={responsive}
            ssr={true} // means to render carousel on server-side.
            arrows={false}
            containerClass={clsx("-ml-4 -mt-4 py-5 px-0 w-[calc(100%+30px)]")}
            customButtonGroup={
              <SliderArrow
                length={portfolio.length}
                displayedSlide={2}
                leftClassName="-left-[1vw] top-[52%] w-[42px] h-[42px]"
                rightClassName="-right-[1vw] top-[52%] w-[42px] h-[42px]"
              />
            }
            renderButtonGroupOutside
            className="d-lg-flex d-none"
          >
            {portfolio.map((value, i) => (
              <PortfolioCard data={value} key={i} index={i} />
            ))}
          </Carousel>
        </div>
      ) : (
        <div className={clsx("relative mb-6")} id="portfolio">
          <h2 className={clsx("sm:text-base sm:mb-0 font-semibold text-green", "text-sm mb-1")}>
            Portofolio Alumni
          </h2>
          <h3 className={clsx("sm:text-2xl sm:mb-5 sm:leading-9", "text-lg mb-4 leading-7")}>
            Yuk, Intip Hasil Karya dari Para Alumni!
          </h3>

          <div className={clsx("w-[calc(100%+32px)] slider-mobile -ml-4 -mr-4 mb-0")}>
            <div className="flex w-max pb-6">
              {portfolio.map((value, i) => (
                <PortfolioCard data={value} key={i} index={i} />
              ))}
            </div>
          </div>
        </div>
      )}

      <ModalFileRequest
        data={tokenData}
        schoolId={portfolioId.toString()}
        show={openSyllabus}
        onHide={() => setOpenSyllabus(false)}
        mode="portfolio"
      />

      <LoginModal show={openLogin} onHide={() => setOpenLogin(false)} />
    </>
  )
}
=======
import React, { FC, useState } from "react"
import { SchoolPortfolio } from "../../../../constants/interfaces/school"
import style from "../styles/program.module.scss"
import Carousel from "react-multi-carousel"
import SliderArrow from "../../../modules/sliders/SliderArrow"
import { FaArrowRight } from "react-icons/fa"
import { TextButton } from "../../../modules/buttons"
import { useUserData } from "../../../../lib/utils/hooks/useUserData"
import ModalFileRequest from "./ModalFileRequest"
import { LoginModal } from "../../../modules/modal/LoginModal"
import { HARISENIN_PUBLIC_PAGE_ASSETS } from "../../../../constants/pictures"

export interface ProgramPortfolioProps {
  screenWidth: number
  portfolio: SchoolPortfolio[]
}

function ProgramPortfolio({ screenWidth, portfolio }: ProgramPortfolioProps) {
  // Slider breakpoint
  const responsive = {
    superLargeDesktop: {
      breakpoint: { max: 4000, min: 3000 },
      items: 1,
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 2,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 1,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
    },
  }

  const { isLogin, tokenData } = useUserData()

  const [openLogin, setOpenLogin] = useState(false)
  const [openSyllabus, setOpenSyllabus] = useState(false)
  const [portfolioId, setPortfolioId] = useState(-1)

  const openModal = (id: number) => () => {
    if (isLogin) {
      setOpenSyllabus(true)
      setPortfolioId(id)
    } else {
      setOpenLogin(true)
    }
  }

  const PortfolioCard: FC<{ data: SchoolPortfolio; index: number }> = ({ data, index }) => {
    return (
      <div className={style.program_page__portfolio__card} id={`portfolio-card-${index}`}>
        <div className={style.program_page__portfolio__card__image}>
          <img
            src={
              data.portfolio_thumbnail.length
                ? data.portfolio_thumbnail
                : HARISENIN_PUBLIC_PAGE_ASSETS + "/program_portfolio-dummy.png"
            }
            alt={data.portfolio_title}
          />
        </div>
        <div className={style.program_page__portfolio__card__body}>
          <div
            className={style.program_page__portfolio__card__body__title}
            id={`portfolio-${index}`}
          >
            {data.portfolio_title}
          </div>
          <div className={style.program_page__portfolio__card__body__description}>
            {data.portfolio_description}
          </div>
          <TextButton
            className={style.program_page__portfolio__card__body__link}
            onClick={openModal(data.portfolio_id)}
            id={`btn-download-portfolio-${index}`}
          >
            <u>Lihat Selengkapnya</u> <FaArrowRight />
          </TextButton>
        </div>
      </div>
    )
  }

  return (
    <>
      {screenWidth > 576 ? (
        <div className={style.program_page__portfolio} id="portfolio1">
          <div className={style["program_page__anchor"]} id="portfolio" />
          <h2>Portofolio Alumni</h2>
          <h3>Hasil Karya Murid Harisenin.com</h3>

          <Carousel
            responsive={responsive}
            ssr={true} // means to render carousel on server-side.
            arrows={false}
            containerClass={style.program_page__portfolio__container}
            customButtonGroup={<SliderArrow length={portfolio.length} displayedSlide={2} />}
            renderButtonGroupOutside
            className="d-lg-flex d-none"
          >
            {portfolio.map((value, i) => (
              <PortfolioCard data={value} key={i} index={i} />
            ))}
          </Carousel>
        </div>
      ) : (
        <div className={style.program_page__portfolio} id="portfolio">
          <h2>Portofolio Alumni</h2>
          <h3>Hasil Karya Murid Harisenin.com</h3>

          <div className={style.program_page__portfolio__container}>
            <div className={style.program_page__portfolio__row}>
              {portfolio.map((value, i) => (
                <PortfolioCard data={value} key={i} index={i} />
              ))}
            </div>
          </div>
        </div>
      )}

      <ModalFileRequest
        data={tokenData}
        schoolId={portfolioId.toString()}
        show={openSyllabus}
        onHide={() => setOpenSyllabus(false)}
        mode="portfolio"
      />

      <LoginModal show={openLogin} onHide={() => setOpenLogin(false)} />
    </>
  )
}

export default ProgramPortfolio
>>>>>>> 94ccec2 (setup fix):src/ui/components/program/components/ProgramPortfolio.tsx
