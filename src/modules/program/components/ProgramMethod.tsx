import { NextImage } from "@components/misc"

import { FlexBox } from "@components/wrapper"
import { HARISENIN_PUBLIC_PAGE_ASSETS } from "@constants/pictures"
import clsx from "clsx"

export const STUDY_METHOD = [
  {
    icon: "course_method-1.png",
    title: "Interactive Learning",
    description:
      "Gak hanya materi, di kelas juga akan ada sesi diskusi biar proses belajarmu makin hidup.",
  },
  {
    icon: "course_method-2.png",
    title: "Missions & Challenges",
    description:
      "Belajar jadi paham dan makin seru karena ada misi yang perlu diselesaikan untuk bantu improve skill-mu.",
  },
  {
    icon: "course_method-3.png",
    title: "Team Buddy",
    description:
      "Kamu bisa bertanya dan akan dibimbing oleh Team Buddy saat di dalam maupun di luar kelas.",
  },
  {
    icon: "course_method-4.png",
    title: "Career Support",
    description:
      "Kamu akan mendapatkan akses terhadap konsultasi karier seperti pembuatan CV hingga persiapan interview.",
  },
]

export const ProgramMethod = ({ screenWidth }: { screenWidth: number }) => {
  return (
    <div className={clsx("relative sm:mb-15", "mb-8")} id="method1">
      <div className={clsx("absolute -top-24")} id="method" />
      <h2 className={clsx("sm:text-2xl sm:mb-6 font-medium", "text-lg mb-[18px]")}>
        Dapatkan Akses Terhadap Metode Belajar Terbaik
      </h2>

      <div
        className={clsx(
          screenWidth < 576 && "slider-mobile w-[calc(100%+32px)] sm-only:-mx-4 sm-only:pl-4"
        )}
      >
        <div className={clsx("grid grid-cols-4 sm:gap-9", " gap-4 sm-only:w-max")}>
          {/* Reward point component */}
          {STUDY_METHOD.map((value, index) => (
            <div key={index} className={`relative method-animation`}>
              <NextImage
                src={`${HARISENIN_PUBLIC_PAGE_ASSETS}/${value.icon}`}
                alt={value.title}
                // className={clsx("sm:h-12 w-auto", "h-10 sm-only:mb-3")}
                size={[500, 500]}
                className={clsx("object-cover", "sm:w-full sm:h-[290px] w-[55vw] h-[85vw]")}
              />

              <FlexBox
                className={clsx(
                  "absolute top-0 left-0 wh-full method-animation__background",
                  "p-6 text-white"
                )}
                direction="col"
                justify="end"
              >
                <div className={clsx("font-semibold mb-3 text-left text-xl", "")}>
                  {value.title}
                </div>
                <div className={clsx("text-sm method-animation__description")}>
                  {value.description}
                </div>
              </FlexBox>
            </div>
          ))}
        </div>
      </div>
    </div>
  )
}
