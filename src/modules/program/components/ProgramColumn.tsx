import { FC, useState } from "react"

import { SchoolDetail, SchoolProgram, SchoolProgramType } from "@interfaces/school"
import { createMarkup, priceFormatter } from "@lib/functions"

import { LoginModal } from "@components/modal/LoginModal"
import { useUserData } from "@hooks/useUserData"
import { CheckoutSummary } from "@interfaces/checkout-state"

import { LinkButton, RegularButton } from "@components/buttons"
import { FlexBox } from "@components/wrapper"
import { HARISENIN_LOGO_COLORED } from "@constants/pictures"
import { Tab } from "@headlessui/react"
import clsx from "clsx"
import Image from "next/image"
import { useRouter } from "next/router"
import { FaCheckCircle, FaTimesCircle } from "react-icons/fa"
import { RiErrorWarningLine, RiWhatsappFill } from "react-icons/ri"
import { ProgramHeading } from "./ProgramHeading"
import {trackAddToCart, trackViewItem} from "@utils/analytics";

export interface ProgramColumnProps {
  programs: SchoolProgramType[]
  schoolDetail: SchoolDetail
  screenWidth: number
  requirement?: string
}

export const ProgramColumn: FC<ProgramColumnProps> = ({ programs, schoolDetail, requirement }) => {
  const [loginModal, setLoginModal] = useState(false)

  const { isLogin, tokenData } = useUserData()
  const router = useRouter()
  const pathName = router.pathname
  const [activeTab, setActiveTab] = useState(0)

  // Function for program button `daftar sekarang`
  const nextPageHandler = (data: SchoolProgram) => () => {
    // setPackageId(index)
    if (!isLogin) {
      return setLoginModal(true)
    }

    if (programs[activeTab].program_type === "PAY_NOW") {
      try {
        const storage: CheckoutSummary = {
          school_id: schoolDetail.product_id,
          school_slug: schoolDetail.product_slug,
          school_title: schoolDetail.product_name,
          image: schoolDetail.product_asset.product_image,
          program_id: data.id,
          program_description: data.program_description,
          program_title: data.program_title,
          program_price: data.program_price,
        }

        if (tokenData.user_active) {
          const isProClass = pathName.includes("proclass")

          window.localStorage.setItem(
            isProClass ? "hsp_checkout" : "hs_checkout",
            JSON.stringify(storage)
          )

          trackViewItem({
            item_id: schoolDetail.product_id,
            item_name: `${schoolDetail.product_name}`,
            item_category: isProClass ? "ProClass" : "Bootcamp",
            item_brand: "HMS",
            item_variant: data.program_title,
            item_image_url: schoolDetail.product_asset.product_image,
            item_url:`${process.env.HOME_URL}/school/${schoolDetail.product_slug}`,
            price: data.program_price,
            currency: "IDR",
            quantity: 1
          })

          trackAddToCart({
            item_id: schoolDetail.product_id,
            item_name: `${schoolDetail.product_name}`,
            item_category: isProClass ? "ProClass" : "Bootcamp",
            item_brand: "HMS",
            item_variant: data.program_title,
            item_image_url: schoolDetail.school_banner.banner_image,
            item_url:`${process.env.HOME_URL}/school/${schoolDetail.product_slug}`,
            price: data.program_price,
            currency: "IDR",
            quantity: 1
          })
          
          window.location.assign(`/school/${isProClass ? "/proclass/checkout" : "checkout"}`)
        } else {
          alert(
            "Akun kamu belum diverifikasi. silahkan coba login ulang jika kamu telah melakukan verikasi"
          )
        }
      } catch (e) {
        alert(e);
        alert("Terjadi kesalahan. Silahkan coba login ulang dan coba lagi")
      }
    }

    if (programs[activeTab].program_type === "INSTALLMENT") {
      return window.open(
        `https://api.whatsapp.com/send?phone=6281312117711&text=Saya%20Mau%20ikut%Program%20Cicilan%20di%20harisenin.com%20min`,
        "_blank"
      )
    }
  }

  const programColumn = () => {
    const PROGRAMS = [
      {
        title: "GET A JOB Program",
        description:
          "Upskill + Get A Job Program. Cocok buat kamu yang mau pindah jalur karier & dapetin kerjaan lebih cepet.",
        descriptionList: [
          {
            icon_type: "true-blue",
            desc: "3 - 5 Bulan Intensive & Fun Online Live Class",
          },
          {
            icon_type: "true-blue",
            desc: "+ 3 bulan Career Support & Job Preparation Program. Plus, terjun langsung di real project/apprenticeship",
          },
          {
            icon_type: "true-blue",
            desc: "Lifetime Job Connect Program",
          },
          {
            icon_type: "true-blue",
            desc: "Freelance or Part-time Project for Alumni",
          },
          {
            icon_type: "true-blue",
            desc: "Professional English & Mental Health Session",
          },
          {
            icon_type: "true-blue",
            desc: "Recording & Class Material Lifetime Access",
          },
          {
            icon_type: "true-blue",
            desc: "3 - 15 Profesional Portofolio",
          },
          {
            icon_type: "true-blue",
            desc: "Anytime 1-on-1 Career Consultation",
          },
          {
            icon_type: "true-blue",
            desc: "Refund hingga 110% apabila belum bekerja selama 360 hari setelah lulus",
          },
          {
            icon_type: "true-blue",
            desc: "Opsi Pembayaran: Upfront atau Bisa dicicil 12 - 18 Bulan",
          },
        ],
        programs: programs[0].school_programs.filter(
          (p) => p.program_sub_type === "GET_A_JOB_PROGRAM"
        ),
        programSubType: "GET_A_JOB_PROGRAM",
      },
    ]

    if (programs[0].school_programs.some((s) => s.program_sub_type === "FULL_COURSE")) {
      PROGRAMS.push({
        title: "Full Course",
        description: "Upskill Program from 0 to 1. Cocok buat kamu yang mau ngembangin skill baru.",
        descriptionList: [
          {
            icon_type: "true-green",
            desc: "3 - 5 Bulan Intensive & Fun Online Live Class",
          },
          {
            icon_type: "no",
            desc: "+ 3 bulan Career Support & Job Preparation Program. Plus, terjun langsung di real project/apprenticeship",
          },
          {
            icon_type: "no",
            desc: "Lifetime Job Connect Program",
          },
          {
            icon_type: "true-green",
            desc: "Freelance or Part-time Project for Alumni",
          },
          {
            icon_type: "true-green",
            desc: "Professional English & Mental Health Session",
          },
          {
            icon_type: "true-green",
            desc: "Recording & Class Material Lifetime Access",
          },
          {
            icon_type: "true-green",
            desc: "3 - 15 Profesional Portofolio",
          },
          {
            icon_type: "no",
            desc: "Anytime 1-on-1 Career Consultation",
          },
          {
            icon_type: "no",
            desc: "Refund hingga 110% apabila belum bekerja selama 360 hari setelah lulus",
          },
          {
            icon_type: "no",
            desc: "Opsi Pembayaran: Upfront atau Bisa dicicil 12 - 18 Bulan",
          },
        ],
        programs: programs[0].school_programs.filter((p) => p.program_sub_type === "FULL_COURSE"),
        programSubType: "FULL_COURSE",
      })
    }

    return PROGRAMS
  }

  const DESCRIPTION_FOR_PROCLASS = [
    {
      icon_type: "true-green",
      desc: "Kurikulum up to date",
    },
    {
      icon_type: "true-green",
      desc: "Lingkungan belajar yang fun dan suportif",
    },
    {
      icon_type: "true-green",
      desc: "Bersertifikat",
    },
    {
      icon_type: "no",
      desc: "Job Connect",
    },
  ]

  if (!programs || !programs.length) {
    return <></>
  }

  return (
    <div className="relative sm:mb-15" id="fee1">
      <ProgramHeading
        id="fee"
        title="Biaya & Pendaftaran"
        subTitle={`Lebih Terjangkau ${programs.length > 1 ? "& Bisa Dicicil" : ""}`}
      />

      <Tab.Group>
        {programs.length > 1 && (
          <Tab.List className={clsx(`grid grid-cols-${programs.length}`, "sm:mb-9 mb-[18px]")}>
            {programs.map((value, index) => (
              <Tab
                key={index}
                onClick={() => setActiveTab(index)}
                className={({ selected }) =>
                  clsx(
                    "py-4 font-medium",
                    selected
                      ? "text-blue bg-[#EBEFFC] border-b-blue border-b-3"
                      : "border-b-[rgba(58,53,65,0.12)] border-b"
                  )
                }
              >
                {value.program_type_alias}
              </Tab>
            ))}
          </Tab.List>
        )}

        <Tab.Panels className="">
          <Tab.Panel>
            {pathName.includes("/bootcamp") && (
              <div
                className={clsx(
                  "grid sm:gap-9 gap-[18px]",
                  programs[0].school_programs.some((s) => s.program_sub_type === "FULL_COURSE") &&
                    "sm:grid-cols-2"
                )}
              >
                {programColumn().map((item, i) => (
                  <div className={clsx("border border-[rgba(58,53,65,0.12)] h-fit")} key={i}>
                    <div className={clsx("p-[26px]")}>
                      <div
                        className={clsx(
                          "font-medium text-sm mb-3 uppercase tracking-widest",
                          item.programSubType === "GET_A_JOB_PROGRAM" ? "text-blue" : "text-green"
                        )}
                      >
                        {item.title}
                      </div>
                      <div className="mb-3 sm:h-[5em]">{item.description}</div>

                      <div className="grid gap-1.5">
                        {item.descriptionList.map((d, i) => (
                          <FlexBox key={i} align="start">
                            <div className="w-6 mt-0.5">
                              {d.icon_type === "true-blue" && (
                                <FaCheckCircle className="text-blue" />
                              )}
                              {d.icon_type === "true-green" && (
                                <FaCheckCircle className="text-green" />
                              )}
                              {d.icon_type === "no" && <FaTimesCircle className="text-red" />}
                            </div>
                            <div className="w-[90%]">{d.desc}</div>
                          </FlexBox>
                        ))}
                      </div>
                    </div>

                    {item.programs.map((p, i) => (
                      <div
                        key={i}
                        className={clsx("border-t border-t-[rgba(58,53,65,0.12)]", "py-4 px-6")}
                      >
                        <div className="sm:flex gap-4 sm:mb-5 mb-4">
                          <div className={clsx("font-semibold text-xl ")}>{p.program_title}</div>
                          {Boolean(p.program_best_seller) && (
                            <div className="sm:min-w-[150px] w-fit h-fit text-sm py-2 px-3 text-blue bg-[#E4F2FE] font-medium tracking-widest uppercase text-center">
                              ⚡ Best Seller
                            </div>
                          )}
                        </div>

                        <FlexBox className={clsx("sm:gap-4")} justify="between">
                          <div>
                            {Boolean(p.program_discount_percentage) && (
                              <FlexBox align="center" className="gap-2">
                                <div className="text-[10px] text-white bg-red py-0.5 px-1 rounded h-fit">
                                  {p.program_discount_percentage * 100}%
                                </div>
                                <div className="text-sm text-[rgba(58,53,65,0.54)] line-through">
                                  {priceFormatter(p.program_original_price)}
                                </div>
                              </FlexBox>
                            )}

                            <div
                              className={clsx(
                                "sm:text-2xl font-semibold text-xl",
                                item.programSubType === "GET_A_JOB_PROGRAM"
                                  ? "text-blue"
                                  : "text-green"
                              )}
                            >
                              {priceFormatter(p.program_price)}
                            </div>
                          </div>
                          <RegularButton
                            variant={
                              item.programSubType === "GET_A_JOB_PROGRAM" ? "solid-blue" : "solid"
                            }
                            disabled={!p.can_order}
                            className="!rounded-none h-fit"
                            customPadding="py-2 sm:px-6 px-[22px]"
                            onClick={nextPageHandler(p)}
                          >
                            Daftar
                          </RegularButton>
                        </FlexBox>
                      </div>
                    ))}
                  </div>
                ))}
              </div>
            )}

            {pathName.includes("/proclass") && (
              <div className={clsx("border border-[rgba(58,53,65,0.12)] h-fit")}>
                <div className={clsx("p-[26px]")}>
                  <div
                    className={clsx(
                      "font-medium text-sm mb-3 uppercase tracking-widest",
                      "text-green"
                    )}
                  >
                    Full Course
                  </div>
                  <div className="mb-3">Belajar buat upgrade skill dalam 1–2 bulan</div>

                  <div className="grid gap-1.5">
                    {DESCRIPTION_FOR_PROCLASS.map((d, i) => (
                      <FlexBox key={i} align="start">
                        <div className="w-6 mt-0.5">
                          {d.icon_type === "true-blue" && <FaCheckCircle className="text-blue" />}
                          {d.icon_type === "true-green" && <FaCheckCircle className="text-green" />}
                          {d.icon_type === "no" && <FaTimesCircle className="text-red" />}
                        </div>
                        <div className="w-[90%]">{d.desc}</div>
                      </FlexBox>
                    ))}
                  </div>
                </div>

                {programs[0].school_programs.map((p, i) => (
                  <div
                    key={i}
                    className={clsx("border-t border-t-[rgba(58,53,65,0.12)]", "py-4 px-6")}
                  >
                    <div className="sm:flex gap-4 sm:mb-5 mb-4">
                      <div className={clsx("font-semibold text-xl ")}>{p.program_title}</div>
                      {Boolean(p.program_best_seller) && (
                        <div className="sm:min-w-[150px] w-fit h-fit text-sm py-2 px-3 text-blue bg-[#E4F2FE] font-medium tracking-widest uppercase text-center">
                          ⚡ Best Seller
                        </div>
                      )}
                    </div>

                    <FlexBox className={clsx("sm:gap-4")} justify="between">
                      <div>
                        {Boolean(p.program_discount_percentage) && (
                          <FlexBox align="center" className="gap-2">
                            <div className="text-[10px] text-white bg-red py-0.5 px-1 rounded h-fit">
                              {p.program_discount_percentage * 100}%
                            </div>
                            <div className="text-sm text-[rgba(58,53,65,0.54)] line-through">
                              {priceFormatter(p.program_original_price)}
                            </div>
                          </FlexBox>
                        )}

                        <div className={clsx("sm:text-2xl font-semibold text-xl", "text-green")}>
                          {priceFormatter(p.program_price)}
                        </div>
                      </div>
                      <RegularButton
                        variant={"solid"}
                        className="!rounded-none h-fit"
                        customPadding="py-2 sm:px-6 px-[22px]"
                        onClick={nextPageHandler(p)}
                        disabled={!p.can_order}
                      >
                        Daftar
                      </RegularButton>
                    </FlexBox>
                  </div>
                ))}
              </div>
            )}

            {requirement && (
              <div
                className={clsx(
                  "sm:py-9 sm:px-8 border border-[#dfe0e1] sm:mt-9",
                  "py-3 px-[14px] mb-4 mt-[18px]"
                )}
              >
                <FlexBox align="center" className="text-xl gap-2 mb-3">
                  <RiErrorWarningLine className="text-blue text-xl" />
                  <span className="sm:text-xl text-base">
                    Persyaratan Peserta <b>Get A Job</b> Program
                  </span>
                </FlexBox>
                <div
                  dangerouslySetInnerHTML={createMarkup(requirement)}
                  className="sm-only:text-xs markup"
                />
              </div>
            )}
          </Tab.Panel>

          {programs.length > 1 && (
            <Tab.Panel>
              {programs[1].school_programs.map((item, i) => (
                <div key={i} className={clsx("p-[26px] border border-[rgba(58,53,65,0.12)]")}>
                  <div
                    className={clsx(
                      "font-medium text-sm mb-3 uppercase tracking-widest text-green"
                    )}
                  >
                    cicilan
                  </div>

                  <div>
                    <Image
                      src={item.program_type_logo ?? HARISENIN_LOGO_COLORED}
                      alt={item.program_title}
                      width={500}
                      height={500}
                      className="sm:h-8 h-7 w-auto object-contain mb-3"
                    />
                  </div>
                  <div
                    dangerouslySetInnerHTML={createMarkup(item.program_description)}
                    className="sm-only:mb-3"
                  />

                  <FlexBox className="sm:justify-end">
                    <RegularButton
                      variant="solid"
                      textClassName="flex items-center gap-2"
                      className="!rounded-none"
                      customPadding="px-6 py-2"
                      onClick={nextPageHandler(item)}
                    >
                      <RiWhatsappFill /> Hubungi Kami
                    </RegularButton>
                  </FlexBox>
                </div>
              ))}
            </Tab.Panel>
          )}
          {/* <Tab.Panel></Tab.Panel> */}
        </Tab.Panels>
      </Tab.Group>

      <FlexBox
        className={clsx(
          "bg-[#F7F9FA] sm:py-7 sm:px-8 sm:mt-9 border border-[rgba(58,53,65,0.12)]",
          "sm-only:flex-col px-5 py-5 sm-only:gap-5 sm-only:mt-[18px] sm-only:mb-8"
        )}
        justify="between"
        align="center"
      >
        <div className="sm:text-lg font-medium">
          Ada yang mau ditanyakan lebih lanjut? Boleh banget chat tim kami!
        </div>
        <LinkButton
          href="https://api.whatsapp.com/send?phone=6281312117711"
          className="!rounded-none sm-only:w-full"
          customPadding="py-3 px-6"
        >
          Tanya Sekarang
        </LinkButton>
      </FlexBox>

      <LoginModal onHide={() => setLoginModal(false)} show={loginModal} />
    </div>
  )
}
