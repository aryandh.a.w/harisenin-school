<<<<<<< HEAD:src/modules/program/components/not-used/ProgramCompare.tsx
import React from "react"
import {
  PROGRAM_COMPARE_LIST,
  ProgramCompareList,
  ProgramCompareValue,
  ProgramCompareType,
} from "@constants/program-compare-list"
import { FaCheckCircle, FaTimesCircle } from "react-icons/fa"

import style from "../../styles/program.module.scss"

const WhyTableRow = ({ data }: { data: ProgramCompareList }) => {
  const { label, fullcourse, guarantee } = data

  const CellComponent = ({ value }: { value: ProgramCompareValue }) => {
    let className
    let component

    switch (value.type) {
      case "yes":
        className = `${style["program_page__why__table__cell"]} ${style["program_page__why__table__cell--yes"]}`
        component = <FaCheckCircle />
        break
      case "no":
        className = `${style["program_page__why__table__cell"]} ${style["program_page__why__table__cell--no"]}`
        component = <FaTimesCircle />
        break
      default:
        className = `${style["program_page__why__table__cell"]}`
        component = value.text
        break
    }

    return <div className={className}>{component}</div>
  }

  function tableCellFullCourse(type: ProgramCompareType) {
    if (type === "yes") {
      return (
        <div
          className={`${style["program_page__why__table__cell"]} ${style["program_page__why__table__cell--yes"]}`}
        >
          <FaCheckCircle />
        </div>
      )
    } else if (type === "no") {
      return (
        <div
          className={`${style["program_page__why__table__cell"]} ${style["program_page__why__table__cell--no"]}`}
        >
          <FaTimesCircle />
        </div>
      )
    } else {
      return (
        <div
          className={`${style["program_page__why__table__cell"]} ${style["program_page__why__table__cell--fullcourse"]}`}
        >
          {"1 - 3 Bulan"}
        </div>
      )
    }
  }

  return (
    <div className="d-lg-flex d-block">
      <div
        className={`${style["program_page__why__table__cell"]} ${style["program_page__why__table__cell--left"]}`}
      >
        {label}
      </div>

      <div className={style["program_page__why__table__content"]}>
        <div className="w-100 d-lg-none d-flex">
          <div
            className={`${style["program_page__why__table__cell"]} ${style["program_page__why__table__cell--fullcourse"]}`}
          >
            Full Course
          </div>
          <div
            className={`${style["program_page__why__table__cell"]} ${style["program_page__why__table__cell--guarantee"]}`}
          >
            Guarantee
          </div>
        </div>

        <div className="w-100 d-flex">
          {tableCellFullCourse(fullcourse.type)}

          <CellComponent value={guarantee} />
        </div>
      </div>
    </div>
  )
}

const ProgramCompare = () => {
  return (
    <div className={style["program_page__why"]}>
      <h2 className="text-center">Full Course VS Guarantee Program</h2>

      <table className={style["program_page__why__table"]}>
        <thead className={`${style["program_page__why__table__header"]} d-lg-flex d-none`}>
          <div
            className={`${style["program_page__why__table__cell--left"]} ${style["program_page__why__table__cell--grey"]}`}
          />
          <div
            className={`${style["program_page__why__table__header__cell"]} ${style["program_page__why__table__cell--fullcourse"]}`}
          >
            Full Course
          </div>
          <div
            className={`${style["program_page__why__table__header__cell"]} ${style["program_page__why__table__cell--guarantee"]}`}
          >
            Guarantee
          </div>
        </thead>

        {PROGRAM_COMPARE_LIST.map((value, index) => (
          <WhyTableRow data={value} key={index} />
        ))}
      </table>
    </div>
  )
}

export default ProgramCompare
=======
import React from "react"
import { Container } from "react-bootstrap"
import {
  PROGRAM_COMPARE_LIST,
  ProgramCompareList,
  ProgramCompareValue,
  ProgramCompareType,
} from "../../../../../constants/program-compare-list"
import { FaCheckCircle, FaTimesCircle } from "react-icons/fa"

import style from "../../styles/program.module.scss"

const WhyTableRow = ({ data }: { data: ProgramCompareList }) => {
  const { label, fullcourse, guarantee } = data

  const CellComponent = ({ value }: { value: ProgramCompareValue }) => {
    let className
    let component

    switch (value.type) {
      case "yes":
        className = `${style["program_page__why__table__cell"]} ${style["program_page__why__table__cell--yes"]}`
        component = <FaCheckCircle />
        break
      case "no":
        className = `${style["program_page__why__table__cell"]} ${style["program_page__why__table__cell--no"]}`
        component = <FaTimesCircle />
        break
      default:
        className = `${style["program_page__why__table__cell"]}`
        component = value.text
        break
    }

    return <div className={className}>{component}</div>
  }

  function tableCellFullCourse(type: ProgramCompareType) {
    if (type === "yes") {
      return (
        <div
          className={`${style["program_page__why__table__cell"]} ${style["program_page__why__table__cell--yes"]}`}
        >
          <FaCheckCircle />
        </div>
      )
    } else if (type === "no") {
      return (
        <div
          className={`${style["program_page__why__table__cell"]} ${style["program_page__why__table__cell--no"]}`}
        >
          <FaTimesCircle />
        </div>
      )
    } else {
      return (
        <div
          className={`${style["program_page__why__table__cell"]} ${style["program_page__why__table__cell--fullcourse"]}`}
        >
          {"1 - 3 Bulan"}
        </div>
      )
    }
  }

  return (
    <div className="d-lg-flex d-block">
      <div
        className={`${style["program_page__why__table__cell"]} ${style["program_page__why__table__cell--left"]}`}
      >
        {label}
      </div>

      <div className={style["program_page__why__table__content"]}>
        <div className="w-100 d-lg-none d-flex">
          <div
            className={`${style["program_page__why__table__cell"]} ${style["program_page__why__table__cell--fullcourse"]}`}
          >
            Full Course
          </div>
          <div
            className={`${style["program_page__why__table__cell"]} ${style["program_page__why__table__cell--guarantee"]}`}
          >
            Guarantee
          </div>
        </div>

        <div className="w-100 d-flex">
          {tableCellFullCourse(fullcourse.type)}

          <CellComponent value={guarantee} />
        </div>
      </div>
    </div>
  )
}

const ProgramCompare = () => {
  return (
    <Container className={style["program_page__why"]}>
      <h2 className="text-center">Full Course VS Guarantee Program</h2>

      <table className={style["program_page__why__table"]}>
        <thead className={`${style["program_page__why__table__header"]} d-lg-flex d-none`}>
          <div
            className={`${style["program_page__why__table__cell--left"]} ${style["program_page__why__table__cell--grey"]}`}
          />
          <div
            className={`${style["program_page__why__table__header__cell"]} ${style["program_page__why__table__cell--fullcourse"]}`}
          >
            Full Course
          </div>
          <div
            className={`${style["program_page__why__table__header__cell"]} ${style["program_page__why__table__cell--guarantee"]}`}
          >
            Guarantee
          </div>
        </thead>

        {PROGRAM_COMPARE_LIST.map((value, index) => (
          <WhyTableRow data={value} key={index} />
        ))}
      </table>
    </Container>
  )
}

export default ProgramCompare
>>>>>>> 94ccec2 (setup fix):src/ui/components/program/components/not-used/ProgramCompare.tsx
