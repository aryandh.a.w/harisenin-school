import React from "react"

import { SchoolTools } from "@interfaces/school"
import { NextImage } from "@components/misc"

import clsx from "clsx"
import { ProgramHeading } from "./ProgramHeading"
import { FlexBox } from "@components/wrapper"

export const ProgramTools = ({
  screenWidth,
  schoolTools,
}: {
  screenWidth: number
  schoolTools: SchoolTools[]
}) => {
  if (!screenWidth || !schoolTools.length) {
    return <></>
  }

  return (
    <div className={clsx("relative sm:mb-10", "mb-8")} id="tools1">
      <ProgramHeading id="tools" title="Tools" subTitle="Tools yang akan Kamu Gunakan di Kelas" />

      <div
        className={clsx(
          screenWidth < 576 && "slider-mobile -mx-[18px] w-[calc(100%+32px)] px-[18px]"
        )}
      >
        {" "}
        <div
          className={clsx(
            screenWidth > 576 && "grid sm:grid-cols-4 sm:gap-8",
            screenWidth < 576 && "flex w-max gap-4"
          )}
        >
          {schoolTools.map((value, index) => (
            <FlexBox
              key={index}
              className={clsx(
                "sm:w-full border border-[rgba(58,53,65,0.12)] rounded",
                "max-w-[160px] sm:py-6 py-2.5 px-[18px]"
              )}
              justify="center"
              align="center"
              title={value.tool_title}
            >
              <NextImage
                className="object-contain sm:max-w-[80%] sm-only:h-[50px]"
                src={value.tool_path}
                alt={value.tool_title}
                size={[1000, 1000]}
                title={value.tool_title}
              />
            </FlexBox>
          ))}
        </div>
      </div>
    </div>
  )
}
