import { FlexBox } from "@components/wrapper"
import { SchoolDetail } from "@interfaces/school"
import { programTypeHandler } from "@lib/functions"
import clsx from "clsx"
import { useEffect, useState } from "react"

interface ProgramNavigatorProps {
  screenWidth: number
  screenHeight: number
  schoolDetail: SchoolDetail
}

interface NavInfo {
  link: string
  label: string
}

export function ProgramNavigator({ schoolDetail, screenWidth }: ProgramNavigatorProps) {
  const [linkActive, setLinkActive] = useState("")
  const [navigationLinks, setNavigationLinks] = useState<NavInfo[]>([])

  const NAVIGATION_LIST = [
    {
      label: "Benefit",
      link: "benefit",
      detail: "school_rewards",
      id: "btn-benefit",
    },
    {
      label: "Kurikulum",
      link: "curiculum",
      detail: "school_courses",
      id: "btn-curriculum",
    },
    {
      label: "Tools",
      link: "tools",
      detail: "school_tools",
      id: "btn-tools",
    },
    {
      label: "Tutor",
      link: "tutor",
      detail: "school_tutors",
      id: "btn-tutor",
    },
    {
      label: "Learning Experience",
      link: "experience",
      detail: "school_learning_experiences",
      id: "btn-experience",
    },
    {
      label: "Metode Belajar",
      link: "method",
      detail: "",
      id: "btn-method",
    },
    {
      label: `Jadwal ${programTypeHandler(schoolDetail.school_type)}`,
      link: "schedule",
      detail: "school_batch",
      id: "btn-schedule",
    },
    {
      label: "Biaya & Pendaftaran",
      link: "fee",
      detail: "school_program_type",
      id: "btn-price",
    },
    {
      label: "Alur Pendaftaran",
      link: "admission",
      detail: "school_admission",
      id: "btn-admission",
    },
    {
      label: "Testimoni",
      link: "testimonial",
      detail: "school_testimonies",
      id: "btn-testimonial",
    },
    {
      label: "Portofolio Alumni",
      link: "portfolio",
      detail: "school_portfolio",
      id: "btn-portfolio",
    },
    {
      label: "FAQ",
      link: "faq",
      detail: "school_faqs",
      id: "btn-faq-hms",
    },
  ]

  useEffect(() => {
    if (schoolDetail) {
      const links: NavInfo[] = []

      NAVIGATION_LIST.forEach((value) => {
        if (value.id === "btn-admission") {
          links.push({
            label: value.label,
            link: value.link,
          })
        }

        if (value.id === "btn-method" && schoolDetail.school_type === "BOOTCAMP") {
          links.push({
            label: value.label,
            link: value.link,
          })
        }

        if (
          value.detail === "school_learning_experiences" &&
          schoolDetail.school_type === "PRO_CLASS"
        ) {
          links.push({
            label: value.label,
            link: value.link,
          })
        }

        if (schoolDetail[value.detail] && schoolDetail[value.detail].length) {
          links.push({
            label: value.label,
            link: value.link,
          })
        }
      })

      setNavigationLinks(links)
    }
  }, [schoolDetail])

  useEffect(() => {
    let HTMLs: any[] = []

    if (navigationLinks.length) {
      HTMLs = navigationLinks.map((value) => {
        return document.getElementById(`${value.link}1`)
      })

      window.addEventListener("scroll", trackScrolling(HTMLs), { passive: true })
    }

    return () => {
      window.removeEventListener("scroll", trackScrolling(HTMLs))
    }
  }, [navigationLinks])

  const trackScrolling = (elements: HTMLElement[]) => () => {
    if (!elements.length) {
      return
    }

    const eleLength = elements.length
    let index = 0

    const getAll = elements.map((value) => {
      return {
        height: value?.getBoundingClientRect()?.height,
        top: value?.getBoundingClientRect()?.top,
      }
    })

    while (index < eleLength) {
      if (getAll[index].top < 110) {
        setLinkActive(navigationLinks[index].link)
      }
      index++
    }
  }

  const executeScroll = (id: string) => () => {
    const ele = document.getElementById(id)

    if (!ele) {
      return
    }

    ele.scrollIntoView({ behavior: "smooth" })
  }

  const LinkList = ({ label, link }: { label: string; link: string }) => {
    if (screenWidth > 576) {
      return (
        <li
          onClick={executeScroll(link)}
          className={clsx(
            linkActive === link &&
              "font-semibold border-l-4 border-l-blue bg-[rgba(58,53,65,0.12)]",
            "text-md cursor-pointer",
            "px-[26px] py-4 border-b border-b-[rgba(58,53,65,0.12)]"
          )}
        >
          {label}
        </li>
      )
    } else {
      return (
        <div
          onClick={executeScroll(link)}
          className={clsx(
            "py-3 px-5 font-medium border-b-2",
            linkActive === link ? "border-green text-green" : "border-[rgba(58,53,65,0.12)]"
          )}
        >
          {label}
        </div>
      )
    }
  }

  if (screenWidth > 576) {
    return (
      <div
        className={clsx(
          "sticky self-start top-24 bg-white w-1/5 border border-[rgba(58,53,65,0.12)]",
          "sm-only:hidden"
        )}
      >
        <ul className={clsx("list-none pl-0 mb-0")}>
          {navigationLinks.map((value) => (
            <LinkList link={value.link} label={value.label} key={value.link} />
          ))}
        </ul>
        {/* <RegularButton
        variant="solid"
        color="green"
        className={clsx("w-full")}
        onClick={executeScroll("#fee")}
        id="btn-regist-class"
      >
        {" "}
        Daftar Kelas
      </RegularButton>
      <ProgramShare screenWidth={screenWidth} slug={schoolDetail.product_slug} /> */}
      </div>
    )
  } else {
    return (
      <div className="slider-mobile top-[46px] sticky h-fit w-[calc(100%+32px)] bg-white z-10 ml-[-18px]">
        <FlexBox className="w-max">
          {navigationLinks.map((value) => (
            <LinkList link={value.link} label={value.label} key={value.link} />
          ))}
        </FlexBox>
      </div>
    )
  }
}
