import { SliderArrow } from "@components/slider"
import VideoPlayer from "@components/video/VideoPlayer"
import { FlexBox } from "@components/wrapper"
import { flex_center } from "@constants/styles"
import { SchoolVideoPreview } from "@interfaces/school"
import clsx from "clsx"
import Image from "next/image"
import { IoPlay } from "react-icons/io5"
import Carousel, { ButtonGroupProps, DotProps } from "react-multi-carousel"
import { ProgramHeading } from "./ProgramHeading"

// const SLIDER_ARROW =
//   "absolute top-[42%] flex items-center justify-center bg-white rounded-full shadow-[0_2px_5px_-0px_rgba(0,0,0,0.3)] absolute "

// const CustomLeftArrow = (props: any) => {
//   return (
//     <button className={clsx("left-0", SLIDER_ARROW)} onClick={props.onClick}>
//       <FaChevronLeft className={clsx("text-[18px] text-black", "text-[18px]")} />
//     </button>
//   )
// }

// const CustomRightArrow = (props: any) => {
//   return (
//     <button className={clsx("right-0", SLIDER_ARROW)} onClick={props.onClick}>
//       <FaChevronRight className={clsx("text-[18px] text-black", "text-[18px]")} />
//     </button>
//   )
// }

const CustomDot = ({ active, onClick }: DotProps) => {
  return (
    <button
      className={clsx(
        "wh-3 rounded-full mx-1.5 border-0 transition-all duration-200",
        active ? "bg-blue" : "bg-grey-c4 w-auto rounded-0"
      )}
      onClick={(e) => {
        if (onClick) {
          onClick()
        }
        e.preventDefault()
      }}
    />
  )
}

const ProgramVideoPreview = ({
  data,
  screenWidth,
}: {
  data: SchoolVideoPreview[]
  screenWidth: number
}) => {
  const responsive = {
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 1,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 1,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
    },
  }

  const dotResponsive = {
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 4,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 4,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 4,
    },
  }

  const SliderButton = ({ goToSlide, carouselState }: ButtonGroupProps) => {
    return (
      <div className="relative">
        <Carousel
          responsive={dotResponsive}
          ssr={true} // means to render carousel on server-side.
          itemClass="sm-only:mb-5"
          containerClass={clsx("sm:-ml-9")}
          arrows={false}
          {...(screenWidth > 576 && {
            customButtonGroup: (
              <SliderArrow
                length={data.length}
                displayedSlide={4}
                leftClassName="w-[44px] h-[44px] -left-[16px] top-[35%]"
                rightClassName="w-[44px] h-[44px] -right-[18px] top-[35%]"
              />
            ),
            renderButtonGroupOutside: true,
          })}
          className=""
        >
          {data.map((value, i) => (
            <div key={i} className={clsx("pl-9 cursor-pointer")}>
              <div
                className={clsx(
                  "relative ",
                  carouselState.currentSlide === i && "border border-green"
                )}
                onClick={() => goToSlide(i)}
              >
                <FlexBox className="wh-full absolute top-0 " align="center" justify="center">
                  <FlexBox
                    align="center"
                    justify="center"
                    className="rounded-full w-[42px] h-[42px]"
                    style={{
                      background: "rgba(217, 217, 217, 0.9)",
                      backgroundBlendMode: "soft-light",
                      backdropFilter: "blur(3.55036px)",
                    }}
                  >
                    <div className={clsx("rounded-full bg-white w-[31px] h-[31px]", flex_center)}>
                      <IoPlay className="sm:text-xl" />
                    </div>
                  </FlexBox>
                </FlexBox>
                <Image
                  src={value.video_thumbnail}
                  width={400}
                  height={212}
                  alt={value.video_title}
                  className="w-full sm:h-[8.2vw]"
                />
              </div>
            </div>
          ))}
        </Carousel>
      </div>
    )
  }

  if (!data.length) {
    return <></>
  }

  return (
    <div className={clsx("relative sm:mb-15", "mb-8")} id="video-preview1">
      <ProgramHeading
        id="video-preview"
        title="cuplikan"
        subTitle="Mau Tau Serunya Belajar di Harisenin.com? Jawabannya Ada di Video Ini!"
      />

      <div className="relative">
        <Carousel
          responsive={responsive}
          ssr={true} // means to render carousel on server-side.
          itemClass="sm-only:mb-5"
          arrows={false}
          {...(screenWidth > 576 && {
            customButtonGroup: <SliderButton />,
            renderButtonGroupOutside: true,
          })}
          {...(screenWidth < 576 && {
            showDots: true,
            renderDotsOutside: true,
            customDot: <CustomDot />,
            dotListClass: "relative",
            // autoPlay: true,
            infinite: true,
            // autoPlaySpeed: 3000,
          })}
          className="d-lg-flex d-none sm:mb-9"
        >
          {data.map((value, i) => (
            <VideoPlayer
              key={i}
              url={value.video_link}
              videoTitle="Introduction Video"
              className="w-full h-auto sm-only:mb-[18px]"
              thumbnail={value.video_thumbnail}
            />
          ))}
        </Carousel>
      </div>
    </div>
  )
}

export default ProgramVideoPreview
