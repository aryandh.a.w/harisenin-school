import { FAQ, SchoolType } from "@interfaces/school"
import { useEffect, useState } from "react"

import { Disclosure, Transition } from "@headlessui/react"
import clsx from "clsx"
import { FaChevronDown } from "react-icons/fa"
import { ProgramHeading } from "./ProgramHeading"

export const ProgramFaqContainer = ({
  faqs,
  schoolType,
}: {
  faqs: FAQ[]
  schoolType: SchoolType
}) => {
  const [faqData, setFaqData] = useState([])

  useEffect(() => {
    if (faqs && faqs.length) {
      setFaqData(faqs)
    } else {
      setFaqData([])
    }
  }, [faqs])

  return (
    <div className="sm:relative" id="faq1">
      <ProgramHeading
        id="faq"
        title="FAQ"
        subTitle={`Pertanyaan Seputar ${schoolType === "PRO_CLASS" ? "ProClass" : "Bootcamp"}`}
      />

      <div className="flex flex-col gap-4">
        {faqData.map((value, index) => (
          <Disclosure key={index}>
            {({ open }) => (
              <div className="border border-[rgba(58,53,65,0.12)]">
                <Disclosure.Button
                  className={clsx(
                    "flex justify-between bg-[#f7f9fa] p-4 font-medium sm:text-base text-left w-full",
                    "text-sm"
                  )}
                >
                  {value.faq_question}
                  <FaChevronDown className={clsx(open && "rotate-180 transform text-green")} />
                </Disclosure.Button>
                <Transition
                  show={open}
                  enter="transition duration-100 ease-out"
                  enterFrom="transform scale-95 opacity-0"
                  enterTo="transform scale-100 opacity-100"
                  leave="transition duration-75 ease-out"
                  leaveFrom="transform scale-100 opacity-100"
                  leaveTo="transform scale-95 opacity-0"
                >
                  <Disclosure.Panel static>
                    <div
                      className={clsx(
                        "bg-[#f7f9fa] px-4 py-2 sm:text-base text-light-grey",
                        "text-sm"
                      )}
                    >
                      {value.faq_answer}
                    </div>
                  </Disclosure.Panel>
                </Transition>
              </div>
            )}
          </Disclosure>
        ))}
      </div>
    </div>
  )
}
