import { LinkButton } from "@components/buttons"
import clsx from "clsx"
import { FaWhatsapp } from "react-icons/fa"

export function ProgramISA() {
  return (
    <div className={clsx("rounded p-4 mt-4")} style={{ boxShadow: "0 10px 20px 0 #9aaacf40" }}>
      <div
        className={clsx(
          "text-lg mb-3 sm:font-semibold",
          "sm-only:w-4/5 sm-only:text-center sm-only:mt-0 sm-only:mx-auto sm-only:mb-2 font-normal"
        )}
      >
        {" "}
        Program ISA
      </div>
      <div className={clsx("text-left")}>
        Nikmati cicilan 0% tanpa bunga dan tanpa denda dengan cicilan 2x dari harisenin.com
        (Rp3.499.500 x 2 cicilan) dengan menghubungi tim admission kami.
      </div>
      <LinkButton
        type="solid"
        color="green"
        href="https://api.whatsapp.com/send?phone=6281312117711&text=Saya%20Mau%20ikut%20Harisenin%20Millennial%20School%20di%20harisenin.com%20min"
        isExternal
        className="w-full mt-2"
      >
        <FaWhatsapp className="text-white mr-2" /> Hubungi Kami
      </LinkButton>
    </div>
  )
}
