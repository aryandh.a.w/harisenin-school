import React from "react"
import { HARISENIN_PUBLIC_ICON } from "@constants/pictures"
import { NextImage } from "@components/misc"
import clsx from "clsx"
import { ProgramHeading } from "./ProgramHeading"
import { FlexBox } from "@components/wrapper"

const LIST = [
  {
    id: 1,
    icon: "icon_register-4.png",
    title: "Pilih Program",
    description: "Pilih program dan jenis investasi belajar sesuai dengan yang kamu mau.",
  },
  {
    id: 2,
    icon: "icon_register-5.png",
    title: "Isi Form Pendaftaran",
    description:
      "Lengkapi form pendaftaran dalam 5-10 menit, agar kami dapat mengetahui data diri dan tujuan kariermu.",
  },
  {
    id: 3,
    icon: "icon_register-6.png",
    title: "Lakukan Pembayaran",
    description:
      "Pilih opsi pembayaran. Setelah melunasi pembayaran, kamu sudah bisa belajar di harisenin.com.",
  },
]

function ProgramAdmissionFlow() {
  return (
    <div className={clsx("relative sm:mb-15", "mb-8")} id="admission1">
      <ProgramHeading
        id="admission"
        title="Alur Pendaftaran"
        subTitle="Tanpa Test. Harisenin.com Percaya Semua Berhak Belajar dan Mewujudkan Mimpinya"
      />

      <div className={clsx("w-full relative", "")}>
        <div className={clsx("grid sm:grid-cols-3 sm:gap-9 gap-[18px]")}>
          {LIST.map((value, index) => (
            <div
              key={index}
              className={clsx("border border-[rgba(58,53,65,0.12)] bg-white z-[5]", "p-6")}
            >
              <FlexBox className={clsx("p-3 bg-[#F7F9FA] w-fit")} justify="center" align="center">
                <NextImage
                  src={`${HARISENIN_PUBLIC_ICON}/${value.icon}`}
                  alt={value.title}
                  size={[200, 200]}
                  className={clsx("wh-9")}
                />
              </FlexBox>

              <div className="mt-4">
                <div className={clsx("sm:mb-1.5 font-semibold", "mb-2")}>{value.title}</div>
                <div className={clsx("w-11/12 text-sm")}>{value.description}</div>
              </div>
            </div>
          ))}
        </div>
        <div
          className={clsx(
            "bg-[rgba(0,59,222,0.08)] absolute",
            "sm:h-2 sm:w-full sm:top-[55px]",
            "w-2 h-full top-0 sm-only:left-[50px]"
          )}
        />
      </div>
    </div>
  )
}

export default ProgramAdmissionFlow
