export const STUDY_METHOD_PROCLASS = [
  {
    icon: "class_detail-mobile.png",
    title: "Final Project and Portfolio",
    description:
      "Kamu akan menyelesaikan final project yang dapat digunakan sebagai portfolio sebagai dokumen penunjang saat melamar kerja",
  },
  {
    icon: "icon_method-6.png",
    title: "Live Class via Zoom",
    description:
      "Kamu dapat belajar di mana saja karena setiap sesi dilaksanakan lewat Zoom dengan jadwal yang telah ditentukan",
  },
  {
    icon: "icon_method-5.png",
    title: "Transcript and Certification",
    description: "Kamu akan menerima transkrip nilai dan sertifikat setelah menyelesaikan program",
  },
  {
    icon: "icon_method-7.png",
    title: "Q&A Session",
    description: "Kamu dapat mengikuti sesi tanya jawab secara live dengan mentor di kelas",
  },
]

export const STUDY_METHOD_BOOTCAMP = [
  {
    icon: "icon_method-1.png",
    title: "Team Buddy",
    description: "Kamu bisa bertanya dan dibimbing oleh Team Buddy saat di dalam dan luar kelas",
  },
  {
    icon: "icon_method-2.png",
    title: "Career Support",
    description:
      "Kamu akan mendapatkan akses terhadap konsultasi karier seperti pembuatan CV hingga persiapan interview",
  },
  {
    icon: "icon_method-3.png",
    title: "Final Project and Portfolio",
    description:
      "Kamu akan menyelesaikan final project yang dapat digunakan sebagai portfolio sebagai dokumen penunjang saat melamar kerja",
  },
  {
    icon: "icon_method-4.png",
    title: "Special Session",
    description:
      "Kamu akan mendapatkan sesi pemahaman mental health dan financial freedom yang dilaksanakan melalui Zoom sesuai dengan jadwal yang ditentukan",
  },
  {
    icon: "icon_method-5.png",
    title: "Transcript and Certification",
    description: "Kamu akan menerima transkrip nilai dan sertifikat setelah menyelesaikan program",
  },
  {
    icon: "icon_method-6.png",
    title: "Live Class via Zoom",
    description:
      "Kamu dapat belajar di mana saja karena setiap sesi dilaksanakan lewat Zoom dengan jadwal yang telah ditentukan",
  },
]
