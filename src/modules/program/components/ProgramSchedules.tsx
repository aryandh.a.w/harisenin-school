import { TextButton } from "@components/buttons"
import { FlexBox } from "@components/wrapper"
import { HARISENIN_PUBLIC_ICON } from "@constants/pictures"
import { SchoolBatch, SchoolType } from "@interfaces/school"
import { dateFormatter } from "@lib/functions"
import clsx from "clsx"
import Image from "next/image"

interface ProgramSchedulesProps {
  data?: SchoolBatch[]
  schoolType: SchoolType
  scroll: (link: string) => () => void
}

export default function ProgramSchedules({ data, scroll, schoolType }: ProgramSchedulesProps) {
  if (!data || !data.length) {
    return <></>
  }

  const ScheduleColumn = ({ index }: { index: number }) => {
    if ((index === 2 && data.length < 3) || (index === 1 && data.length < 2)) {
      return <div className="sm:w-1/5" />
    }

    return (
      <div className={clsx("flex flex-col")}>
        <div className="sm:text-lg mb-1.5">
          {dateFormatter(data[index].open_registration, "DD MMM")} -{" "}
          {dateFormatter(data[index].close_registration, "DD MMM YY")}
        </div>
        <div className="text-black-48 sm-only:text-sm">
          Batch {data[index].batch}{" "}
          <TextButton className="text-blue underline" onClick={scroll("fee")}>
            Daftar
          </TextButton>
        </div>
      </div>
    )
  }

  return (
    <div
      className={clsx("border border-[rgba(58,53,65,0.12)] sm:p-8 sm:mb-[60px]", "py-4 px-6 mb-8")}
    >
      <FlexBox className="gap-2 sm:mb-6 mb-[18px]">
        <Image
          src={`${HARISENIN_PUBLIC_ICON}/icon_calendar.png`}
          width={24}
          height={24}
          alt="calendar"
          className="sm-only:wh-5 sm-only:mt-1"
        />
        <div className="font-medium sm:text-xl text-lg">
          Jadwal {schoolType === "BOOTCAMP" ? "Bootcamp" : "ProClass"} Terdekat!
        </div>
      </FlexBox>

      <div className={clsx("flex sm:mb-6 justify-between", "sm-only:flex-col gap-4 mb-[18px]")}>
        <ScheduleColumn index={0} />
        <div className={clsx("h-auto w-[1px]", data.length > 1 && "bg-[rgba(58,53,65,0.12)]")} />
        <ScheduleColumn index={1} />
        <div className={clsx("h-auto w-[1px]", data.length > 2 && "bg-[rgba(58,53,65,0.12)]")} />
        <ScheduleColumn index={2} />
      </div>

      <div className="underline-animation">
        <TextButton
          className={clsx("text-blue font-semibold sm-only:text-sm")}
          onClick={scroll("schedule")}
        >
          Lihat Semua Jadwal →{" "}
        </TextButton>
      </div>
    </div>
  )
}
