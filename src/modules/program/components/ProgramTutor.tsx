import { FC } from "react"
import { ProgramHeading } from "./ProgramHeading"

import TutorSlider, { TutorSliderProps } from "./TutorSlider"

export const ProgramTutor: FC<TutorSliderProps> = ({ screenWidth, tutors }) => {
  if (!tutors || !tutors.length) {
    return <></>
  }

  return (
    <div className="relative sm:mb-15 mb-8" id="tutor1">
      <ProgramHeading
        id="tutor"
        title="Tutor"
        subTitle="Belajar Bareng Tutor yang Ahli di Bidangnya"
      />
      <TutorSlider screenWidth={screenWidth} tutors={tutors} />
    </div>
  )
}
