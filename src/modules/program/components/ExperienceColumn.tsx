import { Tab } from "@headlessui/react"
import { SchoolLearningExperience } from "@interfaces/school"
import clsx from "clsx"
import { useEffect, useState } from "react"

export const ExperienceWebColumn = ({
  experiences,
}: {
  experiences: SchoolLearningExperience[]
}) => {
  const [selectedIndex, setSelectedIndex] = useState(0)

  useEffect(() => {
    const timer1 = setTimeout(() => {
      if (selectedIndex === 5) {
        setSelectedIndex(0)
      } else {
        setSelectedIndex(selectedIndex + 1)
      }
    }, 5000)

    return () => {
      clearTimeout(timer1)
    }
  }, [selectedIndex])

  return (
    <>
      <div className="flex relative gap-[40px]">
        <div className="uppercase tracking-widest arrow_ribbon arrow_ribbon--green">
          Rise Learn & Play
        </div>
        <div className="uppercase tracking-widest arrow_ribbon arrow_ribbon--blue">Riseup+</div>
      </div>

      <Tab.Group selectedIndex={selectedIndex}>
        <Tab.List className="grid grid-cols-6 py-1 gap-4 mt-3">
          {experiences.map((item, i) => (
            <Tab
              key={i}
              onClick={() => setSelectedIndex(i)}
              className={({ selected }) =>
                clsx(
                  "border sm:p-5 text-left flex items-start flex-col",
                  !selected && "border-[rgba(58,53,65,0.12)]",
                  selected && "bg-[#F7F9FA]",
                  selected && i < 4 && "border-green",
                  selected && i >= 4 && "border-blue"
                )
              }
            >
              <div
                className={clsx(
                  "uppercase text-sm font-medium tracking-widest mb-1.5",
                  i < 4 ? "text-green" : "text-blue"
                )}
              >
                Week {item.learning_name}
              </div>
              <div>{item.learning_title}</div>
            </Tab>
          ))}
        </Tab.List>

        <Tab.Panels className="mt-2">
          {experiences.map((item, idx) => (
            <Tab.Panel key={idx} className={clsx("sm:p-6 border border-[rgba(58,53,65,0.12)]")}>
              <div
                className={clsx(
                  "uppercase text-sm font-medium tracking-widest mb-1.5",
                  idx < 4 ? "text-green" : "text-blue"
                )}
              >
                {item.learning_title}
              </div>
              <div>{item.learning_description}</div>
            </Tab.Panel>
          ))}
        </Tab.Panels>
      </Tab.Group>
    </>
  )
}

export const ExperienceMobileColumn = ({
  experiences,
}: {
  experiences: SchoolLearningExperience[]
}) => {
  return (
    <>
      <div className="mb-3">
        <div className="uppercase tracking-widest arrow_ribbon arrow_ribbon--green">
          Rise Learn & Play
        </div>

        {/* <div className="uppercase tracking-widest relative">
          <div className="text-white bg-green py-3 pl-6">Rise Learn & Play</div>
          <div className="">&#129170;</div>
        </div> */}

        <Tab.Group>
          <div className="slider-mobile !mb-3 w-[calc(100%+32px)] sm-only:-mx-4 sm-only:pl-4">
            <Tab.List className="flex p-1 gap-3 mt-3 w-max">
              {[...experiences].splice(0, 4).map((item, i) => (
                <Tab
                  key={i}
                  className={({ selected }) =>
                    clsx(
                      "border p-4 text-left flex items-start flex-col w-[40vw]",
                      !selected && "border-[rgba(58,53,65,0.12)]",
                      selected && "bg-[#F7F9FA] border-green"
                    )
                  }
                >
                  <div
                    className={clsx(
                      "uppercase text-sm font-medium tracking-widest mb-1.5 text-green"
                    )}
                  >
                    Week {item.learning_name}
                  </div>
                  <div>{item.learning_title}</div>
                </Tab>
              ))}
            </Tab.List>
          </div>

          <Tab.Panels className="mt-2">
            {[...experiences].splice(0, 4).map((item, idx) => (
              <Tab.Panel
                key={idx}
                className={clsx("py-6 px-[18px] border border-[rgba(58,53,65,0.12)]")}
              >
                <div
                  className={clsx(
                    "uppercase text-sm font-medium tracking-widest mb-1.5",
                    idx < 4 ? "text-green" : "text-blue"
                  )}
                >
                  {item.learning_title}
                </div>
                <div className="text-sm leading-6">{item.learning_description}</div>
              </Tab.Panel>
            ))}
          </Tab.Panels>
        </Tab.Group>
      </div>

      <div className="uppercase tracking-widest arrow_ribbon arrow_ribbon--blue">Riseup+</div>

      <Tab.Group>
        <Tab.List className="grid grid-cols-2 p-1 gap-3 mt-3">
          {[...experiences].splice(4, 5).map((item, i) => (
            <Tab
              key={i}
              className={({ selected }) =>
                clsx(
                  "border p-4 text-left flex items-start flex-col",
                  !selected && "border-[rgba(58,53,65,0.12)]",
                  selected && "bg-[#F7F9FA] border-blue"
                )
              }
            >
              <div
                className={clsx("uppercase text-sm font-medium tracking-widest mb-1.5 text-blue")}
              >
                Week {item.learning_name}
              </div>
              <div>{item.learning_title}</div>
            </Tab>
          ))}
        </Tab.List>

        <Tab.Panels className="mt-2">
          {[...experiences].splice(4, 5).map((item, idx) => (
            <Tab.Panel
              key={idx}
              className={clsx("py-6 px-[18px] border border-[rgba(58,53,65,0.12)]")}
            >
              <div
                className={clsx("uppercase text-sm font-medium tracking-widest mb-1.5 text-blue")}
              >
                {item.learning_title}
              </div>
              <div className="text-sm leading-6">{item.learning_description}</div>
            </Tab.Panel>
          ))}
        </Tab.Panels>
      </Tab.Group>
    </>
  )
}
