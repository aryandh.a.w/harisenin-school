import { SchoolLearningExperience, SchoolType } from "@interfaces/school"
import clsx from "clsx"
import { ExperienceMobileColumn, ExperienceWebColumn } from "./ExperienceColumn"
import { ProgramHeading } from "./ProgramHeading"

const DESCRIPTION = [
  {
    title: "Rise Learn & Play",
    description:
      "Kuasai technical dan soft skill dalam 3–5 bulan secara intensif. Belajar secara kelompok? tugas praktik? atau final project? Semua ada di sini. Yang gak kalah penting, bakal dibimbing buat bikin portofolio yang ngebantu buat dapet kerja.",
  },
  {
    title: "RiseUp+",
    description:
      "Selain yang kamu dapatkan di Full Course, di program ini kamu mendapat pelatihan dan career consultation 1-on-1, dibantu untuk job-connect dengan hiring partner harisenin.com, sampai kesempatan apprenticeship.",
  },
]

const DescriptionColumn = ({
  description,
  title,
  index,
}: {
  title: string
  description: string
  index: number
}) => {
  return (
    <div className={clsx("border border-[rgba(58,53,65,0.12)]", "sm:p-6 py-4 px-[18px]")}>
      <div className="font-semibold mb-1.5">
        {title} {index === 1 && <span className="text-red">*</span>}
      </div>
      <div className="text-black-48 text-sm">{description}</div>
      {index === 1 && (
        <div className="text-black-48 text-sm mt-1.5">
          <span className="text-red">*</span> khusus Get A Job Program
        </div>
      )}
    </div>
  )
}

export function ProgramExperience({
  experiences,
  screenWidth,
  schoolType,
}: {
  screenWidth: number
  experiences: SchoolLearningExperience[]
  schoolType: SchoolType
}) {
  if (schoolType !== "BOOTCAMP") {
    return (
      <div className={clsx("relative mb-15")} id="experience1">
        <ProgramHeading
          id="experience"
          title="Learning Experience"
          subTitle="Gimana Sistem Belajar di ProClass Harisenin.com?"
        />

        <div className={clsx("border border-[rgba(58,53,65,0.12)]", "sm:p-6 py-4 px-[18px]")}>
          <div className="font-semibold mb-1.5">Rise Learn & Play</div>
          <div className="text-black-48 text-sm">
            Kuasai technical dan soft skill yang kamu inginkan dalam 1–2 bulan secara intensif. Yang
            gak kalah penting, kamu bakal dibimbing selama prosesnya dan juga mendapatkan career
            preparation.
          </div>
        </div>
      </div>
    )
  }

  if (!experiences || !experiences.length) {
    return <></>
  }

  return (
    <div className={clsx("relative mb-15")} id="experience1">
      <ProgramHeading
        id="experience"
        title="Learning Experience"
        subTitle="Gimana Sistem Belajar di Bootcamp Harisenin.com?"
      />

      <div className={clsx("grid sm:grid-cols-2 sm:gap-9 sm:mb-9", "gap-[18px] mb-8")}>
        {DESCRIPTION.map((item, i) => (
          <DescriptionColumn {...item} index={i} key={i} />
        ))}
      </div>

      {screenWidth > 576 && <ExperienceWebColumn experiences={experiences} />}

      {screenWidth < 576 && <ExperienceMobileColumn experiences={experiences} />}
    </div>
  )
}
