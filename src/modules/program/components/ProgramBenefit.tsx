import { NextImage } from "@components/misc"
import { FlexBox } from "@components/wrapper"
import { SchoolDetail } from "@interfaces/school"

import clsx from "clsx"
import { ProgramHeading } from "./ProgramHeading"

export const ProgramBenefit = ({ detail }: { detail: SchoolDetail; screenWidth: number }) => {
  if (!detail?.school_rewards || !detail?.school_rewards.length) {
    return <></>
  }

  return (
    <div className={clsx("relative sm:mb-15", "mb-8")} id="benefit1">
      {/* {screenWidth < 576 && <ProgramShare screenWidth={screenWidth} slug={detail.product_slug} />} */}

      <ProgramHeading id="benefit" title="Benefit" subTitle="Yang Bakal Kamu Dapetin Itu..." />

      <div className={clsx("grid sm:grid-cols-2 sm:gap-8", "gap-4")}>
        {/* Reward point component */}
        {detail.school_rewards.map((value, index) => (
          // no margin = last component in row
          <FlexBox
            key={index}
            align="center"
            className={clsx(
              "border border-[rgba(58,53,65,0.12)] sm:py-[26px] sm:px-6",
              "benefit-animation",
              "py-4 px-[18px] sm-only:gap-4"
            )}
          >
            {/* Reward icon */}
            <div
              className={clsx(
                "sm:flex sm:align-center sm:mr-5 rounded",
                "block benefit-animation__icon"
              )}
            >
              <NextImage
                size={[50, 50]}
                src={value.reward_icon}
                alt={value.reward_title}
                className="sm-only:wh-2"
              />
            </div>

            {/* Reward text */}
            <div className={clsx("sm:w-[70%]", "w-4/5")}>
              <div
                className={clsx("font-semibold mb-1.5 benefit-animation__title sm-only:text-sm")}
              >
                {value.reward_title}
              </div>
              <div className={clsx("sm:text-sm text-xs text-black-48")}>{value.reward_content}</div>
            </div>
          </FlexBox>
        ))}
      </div>
    </div>
  )
}
