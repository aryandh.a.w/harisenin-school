import { StudentTestimonial } from "@interfaces/school"
import { FC } from "react"
import Carousel, { DotProps } from "react-multi-carousel"

import { SliderArrow } from "@components/slider"
import { FlexBox } from "@components/wrapper"
import clsx from "clsx"
import Image from "next/image"
import { ProgramHeading } from "./ProgramHeading"
import { HARISENIN_PLACEHOLDER } from "@constants/pictures"

const CustomDot = ({ active, onClick }: DotProps) => {
  return (
    <button
      className={clsx(
        "wh-3 rounded-full mx-1.5 border-0 transition-all duration-200",
        active ? "bg-blue" : "bg-grey-c4 w-auto rounded-0"
      )}
      onClick={(e) => {
        if (onClick) {
          onClick()
        }
        e.preventDefault()
      }}
    />
  )
}

const TestimonialCard = ({ value }: { value: StudentTestimonial }) => {
  if (!value) {
    return <></>
  }

  return (
    <div className={clsx("border border-[rgba(58,53,65,0.12)]", "sm:p-[26px] py-6 px-7")}>
      <div
        className={clsx("sm:leading-6 line-clamp-6 h-[9em] mb-6", "sm-only:text-sm leading-[22px]")}
      >
        {value.testimony}
      </div>

      <FlexBox justify="between" align="center" className="sm:mt-[18px]">
        <FlexBox className="gap-4" align="center">
          <div className="wh-15">
            <Image
              src={value.testimony_user_picture}
              alt={value.testimony_username}
              width={60}
              height={60}
            />
          </div>

          <div>
            <div className="font-semibold">{value.testimony_username ?? "Jane Doe"}</div>
            <div className="text-sm text-[#484848]">{value.testimony_user_as}</div>
          </div>
        </FlexBox>

        <div>
          <Image
            src={
              value.company?.company_picture.includes("https://")
                ? value.company?.company_picture
                : HARISENIN_PLACEHOLDER
            }
            alt={value.company?.company_name}
            width={60}
            height={60}
          />
        </div>
      </FlexBox>
    </div>
  )
}
export interface StudentContainerProps {
  data: StudentTestimonial[]
  screenWidth: number
}

export const ProgramStudent: FC<StudentContainerProps> = ({ data, screenWidth }) => {
  // Slider breakpoint
  const responsive = {
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 2,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 1,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
    },
  }

  if (!data || !data.length || !screenWidth) {
    return <></>
  }

  return (
    <div className={clsx("relative mb-10", "d-sm-block d-none")} id="testimonial1">
      <ProgramHeading
        id="testimonial"
        title="Testimoni"
        subTitle="Apa Kata Alumni Harisenin.com?"
      />

      <Carousel
        responsive={responsive}
        ssr={true} // means to render carousel on server-side.
        arrows={false}
        itemClass="px-[1.35vw]"
        containerClass="pb-5 -mx-[1.35vw]"
        showDots={screenWidth < 576}
        {...(screenWidth > 576 && {
          customButtonGroup: (
            <SliderArrow
              length={data.length}
              displayedSlide={3}
              leftClassName="-left-[1vw] top-[55%] w-[42px] h-[42px]"
              rightClassName="-right-[1vw] top-[55%] w-[42px] h-[42px]"
            />
          ),
          renderButtonGroupOutside: true,
        })}
        {...(screenWidth < 576 && {
          customDot: <CustomDot />,
          renderDotsOutside: true,
          dotListClass: "!-bottom-3",
          autoPlay: true,
          infinite: true,
          autoPlaySpeed: 3000,
        })}
      >
        {data.map((value, index) => (
          <TestimonialCard value={value} key={index} />
        ))}
      </Carousel>
    </div>
  )
}
