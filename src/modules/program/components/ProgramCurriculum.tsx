import { FC, useState } from "react"

import { createMarkup } from "@lib/functions"

import { SchoolCourse } from "@interfaces/school"

import { RegularButton } from "@components/buttons"
import { LoginModal } from "@components/modal/LoginModal"
import { FlexBox } from "@components/wrapper"
import { Disclosure, Transition } from "@headlessui/react"
import { useUserData } from "@hooks/useUserData"
import clsx from "clsx"
import { FaChevronDown } from "react-icons/fa"
import { MdNote } from "react-icons/md"
import ModalFileRequest from "./ModalFileRequest"
import { ProgramHeading } from "./ProgramHeading"
import { chunk } from "@lib/functions/array"

export interface ProgramLearnProps {
  learnMaterial: SchoolCourse[]
  schoolName: string
  schoolId: string
}

export const ProgramCurriculum: FC<ProgramLearnProps> = ({ learnMaterial, schoolId }) => {
  const { isLogin, tokenData } = useUserData()

  const [openLogin, setOpenLogin] = useState(false)
  const [openSyllabus, setOpenSyllabus] = useState(false)

  const openModal = () => {
    if (isLogin) {
      setOpenSyllabus(true)
    } else {
      setOpenLogin(true)
    }
  }

  if (!learnMaterial || !learnMaterial.length) {
    return <></>
  }

  return (
    <div className={clsx("relative sm:mb-15", "mb-8")} id="curiculum1">
      <ProgramHeading
        id="curiculum"
        title="Kurikulum"
        subTitle="Materi Ter-update. Reviewed by Expert"
      />

      <div className={clsx("w-full", "sm:py-4")}>
        <div className={clsx("sm:mb-9 mb-[18px] sm:flex grid gap-4")}>
          {chunk(learnMaterial, Math.round(learnMaterial.length / 2)).map((chunks, i) => (
            <div key={i} className={clsx("sm:w-1/2 flex flex-col gap-4")}>
              {chunks.map((value, index) => (
                <Disclosure key={index}>
                  {({ open }) => (
                    <div
                      className={clsx(
                        "border border-[rgba(58,53,65,0.12)] sm:pb-5 pb-4 h-fit",
                        open && "bg-[#f7f9fa]"
                      )}
                    >
                      <Disclosure.Button
                        className={clsx(
                          "sm:pt-5 px-5 pt-4",
                          "flex justify-between w-full font-semibold text-left sm-only:text-sm"
                        )}
                      >
                        <FlexBox className="gap-[18px]" align="center">
                          <MdNote className="text-blue" />
                          <div className="max-w-[88%] line-clamp-1" title={value.courses_title}>
                            {value.courses_title}
                          </div>
                        </FlexBox>
                        <FaChevronDown className={open && "rotate-180 transform"} />
                      </Disclosure.Button>

                      <Transition
                        show={open}
                        enter="transition duration-100 ease-out"
                        enterFrom="transform scale-95 opacity-0"
                        enterTo="transform scale-100 opacity-100"
                        leave="transition duration-75 ease-out"
                        leaveFrom="transform scale-100 opacity-100"
                        leaveTo="transform scale-95 opacity-0"
                      >
                        <Disclosure.Panel static>
                          <div
                            className={clsx("sm:px-4 with-list pt-4", "px-5 sm-only:text-sm")}
                            dangerouslySetInnerHTML={createMarkup(value.courses_description)}
                          />
                        </Disclosure.Panel>
                      </Transition>
                    </div>
                  )}
                </Disclosure>
              ))}
            </div>
          ))}
        </div>

        <FlexBox
          align="center"
          className={clsx(
            "bg-[#F7F9FA] sm:py-7 sm:px-8",
            "sm:justify-between sm:flex-row",
            "flex-col sm-only:mb-5 py-4 px-5"
          )}
        >
          <div className={clsx("sm:w-3/4 font-medium sm:text-lg", "text-sm w-full sm-only:mb-4")}>
            Ini cuma sebagian, loh. Yuk download kurikulum selengkapnya!
          </div>
          <RegularButton
            variant="solid-blue"
            onClick={openModal}
            id="btn-download-syllabus"
            className="!rounded-none sm-only:w-full"
          >
            Download Kurikulum
          </RegularButton>
        </FlexBox>
      </div>

      <ModalFileRequest
        data={tokenData}
        schoolId={schoolId}
        show={openSyllabus}
        onHide={() => setOpenSyllabus(false)}
        mode="syllabus"
      />

      <LoginModal show={openLogin} onHide={() => setOpenLogin(false)} />
    </div>
  )
}
