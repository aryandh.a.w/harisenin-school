import { FC } from "react"
import Carousel from "react-multi-carousel"

import { SchoolTutor } from "@interfaces/school"

import { FaLinkedin } from "react-icons/fa"

import { SliderArrow } from "@components/slider"
import { FlexBox } from "@components/wrapper"
import clsx from "clsx"
import Image from "next/image"

export interface TutorSliderProps {
  screenWidth: number
  tutors: SchoolTutor[]
}

function TutorSlider({ screenWidth, tutors }: TutorSliderProps) {
  // Slider breakpoint
  const responsive = {
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 3,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 1,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
    },
  }

  if (!tutors) {
    return <></>
  }

  const TutorCard: FC<{ data: SchoolTutor }> = ({ data }) => {
    return (
      <div
        className={clsx("m-auto relative bg-white sm:px-5 sm-only:mb-[18px]", "sm-only:w-[56vw]")}
      >
        <div className="border border-[rgba(58,53,65,0.12)]">
          <Image
            src={data.tutor_picture}
            alt={data.tutor_name}
            className={clsx("flex mx-auto w-full sm:h-[240px] object-cover", "h-[48.5vw]")}
            width={1000}
            height={1000}
          />

          <div className="sm:p-6 p-[18px] relative">
            {data.tutor_linkedin && (
              <FlexBox
                className={clsx(
                  "h-[48px] w-[48px] bg-[#F7F9FA] border border-[rgba(58,53,65,0.12)]",
                  "absolute right-4 -top-6"
                )}
                justify="center"
                align="center"
              >
                <a
                  href={data.tutor_linkedin}
                  target="_blank"
                  rel="noopener noreferrer"
                  className="text-[#0077b5] hover:text-[#0077b5] text-xl"
                >
                  <FaLinkedin />
                </a>
              </FlexBox>
            )}

            <div className={clsx("font-semibold sm:mb-2.5 mb-1 sm:text-xl")}>{data.tutor_name}</div>
            <div
              className={clsx(
                "text-light-grey sm:mb-4 sm-only:text-sm h-[3em] line-clamp-2",
                "mb-2.5"
              )}
              title={data.tutor_as}
            >
              {data.tutor_as}
            </div>
            {data.company && (
              <div>
                <Image
                  src={data.company.company_picture}
                  alt={data.company.company_name}
                  width={500}
                  height={500}
                  className="h-[42px] w-auto object-contain"
                />
              </div>
            )}
          </div>
        </div>
      </div>
    )
  }

  if (!screenWidth) {
    return <></>
  }

  if (screenWidth > 576) {
    return (
      <>
        <Carousel
          responsive={responsive}
          ssr={true} // means to render carousel on server-side.
          containerClass={clsx("sm:-ml-4 sm:py-5 px-0 gap-5")}
          itemClass="sm-only:mb-5"
          arrows={false}
          {...(screenWidth > 576 && {
            customButtonGroup: (
              <SliderArrow
                length={tutors.length}
                displayedSlide={3}
                leftClassName="top-[50%] w-[42px] h-[42px] -left-[16px]"
                rightClassName="top-[50%] w-[42px] h-[42px] -right-[8px]"
              />
            ),
            renderButtonGroupOutside: true,
          })}
          className="d-lg-flex d-none"
        >
          {tutors.map((value, i) => (
            <TutorCard data={value} key={i} />
          ))}
        </Carousel>
      </>
    )
  } else {
    return (
      <div className={clsx("slider-mobile -mx-[18px] w-[calc(100%+32px)] px-[18px]")}>
        {" "}
        <div className={clsx("flex w-max gap-4")}>
          {tutors.map((value, i) => (
            <TutorCard data={value} key={i} />
          ))}
        </div>
      </div>
    )
  }
}

export default TutorSlider
