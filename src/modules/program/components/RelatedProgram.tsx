import { School } from "@interfaces/school"
import SchoolServices from "@services/school.services"
import { useEffect, useState } from "react"

import { TextButton } from "@components/buttons"
import { ProgramCard } from "@components/card/SchoolCard"
import { SliderArrow } from "@components/slider"
import { Container, container, FlexBox } from "@components/wrapper"
import clsx from "clsx"
import { useRouter } from "next/router"
import Carousel from "react-multi-carousel"
import { useIsMounted } from "@lib/hooks/useIsMounted"

export function RelatedProgram({ screenWidth }: { screenWidth: number }) {
  // const [isLoading, setIsLoading] = useState(true)
  const [schoolList, setSchoolList] = useState<School[]>([])
  const router = useRouter()
  const { asPath } = router

  const isBootcamp = asPath.includes("/bootcamp")
  const isProClass = asPath.includes("/proclass")

  const school = new SchoolServices()

  const responsive = {
    superLargeDesktop: {
      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: 3,
      slidesToSlide: 3,
      partialVisibilityGutter: 30,
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 3,
      slidesToSlide: 3,
      partialVisibilityGutter: 30,
    },
    tablet: {
      breakpoint: { max: 768, min: 464 },
      items: 3,
      slidesToSlide: 2,
    },
  }

  const isMounted = useIsMounted()

  useEffect(() => {
    const getData = async () => {
      try {
        const res = await school.getSchoolListV2(
          isBootcamp ? "BOOTCAMP" : isProClass ? "PRO_CLASS" : "CODING_CAMP",
          {
            limit: 8,
            page: 1,
          }
        )

        if (res.isSuccess) {
          const result = res.getValue()
          const bootcamp = []
          const proclass = []
          const codingcamp = []

          result.data.forEach((value) => {
            if (value.school_type === "BOOTCAMP") {
              bootcamp.push(value)
            } else if (value.school_type === "PRO_CLASS") {
              proclass.push(value)
            } else {
              codingcamp.push(value)
            }
          })

          setSchoolList(isBootcamp ? bootcamp : isProClass ? proclass : codingcamp)
        }
      } catch (e) {
        if (process.env.NODE_ENV === "development") {
          console.log(e)
        }
      }
    }

    if (isMounted) {
      getData()
    }
  }, [isBootcamp, isProClass, isMounted])

  if (!schoolList.length) {
    return <></>
  }

  if (screenWidth > 768) {
    return (
      <div className={clsx("py-10 relative", container)}>
        <FlexBox justify="between" align="center" className="sm:mb-[48px]">
          <div>
            <h2 className={clsx("sm:text-[32px] sm:mb-3 font-medium", "text-sm mb-1")}>
              {isBootcamp ? "Bootcamp" : isProClass ? "ProClass" : "Coding Camp"} Lainnya
            </h2>
            <h3 className="text-[#484848]">Tertarik belajar bidang lain?</h3>
          </div>

          <div className="underline-animation">
            <TextButton
              className={clsx("text-blue font-semibold sm-only:text-sm")}
              onClick={() =>
                (window.location.href = isBootcamp ? "/school/bootcamp" : "/school/proclass")
              }
            >
              Lihat Semua {isBootcamp ? "Bootcamp" : isProClass ? "ProClass" : "Coding Camp"} →
            </TextButton>
          </div>
        </FlexBox>

        <Carousel
          responsive={responsive}
          ssr={true} // means to render carousel on server-side.
          arrows={false}
          customButtonGroup={
            <SliderArrow
              length={schoolList.length}
              displayedSlide={3}
              leftClassName="top-[55%] w-[42px] h-[42px] left-[0.6vw]"
              rightClassName="top-[55%] w-[42px] h-[42px] right-[0.6vw]"
            />
          }
          renderButtonGroupOutside
          containerClass="-mx-[1.15vw]"
          itemClass="py-0 hover:text-inherit px-[1.15vw]"
        >
          {schoolList.map((value, i) => (
            <ProgramCard item={value} key={i} index={i} screenWidth={screenWidth} />
          ))}
        </Carousel>
      </div>
    )
  } else {
    return (
      <Container className={clsx("relative mb-10 mt-10")}>
        <h2 className={clsx("text-2xl mb-3 font-semibold")}>
          {isBootcamp ? "Bootcamp" : isProClass ? "ProClass" : "Coding Camp"} Lainnya
        </h2>
        <h3 className="text-[#484848] mb-[18px]">Tertarik belajar bidang lain?</h3>

        <div className="grid gap-[18px] w-full">
          {schoolList.map((value, i) => (
            <ProgramCard item={value} key={i} index={i} screenWidth={screenWidth} />
          ))}
        </div>
      </Container>
    )
  }
}
