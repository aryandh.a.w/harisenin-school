import React, { ReactNode } from "react"

import { SchoolBatch } from "@interfaces/school"
import { createMarkup, dateFormatter } from "@lib/functions"
import clsx from "clsx"
import { useRouter } from "next/router"
import { ProgramHeading } from "./ProgramHeading"
import Carousel, { DotProps } from "react-multi-carousel"
import { FlexBox } from "@components/wrapper"

interface ProgramBatchProps {
  data?: SchoolBatch[]
  screenWidth: number
}

const TableColumn = ({ children, className, ...props }: React.HTMLAttributes<HTMLDivElement>) => {
  return (
    <th
      className={clsx(
        "border border-[rgba(58,53,65,0.12)] text-left",
        "sm:px-4 sm:py-6",
        className
      )}
      {...props}
    >
      {children}
    </th>
  )
}

const BatchHeadColumn = ({ children }: { children: ReactNode }) => {
  return (
    <FlexBox
      align="center"
      className="p-4 bg-[#F7F9FA] font-semibold text-sm w-1/3 border-r border-r-[rgba(58,53,65,0.12)]"
    >
      {children}
    </FlexBox>
  )
}

const BatchRow = ({ children }: { children: ReactNode }) => {
  return (
    <FlexBox className="py-[22px] px-4 text-sm font-semibold w-2/3" align="center">
      {children}
    </FlexBox>
  )
}

const BatchTableColumn = ({ batch }: { batch: SchoolBatch }) => {
  return (
    <div className="border border-[rgba(58,53,65,0.12)]">
      <FlexBox className="border-b border-b-[rgba(58,53,65,0.12)]">
        <BatchHeadColumn>Batch</BatchHeadColumn>
        <FlexBox className="py-[22px] px-4 gap-2 w-2/3" align="center">
          <span className="font-semibold text-sm">Batch {batch.batch} </span>
          {batch.batch_status === "SOLD_OUT" && (
            <div
              className={clsx(
                "bg-[#FFE8E7] text-[#EF2547] text-xs w-fit rounded uppercase tracking-widest",
                "py-2 px-3"
              )}
            >
              Sold Out
            </div>
          )}
          {batch.remaining_seat_quantity > 0 && (
            <div
              className={clsx(
                "bg-[#E4F2FE] text-blue text-xs w-fit rounded uppercase tracking-widest",
                "py-2 px-3"
              )}
            >
              🔥 {batch.remaining_seat_quantity} Kursi Tersisa
            </div>
          )}
        </FlexBox>
      </FlexBox>

      <FlexBox className="border-b border-b-[rgba(58,53,65,0.12)]">
        <BatchHeadColumn>Pendaftaran</BatchHeadColumn>
        <BatchRow>
          {dateFormatter(batch.open_registration, "DD MMMM")} -{" "}
          {dateFormatter(batch.close_registration, "DD MMMM YY")}
        </BatchRow>
      </FlexBox>

      <FlexBox className="border-b border-b-[rgba(58,53,65,0.12)]">
        <BatchHeadColumn>Kelas Dimulai</BatchHeadColumn>
        <BatchRow>{dateFormatter(batch.batch_start, "DD MMMM")}</BatchRow>
      </FlexBox>

      <FlexBox>
        <BatchHeadColumn>Jadwal</BatchHeadColumn>
        <BatchRow>
          <div
            dangerouslySetInnerHTML={createMarkup(batch.batch_study_schedule)}
            className="markup_small"
          />
        </BatchRow>
      </FlexBox>
    </div>
  )
}

const CustomDot = ({ active, onClick }: DotProps) => {
  return (
    <button
      className={clsx(
        "wh-3 rounded-full mx-1.5 border-0 transition-all duration-200",
        active ? "bg-blue" : "bg-grey-c4 w-auto rounded-0"
      )}
      onClick={(e) => {
        if (onClick) {
          onClick()
        }
        e.preventDefault()
      }}
    />
  )
}

export const ProgramBatch = ({ data, screenWidth }: ProgramBatchProps) => {
  const { asPath } = useRouter()

  const isBootcamp = asPath.includes("bootcamp")

  if (!data || !data.length || !screenWidth) {
    return <></>
  }

  // Slider breakpoint
  const responsive = {
    superLargeDesktop: {
      breakpoint: { max: 4000, min: 3000 },
      items: 1,
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 2,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 1,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
    },
  }

  return (
    <div className="relative sm:mb-15 mb-8" id="schedule1">
      <ProgramHeading
        id="schedule"
        title={`Jadwal ${isBootcamp ? "Bootcamp" : "ProClass"}`}
        subTitle={`Hanya dengan ${isBootcamp ? "3 - 5" : "1 - 2"} Bulan Menggali Potensi Kamu`}
      />

      {screenWidth > 576 && (
        <table className="w-full border border-[rgba(58,53,65,0.12)]">
          <thead className={clsx("bg-[#F7F9FA]")}>
            {["Batch", "Pendaftaran", "Kelas Dimulai", "Jadwal"].map((item, idx) => (
              <TableColumn key={idx}>{item}</TableColumn>
            ))}
          </thead>
          <tbody>
            {data.map((value, i) => (
              <tr key={i}>
                <TableColumn>
                  <div> Batch {value.batch}</div>
                  {value.batch_status === "SOLD_OUT" && (
                    <div
                      className={clsx(
                        "bg-[#FFE8E7] text-[#EF2547] text-xs  w-fit rounded uppercase tracking-widest",
                        "sm:py-2 sm:px-3 mt-2"
                      )}
                    >
                      Sold Out
                    </div>
                  )}
                  {value.remaining_seat_quantity > 0 && (
                    <div
                      className={clsx(
                        "bg-[#E4F2FE] text-blue text-xs w-fit rounded uppercase tracking-widest",
                        "sm:py-2 sm:px-3 mt-2"
                      )}
                    >
                      🔥 {value.remaining_seat_quantity} Kursi Tersisa
                    </div>
                  )}
                </TableColumn>
                <TableColumn className="text-sm">
                  {dateFormatter(value.open_registration, "DD MMMM")} -{" "}
                  {dateFormatter(value.close_registration, "DD MMMM YY")}
                </TableColumn>
                <TableColumn>{dateFormatter(value.batch_start, "DD MMMM")}</TableColumn>
                <TableColumn>
                  <div dangerouslySetInnerHTML={createMarkup(value.batch_study_schedule)} />
                </TableColumn>
              </tr>
            ))}
          </tbody>
        </table>
      )}

      {screenWidth < 576 && (
        <Carousel
          responsive={responsive}
          ssr={true} // means to render carousel on server-side.
          containerClass={clsx("sm:-ml-4 sm:py-5 px-0 gap-5 mb-5")}
          itemClass="mb-5"
          arrows={false}
          showDots
          renderDotsOutside
          {...(screenWidth < 576 && {
            customDot: <CustomDot />,
            dotListClass: "!-bottom-2",
            autoPlay: true,
            infinite: true,
            autoPlaySpeed: 3000,
          })}
          className="d-lg-flex d-none"
        >
          {data.map((value, i) => (
            <BatchTableColumn batch={value} key={i} />
          ))}
        </Carousel>
      )}
    </div>
  )
}
