import { FC } from "react"

import { HARISENIN_PUBLIC_PAGE_ASSETS } from "@constants/pictures"
import { SchoolDetail } from "@interfaces/school"

import { createMarkup } from "@lib/functions"

import { RegularButton } from "@components/buttons"

import { NextImage } from "@components/misc"
import { Heading } from "@components/typography"
import { container } from "@components/wrapper"
import clsx from "clsx"

export interface ProgramBannerProps {
  schoolDetail: SchoolDetail
  scroll: () => void
  screenWidth: number
}

export const ProgramBanner: FC<ProgramBannerProps> = ({ schoolDetail, scroll, screenWidth }) => {
  if (!schoolDetail || !schoolDetail.school_banner) {
    return <></>
  }

  return (
    <div className={clsx("relative")}>
      {/* Background Image */}
      <NextImage
        size={[1440, 789]}
        src={`${HARISENIN_PUBLIC_PAGE_ASSETS}/homepage_banner_3.png`}
        alt="program banner"
        className={clsx("sm:w-full sm:h-[48vw] sm:object-cover", "sm-only:h-full sm-only:absolute")}
      />

      <div
        className={clsx(
          "absolute top-0 sm:left-0 sm:h-full sm:w-full sm:pt-15 bg-[rgba(0,0,0,0.9)]",
          "sm-only:relative pt-0 "
        )}
      >
        <div
          className={clsx(
            container,
            "sm:flex sm:justify-center sm:h-full sm:my-auto sm:mx-auto items-center sm:gap-9",
            "sm-only:block sm-only:!w-full sm-only:relative"
          )}
        >
          {/* Left section of banner */}
          <div
            className={clsx(
              "sm:w-1/2 h-fit",
              "w-full sm-only:py-10 sm-only:h-auto sm-only:flex sm-only:flex-col sm-only:justify-around sm-only:items-center"
            )}
          >
            {/* Title */}
            <Heading
              headingLevel="h1"
              className={clsx(
                "text-white sm:font-semibold sm:text-4xl sm:mb-4 sm:leading-10",
                "text-2xl leading-[34px] mb-2"
              )}
              id="header1"
            >
              {schoolDetail.school_banner.banner_title}
            </Heading>

            {/* Description */}
            {schoolDetail?.school_banner.banner_sub_title ? (
              <div
                className={clsx(
                  "text-white sm:mb-8 sm:leading-8 sm:text-lg",
                  "mb-[30px] leading-6 sm-only:text-sm"
                )}
                dangerouslySetInnerHTML={createMarkup(schoolDetail.school_banner.banner_sub_title)}
              />
            ) : (
              <div className={clsx("h-8")} />
            )}

            {/* Button for scrolling*/}
            {Boolean(schoolDetail.school_program_type.length) && (
              <RegularButton
                onClick={scroll}
                id="btn-regist-class-banner"
                customPadding="sm:px-10 sm:py-3 py-2 px-4"
                className="sm-only:w-full !rounded-none"
              >
                Daftar Sekarang
              </RegularButton>
            )}
          </div>

          {/* Right section of banner */}
          {screenWidth > 576 && (
            <div className={clsx("sm:w-1/2 h-fit")}>
              <NextImage
                size={[1440, 789]}
                src={schoolDetail.school_banner.banner_image}
                alt="program banner"
                className={clsx("sm:wh-auto", "sm-only:h-full sm-only:absolute")}
              />
            </div>
          )}
        </div>
      </div>
    </div>
  )
}
