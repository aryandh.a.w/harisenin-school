import React, { FC, useEffect, useState } from "react"
import {
  EmailShareButton,
  FacebookShareButton,
  LinkedinShareButton,
  TelegramShareButton,
  TwitterShareButton,
  WhatsappShareButton,
} from "react-share"

import { RegularButton, TextButton } from "@components/buttons"
import {
  FaFacebookSquare,
  FaLinkedin,
  FaTelegramPlane,
  FaTimes,
  FaTwitter,
  FaWhatsapp,
} from "react-icons/fa"
import { BsFillEnvelopeFill } from "react-icons/bs"
import SchoolServices from "@services/school.services"
import clsx from "clsx"
import { FiShare2 } from "react-icons/fi"
import { HARISENIN_PUBLIC_ICON } from "@constants/pictures"

import { useRouter } from "next/router"
import { FlexBox } from "@components/wrapper"
import Image from "next/image"
import { Spinner } from "@components/misc"
import { Modal } from "@components/modal"

export interface ProgramShareProps {
  screenWidth: number
  slug: string
}

type PopUpState = "none" | "modal" | "bottom-bar"

const ProgramShare: FC<ProgramShareProps> = ({ screenWidth, slug }) => {
  const [popUpState, setPopUpState] = useState<PopUpState>("none")
  const [isFetchingLink, setIsFetchingLink] = useState(false)
  const [shareLink, setShareLink] = useState("")
  const [linkCopied, setLinkCopied] = useState(false)

  const handleCopy = async () => {
    await navigator.clipboard.writeText(shareLink ?? "")
    setLinkCopied(true)
    setTimeout(() => setLinkCopied(false), 3000)
  }

  const school = new SchoolServices()
  const router = useRouter()
  const isBootcamp = router.asPath.includes("bootcamp")

  useEffect(() => {
    const getLink = async () => {
      try {
        setIsFetchingLink(true)
        const res = await school.getSchoolShareLink(slug, isBootcamp)

        if (res.isSuccess) {
          const data = res.getValue() as string
          setShareLink(data)
          setIsFetchingLink(false)
        } else {
          setShareLink(`https://harisenin.link/${slug}`)
          setIsFetchingLink(false)
        }
      } catch (e) {
        setShareLink(`https://harisenin.link/${slug}`)
        setIsFetchingLink(false)
      }
    }

    if (popUpState === "none") {
      setShareLink("")
    } else {
      getLink()
    }
  }, [popUpState])

  const handleOpen = async () => {
    if (screenWidth > 768) {
      setPopUpState("modal")
    } else {
      setPopUpState("bottom-bar")
    }
  }

  const handleHideModal = () => {
    setPopUpState("none")
  }

  const EmailLink = () => {
    return (
      <div className="w-[16%] text-center">
        <EmailShareButton
          url={shareLink}
          className={`mt-0 mx-auto mb-[6px] w-[44px] h-[44px] rounded-lg !bg-[#696969] `}
        >
          <BsFillEnvelopeFill className="text-white mx-auto" />
        </EmailShareButton>
        <p className="text-center text-sm">Email</p>
      </div>
    )
  }

  const FacebookLink = () => {
    return (
      <div className="w-[16%] text-center">
        <FacebookShareButton
          url={shareLink}
          className={`-mt-1 -ml-1 mx-auto mb-[6px] w-[44px] h-[44px] rounded-lg !bg-white`}
        >
          <FaFacebookSquare className="text-[#4267b2] m-auto text-[49px]" />
        </FacebookShareButton>
        <p className="text-center text-sm">Facebook</p>
      </div>
    )
  }

  const TwitterLink = () => {
    return (
      <div className="w-[16%] text-center">
        <TwitterShareButton
          url={shareLink}
          className={`mt-0 mx-auto mb-[6px] w-[44px] h-[44px] rounded-lg !bg-[#1da1f2]`}
        >
          <FaTwitter className="text-white mx-auto text-[24px]" />
        </TwitterShareButton>
        <p className="text-center text-sm">Twitter</p>
      </div>
    )
  }

  const LinkedInLink = () => {
    return (
      <div className="w-[16%] text-center">
        <LinkedinShareButton
          url={shareLink}
          className={`-mt-1 -ml-1 mx-auto mb-[6px] w-[44px] h-[44px] rounded-lg !bg-white`}
        >
          <FaLinkedin className="text-[#2867b2] text-[49px]" />
        </LinkedinShareButton>
        <p className="text-center text-sm">Linkedin</p>
      </div>
    )
  }

  const WhatsappLink = () => {
    return (
      <div className="w-[16%] text-center">
        <WhatsappShareButton
          url={shareLink}
          className={`mt-0 mx-auto mb-[6px] w-[44px] h-[44px] rounded-lg !bg-[#4ac959]`}
        >
          <FaWhatsapp className="text-white text-[28px] mx-auto" />
        </WhatsappShareButton>
        <p className="text-center text-sm">Whatsapp</p>
      </div>
    )
  }

  const TelegramLink = () => {
    return (
      <div className="w-[16%] text-center">
        <TelegramShareButton
          url={shareLink}
          className={`mt-0 mx-auto mb-[6px] w-[44px] h-[44px] rounded-lg !bg-[#0088cc]`}
        >
          <FaTelegramPlane className="text-white text-[26px] mx-auto" />
        </TelegramShareButton>
        <p className="text-center text-sm">Telegram</p>
      </div>
    )
  }

  const LinkComponent = () => {
    return (
      <div className="text-sm bg-[#f7f9fa] relative border border-[#ececec] py-[14px] px-[18px] flex justifiy-between mb-4">
        <TextButton className="text-lg" onClick={handleCopy}>
          <Image
            width={20}
            height={20}
            className="ml-lg-2 ml-0 object contain mr-4"
            src={`${HARISENIN_PUBLIC_ICON}/icon_copytext.png`}
            alt="icon"
          />{"   "}
        </TextButton>
        <div className="w-[90%] ml-2 text-ellipsis overflow-hidden whitespace-nowrap">{shareLink}</div>
        {linkCopied && (
          <small className="text-green absolute -bottom-5 left-[14px]">Link berhasil disalin</small>
        )}
      </div>
    )
  }

  return (
    <>
      <RegularButton
        variant="transparent"
        className={clsx("sm:w-full sm:mt-2", "absolute sm-only:ml-auto")}
        onClick={handleOpen}
        id="btn-share-hms"
        textClassName="flex gap-1"
      >
        <FiShare2 className="my-auto" />
        Bagikan
      </RegularButton>
      <Modal show={popUpState === "modal"} onClose={handleHideModal}>
        {isFetchingLink ? (
          <div className="flex justify-center items-center h-[300px]">
            <Spinner />
          </div>
        ) : (
          <>
            <div className={clsx("flex justify-between mb-9")}>
              <h4 className="text-lg">Share Program Ini</h4>
              <div className="modal-times d-flex cursor-pointer">
                <FaTimes onClick={handleHideModal} className="text-lg" />
              </div>
            </div>

            <FlexBox justify="between" className="sm-only:flex-wrap">
              <EmailLink />
              <FacebookLink />
              <TwitterLink />
              <LinkedInLink />
              <WhatsappLink />
              <TelegramLink />
            </FlexBox>

            <LinkComponent />
          </>
        )}
      </Modal>

      <>
        <div
          className={clsx(
            "fixed top-0 left-0 w-full h-full bg-[#0000005c] z-30 cursor-pointer hidden",
            popUpState !== "bottom-bar" && "hidden"
          )}
          onClick={handleHideModal}
        />
        <div
          className={clsx(
            `z-40 w-full fixed left-0 bottom-0 bg-white rounded-2xl py-3 px-5 transition-max-height duration-75 d-lg-none`,
            popUpState !== "bottom-bar" && "hidden max-h-0 p-0 "
          )}
        >
          <div className="modal-times d-flex absolute right-4 top-4">
            <FaTimes onClick={handleHideModal} className="text-lg" />
          </div>

          {isFetchingLink ? (
            <div className="flex justify-center items-center h-[300px]">
              <Spinner />
            </div>
          ) : (
            <>
              {" "}
              <h4>Share</h4>
              <div className="grid grid-cols-3">
                <EmailLink />
                <FacebookLink />
                <WhatsappLink />
                <TwitterLink />
                <LinkedInLink />
                <TelegramLink />
              </div>
              <LinkComponent />
            </>
          )}
        </div>
      </>
    </>
  )
}

export default ProgramShare
