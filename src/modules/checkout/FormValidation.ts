<<<<<<< HEAD:src/modules/checkout/FormValidation.ts
import * as yup from "yup"
import { FormStatus } from "@interfaces/checkout-state"

export const initialValidationSchema = yup.object({
  user_fullname: yup
    .string()
    .min(3, "*Minimal 3 karakter")
    .max(30, "Maksimal 30 karakter")
    .required("*Wajib diisi")
    .matches(/^[a-zA-Z\s]*$/, "Tidak boleh ada angka"),
  user_phone_number: yup
    .string()
    .min(12, "*Pastikan kode negara di pilih, dan nomor telepon minimal 10 nomor")
    .max(15, "*Nomor telepon maksimal 13 nomor")
    .required("*Wajib diisi"),
  user_desired_job_sector: yup
    .string()
    .min(2, "*Minimal 2 karakter")
    .required("*Wajib diisi")
    .matches(/^[a-zA-Z,.()\s]*$/, "Tidak boleh ada angka"),
  user_desired_job: yup
    .string()
    .required("*Wajib diisi")
    .min(2, "*Minimal 2 karakter")
    .matches(/^[a-zA-Z\s]*$/, "Tidak boleh ada angka"),
  user_batch_request: yup.string().required("*Wajib diisi"),
  form_status: yup.string().required("*Wajib diisi"),
  user_linkedin_url: yup
    .string()
    .matches(/www.linkedin.com\/[a-zA-Z0-9]/gm, "Format yang kamu masukan salah")
    .required("*URL wajib diisi"),
})

export const workingSchema = initialValidationSchema.concat(
  yup.object({
    user_job: yup
      .string()
      .min(2, "*Minimal 2 karakter")
      .required("*Wajib diisi")
      .matches(/^[a-zA-Z\s]*$/, "Tidak boleh ada angka"),
  })
)

export const universitySchema = initialValidationSchema.concat(
  yup.object({
    user_university: yup
      .string()
      .required("*Nama universitas wajib diisi")
      .min(3, "*Minimal 3 karakter")
      .matches(/^[a-zA-Z\d\s:]*$/, "Nama universitas hanya diperbolehkan kombinasi angka dan huruf")
      .required("*Nama universitas wajib diisi"),
    user_faculty: yup
      .string()
      .min(3, "*Minimal 3 karakter")
      .required("*Fakultas wajib diisi")
      .matches(/^[a-zA-Z\s]*$/, "Tidak boleh ada angka"),
    user_majors: yup
      .string()
      .min(3, "*Minimal 3 karakter")
      .required("*Jurusan wajib diisi")
      .matches(/^[a-zA-Z\s]*$/, "Tidak boleh ada angka"),
  })
)

export function validationHandler(type: FormStatus) {
  switch (type) {
    case "Belum Bekerja":
    case "":
      return initialValidationSchema
    case "Karyawan":
      return workingSchema
    case "Mahasiswa":
      return universitySchema
    default:
      return initialValidationSchema
  }
}

export const creditCardValidation = yup.object({
  given_names: yup
    .string()
    .min(3, "*Minimal 3 karakter")
    .max(30, "Maksimal 30 karakter")
    .matches(/^[a-zA-Z\s]*$/, "Tidak boleh ada angka")
    .required("*Wajib diisi"),
  surname: yup
    .string()
    .matches(/^[a-zA-Z\s]*$/, "Tidak boleh ada angka")
    .min(3, "*Minimal 3 karakter")
    .max(30, "Maksimal 30 karakter")
    .required("*Wajib diisi"),
  email: yup.string().email("*Email tidak valid").required("*Wajib diisi"),
  country: yup.string().required("*Wajib dipilih"),
  postal_code: yup.string().min(5, "*Minimal 5 karakter").required("*Wajib diisi"),
  account_number: yup.string().min(19, "*Nomor kartu minimal 16 digit").required("*Wajib diisi"),
  exp_date: yup
    .string()
    .min(7, "*Penulisan wajib mengikuti format MM/YYYY")
    .required("*Wajib diisi"),
  card_cvn: yup.string().min(3, "*Wajib 3 karakter").required("*Wajib diisi"),
  street_line1: yup.string().required("*Wajib diisi"),
  province_state: yup.string().required("*Wajib diisi"),
  city: yup.string().required("*Wajib diisi"),
})
=======
import * as yup from "yup"
import { FormStatus } from "../../../constants/interfaces/checkout-state"

export const initialValidationSchema = yup.object({
  user_fullname: yup
    .string()
    .min(3, "*Minimal 3 karakter")
    .max(30, "Maksimal 30 karakter")
    .required("*Wajib diisi")
    .matches(/^[a-zA-Z\s]*$/, "Tidak boleh ada angka"),
  user_phone_number: yup
    .string()
    .min(12, "*Pastikan kode negara di pilih, dan nomor telepon minimal 10 nomor")
    .max(15, "*Nomor telepon maksimal 13 nomor")
    .required("*Wajib diisi"),
  user_desired_job_sector: yup
    .string()
    .min(2, "*Minimal 2 karakter")
    .required("*Wajib diisi")
    .matches(/^[a-zA-Z\s]*$/, "Tidak boleh ada angka"),
  user_desired_job: yup
    .string()
    .required("*Wajib diisi")
    .min(2, "*Minimal 2 karakter")
    .matches(/^[a-zA-Z\s]*$/, "Tidak boleh ada angka"),
  user_batch_request: yup.string().required("*Wajib diisi"),
  form_status: yup.string().required("*Wajib diisi"),
  user_linkedin_url: yup
    .string()
    .matches(/www.linkedin.com/gm, "Format yang kamu masukan salah")
    .required("*URL wajib diisi"),
})

export const workingSchema = initialValidationSchema.concat(
  yup.object({
    user_job: yup
      .string()
      .min(2, "*Minimal 2 karakter")
      .required("*Wajib diisi")
      .matches(/^[a-zA-Z\s]*$/, "Tidak boleh ada angka"),
  })
)

export const universitySchema = initialValidationSchema.concat(
  yup.object({
    user_university: yup
      .string()
      .required("*Nama universitas wajib diisi")
      .min(3, "*Minimal 3 karakter")
      .matches(/^[a-zA-Z\d\s:]*$/, "Nama universitas hanya diperbolehkan kombinasi angka dan huruf")
      .required("*Nama universitas wajib diisi"),
    user_faculty: yup
      .string()
      .min(3, "*Minimal 3 karakter")
      .required("*Fakultas wajib diisi")
      .matches(/^[a-zA-Z\s]*$/, "Tidak boleh ada angka"),
    user_majors: yup
      .string()
      .min(3, "*Minimal 3 karakter")
      .required("*Jurusan wajib diisi")
      .matches(/^[a-zA-Z\s]*$/, "Tidak boleh ada angka"),
  })
)

export function validationHandler(type: FormStatus) {
  switch (type) {
    case "Belum Bekerja":
    case "":
      return initialValidationSchema
    case "Karyawan":
      return workingSchema
    case "Mahasiswa":
      return universitySchema
    default:
      return initialValidationSchema
  }
}

export const creditCardValidation = yup.object({
  given_names: yup
    .string()
    .min(3, "*Minimal 3 karakter")
    .max(30, "Maksimal 30 karakter")
    .matches(/^[a-zA-Z\s]*$/, "Tidak boleh ada angka")
    .required("*Wajib diisi"),
  surname: yup
    .string()
    .matches(/^[a-zA-Z\s]*$/, "Tidak boleh ada angka")
    .min(3, "*Minimal 3 karakter")
    .max(30, "Maksimal 30 karakter")
    .required("*Wajib diisi"),
  email: yup.string().email("*Email tidak valid").required("*Wajib diisi"),
  country: yup.string().required("*Wajib dipilih"),
  postal_code: yup.string().min(5, "*Minimal 5 karakter").required("*Wajib diisi"),
  account_number: yup.string().min(19, "*Nomor kartu minimal 16 digit").required("*Wajib diisi"),
  exp_date: yup
    .string()
    .min(7, "*Penulisan wajib mengikuti format MM/YYYY")
    .required("*Wajib diisi"),
  card_cvn: yup.string().min(3, "*Wajib 3 karakter").required("*Wajib diisi"),
  street_line1: yup.string().required("*Wajib diisi"),
  province_state: yup.string().required("*Wajib diisi"),
  city: yup.string().required("*Wajib diisi"),
})
>>>>>>> 94ccec2 (setup fix):src/ui/components/checkout/FormValidation.ts
