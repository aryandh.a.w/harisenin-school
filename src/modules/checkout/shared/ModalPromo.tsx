<<<<<<< HEAD:src/modules/checkout/shared/ModalPromo.tsx
import React, { Dispatch, FC, useState } from "react"
import { CheckoutAction, CheckoutActionType, CheckoutState } from "@interfaces/checkout-state"
import CheckoutService from "@services/checkout.services"
import { RegularButton } from "@components/buttons"
import { ManualInput } from "@components/input"

import clsx from "clsx"
import { Modal } from "@components/modal"

interface ModalPromoProps {
  checkoutState: CheckoutState
  dispatch: Dispatch<CheckoutAction>
  serviceId: string | number
}

const ModalPromo: FC<ModalPromoProps> = ({ dispatch, checkoutState, serviceId }) => {
  const [isSubmittingPromo, setIsSubmittingPromo] = useState(false)

  const checkout = new CheckoutService()

  async function submitPromo() {
    try {
      setIsSubmittingPromo(true)
      const res = await checkout.applyPromoCode(serviceId as number, {
        code: checkoutState.promo_code,
      })

      if (res.isSuccess) {
        const discount = res.getValue()
        const payments = discount.available_payment

        dispatch({
          type: CheckoutActionType.PaymentOptionInitializer,
          payload: {
            bank_method: payments.BANK_TRANSFER,
            e_wallet_method: payments.EWALLET,
            ...(payments.CREDIT_CARD && {
              credit_card: {
                payment_method: "CREDIT_CARD",
                payment_fee: payments.CREDIT_CARD[0].payment_fee,
                payment_channel_logo: "",
                payment_channel: "",
              },
            }),
            pay_later_method: payments.PAY_LATER,
          },
        })

        // Discount is applied and promo modal will be closed
        dispatch({ type: CheckoutActionType.ApplyPromo, payload: discount })
      } else {
        const data = res.error
        const message: string = data.message
        dispatch({ type: CheckoutActionType.ErrorDiscount, payload: message })
      }

      setIsSubmittingPromo(false)
    } catch (e) {
      setIsSubmittingPromo(false)
      dispatch({
        type: CheckoutActionType.ErrorDiscount,
        payload: "Terjadi kesalahan. Silahkan coba lagi",
      })
    }
  }

  // const handleChoosePromo = (promo: AvailablePromo) => () => {
  //   dispatch({
  //     type: CheckoutActionType.InputPromoCode,
  //     payload: promo.discount_code,
  //   })
  // }

  return (
    <>
      <Modal
        show={checkoutState.modal_open === "promo"}
        onClose={() => dispatch({ type: CheckoutActionType.ClosePromoModal })}
      >
        <div className={clsx("sm:pt-8 sm:px-8 sm:pb-10", "pt-5 px-5 pb-10")}>
          <h4 className={clsx("mb-5 font-semibold sm-only:text-sm")}>Kode Promo</h4>

          <div className={clsx(" flex justify-between items-center  ")}>
            <ManualInput
              errorText={checkoutState.error_discount}
              value={checkoutState.promo_code}
              className={clsx("w-full relative sm:w-4/5 mb-0", "w-3/5")}
              onChange={(e) =>
                dispatch({
                  type: CheckoutActionType.InputPromoCode,
                  payload: e.target.value,
                })
              }
              placeholder="Masukkan kode promo kamu di sini"
            />
            <RegularButton
              variant="solid"
              color="blue"
              disabled={!checkoutState.promo_code}
              isSubmitting={isSubmittingPromo}
              onClick={submitPromo}
              className={clsx("sm:w-[18%] py-[10px] px-0", "w-[30%]")}
            >
              Pakai kode
            </RegularButton>
          </div>

          {/*{checkoutState?.available_promos && checkoutState?.available_promos.length ? (*/}
          {/*  <div className="modal_promo__available">*/}
          {/*    {checkoutState?.available_promos.map((value, index) => (*/}
          {/*      <div*/}
          {/*        className={clsx(*/}
          {/*          "modal_promo__available__container",*/}
          {/*          value.discount_code === checkoutState.promo_code &&*/}
          {/*            "modal_promo__available__container--active"*/}
          {/*        )}*/}
          {/*        onClick={handleChoosePromo(value)}*/}
          {/*        key={index}*/}
          {/*      >*/}
          {/*        <div className="modal_promo__available__title">*/}
          {/*          Diskon{" "}*/}
          {/*          {value.discount_price*/}
          {/*            ? `Rp ${value.discount_price}`*/}
          {/*            : `${value.discount_percentage * 100}%`}*/}
          {/*        </div>*/}
          {/*        <div className="modal_promo__available__description">{value.discount_name}</div>*/}
          {/*        <div className="modal_promo__available__date">*/}
          {/*          Berlaku hingga {dayjs(value.discount_end_at).format("DD MMMM YYYY hh:mm")}*/}
          {/*        </div>*/}
          {/*      </div>*/}
          {/*    ))}*/}
          {/*  </div>*/}
          {/*) : null}*/}
        </div>
      </Modal>
    </>
  )
}

export default ModalPromo
=======
import React, { Dispatch, FC, useState } from "react"
import { Modal } from "react-bootstrap"
import {
  CheckoutAction,
  CheckoutActionType,
  CheckoutState,
} from "../../../../constants/interfaces/checkout-state"
import CheckoutService from "../../../../lib/services/checkout.services"
import { RegularButton } from "../../../modules/buttons"
import { ManualInput } from "../../../modules/input"

import style from "../styles/checkout.module.scss"

interface ModalPromoProps {
  checkoutState: CheckoutState
  dispatch: Dispatch<CheckoutAction>
  serviceId: string | number
}

const ModalPromo: FC<ModalPromoProps> = ({ dispatch, checkoutState, serviceId }) => {
  const [isSubmittingPromo, setIsSubmittingPromo] = useState(false)

  const checkout = new CheckoutService()

  async function submitPromo() {
    try {
      setIsSubmittingPromo(true)
      const res = await checkout.applyPromoCode(serviceId as number, {
        code: checkoutState.promo_code,
      })

      if (res.isSuccess) {
        const discount = res.getValue()
        const payments = discount.available_payment

        dispatch({
          type: CheckoutActionType.PaymentOptionInitializer,
          payload: {
            bank_method: payments.BANK_TRANSFER,
            e_wallet_method: payments.EWALLET,
            ...(payments.CREDIT_CARD && {
              credit_card: {
                payment_method: "CREDIT_CARD",
                payment_fee: payments.CREDIT_CARD[0].payment_fee,
                payment_channel_logo: "",
                payment_channel: "",
              },
            }),
          },
        })

        // Discount is applied and promo modal will be closed
        dispatch({ type: CheckoutActionType.ApplyPromo, payload: discount })
      } else {
        const data = res.error
        const message: string = data.message
        dispatch({ type: CheckoutActionType.ErrorDiscount, payload: message })
      }

      setIsSubmittingPromo(false)
    } catch (e) {
      setIsSubmittingPromo(false)
      dispatch({
        type: CheckoutActionType.ErrorDiscount,
        payload: "Terjadi kesalahan. Silahkan coba lagi",
      })
    }
  }

  // const handleChoosePromo = (promo: AvailablePromo) => () => {
  //   dispatch({
  //     type: CheckoutActionType.InputPromoCode,
  //     payload: promo.discount_code,
  //   })
  // }

  return (
    <>
      <Modal
        show={checkoutState.modal_open === "promo"}
        dialogClassName={style.modal_promo__dialog}
        onHide={() => dispatch({ type: CheckoutActionType.ClosePromoModal })}
        centered
      >
        <Modal.Body className={style.modal_promo__content}>
          <h4>Kode Promo</h4>

          <div className={style["modal_promo__handler"]}>
            <ManualInput
              errorText={checkoutState.error_discount}
              value={checkoutState.promo_code}
              className={style["modal_promo__handler__input"]}
              onChange={(e) =>
                dispatch({
                  type: CheckoutActionType.InputPromoCode,
                  payload: e.target.value,
                })
              }
              placeholder="Masukkan kode promo kamu di sini"
            />
            <RegularButton
              type="solid"
              color="blue"
              disabled={!checkoutState.promo_code}
              isSubmitting={isSubmittingPromo}
              onClick={submitPromo}
            >
              Pakai kode
            </RegularButton>
          </div>

          {/*{checkoutState?.available_promos && checkoutState?.available_promos.length ? (*/}
          {/*  <div className="modal_promo__available">*/}
          {/*    {checkoutState?.available_promos.map((value, index) => (*/}
          {/*      <div*/}
          {/*        className={clsx(*/}
          {/*          "modal_promo__available__container",*/}
          {/*          value.discount_code === checkoutState.promo_code &&*/}
          {/*            "modal_promo__available__container--active"*/}
          {/*        )}*/}
          {/*        onClick={handleChoosePromo(value)}*/}
          {/*        key={index}*/}
          {/*      >*/}
          {/*        <div className="modal_promo__available__title">*/}
          {/*          Diskon{" "}*/}
          {/*          {value.discount_price*/}
          {/*            ? `Rp ${value.discount_price}`*/}
          {/*            : `${value.discount_percentage * 100}%`}*/}
          {/*        </div>*/}
          {/*        <div className="modal_promo__available__description">{value.discount_name}</div>*/}
          {/*        <div className="modal_promo__available__date">*/}
          {/*          Berlaku hingga {dayjs(value.discount_end_at).format("DD MMMM YYYY hh:mm")}*/}
          {/*        </div>*/}
          {/*      </div>*/}
          {/*    ))}*/}
          {/*  </div>*/}
          {/*) : null}*/}
        </Modal.Body>
      </Modal>
    </>
  )
}

export default ModalPromo
>>>>>>> 94ccec2 (setup fix):src/ui/components/checkout/shared/ModalPromo.tsx
