<<<<<<< HEAD:src/modules/checkout/shared/QrModal.tsx
import React, { FC, useState } from "react"
import QRCode from "qrcode.react"
import { HARISENIN_PUBLIC_LOGO } from "@constants/pictures"
import { useEWalletPaymentSocket } from "@hooks/useWebsocket"

import clsx from "clsx"
import { FlexBox } from "@components/wrapper"
import { Spinner } from "@components/misc"
import { Modal } from "@components/modal"
import Image from "next/image"

export interface QrModalProps {
  qrString?: string
  checkoutId?: string | string[]
  productId?: string
  show: boolean
  onHide: () => void
}

const QrModal: FC<QrModalProps> = ({ show, onHide, qrString, checkoutId, productId }) => {
  const [isListening, setIsListening] = useState(true)
  const [errorText, setErrorText] = useState("")

  const setError = () => {
    setIsListening(false)
    setErrorText("Terjadi kesalahan, silahkan coba lagi")
  }

  if (show) {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    useEWalletPaymentSocket({
      checkoutId,
      productId: productId,
      handleError: setError,
    })
  }

  return (
    <Modal show={show} onClose={onHide}>
      <Image
        width={400}
        height={250}
        src={`${HARISENIN_PUBLIC_LOGO}/logo_shopeepay.png`}
        alt="shoopeepay"
        className={clsx("w-1/5 h-auto")}
      />
      <QRCode value={qrString} size={250} className={clsx("mb-8")} />
      <div className={clsx("font-medium mb-5")}>Scan QR code ini untuk melanjutkan pembayaran</div>

      <FlexBox direction="col" align="center">
        {isListening ? (
          <>
            <Spinner />
            <div className={clsx("mt-3")}>Menunggu pembayaran</div>
          </>
        ) : null}

        {errorText ? <div className="text-red">Terjadi kesalahan. Silahkan coba lagi</div> : null}
      </FlexBox>
    </Modal>
  )
}

export default QrModal
=======
import React, { FC, useState } from "react"
import { Modal, ModalProps, Spinner } from "react-bootstrap"
import QRCode from "qrcode.react"
import { HARISENIN_PUBLIC_LOGO } from "../../../../constants/pictures"
import { useEWalletPaymentSocket } from "../../../../lib/utils/hooks/useWebsocket"

import style from "../styles/checkout.module.scss"

export interface QrModalProps extends ModalProps {
  qrString?: string
  checkoutId?: string | string[]
  productId?: string
}

const QrModal: FC<QrModalProps> = ({ show, onHide, qrString, checkoutId, productId }) => {
  const [isListening, setIsListening] = useState(true)
  const [errorText, setErrorText] = useState("")

  const setError = () => {
    setIsListening(false)
    setErrorText("Terjadi kesalahan, silahkan coba lagi")
  }

  if (show) {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    useEWalletPaymentSocket({
      checkoutId,
      productId: productId,
      handleError: setError,
    })
  }

  return (
    <Modal show={show} onHide={onHide} keyboard={false} centered contentClassName={style.modal_qr}>
      <img
        src={`${HARISENIN_PUBLIC_LOGO}/logo_shopeepay.png`}
        alt=""
        className={style["modal_qr__image"]}
      />
      <QRCode value={qrString} size={250} className={style["modal_qr__code"]} />
      <div className={style["modal_qr__info"]}>Scan QR code ini untuk melanjutkan pembayaran</div>

      <div className={style["modal_qr__waiting"]}>
        {isListening ? (
          <>
            <Spinner animation="grow" variant="info" />
            <div className={style["modal_qr__waiting__info"]}>Menunggu pembayaran</div>
          </>
        ) : null}

        {errorText ? <div className="text-red">Terjadi kesalahan. Silahkan coba lagi</div> : null}
      </div>
    </Modal>
  )
}

export default QrModal
>>>>>>> 94ccec2 (setup fix):src/ui/components/checkout/shared/QrModal.tsx
