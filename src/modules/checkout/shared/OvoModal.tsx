<<<<<<< HEAD:src/modules/checkout/shared/OvoModal.tsx
import { FC, useEffect, useState } from "react"
import { HARISENIN_PUBLIC_LOGO } from "@constants/pictures"
import { priceFormatter } from "@lib/functions"
import { RegularButton } from "@components/buttons"

import { useEWalletPaymentSocket } from "@hooks/useWebsocket"
import ManualPhoneNumberInput from "@components/input/ManualPhoneNumberInput"
import CheckoutService from "@services/checkout.services"

import { FlexBox } from "@components/wrapper"
import Image from "next/image"
import { Spinner } from "@components/misc"
import { Modal } from "@components/modal"

export interface OvoModalProps {
  checkoutId?: string
  price?: number
  productId?: string
  show:boolean
  onHide:() => any
}

export const OvoModal: FC<OvoModalProps> = ({ show, onHide, checkoutId, productId, price }) => {
  const [phoneNumber, setPhoneNumber] = useState("")
  const [timeoutResponse, setTimeoutResponse] = useState(0)

  const [isSubmitting, setIsSubmitting] = useState(false)
  const [isListening, setIsListening] = useState(false)
  const [errorText, setErrorText] = useState("")

  const checkout = new CheckoutService()

  useEffect(() => {
    timeoutResponse > 0 &&
      setTimeout(() => {
        setTimeoutResponse(timeoutResponse - 1)
      }, 1000)
  }, [timeoutResponse])

  const handleSubmit = async () => {
    if (phoneNumber.length < 12) {
      setErrorText("Pastikan kode negara di pilih, dan nomor telepon minimal 10 digit")
    } else if (phoneNumber.length > 15) {
      setErrorText("Nomor telepon maksimal 13 karakter")
    } else {
      try {
        setErrorText("")
        setIsSubmitting(true)

        const res = await checkout.submitPayment({
          checkoutId,
          body: {
            payment_method: "EWALLET",
            payment_channel: "ID_OVO",
            payment_attributes: {
              mobile_number: phoneNumber,
            },
          },
        })

        if (res.isFailure) {
          setErrorText("Terjadi kesalahan, silahkan coba lagi")
          setIsSubmitting(false)
        } else {
          setIsListening(true)
          setTimeoutResponse(31)

          const setError = () => setErrorText("Terjadi kesalahan, silahkan coba lagi")
          // eslint-disable-next-line react-hooks/rules-of-hooks
          useEWalletPaymentSocket({
            checkoutId,
            productId: productId,
            handleError: setError,
          })
        }
      } catch (error) {
        setErrorText("Terjadi kesalahan, silahkan coba lagi")
        setIsSubmitting(false)
      }
    }
  }

  return (
    <Modal show={show} onClose={onHide}>
      <div className="modal_ovo__body">
        <FlexBox justify="center" align="center" direction="col" className="text-center font-semibold">
          <div>Pembayaran dengan OVO</div>
          <Image width={64} height={64} className="object-contain" src={`${HARISENIN_PUBLIC_LOGO}/logo_ovo.png`} alt="ovo" />
        </FlexBox>
        <div className="text-center text-lg mb-4">
          <div>Jumlah tagihan kamu</div>
          <div className="font-semibold">{priceFormatter(price)}</div>
        </div>

        {isListening ? (
          <FlexBox direction="col" align="center" className="mt-4 mb-5">
            <Spinner/>
            <span className="mt-4 font-semibold text-base text-center">
              Silahkan buka aplikasi OVO kamu untuk melanjutkan pembayaran
            </span>
            <br />
            {timeoutResponse > 1 ? (
              <small>Tunggu {timeoutResponse - 1} detik untuk mengulangi pembayaran</small>
            ) : (
              <small>
                Klik{" "}
                <span className="link" onClick={handleSubmit}>
                  disini
                </span>{" "}
                mengulangi pembayaran atau{" "}
                <span onClick={onHide} className="text-green cursor-pointer">
                  pilih metode pembayaran lain
                </span>
              </small>
            )}
          </FlexBox>
        ) : (
          <div>
            {/*TODO benerin dlu ini, typesafety nya berantakan*/}
            <ManualPhoneNumberInput
              className="mb-5"
              // value={phoneNumber}
              placeholder="Nomor telepon"
              onChange={(e: any) => setPhoneNumber(e)}
              error={errorText}
            />
            <RegularButton
              disabled={isSubmitting}
              onClick={handleSubmit}
              variant="solid"
              color="green"
              isSubmitting={isSubmitting}
              className="w-full py-[10px] px-0 font-medium "
            >
              Bayar Sekarang
            </RegularButton>
          </div>
        )}

        <div className="mt-5">
          <p>Apabila transaksi gagal, silahkan coba cara berikut</p>
          <ol>
            <li>Masukkan kembali nomor telepon dan coba kirim kembali pembayaran</li>
            <li>Bersihkan cache aplikasi OVO kamu</li>
            <li>
              Apabila transaksi masih gagal, silahkan coba kembali dengan metode pembayaran lain
            </li>
          </ol>
          <p>Kamu TIDAK akan dikenakan biaya dua kali untuk mencoba kembali pembayaran</p>
        </div>
      </div>
      <div className="flex justify-center">
        <div className="text-grey-97">
          Powered by <Image width={32} height={16} className="object-contain" src={`${HARISENIN_PUBLIC_LOGO}/logo_xendit.png`} alt="xendit" />
        </div>
      </div>
    </Modal>
  )
}

export default OvoModal
=======
import { FC, useEffect, useState } from "react"
import { Modal, ModalProps, Spinner } from "react-bootstrap"
import { HARISENIN_PUBLIC_LOGO } from "../../../../constants/pictures"
import { priceFormatter } from "../../../../lib/utils/method"
import { RegularButton } from "../../../modules/buttons"

import { useEWalletPaymentSocket } from "../../../../lib/utils/hooks/useWebsocket"
import ManualPhoneNumberInput from "../../../modules/input/ManualPhoneNumberInput"
import CheckoutService from "../../../../lib/services/checkout.services"

import style from "../styles/checkout.module.scss"

export interface OvoModalProps extends ModalProps {
  checkoutId?: string
  price?: number
  productId?: string
}

export const OvoModal: FC<OvoModalProps> = ({ show, onHide, checkoutId, productId, price }) => {
  const [phoneNumber, setPhoneNumber] = useState("")
  const [timeoutResponse, setTimeoutResponse] = useState(0)

  const [isSubmitting, setIsSubmitting] = useState(false)
  const [isListening, setIsListening] = useState(false)
  const [errorText, setErrorText] = useState("")

  const checkout = new CheckoutService()

  useEffect(() => {
    timeoutResponse > 0 &&
      setTimeout(() => {
        setTimeoutResponse(timeoutResponse - 1)
      }, 1000)
  }, [timeoutResponse])

  const handleSubmit = async () => {
    if (phoneNumber.length < 12) {
      setErrorText("Pastikan kode negara di pilih, dan nomor telepon minimal 10 digit")
    } else if (phoneNumber.length > 15) {
      setErrorText("Nomor telepon maksimal 13 karakter")
    } else {
      try {
        setErrorText("")
        setIsSubmitting(true)

        const res = await checkout.submitPayment({
          checkoutId,
          body: {
            payment_method: "EWALLET",
            payment_channel: "ID_OVO",
            payment_attributes: {
              mobile_number: phoneNumber,
            },
          },
        })

        if (res.isFailure) {
          setErrorText("Terjadi kesalahan, silahkan coba lagi")
          setIsSubmitting(false)
        } else {
          setIsListening(true)
          setTimeoutResponse(31)

          const setError = () => setErrorText("Terjadi kesalahan, silahkan coba lagi")
          // eslint-disable-next-line react-hooks/rules-of-hooks
          useEWalletPaymentSocket({
            checkoutId,
            productId: productId,
            handleError: setError,
          })
        }
      } catch (error) {
        setErrorText("Terjadi kesalahan, silahkan coba lagi")
        setIsSubmitting(false)
      }
    }
  }

  return (
    <Modal show={show} onHide={onHide} keyboard={false} centered>
      <Modal.Body className="modal_ovo__body">
        <div className={style["modal_ovo__header"]}>
          <div>Pembayaran dengan OVO</div>
          <img src={`${HARISENIN_PUBLIC_LOGO}/logo_ovo.png`} alt="ovo" />
        </div>
        <div className={style["modal_ovo__body__price"]}>
          <div>Jumlah tagihan kamu</div>
          <div className={style["modal_ovo__body__price__amount"]}>{priceFormatter(price)}</div>
        </div>

        {isListening ? (
          <div className={style["modal_ovo__body__listening"]}>
            <Spinner animation="grow" variant="success" />
            <span className={style["modal_ovo__body__listening__text"]}>
              Silahkan buka aplikasi OVO kamu untuk melanjutkan pembayaran
            </span>
            <br />
            {timeoutResponse > 1 ? (
              <small>Tunggu {timeoutResponse - 1} detik untuk mengulangi pembayaran</small>
            ) : (
              <small>
                Klik{" "}
                <span className="link" onClick={handleSubmit}>
                  disini
                </span>{" "}
                mengulangi pembayaran atau{" "}
                <span onClick={onHide} className={style["modal_ovo__body__listening__link"]}>
                  pilih metode pembayaran lain
                </span>
              </small>
            )}
          </div>
        ) : (
          <div className={style["modal_ovo__body__input"]}>
            {/*TODO benerin dlu ini, typesafety nya berantakan*/}
            <ManualPhoneNumberInput
              className={style["modal_ovo__body__input__phone"]}
              // value={phoneNumber}
              placeholder="Nomor telepon"
              onChange={(e: any) => setPhoneNumber(e)}
              error={errorText}
            />
            <RegularButton
              disabled={isSubmitting}
              onClick={handleSubmit}
              type="solid"
              color="green"
              isSubmitting={isSubmitting}
            >
              Bayar Sekarang
            </RegularButton>
          </div>
        )}

        <div className={style["modal_ovo__body__notes"]}>
          <p>Apabila transaksi gagal, silahkan coba cara berikut</p>
          <ol>
            <li>Masukkan kembali nomor telepon dan coba kirim kembali pembayaran</li>
            <li>Bersihkan cache aplikasi OVO kamu</li>
            <li>
              Apabila transaksi masih gagal, silahkan coba kembali dengan metode pembayaran lain
            </li>
          </ol>
          <p>Kamu TIDAK akan dikenakan biaya dua kali untuk mencoba kembali pembayaran</p>
        </div>
      </Modal.Body>
      <Modal.Footer className={"modal_ovo__footer"}>
        <div className={style["modal_ovo__footer__credit"]}>
          Powered by <img src={`${HARISENIN_PUBLIC_LOGO}/logo_xendit.png`} alt="xendit" />
        </div>
      </Modal.Footer>
    </Modal>
  )
}

export default OvoModal
>>>>>>> 94ccec2 (setup fix):src/ui/components/checkout/shared/OvoModal.tsx
