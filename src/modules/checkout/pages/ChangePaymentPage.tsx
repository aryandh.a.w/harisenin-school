import React, { useEffect, useState } from "react"
import Helmet from "@helpers/Helmet"
import { Header } from "@components/header"

import BasicCard from "@components/card/BasicCard"
import { priceFormatter } from "@lib/functions"
import { RegularButton } from "@components/buttons"
import { useRouter } from "next/router"
import CheckoutService from "@services/checkout.services"
import { AvailablePayment, CheckoutCart, DiscountDetail, PaymentOption } from "@interfaces/checkout"
import clsx from "clsx"
import { HARISENIN_PUBLIC_LOGO } from "@constants/pictures"
import OvoModal from "../shared/OvoModal"
import QrModal from "../shared/QrModal"
import CreditCardModal from "../shared/CreditCardModal"
import { isBrowser } from "react-device-detect"
import { useEWalletPaymentSocket } from "@hooks/useWebsocket"
import { container, FlexBox } from "@components/wrapper"
import Image from "next/image"
import { Spinner } from "@components/misc"

type ModalType = "none" | "qr" | "ovo" | "credit_card"

function ChangePaymentPage() {
  const [isFetching, setIsFetching] = useState(true)
  const [cart, setCart] = useState<null | CheckoutCart>(null)
  const [productId, setProductId] = useState("")
  const [discount, setDiscount] = useState<null | DiscountDetail>(null)
  const [discountDeal, setDiscountDeal] = useState(0)
  const [paymentOptions, setPaymentOptions] = useState<AvailablePayment | null>(null)

  const [adminFee, setAdminFee] = useState(0)
  const [selectedChannel, setSelectedChannel] = useState("")
  const [selectedMethod, setSelectedMethod] = useState("")
  const [totalPrice, setTotalPrice] = useState(0)
  const [qrCode, setQrCode] = useState("")

  const [modalOpen, setModalOpen] = useState<ModalType>("none")

  const [isListening, setIsListening] = useState(false)
  const [isSubmittingPayment, setIsSubmittingPayment] = useState(false)
  const [errorMessage, setErrorMessage] = useState("")

  const checkout = new CheckoutService()
  const router = useRouter()
  const { id } = router.query

  useEffect(() => {
    const getData = async () => {
      try {
        const res = await checkout.getCheckoutDetail(id as string)

        if (res.isSuccess) {
          const data = res.getValue()
          setCart(data.order_cart[0])
          setAdminFee(data.order_fee)
          setTotalPrice(data.order_payment.initial_amount)
          setSelectedChannel(data.order_payment.payment_channel)
          setSelectedMethod(data.order_payment.payment_method)
          setProductId(data.order_cart[0].id)
          if (data.discount) {
            setDiscount(data.discount.discount)
            setDiscountDeal(data.discount.discount_deal)
          }

          const res2 = await checkout.getAvailablePayment(data.order_cart[0].school_programs[0].id)

          if (res2.isSuccess) {
            const data2 = res2.getValue()
            setPaymentOptions(data2)
          }
        }

        setIsFetching(false)
      } catch (e) {
        setIsFetching(false)
      }
    }

    if (id) {
      getData()
    }
  }, [router])

  const handleCloseModal = () => {
    setModalOpen("none")
    setIsSubmittingPayment(false)
  }

  const credits = ["visa", "mastercard", "jcb"]

  // Loading state
  if (isFetching) {
    return (
      <BasicCard className="flex justify-center items-center h-[40vw] mb-10">
        <Spinner />
      </BasicCard>
    )
  }

  const handleSelectPayment = (option: PaymentOption) => () => {
    if (cart) {
      setTotalPrice(cart.school_programs[0].program_price + option.payment_fee)
      setAdminFee(option.payment_fee)
      setSelectedChannel(option.payment_channel)
      setSelectedMethod(option.payment_method)
    }
  }

  const handleErrorListening = () => {
    setErrorMessage("Terjadi Kesalahan saat proses pembayaran. Silahkan coba lagi")
    setIsSubmittingPayment(false)
    setIsListening(false)
  }

  const handleSubmitPayment = async () => {
    // this is host url for redirect url
    const host = window.location.origin

    if (selectedChannel === "ID_OVO") {
      return setModalOpen("ovo")
    }

    if (selectedChannel === "CREDIT_CARD") {
      return setModalOpen("credit_card")
    }

    // try catch for payment
    try {
      setIsSubmittingPayment(true)
      const resPayment = await checkout.submitPayment({
        checkoutId: id as string,
        body: {
          payment_method: selectedMethod,
          payment_channel: selectedChannel,
          ...(selectedMethod === "EWALLET" && {
            payment_attributes: {
              success_redirect_url: `${host}/school/invoice/${id}/success`,
            },
          }),
        },
      })

      if (resPayment.isSuccess) {
        // Will activate websocket for e-wallet payment callback
        if (selectedMethod === "EWALLET") {
          const data = resPayment.getValue()
          const payment = data.payment
          const checkoutUrl =
            payment.actions.desktop_web_checkout_url ??
            payment.actions.mobile_web_checkout_url ??
            payment.actions.mobile_deeplink_checkout_url

          if (selectedChannel === "ID_SHOPEEPAY" && isBrowser) {
            setQrCode(payment.actions.qr_checkout_string)
            setModalOpen("qr")
            setIsSubmittingPayment(false)
          }

          window.open(checkoutUrl, "_blank")

          // eslint-disable-next-line react-hooks/rules-of-hooks
          useEWalletPaymentSocket({
            checkoutId: id,
            productId: productId,
            handleError: handleErrorListening,
          })
        } else {
          // Will redirect to invoice page
          window.location.assign(`/school/invoice/${id}/unpaid?productId=${productId}`)
        }
      } else {
        // catch error for payment process
        setIsSubmittingPayment(false)
        setErrorMessage(
          "Terjadi kesalahan saat proses pembayaran. Refresh ulang halaman dan Silahkan coba lagi"
        )
      }
    } catch (e) {
      // catch error for payment process
      setIsSubmittingPayment(false)
      setErrorMessage(
        "Terjadi kesalahan saat proses pembayaran. Refresh ulang halaman dan Silahkan coba lagi"
      )
    }
  }

  return (
    <>
      <Helmet title="Harisenin.com: Ubah Metode pembayaran" />
      <Header />

      <div className="bg-primary-grey mx-auto">
        <div className={clsx(container, "!max-w-[700px] mx-auto sm:px-0")}>
          <main className={clsx("flex flex-col gap-10 sm:py-30", "py-6")}>
            <FlexBox direction="col" className="gap-5">
              <h2
                className={clsx("sm:text-2xl font-semibold text-grey-22", "text-lg")}
                id="payment_method"
              >
                Ubah Metode Pembayaran
              </h2>
              <div className="bg-white py-4 sm:px-10 px-4 rounded-10">
                <div className={clsx("sm:text-lg font-semibold text-black mb-2.5", "text-sm")}>
                  Transfer Bank
                </div>
                <div>
                  {paymentOptions?.BANK_TRANSFER && (
                    <FlexBox
                      direction="row"
                      className={clsx("flex-wrap sm:gap-3 sm-only:gap-x-5 sm-only:gap-y-2.5")}
                    >
                      {paymentOptions?.BANK_TRANSFER?.map((value, index) => (
                        <FlexBox
                          direction="row"
                          className="gap-2 items-center sm-only:w-[calc(50%-10px)]"
                          key={index}
                        >
                          <button
                            title={value.payment_channel}
                            className={clsx(
                              "flex justify-center items-center border rounded-[4px] border-grey-c4 sm:p-2 sm:w-[140px] sm:h-[52px] hover:cursor-pointer",
                              "w-full h-[40px] p-1",
                              selectedChannel === value.payment_channel &&
                                "ring-2 ring-green border-transparent"
                            )}
                            key={index}
                            onClick={handleSelectPayment(value)}
                          >
                            <Image
                              width={90}
                              height={34}
                              className="object-contain"
                              src={value.payment_channel_logo}
                              alt={value.payment_channel}
                            />
                          </button>
                        </FlexBox>
                      ))}
                    </FlexBox>
                  )}
                </div>
              </div>

              {/*/!* Will be hidden if e-wallet method is disabled on back-end *!/*/}
              {paymentOptions?.EWALLET && (
                <div className="bg-white py-4 sm:px-10 px-4 rounded-10">
                  {" "}
                  <div className={clsx("sm:text-lg font-semibold text-black mb-2.5", "text-sm")}>
                    E-Wallet
                  </div>
                  <FlexBox
                    direction="row"
                    className={clsx("flex-wrap sm:gap-3 sm-only:gap-x-5 sm-only:gap-y-2.5")}
                  >
                    {paymentOptions?.EWALLET.map((value, index) => (
                      <FlexBox
                        direction="row"
                        className="gap-2 items-center sm-only:w-[calc(50%-10px)]"
                        key={index}
                      >
                        <button
                          title={value.payment_channel}
                          className={clsx(
                            "flex justify-center items-center border rounded-[4px] border-grey-c4 sm:p-2 sm:w-[140px] sm:h-[52px] hover:cursor-pointer",
                            "w-full h-[40px] p-2",
                            selectedChannel === value.payment_channel &&
                              "ring-2 ring-green border-transparent"
                          )}
                          key={index}
                          onClick={handleSelectPayment(value)}
                        >
                          <Image
                            width={90}
                            height={34}
                            className="object-contain"
                            src={value.payment_channel_logo}
                            alt={value.payment_channel}
                          />
                        </button>
                      </FlexBox>
                    ))}
                  </FlexBox>
                </div>
              )}

              {paymentOptions?.CREDIT_CARD ? (
                <div className="bg-white py-4 sm:px-10 px-4 rounded-10">
                  <div className={clsx("sm:text-lg font-semibold text-black mb-2.5", "text-sm")}>
                    Credit Card
                  </div>
                  <FlexBox direction="row" className={clsx("flex-wrap gap-3")}>
                    <FlexBox direction="row" className="gap-2 items-center ">
                      <button
                        className={clsx(
                          "flex justify-center border rounded-[4px] border-grey-c4 sm:p-2 gap-3 sm:w-[170px] sm:h-[50px] hover:cursor-pointer",
                          "w-[150px] h-[40px] py-1 px-2.5",
                          selectedMethod === "CREDIT_CARD" && "ring-2 ring-green border-transparent"
                        )}
                        onClick={handleSelectPayment(paymentOptions?.CREDIT_CARD[0])}
                      >
                        {credits.map((value, index) => (
                          <Image
                            width={40}
                            height={34}
                            className="object-contain my-auto"
                            src={`${HARISENIN_PUBLIC_LOGO}/logo_${value}.png`}
                            alt=""
                            key={index}
                          />
                        ))}
                      </button>
                    </FlexBox>
                  </FlexBox>
                </div>
              ) : null}
            </FlexBox>

            <FlexBox direction="col" className="gap-5">
              <h2 className={clsx("sm:text-2xl font-semibold text-grey-22", "text-lg")}>
                Total Pembayaran
              </h2>
              <FlexBox
                direction="col"
                className={clsx(
                  "sm:gap-10 bg-white rounded-10 py-4 sm:px-10 text-black",
                  "gap-3 px-4"
                )}
              >
                <FlexBox direction="col">
                  <FlexBox justify="between" className="gap-2 sm:text-md text-sm sm:mb-3 mb-4">
                    <div className="w-3/4">
                      {cart?.school_title} : <br />
                      {cart?.school_programs && cart?.school_programs[0]?.program_title}
                    </div>
                    <div className="w-1/4 text-right font-semibold">
                      {priceFormatter(
                        cart?.school_programs ? cart?.school_programs[0]?.program_price : 0
                      )}
                    </div>
                  </FlexBox>

                  {adminFee > 0 && (
                    <FlexBox justify="between" className="gap-2 sm:text-md text-sm">
                      <div>Biaya Admin</div>
                      <div className="w-1/4 text-right font-semibold">
                        {priceFormatter(adminFee)}
                      </div>
                    </FlexBox>
                  )}

                  {/* Will be appeared when there is promo applied */}
                  {Boolean(discountDeal) && (
                    <FlexBox
                      justify="between"
                      className={clsx(
                        "font-semibold sm:text-lg leading-6 text-grey-22",
                        "text-base"
                      )}
                    >
                      <FlexBox className="sm:gap-5 gap-2">{discount.discount_name}</FlexBox>
                      <div className="text-red">-{priceFormatter(discountDeal)}</div>
                    </FlexBox>
                  )}

                  <hr className="h-0.5 border-none bg-blue" />

                  <FlexBox
                    justify="between"
                    className={clsx("font-semibold sm:text-lg text-grey-22", "text-base")}
                  >
                    <div>Total</div>
                    <div className="text-blue">{priceFormatter(totalPrice)}</div>
                  </FlexBox>
                </FlexBox>
              </FlexBox>
            </FlexBox>

            {/* Listening animation when waiting callback on e-wallet payment */}
            {isListening && (
              <div className="listening-component d-flex align-items-center">
                <Spinner />
                <div className="ml-3">Menunggu respon dari pembayaran</div>
              </div>
            )}

            {/* Button for submitting */}
            <FlexBox direction="col" className={clsx("px-20", "sm-only:px-0")}>
              <RegularButton
                className="py-3 bg-blue"
                variant="solid"
                onClick={handleSubmitPayment}
                isSubmitting={isSubmittingPayment}
              >
                Bayar Sekarang
              </RegularButton>
            </FlexBox>

            {/* Error text */}
            <div className="mt-4 text-red">{errorMessage}</div>
          </main>
        </div>
      </div>

      <OvoModal
        show={modalOpen === "ovo"}
        onHide={handleCloseModal}
        checkoutId={id as string}
        productId={productId}
        price={totalPrice}
      />

      <QrModal
        show={modalOpen === "qr"}
        onHide={handleCloseModal}
        checkoutId={id as string}
        productId={productId}
        qrString={qrCode}
      />

      <CreditCardModal
        show={modalOpen === "credit_card"}
        onHide={handleCloseModal}
        checkoutId={id as string}
        productId={productId}
        price={totalPrice}
      />
    </>
  )
}

export default ChangePaymentPage

interface PropsPaymentRadioButton {
  paymentMethod: PaymentOption
  onChange: (v: any) => void
  value: string
}

export function PaymentRadioButton({ paymentMethod, onChange, value }: PropsPaymentRadioButton) {
  return (
    <FlexBox direction="row" className="gap-2 items-center sm-only:w-[calc(50%-10px)]">
      <input
        id={paymentMethod.payment_channel}
        type="radio"
        name="paymentMethod"
        className={clsx("hidden")}
        value={paymentMethod.payment_channel}
        onChange={onChange}
      />
      <label
        htmlFor={paymentMethod.payment_channel}
        className={clsx(
          "flex justify-center border rounded-[4px] border-grey-c4 sm:p-2 sm:w-[140px] sm:h-[52px] hover:cursor-pointer",
          "w-full h-[34px] p-1",
          value === paymentMethod.payment_channel && "ring-2 ring-green border-transparent"
        )}
      >
        <Image
          src={paymentMethod.payment_channel_logo ?? `${HARISENIN_PUBLIC_LOGO}/logo_bca.png`}
          width={90}
          height={34}
          className="object-contain"
          alt="bca"
        />
      </label>
    </FlexBox>
  )
}
