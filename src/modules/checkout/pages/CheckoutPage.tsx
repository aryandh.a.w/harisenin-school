<<<<<<< HEAD:src/modules/checkout/pages/CheckoutPage.tsx
import React, {useEffect, useState} from "react"

import Helmet from "@helpers/Helmet"
import useCheckout from "@hooks/useCheckout"

import {CheckoutSummary} from "@interfaces/checkout-state"

import {Header} from "@components/header"
import {HeaderOrder} from "@components/header/HeaderOrder"
import CheckoutPayment from "@modules/checkout/components/CheckoutPayment"
import FormHandlerCard from "@modules/checkout/components/FormHandlerCard"

import {useRouter} from "next/router"
import clsx from "clsx"
import {container} from "@components/wrapper"

const CheckoutPage = () => {
  const [checkoutSummary, setCheckoutSummary] = useState<CheckoutSummary | null>(null)

  const router = useRouter()
  const pathName = router.pathname
  const isProClass = pathName.includes("proclass")

  // Hooks for checkout
  const { checkoutState, checkoutDispatch, isLoading } = useCheckout(
    checkoutSummary?.program_id,
    checkoutSummary
  )

  useEffect(() => {
    let storage

    if (isProClass) {
      storage = window.localStorage.getItem("hsp_checkout")
    } else {
      storage = window.localStorage.getItem("hs_checkout")
    }

    if (storage) {
      const data: CheckoutSummary = JSON.parse(storage)
      setCheckoutSummary(data)
    }
  }, [router])

  // Scrolling to form component
  const executeScroll = (id: string) => {
    const ele = document.getElementById(id)
    ele.scrollIntoView({ behavior: "smooth" })
  }

  // -- Const for checking form state --
  // const initialForm = {
  //   ...checkoutState.form,
  //   ...{ form_status: checkoutState.form_status },
  // }
  // const initialFileForm = {
  //   ...checkoutState.form_file,
  //   ...{ form_status: checkoutState.form_status },
  // }

  return (
    <>
      <Helmet title="Harisenin.com: Detail Checkout" />
      <Header />
      <HeaderOrder step={1} />

      <div className="grey-background">
        {/* Pre tag for checking form state */}
        <pre
          style={{
            position: "fixed",
            right: 0,
            top: 0,
            fontSize: "10px",
            zIndex: 1000,
          }}
        >
          {/*{JSON.stringify(initialForm, null, 2)}*/}
          {/*{JSON.stringify(initialFileForm, null, 2)}*/}
          {/*{JSON.stringify(checkoutState, null, 2)}*/}
        </pre>

        {checkoutSummary && (
          <div className={clsx(container, "pt-10 !max-w-[700px] pb-2")}>
            {/* <h1>Detail Orderan</h1>
            <OrderSummary summary={checkoutSummary} /> */}

            <h1 className={clsx("sm:text-xl sm:mb-5 font-semibold", "text-lg mb-2")}>
              Form Pendaftaran {checkoutSummary.school_title}
            </h1>
            <FormHandlerCard
              state={checkoutState}
              dispatch={checkoutDispatch}
              isLoading={isLoading}
              summary={checkoutSummary}
              isProClass={isProClass}
            />

            {/* <h1 className={clsx("sm:text-xl mb-5 font-semibold")}>Pembayaran</h1> */}
            <CheckoutPayment
              checkoutState={checkoutState}
              dispatch={checkoutDispatch}
              summary={checkoutSummary}
              scrollToForm={executeScroll}
              isProClass={isProClass}
            />
          </div>
        )}
      </div>
    </>
  )
}

export default CheckoutPage
=======
import React, { useEffect, useState } from "react"
import { Container } from "react-bootstrap"

import Helmet from "../../../lib/utils/Helmet"
import useCheckout from "../../../lib/utils/hooks/useCheckout"

import { CheckoutSummary } from "../../../constants/interfaces/checkout-state"

import HeaderHome from "../../modules/header/HeaderHome"
import HeaderOrder from "../../modules/header/HeaderOrder"
import CheckoutPayment from "./CheckoutPayment"
import FormHandlerCard from "./FormHandlerCard"

import style from "./styles/checkout.module.scss"
import { useRouter } from "next/router"

const CheckoutPage = () => {
  const [checkoutSummary, setCheckoutSummary] = useState<CheckoutSummary | null>(null)

  const router = useRouter()
  const pathName = router.pathname
  const isProClass = pathName.includes("proclass")

  // Hooks for checkout
  const { checkoutState, checkoutDispatch, isLoading } = useCheckout(
    checkoutSummary?.program_id,
    checkoutSummary
  )

  useEffect(() => {
    let storage

    if (isProClass) {
      storage = window.localStorage.getItem("hsp_checkout")
    } else {
      storage = window.localStorage.getItem("hs_checkout")
    }

    if (storage) {
      const data = JSON.parse(storage)
      setCheckoutSummary(data)
    }
  }, [router])

  // Scrolling to form component
  const executeScroll = (id: string) => {
    const ele = document.getElementById(id)
    ele.scrollIntoView({ behavior: "smooth" })
  }

  // -- Const for checking form state --
  // const initialForm = {
  //   ...checkoutState.form,
  //   ...{ form_status: checkoutState.form_status },
  // }
  // const initialFileForm = {
  //   ...checkoutState.form_file,
  //   ...{ form_status: checkoutState.form_status },
  // }

  return (
    <>
      <Helmet title="Harisenin.com: Detail Checkout" />
      <HeaderHome />
      <HeaderOrder step={1} />

      <div className="grey-background">
        {/* Pre tag for checking form state */}
        <pre
          style={{
            position: "fixed",
            right: 0,
            top: 0,
            fontSize: "10px",
            zIndex: 1000,
          }}
        >
          {/*{JSON.stringify(initialForm, null, 2)}*/}
          {/*{JSON.stringify(initialFileForm, null, 2)}*/}
          {/*{JSON.stringify(checkoutState, null, 2)}*/}
        </pre>

        {checkoutSummary && (
          <Container className={`${style["checkout"]} pb-2`}>
            {/* <h1>Detail Orderan</h1>
            <OrderSummary summary={checkoutSummary} /> */}

            <h1>Form Pendaftaran {checkoutSummary.school_title}</h1>
            <FormHandlerCard
              state={checkoutState}
              dispatch={checkoutDispatch}
              isLoading={isLoading}
              summary={checkoutSummary}
              isProClass={isProClass}
            />

            <h1>Pembayaran</h1>
            <CheckoutPayment
              checkoutState={checkoutState}
              dispatch={checkoutDispatch}
              summary={checkoutSummary}
              scrollToForm={executeScroll}
              isProClass={isProClass}
            />
          </Container>
        )}
      </div>
    </>
  )
}

export default CheckoutPage
>>>>>>> 94ccec2 (setup fix):src/ui/components/checkout/CheckoutPage.tsx
