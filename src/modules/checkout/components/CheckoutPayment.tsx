<<<<<<< HEAD:src/modules/checkout/components/CheckoutPayment.tsx
import React, {Dispatch, FC, useState} from "react"
import {isBrowser} from "react-device-detect"
import {FaTimes} from "react-icons/fa"
import {ValidationError} from "yup"
import {CheckoutAction, CheckoutActionType, CheckoutState, CheckoutSummary,} from "@interfaces/checkout-state"
import {DynamicObject} from "@interfaces/ui"
import {HARISENIN_PUBLIC_LOGO} from "@constants/pictures"
import UploadRepo from "@lib/api/server-side/upload.repo"
import CheckoutService from "@services/checkout.services"
import {useEWalletPaymentSocket} from "@hooks/useWebsocket"
import {priceFormatter} from "@lib/functions"
import {RegularButton, TextButton} from "@components/buttons"
import BasicCard from "@components/card/BasicCard"
import CreditCardModal from "../shared/CreditCardModal"
import {validationHandler} from "../FormValidation"
// import ModalPromo from "./ModalPromo"
import OvoModal from "../shared/OvoModal"
import QrModal from "../shared/QrModal"

import clsx from "clsx"
import {ManualInput} from "@components/input"
import {FlexBox} from "@components/wrapper"
import Image from "next/image"
import {Spinner} from "@components/misc"
import {trackAddPaymentInfo, trackBeginCheckout} from "@utils/analytics";
import process from "process";

export interface CheckoutPaymentProps {
  checkoutState: CheckoutState
  dispatch: Dispatch<CheckoutAction>
  scrollToForm: (id: string) => void
  summary: CheckoutSummary
  isProClass: boolean
}

const FORM_LABEL = [
  "user_batch_request",
  "form_status",
  "user_fullname",
  "user_email",
  "user_phone_number",
  "user_university",
  "user_faculty",
  "user_majors",
  "user_job",
  "user_desired_job_sector",
  "user_desired_job",
  "user_linkedin_url",
]

const INSTALLMENT = [
  {
    month: 3,
    interest: 0,
  },
  {
    month: 6,
    interest: 2.6,
  },
  {
    month: 12,
    interest: 2.6,
  },
]

const CheckoutPayment: FC<CheckoutPaymentProps> = ({
  checkoutState,
  dispatch,
  scrollToForm,
  summary,
  isProClass,
}) => {
  const [isSubmittingPayment, setIsSubmittingPayment] = useState(false)
  const [isListening, setIsListening] = useState(false)
  const [isSubmittingPromo, setIsSubmittingPromo] = useState(false)
  const [qrCode, setQrCode] = useState("")
  const [checkoutId, setCheckoutId] = useState("")

  const upload = new UploadRepo()
  const checkout = new CheckoutService()

  // Handler for error when listening
  const handleErrorListening = () => {
    dispatch({
      type: CheckoutActionType.ErrorSubmitting,
      payload: "Terjadi Kesalahan saat proses pembayaran. Silahkan coba lagi",
    })
    setIsSubmittingPayment(false)
    setIsListening(false)
  }

  const handleValidation = async () => {
    const validations = []

    // Choose validation based on form status
    const validation = validationHandler(checkoutState.form_status)

    const form: DynamicObject = {}
    let firstErrorId = ""

    try {
      const formRaw = checkoutState.form

      for (const formRawKey in formRaw) {
        if (Object.prototype.hasOwnProperty.call(formRaw, formRawKey)) {
          form[formRawKey] = formRaw[formRawKey].value
        }
      }
      form.form_status = checkoutState.form_status

      await validation.validate(form, { abortEarly: false })
      delete form.form_status

      dispatch({
        type: CheckoutActionType.ErrorChecking,
        payload: { errorType: "form", message: "" },
      })
    } catch (e: any) {
      // catch error for form validation
      const error: ValidationError = e
      const errors = error.inner

      validations.push("form")
      setIsSubmittingPayment(false)
      const errorLabel = errors.map((value) => {
        return value.path
      })

      for (const label of FORM_LABEL) {
        if (errorLabel.includes(label) && !firstErrorId) {
          firstErrorId = label
        }
      }

      // Looping for error form text
      for (const errorsKey of errors) {
        // @ts-ignore
        dispatch({
          type: CheckoutActionType.FormErrorHandler,
          payload: {
            name: errorsKey?.path,
            error: errorsKey.message,
            type: "field",
          },
        })

        dispatch({
          type: CheckoutActionType.ErrorChecking,
          payload: { errorType: "form", message: "Beberapa kolom form belum diisi" },
        })
      }

      scrollToForm(firstErrorId)
    }

    if (!isProClass) {
      if (
        checkoutState.form_file.identity_card.name === "File..." ||
        (!checkoutState.form_file.identity_card.file &&
          checkoutState.form_file.identity_card.name === "File...")
      ) {
        dispatch({
          type: CheckoutActionType.FormErrorHandler,
          payload: {
            name: "identity_card",
            error: "*Wajib diisi",
            type: "file",
          },
        })

        if (!firstErrorId) {
          firstErrorId = "identity_card"
          scrollToForm("identity_card")
        }

        dispatch({
          type: CheckoutActionType.ErrorChecking,
          payload: { errorType: "form", message: "Beberapa kolom form belum diisi" },
        })
        validations.push("file")
      } else {
        dispatch({
          type: CheckoutActionType.ErrorChecking,
          payload: { errorType: "form", message: "" },
        })
      }
    }

    // First check if payment is selected
    if (checkoutState.gross_price > 0 && checkoutState.admin_fee <= 0) {
      dispatch({
        type: CheckoutActionType.ErrorChecking,
        payload: { errorType: "paymentOption", message: "Silahkan pilih metode pembayaran" },
      })

      validations.push("options")

      if (!firstErrorId) {
        scrollToForm("payment_option")
      }
    } else {
      dispatch({
        type: CheckoutActionType.ErrorChecking,
        payload: { errorType: "paymentOption", message: "" },
      })
    }

    return { isValidated: !validations.length, form }
  }

  async function handleSubmit() {
    let checkoutStep = 0

    try {
      setIsSubmittingPayment(true)
      dispatch({
        type: CheckoutActionType.ErrorSubmitting,
        payload: "",
      })

      const validation = await handleValidation()

      // Form and Payment option validation
      if (!validation.isValidated) {
        return setIsSubmittingPayment(false)
      }

      // Create Checkout id
      checkoutStep = 1

      const res = await checkout.submitCheckout({
        productId: summary.school_id,
        serviceId: summary.program_id,
      })

      if (res.isFailure) {
        const errorMessage = res.error.message

        dispatch({
          type: CheckoutActionType.ErrorSubmitting,
          payload: errorMessage,
        })
        return setIsSubmittingPayment(false)
      }

      // Create Submit Form
      checkoutStep = 2

      const data = res.getValue()
      setCheckoutId(data.checkout_id)
      const id = data.checkout_id
      const form = validation.form as any

      const resForm = await checkout.submitForm({ checkoutId: id, form })

      if (resForm.isFailure) {
        dispatch({
          type: CheckoutActionType.ErrorSubmitting,
          payload:
            "Terjadi kesalahan saat mengirim form, pastikan semua data sudah terisi dengan benar. Silahkan coba lagi",
        })

        return setIsSubmittingPayment(false)
      }

      // Upload file form if there are any
      checkoutStep = 3
      if (
        !isProClass &&
        (checkoutState.form_file.identity_card.file ||
          checkoutState.form_file.ijazah.file ||
          checkoutState.form_file.last_study_card.file ||
          checkoutState.form_file.student_card.file ||
          checkoutState.form_file.cv.file)
      ) {
        const fileForm = checkoutState.form_file
        let isEdit = false

        const fd = new FormData()
        for (const fileFormKey in fileForm) {
          if (
            Object.prototype.hasOwnProperty.call(fileForm, fileFormKey) &&
            fileForm[fileFormKey].file
          ) {
            if (fileForm[fileFormKey].id) {
              fd.append(`${fileFormKey}_id`, fileForm[fileFormKey].id)
              isEdit = true
            }

            fd.append(fileFormKey, fileForm[fileFormKey].file)
          }
        }

        const resUpload = await upload.uploadFile({
          url: `/checkout/order/${id}/registration/upload`,
          file: fd,
          method: isEdit ? "put" : "post",
          version: "v3",
        })

        if (!resUpload.ok) {
          dispatch({
            type: CheckoutActionType.ErrorSubmitting,
            payload:
              "Terjadi kesalahan saat mengunggah file. File maksimal 5 MB dan format harus PNG/JPG/PDF. Silahkan coba file lain",
          })

          return setIsSubmittingPayment(false)
        }
      }

      // Submit discount if there are any
      checkoutStep = 4
      if (checkoutState.discount_amount) {
        await checkout.submitDiscount({
          checkoutId: data.checkout_id,
          code: checkoutState.applied_promo_code,
        })
      }

      // ovo payment modal check
      if (checkoutState.selected_channel === "ID_OVO") {
        setIsSubmittingPayment(false)
        return dispatch({
          type: CheckoutActionType.OpenModal,
          payload: "ovo",
        })
      }

      if (checkoutState.selected_method === "CREDIT_CARD") {
        setIsSubmittingPayment(false)
        return dispatch({
          type: CheckoutActionType.OpenModal,
          payload: "credit_card",
        })
      }

      // Submit payment
      checkoutStep = 5

      // this is host url for redirect url
      const host = window.location.origin

      let planId = ""

      if (checkoutState.selected_method === "PAY_LATER") {
        const paylater = await checkout.submitPayLaterPayment({
          checkoutId: id,
          channelCode: checkoutState.selected_channel,
        })

        if (paylater.isFailure) {
          setIsSubmittingPayment(false)
          return dispatch({
            type: CheckoutActionType.ErrorSubmitting,
            payload:
              "Terjadi kesalahan saat proses pembayaran. Refresh ulang halaman dan Silahkan coba lagi",
          })
        }

        const result = paylater.getValue()
        planId = result.initiate_paylater.id
      }

      const resPayment = await checkout.submitPayment({
        checkoutId: id,
        body: {
          payment_method: checkoutState.selected_method,
          payment_channel: checkoutState.selected_channel,
          ...(checkoutState.selected_method === "EWALLET" && {
            payment_attributes: {
              success_redirect_url: `${host}/school/invoice/${id}/success`,
            },
          }),
          ...(checkoutState.selected_method === "PAY_LATER" && {
            plan_id: planId,
          }),
        },
      })

      if (resPayment.isFailure) {
        setIsSubmittingPayment(false)
        return dispatch({
          type: CheckoutActionType.ErrorSubmitting,
          payload:
            "Terjadi kesalahan saat proses pembayaran. Refresh ulang halaman dan Silahkan coba lagi",
        })
      }

      if (
        checkoutState.selected_method === "EWALLET" ||
        checkoutState.selected_method === "PAY_LATER"
      ) {
        trackBeginCheckout({
          currency: "IDR",
          value: summary.program_price,
          items: [
            {
              item_id: summary.school_id,
              item_name: summary.school_title,
              quantity: 1,
              item_category: isProClass ? "ProClass" : "Bootcamp",
              item_brand: "HMS",
              item_variant: summary.program_title,
              item_image_url: summary.image,
              item_url: `${process.env.HOME_URL}/school/${summary.school_slug}`,
              currency: "IDR",
              price: summary.program_price
            }
          ]
        })

        const data = resPayment.getValue()
        const payment = data.payment
        const checkoutUrl =
          payment.actions.desktop_web_checkout_url ??
          payment.actions.mobile_web_checkout_url ??
          payment.actions.mobile_deeplink_checkout_url

        if (checkoutState.selected_channel === "ID_SHOPEEPAY" && isBrowser) {
          setQrCode(payment.actions.qr_checkout_string)
          return dispatch({
            type: CheckoutActionType.OpenModal,
            payload: "qr",
          })
        }

        window.open(checkoutUrl, "_blank")

        // eslint-disable-next-line react-hooks/rules-of-hooks
        useEWalletPaymentSocket({
          checkoutId: id,
          productId: summary.school_id,
          handleError: handleErrorListening,
        })
      } else {
        trackBeginCheckout({
          currency: "IDR",
          value: summary.program_price,
          items: [
            {
              item_id: summary.school_id,
              item_name: summary.school_title,
              quantity: 1,
              item_category: isProClass ? "ProClass" : "Bootcamp",
              item_brand: "HMS",
              item_variant: summary.program_title,
              item_image_url: summary.image,
              item_url: `${process.env.HOME_URL}/school/${summary.school_slug}`,
              currency: "IDR",
              price: summary.program_price
            }
          ]
        })
        // Will redirect to invoice page
        window.location.assign(`/school/invoice/${id}/unpaid?productId=${summary.school_id}`)
      }
    } catch (e) {
      // catch general error
      setIsSubmittingPayment(false)

      switch (checkoutStep) {
        case 0:
          dispatch({
            type: CheckoutActionType.ErrorSubmitting,
            payload: "Pastikan form terisi dengan benar",
          })
          break
        case 1:
          dispatch({
            type: CheckoutActionType.ErrorSubmitting,
            payload: "Terjadi kesalahan pada proses checkout. Silahkan coba lagi",
          })
          break
        case 2:
          dispatch({
            type: CheckoutActionType.ErrorSubmitting,
            payload:
              "Terjadi kesalahan saat mengirim form, pastikan semua data sudah terisi dengan benar. Silahkan coba lagi",
          })
          break
        case 3:
          dispatch({
            type: CheckoutActionType.ErrorSubmitting,
            payload:
              "Terjadi kesalahan saat mengunggah file. File maksimal 5 MB dan format harus PNG/JPG/PDF. Silahkan coba file lain",
          })
          break
        case 4:
          dispatch({
            type: CheckoutActionType.ErrorSubmitting,
            payload:
              "Terjadi kesalahan saat menggunakan diskon. Pastikan kode yang anda gunakan sudah benar",
          })
          break
        case 5:
          dispatch({
            type: CheckoutActionType.ErrorSubmitting,
            payload: "Terjadi kesalahan saat proses pembayaran. Silahkan coba lagi",
          })
          break
        default:
          dispatch({
            type: CheckoutActionType.ErrorSubmitting,
            payload: "Terjadi kesalahan. Silahkan coba lagi",
          })
      }
    }
  }

  async function deletePromo() {
    try {
      const res = await checkout.getAvailablePayment(summary.program_id)
      if (res.isSuccess) {
        const data = res.getValue()

        dispatch({
          type: CheckoutActionType.PaymentOptionInitializer,
          payload: {
            bank_method: data.BANK_TRANSFER,
            e_wallet_method: data.EWALLET,
            ...(data.CREDIT_CARD && {
              credit_card: {
                payment_method: "CREDIT_CARD",
                payment_fee: data.CREDIT_CARD[0].payment_fee,
                payment_channel: "",
                payment_channel_logo: "",
              },
            }),
            pay_later_method: data.PAY_LATER,
          },
        })
      }
      dispatch({ type: CheckoutActionType.DeleteDiscount })
    } catch (e) {
      if (process.env.NODE_ENV === "development") {
        console.log(e)
      }
    }
  }

  async function submitPromo() {
    try {
      setIsSubmittingPromo(true)
      const res = await checkout.applyPromoCode(summary.program_id as number, {
        code: checkoutState.promo_code,
      })

      if (res.isSuccess) {
        const discount = res.getValue()
        const payments = discount.available_payment

        dispatch({
          type: CheckoutActionType.PaymentOptionInitializer,
          payload: {
            bank_method: payments.BANK_TRANSFER,
            e_wallet_method: payments.EWALLET,
            ...(payments.CREDIT_CARD && {
              credit_card: {
                payment_method: "CREDIT_CARD",
                payment_fee: payments.CREDIT_CARD[0].payment_fee,
                payment_channel_logo: "",
                payment_channel: "",
              },
            }),
            pay_later_method: payments.PAY_LATER,
          },
        })

        // Discount is applied and promo modal will be closed
        dispatch({ type: CheckoutActionType.ApplyPromo, payload: discount })
      } else {
        const data = res.error
        const message: string = data.message
        dispatch({ type: CheckoutActionType.ErrorDiscount, payload: message })
      }

      setIsSubmittingPromo(false)
    } catch (e) {
      setIsSubmittingPromo(false)
      dispatch({
        type: CheckoutActionType.ErrorDiscount,
        payload: "Terjadi kesalahan. Silahkan coba lagi",
      })
    }
  }

  function handleCloseModal() {
    dispatch({ type: CheckoutActionType.ClosePromoModal })
    setIsSubmittingPayment(false)
  }

  const credits = ["visa", "mastercard", "jcb"]

  return (
    <>
      <BasicCard className={clsx("sm:py-5 sm:px-8", "!p-4")}>
        <div className={clsx("relative mb-4 sm-only:mb-8")}>
          <div id="payment_option" className={clsx("absolute -top-24")} />
          {/* No payment method if product is free */}
          {Boolean(checkoutState.total_price) && (
            <>
              <h4 className={clsx("sm:text-xl font-semibold", "text-sm sm-only:mb-5")}>
                Pilih Metode pembayaran{" "}
                <small className="text-red">*{checkoutState.error_checking.paymentOption}</small>
              </h4>

              <hr />

              {Boolean(checkoutState?.bank_method.length) && (
                <div className={clsx("sm:mb-8 mb-3")}>
                  <div className={clsx("sm:text-lg font-medium mb-2")}>Transfer Bank</div>
                  <div className={clsx("grid sm:grid-cols-4 gap-2.5", "grid-cols-2")}>
                    {checkoutState?.bank_method?.map((value, index) => (
                      <button
                        className={clsx(
                          "h-[52px] sm:py-2.5 sm:px-4 border border-[#ececec] rounded bg-transparent flex items-center justify-center cursor-pointer font-semibold",
                          "py-2 px-3",
                          checkoutState?.selected_channel === value.payment_channel &&
                            "border-transparent ring-green ring-2"
                        )}
                        key={index}
                        id={`btn-${value.payment_channel.toLowerCase()}-hms`}
                        onClick={() => {
                          trackAddPaymentInfo({
                            currency: "IDR",
                            value: summary.program_price,
                            items: [
                              {
                                item_id: summary.school_id,
                                item_name: summary.school_title,
                                quantity: 1,
                                item_category: isProClass ? "ProClass" : "Bootcamp",
                                item_brand: "HMS",
                                item_variant: summary.program_title,
                                item_image_url: summary.image,
                                item_url: `${process.env.HOME_URL}/school/${summary.school_slug}`,
                                currency: "IDR",
                                price: summary.program_price
                              }
                            ],
                            payment_type: value.payment_method,
                            payment_method: value.payment_channel
                          })
                          dispatch({
                            type: CheckoutActionType.ChoosePayment,
                            payload: value,
                          })
                        }}
                      >
                        <Image
                          src={value.payment_channel_logo}
                          alt={value.payment_channel}
                          width={100}
                          height={100}
                          className="object-contain"
                        />
                      </button>
                    ))}
                  </div>
                </div>
              )}

              {/* Will be hidden if e-wallet method is disabled on back-end */}
              {Boolean(checkoutState?.e_wallet_method?.length) && (
                <div className={clsx("sm:mb-8 mb-3")}>
                  <div className={clsx("sm:text-lg font-medium mb-2")}>E-Wallet</div>

                  <div className={clsx("grid sm:grid-cols-4 gap-2.5", "grid-cols-2")}>
                    {checkoutState?.e_wallet_method?.map((value, index) => (
                      <button
                        className={clsx(
                          "h-[52px] sm:p-2 mr-[10px] border border-[#ececec] rounded bg-transparent flex items-center justify-center cursor-pointer font-semibold",
                          "py-2 px-3",
                          checkoutState?.selected_channel === value.payment_channel &&
                          "border-transparent ring-green ring-2"
                        )}
                        key={index}
                        id={`btn-${value.payment_channel.toLowerCase()}-hms`}
                        onClick={() => {
                          trackAddPaymentInfo({
                            currency: "IDR",
                            value: summary.program_price,
                            items: [
                              {
                                item_id: summary.school_id,
                                item_name: summary.school_title,
                                quantity: 1,
                                item_category: isProClass ? "ProClass" : "Bootcamp",
                                item_brand: "HMS",
                                item_variant: summary.program_title,
                                item_image_url: summary.image,
                                item_url: `${process.env.HOME_URL}/school/${summary.school_slug}`,
                                currency: "IDR",
                                price: summary.program_price
                              }
                            ],
                            payment_type: value.payment_method,
                            payment_method: value.payment_channel
                          })

                          dispatch({
                            type: CheckoutActionType.ChoosePayment,
                            payload: value,
                          })
                        }}
                      >
                        <Image
                          src={value.payment_channel_logo}
                          alt=""
                          width={60}
                          height={60}
                          className="object-contain"
                        />
                      </button>
                    ))}
                  </div>
                </div>
              )}
            </>
          )}

          {checkoutState.credit_card && (
            <div className={clsx("sm:mb-8 mb-3")}>
              <div className={clsx("sm:text-lg font-medium mb-2", "text-base ")}>
                Kartu Kredit/Debit
              </div>
              <div className={clsx("flex", "sm-only:flex-wrap")}>
                <button
                  className={clsx(
                    "h-[52px] sm:p-2 mr-[10px] border border-[#ececec] rounded bg-transparent flex items-center justify-center gap-4 cursor-pointer font-semibold",
                    "py-2 px-3 sm-only:mb-[10px]",
                    checkoutState?.selected_method === "CREDIT_CARD" &&
                    "border-transparent ring-green ring-2"
                  )}
                  id={`btn-cc-hms`}
                  onClick={() => {
                    trackAddPaymentInfo({
                      currency: "IDR",
                      value: summary.program_price,
                      items: [
                        {
                          item_id: summary.school_id,
                          item_name: summary.school_title,
                          quantity: 1,
                          item_category: isProClass ? "ProClass" : "Bootcamp",
                          item_brand: "HMS",
                          item_variant: summary.program_title,
                          item_image_url: summary.image,
                          item_url: `${process.env.HOME_URL}/school/${summary.school_slug}`,
                          currency: "IDR",
                          price: summary.program_price
                        }
                      ],
                      payment_type: checkoutState.credit_card.payment_method,
                      payment_method: checkoutState.credit_card.payment_channel
                    })
                    dispatch({
                      type: CheckoutActionType.ChoosePayment,
                      payload: checkoutState?.credit_card,
                    })
                  }}
                >
                  {credits.map((value, index) => (
                    <Image
                      src={`${HARISENIN_PUBLIC_LOGO}/logo_${value}.png`}
                      alt=""
                      key={index}
                      width={50}
                      height={50}
                      className="object-contain"
                    />
                  ))}
                </button>
              </div>
            </div>
          )}

          {/* Will be hidden if pay leter method is disabled on back-end */}
          {Boolean(checkoutState?.pay_later_method?.length) && (
            <div className={clsx("sm:mb-8 mb-3")}>
              <div className={clsx("sm:text-lg font-medium mb-2", "text-base")}>Cicilan</div>

              <div className={clsx("grid sm:grid-cols-4 gap-2.5 mb-4", "grid-cols-2")}>
                {checkoutState?.pay_later_method?.map((value, index) => (
                  <button
                    className={clsx(
                      "h-[52px] sm:p-2 mr-[10px] border border-[#ececec] rounded bg-transparent flex items-center justify-center cursor-pointer font-semibold",
                      "py-2 px-3 sm-only:mb-[10px]",
                      checkoutState?.selected_channel === value.payment_channel &&
                      "border-transparent ring-green ring-2"
                    )}
                    key={index}
                    id={`btn-${value.payment_channel.toLowerCase()}-hms`}
                    onClick={() => {
                      trackAddPaymentInfo({
                        currency: "IDR",
                        value: summary.program_price,
                        items: [
                          {
                            item_id: summary.school_id,
                            item_name: summary.school_title,
                            quantity: 1,
                            item_category: isProClass ? "ProClass" : "Bootcamp",
                            item_brand: "HMS",
                            item_variant: summary.program_title,
                            item_image_url: summary.image,
                            item_url: `${process.env.HOME_URL}/school/${summary.school_slug}`,
                            currency: "IDR",
                            price: summary.program_price
                          }
                        ],
                        payment_type: value.payment_method,
                        payment_method: value.payment_channel
                      })
                      dispatch({
                        type: CheckoutActionType.ChoosePayment,
                        payload: value,
                      })
                    }}
                  >
                    <Image
                      src={value.payment_channel_logo}
                      alt=""
                      width={60}
                      height={60}
                      className="object-contain"
                    />
                  </button>
                ))}
              </div>

              <div className="text-sm">
                <b>Metode Tunda Pembayaran</b>
                <ul className="list-disc">
                  <li>30 hari {priceFormatter(checkoutState.total_price)}</li>
                </ul>

                <b>Pembayaran Cicilan</b>
                <ul className="list-disc">
                  {INSTALLMENT.map((ins, idx) => (
                    <li key={idx}>
                      {ins.month} bulan (bunga {ins.interest}%)
                    </li>
                  ))}
                </ul>
              </div>
            </div>
          )}
        </div>

        <div className={clsx("relativ mb-4 sm-only:mb-8")}>
          <h4 className={clsx("sm:text-xl font-semibold sm:mb-3", "text-sm sm-only:mb-5")}>Kode</h4>
          <div
            className={clsx("flex flex-col gap-2", "sm-only:gap-0 sm-only:flex-col sm-only:mb-8")}
          >
            <div className={clsx("font-semibold text-md", "w-full sm-only:mb-3")}>
              <div>
                Kode Diskon <span className="font-normal">(Opsional)</span>{" "}
              </div>
              <div className={clsx("font-normal text-sm")}>
                Isi jika kamu memiliki kode promo/referral
              </div>
            </div>

            <FlexBox align="center" className={clsx("w-full")}>
              <ManualInput
                errorText={checkoutState.error_discount}
                value={checkoutState.promo_code}
                className={clsx("sm:flex-1 mb-0", "w-[calc(60%-10px)] text-sm")}
                id="hms-promo-field"
                onChange={(e) =>
                  dispatch({
                    type: CheckoutActionType.InputPromoCode,
                    payload: e.target.value,
                  })
                }
                placeholder="Masukkan kode diskon"
              />
              <RegularButton
                variant="solid"
                color="green"
                disabled={!checkoutState.promo_code}
                isSubmitting={isSubmittingPromo}
                onClick={submitPromo}
                id="btn-check-code-hms"
                className="text-sm ml-3"
              >
                Cek kode
              </RegularButton>
            </FlexBox>
          </div>
        </div>

        <div className={clsx("relative mb-5 sm-only:mb-6")}>
          <h4 className={clsx("sm:text-xl font-semibold sm:mb-3", "text-sm sm-only:mb-5")}>
            Total Pembayaran
          </h4>

          <div className={clsx("flex justify-between font-medium sm-only:text-sm mb-3")}>
            <div className={clsx("items-center font-medium w-4/5 flex", "sm:only:w-3/5")}>
              {summary?.school_title} : <br />
              {summary?.program_title}
            </div>
            <div className={clsx("sm-only: w-[2%]")}>x1</div>
            <div className={clsx("w-[30%] text-right")}>
              {priceFormatter(checkoutState.gross_price)}
            </div>
          </div>

          {checkoutState.total_price ? (
            <div className={clsx("flex justify-between font-medium sm-only:text-sm")}>
              <div className={clsx("items-center font-medium w-4/5 flex", "sm:only:w-3/5")}>
                Biaya Admin
              </div>
              <div className={clsx("w-[30%] text-right")}>
                {priceFormatter(checkoutState.admin_fee)}
              </div>
            </div>
          ) : null}

          {/* Will be appeared when there is promo applied */}
          {checkoutState.promo_name ? (
            <div className={clsx("flex justify-between font-medium sm-only:text-sm mb-5")}>
              <div className={clsx("items-center font-medium w-4/5 flex", "sm:only:w-3/5")}>
                {checkoutState.promo_name}
                <TextButton onClick={deletePromo} className="w-fit ml-1">
                  <FaTimes className="-leading-[1px]" />
                </TextButton>
              </div>
              <div className={clsx("text-red")}>
                -{priceFormatter(checkoutState.discount_amount)}
              </div>
            </div>
          ) : null}

          <hr />

          <div
            className={clsx(
              "flex justify-between font-medium sm-only:text-sm mb-5",
              "font-semibold text-green"
            )}
          >
            <div className={clsx("items-center font-medium w-4/5 flex", "sm:only:w-3/5")}>
              Total
            </div>
            <div>{priceFormatter(checkoutState.total_price)}</div>
          </div>
        </div>

        {/* Button for open promo modal */}
        {/*<button*/}
        {/*  className={style["checkout__payment__promo-button"]}*/}
        {/*  onClick={() => dispatch({ type: CheckoutActionType.OpenModal, payload: "promo" })}*/}
        {/*>*/}
        {/*  Punya kode promo? Klik disini*/}
        {/*</button>*/}

        {/* Listening animation when waiting callback on e-wallet payment */}
        {isListening ? (
          <div className="listening-component d-flex align-items-center">
            <Spinner />
            <div className="ml-3">Menunggu respon dari pembayaran</div>
          </div>
        ) : null}

        {/* Button for submitting */}
        <div className={clsx("flex justify-center")}>
          <RegularButton
            color="green"
            variant="solid"
            disabled={!checkoutState.admin_fee}
            onClick={handleSubmit}
            isSubmitting={isSubmittingPayment}
            id="btn-buy-now-hms"
            className={clsx("sm:w-1/2 sm:py-4 sm:px-0 font-medium", "w-4/5 py-3 px-4")}
          >
            Bayar Sekarang
          </RegularButton>
        </div>

        {/* Error text */}
        <div className={`text-red mt-4 sm-only:mt-5 `}>{checkoutState.error_submitting}</div>
      </BasicCard>

      {/*<ModalPromo*/}
      {/*  checkoutState={checkoutState}*/}
      {/*  dispatch={dispatch}*/}
      {/*  serviceId={summary.program_id}*/}
      {/*/>*/}

      <OvoModal
        show={checkoutState.modal_open === "ovo"}
        onHide={handleCloseModal}
        checkoutId={checkoutId}
        productId={summary.school_id}
        price={checkoutState?.total_price}
      />

      <QrModal
        show={checkoutState.modal_open === "qr"}
        onHide={handleCloseModal}
        checkoutId={checkoutId}
        productId={summary.school_id}
        qrString={qrCode}
      />

      <CreditCardModal
        show={checkoutState?.modal_open === "credit_card"}
        onHide={handleCloseModal}
        checkoutId={checkoutId as string}
        productId={summary.school_id}
        price={checkoutState?.total_price}
      />
    </>
  )
}

export default CheckoutPayment
=======
import React, { Dispatch, FC, useState } from "react"
import { Spinner } from "react-bootstrap"
import { isBrowser } from "react-device-detect"
import { FaTimes } from "react-icons/fa"
import { ValidationError } from "yup"
import {
  CheckoutAction,
  CheckoutActionType,
  CheckoutState,
  CheckoutSummary,
} from "../../../constants/interfaces/checkout-state"
import { DynamicObject } from "../../../constants/interfaces/ui"
import { HARISENIN_PUBLIC_LOGO } from "../../../constants/pictures"
import UploadRepo from "../../../lib/apis/upload.repo"
import CheckoutService from "../../../lib/services/checkout.services"
import { useEWalletPaymentSocket } from "../../../lib/utils/hooks/useWebsocket"
import { priceFormatter } from "../../../lib/utils/method"
import { RegularButton, TextButton } from "../../modules/buttons"
import BasicCard from "../../modules/card/BasicCard"
import CreditCardModal from "./shared/CreditCardModal"
import { validationHandler } from "./FormValidation"
// import ModalPromo from "./ModalPromo"
import OvoModal from "./shared/OvoModal"
import QrModal from "./shared/QrModal"

import style from "./styles/checkout.module.scss"
import clsx from "clsx"
import { ManualInput } from "../../modules/input"

export interface CheckoutPaymentProps {
  checkoutState: CheckoutState
  dispatch: Dispatch<CheckoutAction>
  scrollToForm: (id: string) => void
  summary: CheckoutSummary
  isProClass: boolean
}

const formLabel = [
  "user_batch_request",
  "form_status",
  "user_fullname",
  "user_email",
  "user_phone_number",
  "user_university",
  "user_faculty",
  "user_majors",
  "user_job",
  "user_desired_job_sector",
  "user_desired_job",
  "user_linkedin_url",
]

const CheckoutPayment: FC<CheckoutPaymentProps> = ({
  checkoutState,
  dispatch,
  scrollToForm,
  summary,
  isProClass,
}) => {
  const [isSubmittingPayment, setIsSubmittingPayment] = useState(false)
  const [isListening, setIsListening] = useState(false)
  const [isSubmittingPromo, setIsSubmittingPromo] = useState(false)
  const [qrCode, setQrCode] = useState("")
  const [checkoutId, setCheckoutId] = useState("")

  const upload = new UploadRepo()
  const checkout = new CheckoutService()

  // Handler for error when listening
  const handleErrorListening = () => {
    dispatch({
      type: CheckoutActionType.ErrorSubmitting,
      payload: "Terjadi Kesalahan saat proses pembayaran. Silahkan coba lagi",
    })
    setIsSubmittingPayment(false)
    setIsListening(false)
  }

  const handleValidation = async () => {
    const validations = []

    // Choose validation based on form status
    const validation = validationHandler(checkoutState.form_status)

    const form: DynamicObject = {}
    let firstErrorId = ""

    try {
      const formRaw = checkoutState.form

      for (const formRawKey in formRaw) {
        if (Object.prototype.hasOwnProperty.call(formRaw, formRawKey)) {
          form[formRawKey] = formRaw[formRawKey].value
        }
      }
      form.form_status = checkoutState.form_status

      await validation.validate(form, { abortEarly: false })
      delete form.form_status

      dispatch({
        type: CheckoutActionType.ErrorChecking,
        payload: { errorType: "form", message: "" },
      })
    } catch (e: any) {
      // catch error for form validation
      const error: ValidationError = e
      const errors = error.inner

      validations.push("form")
      setIsSubmittingPayment(false)
      const errorLabel = errors.map((value) => {
        return value.path
      })

      for (const label of formLabel) {
        if (errorLabel.includes(label) && !firstErrorId) {
          firstErrorId = label
        }
      }

      // Looping for error form text
      for (const errorsKey of errors) {
        // @ts-ignore
        dispatch({
          type: CheckoutActionType.FormErrorHandler,
          payload: {
            name: errorsKey?.path,
            error: errorsKey.message,
            type: "field",
          },
        })

        dispatch({
          type: CheckoutActionType.ErrorChecking,
          payload: { errorType: "form", message: "Beberapa kolom form belum diisi" },
        })
      }

      scrollToForm(firstErrorId)
    }

    if (!isProClass) {
      if (
        checkoutState.form_file.identity_card.name === "File..." ||
        (!checkoutState.form_file.identity_card.file &&
          checkoutState.form_file.identity_card.name === "File...")
      ) {
        dispatch({
          type: CheckoutActionType.FormErrorHandler,
          payload: {
            name: "identity_card",
            error: "*Wajib diisi",
            type: "file",
          },
        })

        if (!firstErrorId) {
          firstErrorId = "identity_card"
          scrollToForm("identity_card")
        }

        dispatch({
          type: CheckoutActionType.ErrorChecking,
          payload: { errorType: "form", message: "Beberapa kolom form belum diisi" },
        })
        validations.push("file")
      } else {
        dispatch({
          type: CheckoutActionType.ErrorChecking,
          payload: { errorType: "form", message: "" },
        })
      }
    }

    // First check if payment is selected
    if (checkoutState.gross_price > 0 && checkoutState.admin_fee <= 0) {
      dispatch({
        type: CheckoutActionType.ErrorChecking,
        payload: { errorType: "paymentOption", message: "Silahkan pilih metode pembayaran" },
      })

      validations.push("options")

      if (!firstErrorId) {
        scrollToForm("payment_option")
      }
    } else {
      dispatch({
        type: CheckoutActionType.ErrorChecking,
        payload: { errorType: "paymentOption", message: "" },
      })
    }

    return { isValidated: !validations.length, form }
  }

  async function handleSubmit() {
    let checkoutStep = 0

    try {
      setIsSubmittingPayment(true)
      const validation = await handleValidation()

      // Form and Payment option validation
      if (!validation.isValidated) {
        return setIsSubmittingPayment(false)
      }

      // Create Checkout id
      checkoutStep = 1

      const res = await checkout.submitCheckout({
        productId: summary.school_id,
        serviceId: summary.program_id,
      })

      if (res.isFailure) {
        const errorMessage = res.error.message

        dispatch({
          type: CheckoutActionType.ErrorSubmitting,
          payload: errorMessage,
        })
        return setIsSubmittingPayment(false)
      }

      // Create Submit Form
      checkoutStep = 2

      const data = res.getValue()
      setCheckoutId(data.checkout_id)
      const id = data.checkout_id
      const form = validation.form as any

      const resForm = await checkout.submitForm({ checkoutId: id, form })

      if (resForm.isFailure) {
        dispatch({
          type: CheckoutActionType.ErrorSubmitting,
          payload:
            "Terjadi kesalahan saat mengirim form, pastikan semua data sudah terisi dengan benar. Silahkan coba lagi",
        })

        return setIsSubmittingPayment(false)
      }

      // Upload file form if there are any
      checkoutStep = 3
      if (
        !isProClass &&
        (checkoutState.form_file.identity_card.file ||
          checkoutState.form_file.ijazah.file ||
          checkoutState.form_file.last_study_card.file ||
          checkoutState.form_file.student_card.file ||
          checkoutState.form_file.cv.file)
      ) {
        const fileForm = checkoutState.form_file
        let isEdit = false

        const fd = new FormData()
        for (const fileFormKey in fileForm) {
          if (
            Object.prototype.hasOwnProperty.call(fileForm, fileFormKey) &&
            fileForm[fileFormKey].file
          ) {
            if (fileForm[fileFormKey].id) {
              fd.append(`${fileFormKey}_id`, fileForm[fileFormKey].id)
              isEdit = true
            }

            fd.append(fileFormKey, fileForm[fileFormKey].file)
          }
        }

        const resUpload = await upload.uploadFile({
          url: `/checkout/order/${id}/registration/upload`,
          file: fd,
          method: isEdit ? "put" : "post",
          version: "v3",
        })

        if (!resUpload.ok) {
          dispatch({
            type: CheckoutActionType.ErrorSubmitting,
            payload:
              "Terjadi kesalahan saat mengunggah file. File maksimal 5 MB dan format harus PNG/JPG/PDF. Silahkan coba file lain",
          })

          return setIsSubmittingPayment(false)
        }
      }

      // Submit discount if there are any
      checkoutStep = 4
      if (checkoutState.discount_amount) {
        await checkout.submitDiscount({
          checkoutId: data.checkout_id,
          code: checkoutState.applied_promo_code,
        })
      }

      // ovo payment modal check
      if (checkoutState.selected_channel === "ID_OVO") {
        setIsSubmittingPayment(false)
        return dispatch({
          type: CheckoutActionType.OpenModal,
          payload: "ovo",
        })
      }

      if (checkoutState.selected_method === "CREDIT_CARD") {
        setIsSubmittingPayment(false)
        return dispatch({
          type: CheckoutActionType.OpenModal,
          payload: "credit_card",
        })
      }

      // Submit payment
      checkoutStep = 5

      // this is host url for redirect url
      const host = window.location.origin

      const resPayment = await checkout.submitPayment({
        checkoutId: id,
        body: {
          payment_method: checkoutState.selected_method,
          payment_channel: checkoutState.selected_channel,
          ...(checkoutState.selected_method === "EWALLET" && {
            payment_attributes: {
              success_redirect_url: `${host}/school/invoice/${id}/success`,
            },
          }),
        },
      })

      if (resPayment.isFailure) {
        setIsSubmittingPayment(false)
        return dispatch({
          type: CheckoutActionType.ErrorSubmitting,
          payload:
            "Terjadi kesalahan saat proses pembayaran. Refresh ulang halaman dan Silahkan coba lagi",
        })
      }

      if (checkoutState.selected_method === "EWALLET") {
        const data = resPayment.getValue()
        const payment = data.payment
        const checkoutUrl =
          payment.actions.desktop_web_checkout_url ??
          payment.actions.mobile_web_checkout_url ??
          payment.actions.mobile_deeplink_checkout_url

        if (checkoutState.selected_channel === "ID_SHOPEEPAY" && isBrowser) {
          setQrCode(payment.actions.qr_checkout_string)
          return dispatch({
            type: CheckoutActionType.OpenModal,
            payload: "qr",
          })
        }

        window.open(checkoutUrl, "_blank")

        // eslint-disable-next-line react-hooks/rules-of-hooks
        useEWalletPaymentSocket({
          checkoutId: id,
          productId: summary.school_id,
          handleError: handleErrorListening,
        })
      } else {
        // Will redirect to invoice page
        window.location.assign(`/school/invoice/${id}/unpaid?productId=${summary.school_id}`)
      }
    } catch (e) {
      // catch general error
      setIsSubmittingPayment(false)

      switch (checkoutStep) {
        case 0:
          dispatch({
            type: CheckoutActionType.ErrorSubmitting,
            payload: "Pastikan form terisi dengan benar",
          })
          break
        case 1:
          dispatch({
            type: CheckoutActionType.ErrorSubmitting,
            payload: "Terjadi kesalahan pada proses checkout. Silahkan coba lagi",
          })
          break
        case 2:
          dispatch({
            type: CheckoutActionType.ErrorSubmitting,
            payload:
              "Terjadi kesalahan saat mengirim form, pastikan semua data sudah terisi dengan benar. Silahkan coba lagi",
          })
          break
        case 3:
          dispatch({
            type: CheckoutActionType.ErrorSubmitting,
            payload:
              "Terjadi kesalahan saat mengunggah file. File maksimal 5 MB dan format harus PNG/JPG/PDF. Silahkan coba file lain",
          })
          break
        case 4:
          dispatch({
            type: CheckoutActionType.ErrorSubmitting,
            payload:
              "Terjadi kesalahan saat menggunakan diskon. Pastikan kode yang anda gunakan sudah benar",
          })
          break
        case 5:
          dispatch({
            type: CheckoutActionType.ErrorSubmitting,
            payload: "Terjadi kesalahan saat proses pembayaran. Silahkan coba lagi",
          })
          break
        default:
          dispatch({
            type: CheckoutActionType.ErrorSubmitting,
            payload: "Terjadi kesalahan. Silahkan coba lagi",
          })
      }
    }
  }

  async function deletePromo() {
    try {
      const res = await checkout.getAvailablePayment(summary.program_id)
      if (res.isSuccess) {
        const data = res.getValue()

        dispatch({
          type: CheckoutActionType.PaymentOptionInitializer,
          payload: {
            bank_method: data.BANK_TRANSFER,
            e_wallet_method: data.EWALLET,
            ...(data.CREDIT_CARD && {
              credit_card: {
                payment_method: "CREDIT_CARD",
                payment_fee: data.CREDIT_CARD[0].payment_fee,
                payment_channel: "",
                payment_channel_logo: "",
              },
            }),
          },
        })
      }
      dispatch({ type: CheckoutActionType.DeleteDiscount })
    } catch (e) {
      if (process.env.NODE_ENV === "development") {
        console.log(e)
      }
    }
  }

  async function submitPromo() {
    try {
      setIsSubmittingPromo(true)
      const res = await checkout.applyPromoCode(summary.program_id as number, {
        code: checkoutState.promo_code,
      })

      if (res.isSuccess) {
        const discount = res.getValue()
        const payments = discount.available_payment

        dispatch({
          type: CheckoutActionType.PaymentOptionInitializer,
          payload: {
            bank_method: payments.BANK_TRANSFER,
            e_wallet_method: payments.EWALLET,
            ...(payments.CREDIT_CARD && {
              credit_card: {
                payment_method: "CREDIT_CARD",
                payment_fee: payments.CREDIT_CARD[0].payment_fee,
                payment_channel_logo: "",
                payment_channel: "",
              },
            }),
          },
        })

        // Discount is applied and promo modal will be closed
        dispatch({ type: CheckoutActionType.ApplyPromo, payload: discount })
      } else {
        const data = res.error
        const message: string = data.message
        dispatch({ type: CheckoutActionType.ErrorDiscount, payload: message })
      }

      setIsSubmittingPromo(false)
    } catch (e) {
      setIsSubmittingPromo(false)
      dispatch({
        type: CheckoutActionType.ErrorDiscount,
        payload: "Terjadi kesalahan. Silahkan coba lagi",
      })
    }
  }

  function handleCloseModal() {
    dispatch({ type: CheckoutActionType.ClosePromoModal })
    setIsSubmittingPayment(false)
  }

  const credits = ["visa", "mastercard", "jcb"]

  return (
    <>
      <BasicCard className={style["checkout__payment"]}>
        <div className={style.checkout__payment__column}>
          <div id="payment_option" className={style["checkout__form__row__anchor"]} />
          {/* No payment method if product is free */}
          {checkoutState.total_price ? (
            <>
              <h4>
                Pilih Metode pembayaran{" "}
                <small className="text-red">*{checkoutState.error_checking.paymentOption}</small>
              </h4>

              <div className={style.checkout__payment__option__title}>Transfer Bank</div>
              <div className={style.checkout__payment__option}>
                {checkoutState?.bank_method?.map((value, index) => (
                  <button
                    className={clsx(
                      style.checkout__payment__option__button,
                      checkoutState?.selected_channel === value.payment_channel &&
                        style["checkout__payment__option__button--selected"]
                    )}
                    key={index}
                    id={`btn-${value.payment_channel.toLowerCase()}-hms`}
                    onClick={() => {
                      import("react-facebook-pixel")
                        .then((module) => module.default)
                        .then((ReactPixel) => {
                          ReactPixel.track("AddPaymentInfo", {
                            content_ids: [summary.school_id, summary.program_id],
                            content_category: "hms",
                            content_type: "product",
                            product_catalog_id: summary.program_id,
                            contents: [
                              {
                                id: summary.school_id,
                                quantity: 1,
                              },
                            ],
                            currency: "IDR",
                            value: checkoutState.total_price,
                          })
                        })
                      dispatch({
                        type: CheckoutActionType.ChoosePayment,
                        payload: value,
                      })
                    }}
                  >
                    <img src={value.payment_channel_logo} alt="" />
                  </button>
                ))}
              </div>

              {/* Will be hidden if e-wallet method is disabled on back-end */}
              {checkoutState?.e_wallet_method?.length ? (
                <>
                  <div className={style.checkout__payment__option__title}>E-Wallet</div>
                  <div className={style.checkout__payment__option}>
                    {checkoutState?.e_wallet_method?.map((value, index) => (
                      <button
                        className={clsx(
                          style.checkout__payment__option__button,
                          checkoutState?.selected_channel === value.payment_channel &&
                            style["checkout__payment__option__button--selected"]
                        )}
                        key={index}
                        id={`btn-${value.payment_channel.toLowerCase()}-hms`}
                        onClick={() =>
                          dispatch({
                            type: CheckoutActionType.ChoosePayment,
                            payload: value,
                          })
                        }
                      >
                        <img src={value.payment_channel_logo} alt="" />
                      </button>
                    ))}
                  </div>
                </>
              ) : null}
            </>
          ) : null}

          {checkoutState.credit_card && (
            <>
              <div className={style.checkout__payment__option__title}>Kartu Kredit/Debit</div>
              <div className={style.checkout__payment__option}>
                <button
                  className={clsx(
                    style.checkout__payment__option__button,
                    checkoutState?.selected_method === "CREDIT_CARD" &&
                      style["checkout__payment__option__button--selected"]
                  )}
                  id={`btn-cc-hms`}
                  onClick={() =>
                    dispatch({
                      type: CheckoutActionType.ChoosePayment,
                      payload: checkoutState?.credit_card,
                    })
                  }
                >
                  {credits.map((value, index) => (
                    <img
                      src={`${HARISENIN_PUBLIC_LOGO}/logo_${value}.png`}
                      alt=""
                      key={index}
                      className={style.checkout__payment__option__button__cc}
                    />
                  ))}
                </button>
              </div>
            </>
          )}
        </div>

        <div className={style.checkout__payment__column}>
          <h4>Kode</h4>

          <div className={style.checkout__payment__code}>
            <div className={style.checkout__payment__code__label}>
              <div className={style.checkout__payment__code__label__title}>
                Kode Diskon (Opsional)
              </div>
              <div className={style.checkout__payment__code__label__info}>
                isi apabila kamu memiliki kode promo/referral
              </div>
            </div>

            <div className={style.checkout__payment__code__input}>
              <ManualInput
                errorText={checkoutState.error_discount}
                value={checkoutState.promo_code}
                className={style.checkout__payment__code__input__field}
                id="hms-promo-field"
                onChange={(e) =>
                  dispatch({
                    type: CheckoutActionType.InputPromoCode,
                    payload: e.target.value,
                  })
                }
                placeholder="Masukkan kode diskon"
              />
              <RegularButton
                type="solid"
                color="green"
                disabled={!checkoutState.promo_code}
                isSubmitting={isSubmittingPromo}
                onClick={submitPromo}
                id="btn-check-code-hms"
              >
                Cek kode
              </RegularButton>
            </div>
          </div>
        </div>

        <div className={style.checkout__payment__column}>
          <h4>Total Pembayaran</h4>

          <div className={style.checkout__payment__detail}>
            <div className={style.checkout__payment__detail__label}>
              {summary?.school_title} : <br />
              {summary?.program_title}
            </div>
            <div className={style.checkout__payment__detail__amount}>x1</div>
            <div className={style.checkout__payment__detail__price}>
              {priceFormatter(checkoutState.gross_price)}
            </div>
          </div>

          {checkoutState.total_price ? (
            <div className={style.checkout__payment__detail}>
              <div className={style.checkout__payment__detail__label}>Biaya Admin</div>
              <div className={style.checkout__payment__detail__price}>
                {priceFormatter(checkoutState.admin_fee)}
              </div>
            </div>
          ) : null}

          {/* Will be appeared when there is promo applied */}
          {checkoutState.promo_name ? (
            <div className={style.checkout__payment__detail}>
              <div className={style.checkout__payment__detail__label}>
                {checkoutState.promo_name}
                <TextButton onClick={deletePromo}>
                  <FaTimes />
                </TextButton>
              </div>
              <div className={clsx(style.checkout__payment__detail__error, "text-red")}>
                -{priceFormatter(checkoutState.discount_amount)}
              </div>
            </div>
          ) : null}

          <hr />

          <div
            className={clsx(
              style.checkout__payment__detail,
              style["checkout__payment__detail--total"]
            )}
          >
            <div className={style.checkout__payment__detail__label}>Total</div>
            <div>{priceFormatter(checkoutState.total_price)}</div>
          </div>
        </div>

        {/* Button for open promo modal */}
        {/*<button*/}
        {/*  className={style["checkout__payment__promo-button"]}*/}
        {/*  onClick={() => dispatch({ type: CheckoutActionType.OpenModal, payload: "promo" })}*/}
        {/*>*/}
        {/*  Punya kode promo? Klik disini*/}
        {/*</button>*/}

        {/* Listening animation when waiting callback on e-wallet payment */}
        {isListening ? (
          <div className="listening-component d-flex align-items-center">
            <Spinner animation="grow" variant="info" />
            <div className="ml-3">Menunggu respon dari pembayaran</div>
          </div>
        ) : null}

        {/* Button for submitting */}
        <div className={style["checkout__payment__button-row"]}>
          <RegularButton
            color="green"
            type="solid"
            onClick={handleSubmit}
            isSubmitting={isSubmittingPayment}
            id="btn-buy-now-hms"
          >
            Bayar Sekarang
          </RegularButton>
        </div>

        {/* Error text */}
        <div className={`${style["checkout__payment__error"]} mt-4`}>
          {checkoutState.error_submitting}
        </div>
      </BasicCard>

      {/*<ModalPromo*/}
      {/*  checkoutState={checkoutState}*/}
      {/*  dispatch={dispatch}*/}
      {/*  serviceId={summary.program_id}*/}
      {/*/>*/}

      <OvoModal
        show={checkoutState.modal_open === "ovo"}
        onHide={handleCloseModal}
        checkoutId={checkoutId}
        productId={summary.school_id}
        price={checkoutState?.total_price}
      />

      <QrModal
        show={checkoutState.modal_open === "qr"}
        onHide={handleCloseModal}
        checkoutId={checkoutId}
        productId={summary.school_id}
        qrString={qrCode}
      />

      <CreditCardModal
        show={checkoutState?.modal_open === "credit_card"}
        onHide={handleCloseModal}
        checkoutId={checkoutId as string}
        productId={summary.school_id}
        price={checkoutState?.total_price}
      />
    </>
  )
}

export default CheckoutPayment
>>>>>>> 94ccec2 (setup fix):src/ui/components/checkout/CheckoutPayment.tsx
