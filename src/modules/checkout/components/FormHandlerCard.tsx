<<<<<<< HEAD:src/modules/checkout/components/FormHandlerCard.tsx
import React, { Dispatch, FC } from "react"
import BasicCard from "@components/card/BasicCard"
import { Checkbox, ManualInput, ManualSelectInput } from "@components/input"
import ManualPhoneNumberInput from "@components/input/ManualPhoneNumberInput"
import FilePicker from "@components/input/FilePicker"

import {
  CheckoutAction,
  CheckoutActionType,
  CheckoutState,
  CheckoutSummary,
  FormStatus,
} from "@interfaces/checkout-state"
import { SelectOption } from "@interfaces/ui"
import CreateableSelectInput from "@components/input/CreateableSelectInput"

import { E164Number } from "libphonenumber-js"
import clsx from "clsx"
import { FlexBox } from "@components/wrapper"
import { Spinner } from "@components/misc"

export interface FormHandlerCard {
  state: CheckoutState
  dispatch: Dispatch<CheckoutAction>
  isLoading: boolean
  summary: CheckoutSummary
  isProClass: boolean
}

interface FormInputProps {
  label: string
  id: string
  required?: boolean
  info?: string
  type?: boolean
  children: any
}

export const INFO_OPTIONS = [
  "Ads Harisenin.com",
  "Content Instagram/Facebook",
  "Content Linkedin",
  "Content YouTube",
  "Content Twitter",
  "Content TikTok",
  "Event Harisenin.com",
  "Article",
  "Email",
  "Teman/Alumni",
  "Approach by Harisenin.com team",
]

const FormInput: FC<FormInputProps> = ({ label, info, children, id, type, required = true }) => {
  return (
    <div className={clsx("flex justify-between relative sm:mb-3", "mb-4 sm-only:block")}>
      <div id={id} />
      <div
        className={clsx(
          "font-semibold self-center text-sm leading-4 sm:w-1/2",
          "w-full sm-only:mb-3"
        )}
      >
        {label}
        {required && <span className="text-red">*</span>}
        {type && <span className={clsx("text-grey-97 font-normal")}> (pdf/jpg/png)</span>}
        {info && <div className={clsx("text-sm font-normal text-grey-97")}>{info}</div>}
      </div>
      {children}
    </div>
  )
}

const FormHandlerCard: FC<FormHandlerCard> = ({
  state,
  dispatch,
  isLoading,
  summary,
  isProClass,
}) => {
  // Select option for work type
  const options: SelectOption[] = [
    {
      label: "Mahasiswa",
      value: "Mahasiswa",
    },
    {
      label: "Karyawan",
      value: "Karyawan",
    },
    {
      label: "Belum Bekerja",
      value: "Belum Bekerja",
    },
  ]

  // Loading state
  if (isLoading) {
    return (
      <BasicCard className="flex justify-center items-center h-[40vw] mb-10">
        <Spinner />
      </BasicCard>
    )
  }

  const handleChangeForm =
    (eventType: "select" | "input" | "phone", formName: FormName) => (e: any) => {
      let value: any

      switch (eventType) {
        case "select":
          value = e.value
          break

        case "input":
          value = e.target.value
          break

        case "phone":
          value = e
          break

        default:
          break
      }

      dispatch({
        type: CheckoutActionType.FormHandler,
        payload: { name: formName, value },
      })
    }

  function handleCheckOption(e: any) {
    const value = e.target.value
    dispatch({ type: CheckoutActionType.FormOptionHandler, payload: value })
  }

  function handleInputOtherOption(e: any) {
    const value = e.target.value

    dispatch({
      type: CheckoutActionType.OtherOptionHandler,
      payload: { isSelected: false, value },
    })
  }

  function handleCheckOther() {
    dispatch({
      type: CheckoutActionType.OtherOptionHandler,
      payload: { ...state.other_option, isSelected: !state.other_option.isSelected },
    })
  }

  return (
    <BasicCard className={clsx("relative mb-10 sm:py-5 sm:px-8", "!p-4")}>
      <h5 className={clsx("font-semibold sm:text-lg", "text-sm")}>Investasi Program & Batch</h5>
      <hr />
      <FlexBox justify="between">
        <div className="h-13" />
        <div className="text-red">
          *<small className="text-black">Wajib Diisi</small>
        </div>
      </FlexBox>

      <div className={clsx("flex justify-between relative sm:mb-3", "mb-4 sm-only:block")}>
        <div
          className={clsx(
            "font-semibold self-center text-sm leading-4 sm:w-1/2",
            "w-full sm-only:mb-3"
          )}
        >
          Investasi Program
        </div>
        <div className={clsx("text-sm sm:w-1/2 self-center", "w-full")}>
          {summary.program_title}
        </div>
      </div>

      <FormInput label="Batch yang diinginkan" id="user_batch_request">
        <ManualSelectInput
          options={state.user_batch_request}
          placeholder="Pilih batch"
          className={clsx("sm:w-1/2 !mb-0 relative self-center", "w-full")}
          error={state.form.user_batch_request.error}
          id="batch-field"
          defaultOption={
            state?.form?.user_batch_request?.value
              ? {
                  value: state.form.user_batch_request.value,
                  label: state.form.user_batch_request.value,
                }
              : null
          }
          onChange={(event) =>
            dispatch({
              type: CheckoutActionType.FormHandler,
              payload: { name: "user_batch_request", value: event.value },
            })
          }
        />
      </FormInput>

      <div className="h-4" />
      <h5 className={clsx("font-semibold sm:text-lg", "text-sm")}>Info Pribadi</h5>
      <hr />
      <FlexBox justify="between">
        <div className="h-13" />
        <div className="text-red">
          *<small className="text-black">Wajib Diisi</small>
        </div>
      </FlexBox>

      <FormInput label="Nama" id="user_fullname" info="Nama lengkap (Sesuai KTP/ID resmi)">
        <ManualInput
          placeholder="Masukkan nama lengkap kamu"
          className={clsx("sm:w-1/2 !mb-0 relative self-center", "w-full")}
          id="name-field"
          value={state.form.user_fullname.value}
          errorText={state.form.user_fullname.error}
          onChange={(event) => {
            dispatch({
              type: CheckoutActionType.FormHandler,
              payload: { name: "user_fullname", value: event.target.value },
            })
          }}
        />
      </FormInput>

      <div className={clsx("flex justify-between relative sm:mb-3", "mb-4 sm-only:block")}>
        <div
          className={clsx(
            "font-semibold self-center text-sm leading-4 sm:w-1/2",
            "w-full sm-only:mb-3"
          )}
        >
          Email
          <div className={clsx("text-sm font-normal text-grey-97")}>Email Aktif</div>
        </div>
        <div className={clsx("text-sm sm:w-1/2 self-center", "w-full")}>
          {state.form.user_email.value}
        </div>
      </div>

      <FormInput label="Nomor Whatsapp" id="user_phone_number" info="Nomor Whatsapp Aktif">
        {/*TODO benerin dlu ini, typesafety nya berantakan*/}
        <ManualPhoneNumberInput
          value={state.form.user_phone_number?.value as E164Number}
          error={state.form.user_phone_number?.error}
          onChange={(event: any) =>
            dispatch({
              type: CheckoutActionType.FormHandler,
              payload: { name: "user_phone_number", value: event },
            })
          }
          placeholder="Masukkan nomor whatsapp kamu"
          className={clsx("sm:w-1/2 !mb-0 relative self-center", "w-full")}
        />
      </FormInput>

      <FormInput label="Status Pekerjaan" id="form_status">
        <ManualSelectInput
          options={options}
          placeholder="Pilih status pekerjaan kamu"
          className={clsx("sm:w-1/2 !mb-0 relative self-center", "w-full")}
          error={state.form_status_error}
          id="job-status-field"
          defaultOption={
            state?.form_status
              ? {
                  value: state?.form_status,
                  label: state?.form_status,
                }
              : null
          }
          onChange={(event: any) =>
            dispatch({
              type: CheckoutActionType.FormTypeHandler,
              payload: event.value as FormStatus,
            })
          }
        />
      </FormInput>

      {state.form_status === "Mahasiswa" ? (
        <>
          <FormInput label="Nama Universitas" id="user_university">
            <CreateableSelectInput
              options={state.university}
              placeholder="Pilih nama universitas kamu"
              className={clsx("sm:w-1/2 !mb-0 relative self-center", "w-full")}
              error={state.form.user_university.error}
              defaultOption={
                state?.form?.user_university?.value
                  ? {
                      value: state.form.user_university.value,
                      label: state.form.user_university.value,
                    }
                  : null
              }
              onChange={(event) =>
                dispatch({
                  type: CheckoutActionType.FormHandler,
                  payload: { name: "user_university", value: event.value },
                })
              }
            />
          </FormInput>

          <FormInput label="Fakultas" id="user_faculty">
            <ManualInput
              placeholder="Masukkan nama fakultas kamu"
              className={clsx("sm:w-1/2 !mb-0 relative self-center", "w-full")}
              value={state.form.user_faculty.value}
              errorText={state.form.user_faculty.error}
              onChange={(event) =>
                dispatch({
                  type: CheckoutActionType.FormHandler,
                  payload: { name: "user_faculty", value: event.target.value },
                })
              }
            />
          </FormInput>

          <FormInput label="Jurusan" id="user_majors">
            <ManualInput
              placeholder="Masukkan nama jurusan kamu"
              className={clsx("sm:w-1/2 !mb-0 relative self-center", "w-full")}
              value={state.form.user_majors.value}
              errorText={state.form.user_majors.error}
              onChange={(event) =>
                dispatch({
                  type: CheckoutActionType.FormHandler,
                  payload: { name: "user_majors", value: event.target.value },
                })
              }
            />
          </FormInput>
        </>
      ) : null}

      {state.form_status === "Karyawan" ? (
        <>
          <FormInput label="Pekerjaan saat ini" id="user_job">
            <ManualInput
              placeholder="Masukkan pekerjaan kamu saat ini"
              className={clsx("sm:w-1/2 !mb-0 relative self-center", "w-full")}
              value={state.form.user_job.value}
              id="profession-field"
              errorText={state.form.user_job.error}
              onChange={(event) =>
                dispatch({
                  type: CheckoutActionType.FormHandler,
                  payload: { name: "user_job", value: event.target.value },
                })
              }
            />
          </FormInput>
        </>
      ) : null}

      {/* This section is hidden when work status is not selected yet */}
      {!state.form_status ? null : (
        <>
          <FormInput label="Pilihan Sektor Pekerjaan" id="user_desired_job_sector">
            <ManualInput
              placeholder="Masukkan Sektor pekerjaan pilihan kamu"
              className={clsx("sm:w-1/2 !mb-0 relative self-center", "w-full")}
              id="job-sector-field"
              value={state.form.user_desired_job_sector.value}
              errorText={state.form.user_desired_job_sector.error}
              onChange={(event) =>
                dispatch({
                  type: CheckoutActionType.FormHandler,
                  payload: {
                    name: "user_desired_job_sector",
                    value: event.target.value,
                  },
                })
              }
            />
          </FormInput>

          <FormInput label="Profesi yang kamu inginkan" id="user_desired_job">
            <ManualInput
              placeholder="Masukkan profesi yang kamu inginkan"
              className={clsx("sm:w-1/2 !mb-0 relative self-center", "w-full")}
              value={state.form.user_desired_job.value}
              errorText={state.form.user_desired_job.error}
              onChange={(event) =>
                dispatch({
                  type: CheckoutActionType.FormHandler,
                  payload: {
                    name: "user_desired_job",
                    value: event.target.value,
                  },
                })
              }
            />
          </FormInput>

          <FormInput
            label="Link Linkedin"
            id="user_linkedin_url"
            info="(contoh: www.linkedin.com/in/johndoe)"
          >
            <ManualInput
              placeholder="Masukkan link linkedin kamu"
              className={clsx("sm:w-1/2 !mb-0 relative self-center", "w-full")}
              value={state.form.user_linkedin_url.value}
              id="link-linkedin-field"
              errorText={state.form.user_linkedin_url.error}
              onChange={(event) =>
                dispatch({
                  type: CheckoutActionType.FormHandler,
                  payload: {
                    name: "user_linkedin_url",
                    value: event.target.value,
                  },
                })
              }
            />
          </FormInput>
        </>
      )}

      <div className="relative">
        <div id="info_from" className="absolute top-[100px]" />
        <div
          className={clsx("font-semibold self-center text-sm leading-4 sm:w-1/2", "w-full mb-3")}
        >
          Dari mana kamu tahu Program {isProClass ? "ProClass" : "Bootcamp"} ini?
          <span className="text-red">*</span>
          <span className="text-red">{state.form.info_from.error}</span>
        </div>

        <div className="grid grid-cols-2">
          {INFO_OPTIONS.map((info, idx) => (
            <Checkbox
              className="sm:py-[9px]"
              key={idx}
              value={info}
              onChange={handleCheckOption}
              id={info}
              checked={state.form?.info_from?.value?.includes(info)}
            >
              {info}
            </Checkbox>
          ))}

          <FlexBox className="sm:py-[9px]" align="center">
            <Checkbox
              placeholder="Lain lain"
              onChange={handleCheckOther}
              checked={state.other_option.isSelected}
              id="others"
            />
            <ManualInput
              placeholder="Lain lain"
              value={state.other_option.value}
              id="link-linkedin-field"
              onChange={handleInputOtherOption}
            />
          </FlexBox>
        </div>
      </div>

      {!isProClass && (
        <>
          <div className="h-2" />
          <h5 className={clsx("font-semibold mt-3 sm:text-lg", "text-sm")}>Dokumen</h5>
          <hr />

          <FormInput
            label="Scan Ijazah Terakhir"
            id="ijazah"
            info="Maksimal ukuran file 5MB"
            type
            required={false}
          >
            <FilePicker name="ijazah" dispatch={dispatch} state={state} />
          </FormInput>

          {/*{state.form_status === "Mahasiswa" ? (*/}
          {/*  <>*/}
          {/*    <div className={style["checkout__form__row"]}>*/}
          {/*      <div className={style["checkout__form__label"]}>*/}
          {/*        Scan Kartu Mahasiswa{" "}*/}
          {/*        <span className="checkout__form__label__type">(pdf/jpg/png)</span>*/}
          {/*        <div className="checkout__form__label__notes">*/}
          {/*          Maksimal ukuran file 5MB*/}
          {/*        </div>*/}
          {/*      </div>*/}
          {/*      <FilePicker name="student_card" dispatch={dispatch} state={state} />*/}
          {/*    </div>*/}
          {/*    */}
          {/*    <div className={style["checkout__form__row"]}>*/}
          {/*      <div className={style["checkout__form__label"]}>*/}
          {/*        Scan Kartu Study Terakhir{" "}*/}
          {/*        <span className="checkout__form__label__type">(pdf/jpg/png)</span>*/}
          {/*        <div className="checkout__form__label__notes">*/}
          {/*          Maksimal ukuran file 5MB*/}
          {/*        </div>*/}
          {/*        <span className="checkout__form__label__type mt-4">*/}
          {/*          *Apabila masih kuliah*/}
          {/*        </span>*/}
          {/*      </div>*/}
          {/*      <FilePicker*/}
          {/*        name="last_study_card"*/}
          {/*        dispatch={dispatch}*/}
          {/*        state={state}*/}
          {/*      />*/}
          {/*    </div>*/}
          {/*  </>*/}
          {/*) : null}*/}

          <FormInput label="Scan KTP/KTM" id="identity_card" info="Maksimal ukuran file 5MB" type>
            <FilePicker name="identity_card" dispatch={dispatch} state={state} />
          </FormInput>

          <FormInput label="CV" id="cv" info="Maksimal ukuran file 5MB" type required={false}>
            <FilePicker name="cv" dispatch={dispatch} state={state} />
          </FormInput>
        </>
      )}
    </BasicCard>
  )
}

export default FormHandlerCard
=======
import React, { Dispatch, FC } from "react"
import { Spinner } from "react-bootstrap"

import BasicCard from "../../modules/card/BasicCard"
import { ManualInput, ManualSelectInput } from "../../modules/input"
import ManualPhoneNumberInput from "../../modules/input/ManualPhoneNumberInput"
import FilePicker from "../../modules/input/FilePicker"

import {
  CheckoutAction,
  CheckoutActionType,
  CheckoutState,
  CheckoutSummary,
  FormStatus,
} from "../../../constants/interfaces/checkout-state"
import { SelectOption } from "../../../constants/interfaces/ui"
import CreateableSelectInput from "../../modules/input/CreateableSelectInput"

import style from "./styles/checkout.module.scss"
import { E164Number } from "libphonenumber-js"

export interface FormHandlerCard {
  state: CheckoutState
  dispatch: Dispatch<CheckoutAction>
  isLoading: boolean
  summary: CheckoutSummary
  isProClass: boolean
}

interface FormInputProps {
  label: string
  id: string
  required?: boolean
  info?: string
  type?: boolean
}

const FormInput: FC<FormInputProps> = ({ label, info, children, id, type, required = true }) => {
  return (
    <div className={style["checkout__form__row"]}>
      <div id={id} className={style["checkout__form__row__anchor"]} />
      <div className={style["checkout__form__label"]}>
        {label}
        {required && <span className="text-red">*</span>}
        {type && <span className={style["checkout__form__label__type"]}> (pdf/jpg/png)</span>}
        {info && <div className={style["checkout__form__label__notes"]}>{info}</div>}
      </div>
      {children}
    </div>
  )
}

const FormHandlerCard: FC<FormHandlerCard> = ({
  state,
  dispatch,
  isLoading,
  summary,
  isProClass,
}) => {
  // Select option for work type
  const options: SelectOption[] = [
    {
      label: "Mahasiswa",
      value: "Mahasiswa",
    },
    {
      label: "Karyawan",
      value: "Karyawan",
    },
    {
      label: "Belum Bekerja",
      value: "Belum Bekerja",
    },
  ]

  // Loading state
  if (isLoading) {
    return (
      <BasicCard className={style["checkout__form--loading"]}>
        <Spinner animation="border" variant="info" />
      </BasicCard>
    )
  }

  return (
    <BasicCard className={style["checkout__form"]}>
      <h5>Investasi Program & Batch</h5>
      <hr />
      <div className={style["checkout__form__warning"]}>
        <div className={style["checkout__form__warning__spacer"]} />
        <div className={style["checkout__form__warning__text"]}>
          *<small>Wajib Diisi</small>
        </div>
      </div>

      <div className={style["checkout__form__row"]}>
        <div className={style["checkout__form__label"]}>Investasi Program</div>
        <div className={style["checkout__form__info"]}>{summary.program_title}</div>
      </div>

      <FormInput label="Batch yang diinginkan" id="user_batch_request">
        <ManualSelectInput
          options={state.user_batch_request}
          placeholder="Pilih batch"
          className={style["checkout__form__input"]}
          error={state.form.user_batch_request.error}
          id="batch-field"
          defaultOption={
            state?.form?.user_batch_request?.value
              ? {
                  value: state.form.user_batch_request.value,
                  label: state.form.user_batch_request.value,
                }
              : null
          }
          onChange={(event) =>
            dispatch({
              type: CheckoutActionType.FormHandler,
              payload: { name: "user_batch_request", value: event.value },
            })
          }
        />
      </FormInput>

      <div style={{ height: "30px" }} />

      <h5>Info Pribadi</h5>
      <hr />
      <div className={style["checkout__form__warning"]}>
        <div className={style["checkout__form__warning__spacer"]} />
        <div className={style["checkout__form__warning__text"]}>
          *<small>Wajib Diisi</small>
        </div>
      </div>

      <FormInput label="Nama" id="user_fullname" info="Nama lengkap (Sesuai KTP/ID resmi)">
        <ManualInput
          placeholder="Masukkan nama lengkap kamu"
          className={style["checkout__form__input"]}
          id="name-field"
          value={state.form.user_fullname.value}
          errorText={state.form.user_fullname.error}
          onChange={(event) => {
            dispatch({
              type: CheckoutActionType.FormHandler,
              payload: { name: "user_fullname", value: event.target.value },
            })
          }}
        />
      </FormInput>

      <div className={style["checkout__form__row"]}>
        <div className={style["checkout__form__label"]}>
          Email
          <div className={style["checkout__form__label__notes"]}>Email Aktif</div>
        </div>
        <div className={style["checkout__form__info"]}>{state.form.user_email.value}</div>
      </div>

      <FormInput label="Nomor Whatsapp" id="user_phone_number" info="Nomor Whatsapp Aktif">
        {/*TODO benerin dlu ini, typesafety nya berantakan*/}
        <ManualPhoneNumberInput
          value={state.form.user_phone_number?.value as E164Number}
          error={state.form.user_phone_number?.error}
          onChange={(event: any) =>
            dispatch({
              type: CheckoutActionType.FormHandler,
              payload: { name: "user_phone_number", value: event },
            })
          }
          placeholder="Masukkan nomor whatsapp kamu"
          className={style["checkout__form__input"]}
        />
      </FormInput>

      <FormInput label="Status Pekerjaan" id="form_status">
        <ManualSelectInput
          options={options}
          placeholder="Pilih status pekerjaan kamu"
          className={style["checkout__form__input"]}
          error={state.form_status_error}
          id="job-status-field"
          defaultOption={
            state?.form_status
              ? {
                  value: state?.form_status,
                  label: state?.form_status,
                }
              : null
          }
          onChange={(event) =>
            dispatch({
              type: CheckoutActionType.FormTypeHandler,
              payload: event.value as FormStatus,
            })
          }
        />
      </FormInput>

      {state.form_status === "Mahasiswa" ? (
        <>
          <FormInput label="Nama Universitas" id="user_university">
            <CreateableSelectInput
              options={state.university}
              placeholder="Pilih nama universitas kamu"
              className={style["checkout__form__input"]}
              error={state.form.user_university.error}
              defaultOption={
                state?.form?.user_university?.value
                  ? {
                      value: state.form.user_university.value,
                      label: state.form.user_university.value,
                    }
                  : null
              }
              onChange={(event) =>
                dispatch({
                  type: CheckoutActionType.FormHandler,
                  payload: { name: "user_university", value: event.value },
                })
              }
            />
          </FormInput>

          <FormInput label="Fakultas" id="user_faculty">
            <ManualInput
              placeholder="Masukkan nama fakultas kamu"
              className={style["checkout__form__input"]}
              value={state.form.user_faculty.value}
              errorText={state.form.user_faculty.error}
              onChange={(event) =>
                dispatch({
                  type: CheckoutActionType.FormHandler,
                  payload: { name: "user_faculty", value: event.target.value },
                })
              }
            />
          </FormInput>

          <FormInput label="Jurusan" id="user_majors">
            <ManualInput
              placeholder="Masukkan nama jurusan kamu"
              className={style["checkout__form__input"]}
              value={state.form.user_majors.value}
              errorText={state.form.user_majors.error}
              onChange={(event) =>
                dispatch({
                  type: CheckoutActionType.FormHandler,
                  payload: { name: "user_majors", value: event.target.value },
                })
              }
            />
          </FormInput>
        </>
      ) : null}

      {state.form_status === "Karyawan" ? (
        <>
          <FormInput label="Pekerjaan saat ini" id="user_job">
            <ManualInput
              placeholder="Masukkan pekerjaan kamu saat ini"
              className={style["checkout__form__input"]}
              value={state.form.user_job.value}
              id="profession-field"
              errorText={state.form.user_job.error}
              onChange={(event) =>
                dispatch({
                  type: CheckoutActionType.FormHandler,
                  payload: { name: "user_job", value: event.target.value },
                })
              }
            />
          </FormInput>
        </>
      ) : null}

      {/* This section is hidden when work status is not selected yet */}
      {!state.form_status ? null : (
        <>
          <FormInput label="Pilihan Sektor Pekerjaan" id="user_desired_job_sector">
            <ManualInput
              placeholder="Masukkan Sektor pekerjaan pilihan kamu"
              className={style["checkout__form__input"]}
              id="job-sector-field"
              value={state.form.user_desired_job_sector.value}
              errorText={state.form.user_desired_job_sector.error}
              onChange={(event) =>
                dispatch({
                  type: CheckoutActionType.FormHandler,
                  payload: {
                    name: "user_desired_job_sector",
                    value: event.target.value,
                  },
                })
              }
            />
          </FormInput>

          <FormInput label="Profesi yang kamu inginkan" id="user_desired_job">
            <ManualInput
              placeholder="Masukkan profesi yang kamu inginkan"
              className={style["checkout__form__input"]}
              value={state.form.user_desired_job.value}
              errorText={state.form.user_desired_job.error}
              onChange={(event) =>
                dispatch({
                  type: CheckoutActionType.FormHandler,
                  payload: {
                    name: "user_desired_job",
                    value: event.target.value,
                  },
                })
              }
            />
          </FormInput>

          <FormInput
            label="Link Linkedin"
            id="user_linkedin_url"
            info="(contoh: www.linkedin.com/in/johndoe)"
          >
            <ManualInput
              placeholder="Masukkan link linkedin kamu"
              className={style["checkout__form__input"]}
              value={state.form.user_linkedin_url.value}
              id="link-linkedin-field"
              errorText={state.form.user_linkedin_url.error}
              onChange={(event) =>
                dispatch({
                  type: CheckoutActionType.FormHandler,
                  payload: {
                    name: "user_linkedin_url",
                    value: event.target.value,
                  },
                })
              }
            />
          </FormInput>
        </>
      )}

      {!isProClass && (
        <>
          <h5 className="mt-3">Dokumen</h5>
          <hr />

          <FormInput
            label="Scan Ijazah Terakhir"
            id="ijazah"
            info="Maksimal ukuran file 5MB"
            type
            required={false}
          >
            <FilePicker name="ijazah" dispatch={dispatch} state={state} />
          </FormInput>

          {/*{state.form_status === "Mahasiswa" ? (*/}
          {/*  <>*/}
          {/*    <div className={style["checkout__form__row"]}>*/}
          {/*      <div className={style["checkout__form__label"]}>*/}
          {/*        Scan Kartu Mahasiswa{" "}*/}
          {/*        <span className="checkout__form__label__type">(pdf/jpg/png)</span>*/}
          {/*        <div className="checkout__form__label__notes">*/}
          {/*          Maksimal ukuran file 5MB*/}
          {/*        </div>*/}
          {/*      </div>*/}
          {/*      <FilePicker name="student_card" dispatch={dispatch} state={state} />*/}
          {/*    </div>*/}
          {/*    */}
          {/*    <div className={style["checkout__form__row"]}>*/}
          {/*      <div className={style["checkout__form__label"]}>*/}
          {/*        Scan Kartu Study Terakhir{" "}*/}
          {/*        <span className="checkout__form__label__type">(pdf/jpg/png)</span>*/}
          {/*        <div className="checkout__form__label__notes">*/}
          {/*          Maksimal ukuran file 5MB*/}
          {/*        </div>*/}
          {/*        <span className="checkout__form__label__type mt-4">*/}
          {/*          *Apabila masih kuliah*/}
          {/*        </span>*/}
          {/*      </div>*/}
          {/*      <FilePicker*/}
          {/*        name="last_study_card"*/}
          {/*        dispatch={dispatch}*/}
          {/*        state={state}*/}
          {/*      />*/}
          {/*    </div>*/}
          {/*  </>*/}
          {/*) : null}*/}

          <FormInput label="Scan KTP/KTM" id="identity_card" info="Maksimal ukuran file 5MB" type>
            <FilePicker name="identity_card" dispatch={dispatch} state={state} />
          </FormInput>

          <FormInput label="CV" id="cv" info="Maksimal ukuran file 5MB" type required={false}>
            <FilePicker name="cv" dispatch={dispatch} state={state} />
          </FormInput>
        </>
      )}
    </BasicCard>
  )
}

export default FormHandlerCard
>>>>>>> 94ccec2 (setup fix):src/ui/components/checkout/FormHandlerCard.tsx
