<<<<<<< HEAD:src/modules/checkout/OrderSummary.tsx
import React from "react"
import { createMarkup } from "@lib/functions/formatter"

import { CheckoutSummary } from "@interfaces/checkout-state"

import BasicCard from "@components/card/BasicCard"

import style from "./styles/checkout.module.scss"
import Image from "next/image"

function OrderSummary({ summary }: { summary: CheckoutSummary }) {
  if (!summary) {
    return <></>
  }

  return (
    <BasicCard className={style["checkout__detail"]}>
      {/* Image */}
      <Image src={summary.image} alt={summary.school_title} width={300} height={300} />

      {/* Info */}
      <div className={style["checkout__detail__info"]}>
        <h1>{summary.school_title}</h1>
        <div className="font-weight-bold">Investasi :</div>
        <div className="mb-3">{summary.program_title}</div>
        <div className="font-weight-bold">Deskripsi Investasi:</div>
        <div dangerouslySetInnerHTML={createMarkup(summary.program_description)} />
      </div>
    </BasicCard>
  )
}

export default OrderSummary
=======
import React from "react"
import { createMarkup } from "../../../lib/utils/method/formatter"

import { CheckoutSummary } from "../../../constants/interfaces/checkout-state"

import BasicCard from "../../modules/card/BasicCard"

import style from "./styles/checkout.module.scss"

function OrderSummary({ summary }: { summary: CheckoutSummary }) {
  if (!summary) {
    return <></>
  }

  return (
    <BasicCard className={style["checkout__detail"]}>
      {/* Image */}
      <img src={summary.image} alt={summary.school_title} />

      {/* Info */}
      <div className={style["checkout__detail__info"]}>
        <h1>{summary.school_title}</h1>
        <div className="font-weight-bold">Investasi :</div>
        <div className="mb-3">{summary.program_title}</div>
        <div className="font-weight-bold">Deskripsi Investasi:</div>
        <div dangerouslySetInnerHTML={createMarkup(summary.program_description)} />
      </div>
    </BasicCard>
  )
}

export default OrderSummary
>>>>>>> 94ccec2 (setup fix):src/ui/components/checkout/OrderSummary.tsx
