import { GetServerSideProps } from "next"
import ProgramListPage from "@modules/program-list/pages/ProgramListPage"
import SchoolRepo from "@lib/api/server-side/school.repo"
import { SchoolType } from "@interfaces/school"

const ProgamList = ({ codingcamps }) =>
  ProgramListPage({
    data: codingcamps,
    metaTitle: "harisenin.com: Semua Program - Coding Camp",
    metaDescription: "Belajar dan langsung bekerja di industri tech hanya dalam 3-5 Bulan",
  })

export const getServerSideProps: GetServerSideProps = async ({ query }) => {
  const school = new SchoolRepo()

  try {
    const res = await school.schoolList(query.type ? (query.type as SchoolType) : "CODING_CAMP")
    let codingcamps = []

    if (res.statusCode === 200) {
      const result = JSON.parse(res.body).result

      if (result) {
        codingcamps = result.data
      }
    }

    return {
      props: {
        codingcamps,
      },
    }
  } catch (e) {
    console.log(e)

    return {
      props: {
        codingcamps: [],
      },
    }
  }
}

export default ProgamList
