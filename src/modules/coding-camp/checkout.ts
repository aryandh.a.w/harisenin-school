import CheckoutPage from "@modules/checkout/pages/CheckoutPage"
import { serverTokenChecker } from "@lib/functions"
import { GetServerSideProps } from "next"

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const token = serverTokenChecker(ctx)

  if (!token) {
    return {
      redirect: {
        destination: "/",
      },
      props: {
        data: {},
      },
    }
  } else {
    return {
      props: {
        data: {},
      },
    }
  }
}

export default CheckoutPage
