<<<<<<< HEAD:src/modules/program-list/components/ProgramPromoList.tsx
import Helmet from "@helpers/Helmet"
import { Header } from "@components/header"
import { ProgramPromoColumn, ProgramPromoBanner } from "@modules/program-list/components"
import { HariseninFooter } from "@components/harisenin/HariseninFooter"
// import HeaderHomePromo from "../../modules/header/HeaderHomePromo"

const ProgramPromoPage = () => {
  return (
    <>
      <Helmet title="Harisenin.com: Promo Program" isIndexed />
      <Header />
      <ProgramPromoBanner />
      <ProgramPromoColumn />
      <HariseninFooter />
    </>
  )
}

export default ProgramPromoPage
=======
import Helmet from "../../../lib/utils/Helmet"
import HeaderHome from "../../modules/header/HeaderHome"
import ProgramPromoColumn from "./ProgramPromoColumn"
import ProgramPromoBanner from "./ProgramPromoBanner"
import { HariseninFooter } from "../../modules/harisenin/HariseninFooter"
// import HeaderHomePromo from "../../modules/header/HeaderHomePromo"

const ProgramPromoPage = () => {
  return (
    <>
      <Helmet title="Harisenin.com: Promo Program" isIndexed />
      <HeaderHome />
      <ProgramPromoBanner />
      <ProgramPromoColumn />
      <HariseninFooter />
    </>
  )
}

export default ProgramPromoPage
>>>>>>> 94ccec2 (setup fix):src/ui/components/program-list/ProgramPromoList.tsx
