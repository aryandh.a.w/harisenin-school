<<<<<<< HEAD:src/modules/program-list/components/ProgramListColumn.tsx
import { useEffect, useState } from "react"
import { useRouter } from "next/router"
import Image from "next/image"

import { School } from "@interfaces/school"
import { HARISENIN_PUBLIC_PAGE_ASSETS } from "@constants/pictures"

import { RegularButton } from "@components/buttons"
import SchoolServices from "@services/school.services"

import clsx from "clsx"
import { Spinner } from "@components/misc"
import { ProgramCard } from "@components/card/SchoolCard"
import { useScreenSize } from "@lib/hooks/useScreenSize"
import { Container } from "@components/wrapper"
import { useIsMounted } from "@lib/hooks/useIsMounted"

export const ProgramListColumn = ({ data }: { data: School[] }) => {
  const [isLoading, setIsLoading] = useState(false)
  const [isSeeMore, setIsSeeMore] = useState(false)
  const [schoolData, setSchoolData] = useState<School[]>(data)
  const [totalData, setTotalData] = useState(0)
  const [currentPage, setCurrentPage] = useState(1)
  const router = useRouter()
  const { query, asPath } = router

  const school = new SchoolServices()
  const { screenWidth } = useScreenSize()

  const isBootcamp = asPath.includes("/bootcamp")
  const isProclass = asPath.includes("/proclass")
  const isMounted = useIsMounted()

  // Data handler
  useEffect(() => {
    const getData = async () => {
      try {
        setIsLoading(true)
        const res = await school.getSchoolListV2(
          isBootcamp ? "BOOTCAMP" : isProclass ? "PRO_CLASS" : "CODING_CAMP",
          {
            limit: 6,
            search: query.search as string,
            page: currentPage,
          }
        )

        if (res.isSuccess) {
          const result = res.getValue()
          setSchoolData(result.data)
          setTotalData(result.totalData)
        }
        setIsLoading(false)
      } catch (e) {
        // Bugsnag.notify(e);
        setIsLoading(false)
      }
    }

    if (isMounted) {
      getData()
    }

    return () => {
      setSchoolData([])
      setTotalData(0)
      setCurrentPage(1)
    }
  }, [query, isMounted])

  async function handleSeeMore() {
    try {
      setCurrentPage(currentPage + 1)
      setIsSeeMore(true)

      const res = await school.getSchoolListV2(isBootcamp ? "BOOTCAMP" : "PRO_CLASS", {
        limit: 6,
        page: currentPage + 1,
        search: query.search as string,
      })

      if (res.isSuccess) {
        const result = res.getValue()
        setSchoolData([...schoolData, ...result.data])
      }
      setIsSeeMore(false)
    } catch (e) {
      setIsSeeMore(false)
    }
  }

  return (
    <div className={clsx("px-0 sm:py-15", "py-8")} id="search">
      <Container>
        <div className={clsx("flex flex-col items-center sm:mb-5", "mb-4")}>
          <h2
            className={clsx(
              "sm:text-2xl font-semibold sm:mb-[10px]  text-[#231f20] text-center",
              "text-base mb-2"
            )}
          >
            Jelajahi Program {isBootcamp ? "Bootcamp" : isProclass ? "ProClass" : "Coding Camp"}
          </h2>

          <div
            className={clsx(
              "sm:text-base font-normal sm:leading-6 text-[#231f20] text-center",
              "text-xs leading-4"
            )}
          >
            {isBootcamp
              ? "Belajar intensif dengan career coaching dan job-guarantee, hanya dalam 3-5 bulan"
              : "Tingkatkan skill profesional yang kamu butuhkan untuk persiapan kariermu, hanya dalam 1-2 bulan"}
          </div>

          <div className={clsx("flex  sm:mt-5 bg-[#ececec] p-2 sm:gap-2 rounded", "mt-4 gap-4")}>
            <RegularButton
              color="custom"
              className={clsx(
                "text-white border-none rounded sm:py[10px] sm:px-3 sm:w-48 sm:text-sm font-semibold leading-6",
                "py-[6px] px-[10px] w-32 text-sm",
                isBootcamp ? "bg-green text-white" : "text-black hover:text-black bg-[#d3d3d3]"
              )}
              onClick={() =>
                router.replace("bootcamp", undefined, { scroll: false, shallow: true })
              }
            >
              BOOTCAMP
            </RegularButton>

            <RegularButton
              color="custom"
              className={clsx(
                "text-white border-none rounded sm:py[10px] sm:px-3 sm:w-48 sm:text-sm font-semibold leading-6",
                "py-[6px] px-[10px] w-32 text-sm",
                isProclass ? "bg-green text-white" : "text-black hover:text-black bg-[#d3d3d3]"
              )}
              onClick={() =>
                router.replace("proclass", undefined, { scroll: false, shallow: true })
              }
            >
              PROCLASS
            </RegularButton>
          </div>
        </div>

        {isLoading && (
          // Loading state component
          <div className="flex justify-center items-center h-[50hv]">
            <Spinner />
          </div>
        )}

        {!isLoading && schoolData?.length !== 0 && (
          <div className={clsx("flex flex-wrap -mx-4")}>
            {schoolData.map((value, index) => (
              <div
                className={clsx("w-[calc(33.33%-20px)] mx-2 sm:mb-4", "sm-only:w-full")}
                key={index}
              >
                <ProgramCard
                  className={clsx("!w-full", "sm-only:mr-0")}
                  item={value}
                  key={index}
                  index={index}
                  screenWidth={screenWidth}
                />
              </div>
            ))}
          </div>
        )}

        {!isLoading && !schoolData.length && (
          <div className={clsx("sm:mt-24 flex justify-center w-full", "mt-20")}>
            <div className={clsx("flex flex-col items-center")}>
              <div>
                <Image
                  src={`${HARISENIN_PUBLIC_PAGE_ASSETS}/school-list_no-data.png`}
                  alt="Empty Product"
                  height={320}
                  width={530}
                />
              </div>
              <h3 className={clsx("sm:mt-6 sm:text-xl text-center font-semibild", "mt-3 text-xs")}>
                Maaf, Kelas yang Kamu Cari Belum Tersedia Saat Ini
              </h3>
              <div
                className={clsx(
                  "sm:mt-1 sm:text-base text-center font-medium text-grey-c4",
                  "mt-0 text-xs"
                )}
              >
                Nantikan program {query.type !== "pro_class" ? "Bootcamp" : "ProClass"} terbaik yang
                dapat segera kamu ikuti, hanya di Harisenin.com
              </div>
            </div>
          </div>
        )}

        {/* Pagination button not appeared when program below 6 */}
        {totalData > 6 && totalData > schoolData.length && (
          <RegularButton
            variant="solid"
            color="green"
            className={clsx(
              "mx-auto sm:2/5 sm:p-3 sm:mt-4",
              "sm-only:w-full p-2 mt-2 sm-only:font-medium sm-only:rounded"
            )}
            onClick={handleSeeMore}
          >
            {isSeeMore ? (
              // Will show loading animation when fetching data
              <Spinner />
            ) : (
              "Muat Lainnya"
            )}
          </RegularButton>
        )}
      </Container>
    </div>
  )
}
=======
import { useEffect, useState } from "react"
import { useRouter } from "next/router"
import Image from "next/image"
import { Spinner } from "react-bootstrap"

import { School } from "../../../constants/interfaces/school"
import { HARISENIN_PUBLIC_PAGE_ASSETS } from "../../../constants/pictures"

import ProgramCard from "../../modules/card/ProgramCard"
import { RegularButton } from "../../modules/buttons"
import SchoolServices from "../../../lib/services/school.services"

import style from "./styles/programlist.module.scss"
import clsx from "clsx"

const ProgramListColumn = ({ data }: { data: School[] }) => {
  const [isLoading, setIsLoading] = useState(false)
  const [isSeeMore, setIsSeeMore] = useState(false)
  const [schoolData, setSchoolData] = useState<School[]>(data)
  const [totalData, setTotalData] = useState(0)
  const [currentPage, setCurrentPage] = useState(1)
  const router = useRouter()
  const { isReady, query, asPath } = router

  const school = new SchoolServices()

  const isBootcamp = asPath.includes("/bootcamp")

  // Data handler
  useEffect(() => {
    if (!isReady) return
    const getData = async () => {
      try {
        setIsLoading(true)

        const res = await school.getSchoolListV2(isBootcamp ? "BOOTCAMP" : "PRO_CLASS", {
          limit: 6,
          search: query.search as string,
          page: currentPage,
        })

        if (res.isSuccess) {
          const result = res.getValue()
          setSchoolData(result.data)
          setTotalData(result.totalData)
        }
        setIsLoading(false)
      } catch (e) {
        // Bugsnag.notify(e);
        setIsLoading(false)
      }
    }

    getData()

    return () => {
      setSchoolData([])
      setTotalData(0)
      setCurrentPage(1)
    }
  }, [query])

  async function handleSeeMore() {
    try {
      setCurrentPage(currentPage + 1)
      setIsSeeMore(true)

      const res = await school.getSchoolListV2(isBootcamp ? "BOOTCAMP" : "PRO_CLASS", {
        limit: 6,
        page: currentPage + 1,
        search: query.search as string,
      })

      if (res.isSuccess) {
        const result = res.getValue()
        setSchoolData([...schoolData, ...result.data])
      }
      setIsSeeMore(false)
    } catch (e) {
      setIsSeeMore(false)
    }
  }

  return (
    <div className={`${style["landing_page__program"]} ${style["landing_page"]}`} id="search">
      <div className="mb-5 container">
        <div className={style.landing_page__program__head}>
          <h2 className={style.landing_page__program__head__title}>
            Jelajahi Program {isBootcamp ? "Bootcamp" : "ProClass"}
          </h2>

          <div className={style.landing_page__program__head__subtitle}>
            {isBootcamp
              ? "Belajar intensif dengan career coaching dan job-guarantee, hanya dalam 3-6 bulan"
              : "Tingkatkan skill profesional yang kamu butuhkan untuk persiapan kariermu, hanya dalam 1-2 bulan"}
          </div>

          <div className={style.landing_page__program__head__menu}>
            <RegularButton
              color="custom"
              className={clsx(
                style.landing_page__program__head__menu__item,
                isBootcamp
                  ? style.landing_page__program__head__menu__item__active
                  : style["landing_page__program__head__menu__item__not-active"]
              )}
              onClick={() =>
                router.replace("bootcamp", undefined, { scroll: false, shallow: true })
              }
            >
              BOOTCAMP
            </RegularButton>

            <RegularButton
              color="custom"
              className={clsx(
                style.landing_page__program__head__menu__item,
                !isBootcamp
                  ? style.landing_page__program__head__menu__item__active
                  : style["landing_page__program__head__menu__item__not-active"]
              )}
              onClick={() =>
                router.replace("proclass", undefined, { scroll: false, shallow: true })
              }
            >
              PROCLASS
            </RegularButton>
          </div>
        </div>

        {isLoading && (
          // Loading state component
          <div className={style["landing_page__program__column--loading"]}>
            <Spinner animation="border" variant="info" />
          </div>
        )}

        {!isLoading && schoolData?.length !== 0 && (
          <div className={style["landing_page__program__column"]}>
            {schoolData.map((value, index) => (
              <div className={style["landing_page__program__column__wrapper"]} key={index}>
                <ProgramCard
                  className={`${style["landing_page__program__column__wrapper__card"]}`}
                  data={value}
                  key={index}
                  index={index}
                />
              </div>
            ))}
          </div>
        )}

        {!isLoading && !schoolData.length && (
          <div className={style["landing_page__program__empty_product"]}>
            <div className={style["landing_page__program__empty_product__column"]}>
              <div>
                <Image
                  src={`${HARISENIN_PUBLIC_PAGE_ASSETS}/school-list_no-data.png`}
                  alt="Empty Product"
                  height={320}
                  width={530}
                />
              </div>
              <h3 className={style["landing_page__program__empty_product__column__info"]}>
                Maaf, Kelas yang Kamu Cari Belum Tersedia Saat Ini
              </h3>
              <div className={style["landing_page__program__empty_product__column__subinfo"]}>
                Nantikan program {query.type !== "pro_class" ? "Bootcamp" : "ProClass"} terbaik yang
                dapat segera kamu ikuti, hanya di Harisenin.com
              </div>
            </div>
          </div>
        )}

        {/* Pagination button not appeared when program below 6 */}
        {totalData > 6 && totalData > schoolData.length && (
          <RegularButton
            type="solid"
            color="green"
            className={`${style["landing_page__program__link"]} mx-auto`}
            onClick={handleSeeMore}
          >
            {isSeeMore ? (
              // Will show loading animation when fetching data
              <Spinner animation="grow" variant="light" />
            ) : (
              "Muat Lainnya"
            )}
          </RegularButton>
        )}
      </div>
    </div>
  )
}

export default ProgramListColumn
>>>>>>> 94ccec2 (setup fix):src/ui/components/program-list/ProgramListColumn.tsx
