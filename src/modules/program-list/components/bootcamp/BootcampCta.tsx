import React from "react"

import { LinkButton } from "@components/buttons"
import clsx from "clsx"
import Image from "next/image"
import { HARISENIN_PUBLIC_PAGE_ASSETS } from "@constants/pictures"

export default function BootcampCta() {
  return (
    <section
      className={clsx(
        "bg-[#001D43] relative w-full sm:py-[90px] py-[72px]",
        "flex items-center justify-center"
      )}
    >
      <Image
        src={`${HARISENIN_PUBLIC_PAGE_ASSETS}/home_cta-assets-1.png`}
        alt="cta-top-left"
        width={300}
        height={300}
        className="absolute top-0 left-0 w-[100px] sm-only:hidden"
      />
      <Image
        src={`${HARISENIN_PUBLIC_PAGE_ASSETS}/home_cta-assets-2.png`}
        alt="cta-top-right"
        width={300}
        height={300}
        className="absolute top-0 right-0 w-[100px] sm-only:hidden"
      />

      <div className="m-auto text-white">
        <h3
          className={clsx("sm:text-[40px] text-white font-semibold mb-3 text-center", "text-2xl")}
        >
          Galau? Bingung?
        </h3>
        <div className="sm:text-2xl mb-8 text-center">
          Obrolin dulu aja dengan team admission harisenin.com!
        </div>
        <LinkButton
          href="https://api.whatsapp.com/send?phone=6281312117711"
          className={clsx("w-fit rounded-none")}
          customPadding="sm:py-[14px] sm:px-[50px] py-4 px-6 mx-auto"
        >
          Klik untuk Konsultasi. Gratis!
        </LinkButton>
      </div>

      <Image
        src={`${HARISENIN_PUBLIC_PAGE_ASSETS}/home_cta-assets-3.png`}
        alt="cta-bottom-right"
        width={300}
        height={300}
        className="absolute bottom-0 right-0 w-[50px] h-auto sm-only:hidden"
      />
      <Image
        src={`${HARISENIN_PUBLIC_PAGE_ASSETS}/home_cta-assets-4.png`}
        alt="cta-bottom-left"
        width={300}
        height={300}
        className="absolute bottom-0 left-0 w-[100px] sm-only:hidden"
      />
    </section>
  )
}
