import { Container, FlexBox } from "@components/wrapper"
import clsx from "clsx"
import { FaCheckCircle, FaTimesCircle } from "react-icons/fa"
import { HeaderBootcamp } from "./HeaderBootcamp"

const DATA = [
  {
    title: "Total Biaya",
    harisenin: "Rp1,5 Juta–15 Juta",
    other: "Rp5 Juta–50 Juta",
  },
  {
    title: "Beasiswa",
    harisenin: "yes",
    other: "Tergantung Penyedia",
  },
  {
    title: "Team Support",
    harisenin: "yes",
    other: "Tergantung Penyedia",
  },
  {
    title: "Durasi Program",
    harisenin: "3–5 Bulan",
    other: "2–12 Bulan",
  },
  {
    title: "Format Penyampaian Materi",
    harisenin: "100% Online",
    other: "Online + Offline (Tergantung Penyedia)",
  },
  {
    title: "Mental Health & Professional English Session",
    harisenin: "yes",
    other: "no",
  },
  {
    title: "Get A Job Program",
    harisenin: "Jaminan Uang Kembali hingga 110%",
    other: "Jaminan Uang Kembali 0%–100%",
  },
  {
    title: "Career Coaching",
    harisenin: "Lengkap. Mulai dari Konsultasi Karier dan CV s.d. Mock Interview",
    other: "Tergantung Penyedia",
  },
  {
    title: "Menjadi Profesional Partner",
    harisenin: "yes",
    other: "Tergantung Penyedia",
  },
]

const BootcampWhyColumn = ({ screenWidth }: { screenWidth: number }) => {
  const TABLE_COLUMN = "text-left border border-[rgba(58,53,65,0.12)]"
  const TABLE_PADDING = "sm:px-4 sm:py-6"

  return (
    <section className={clsx("bg-[#F7F9FA] sm:py-[80px] py-8")}>
      <Container>
        <HeaderBootcamp
          title="Mengapa Bootcamp Harisenin.com?"
          subTitle="Berikut alasan yang bisa ngeyakinin kamu untuk pilih Bootcamp kami"
        />

        {screenWidth > 576 && (
          <table className="w-full">
            <thead className={TABLE_COLUMN}>
              <th className={clsx("sm:w-1/3 bg-white", TABLE_COLUMN)} />
              <th
                className={clsx("sm:w-1/3 bg-[#EBEFFC] sm:text-2xl", TABLE_COLUMN, TABLE_PADDING)}
              >
                Harisenin.com
              </th>
              <th className={clsx("sm:w-1/3 sm:text-2xl bg-white", TABLE_COLUMN, TABLE_PADDING)}>
                Bootcamp Lain
              </th>
            </thead>

            <tbody>
              {DATA.map((d, i) => (
                <tr key={i}>
                  <td
                    className={clsx(
                      "sm:w-1/3 sm:text-lg font-semibold bg-white",
                      TABLE_COLUMN,
                      TABLE_PADDING
                    )}
                  >
                    {d.title}
                  </td>
                  <td
                    className={clsx(
                      "sm:w-1/3 sm:text-lg bg-[#EBEFFC]",
                      TABLE_COLUMN,
                      TABLE_PADDING
                    )}
                  >
                    {d.harisenin === "yes" ? (
                      <FaCheckCircle className="text-green sm:text-2xl" />
                    ) : (
                      d.harisenin
                    )}
                  </td>
                  <td className={clsx("sm:w-1/3 sm:text-lg bg-white", TABLE_COLUMN, TABLE_PADDING)}>
                    {d.other === "no" ? (
                      <FaTimesCircle className="text-red sm:text-2xl" />
                    ) : (
                      d.other
                    )}
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        )}

        {screenWidth < 576 && (
          <FlexBox direction="col" className="gap-4">
            {DATA.map((d, i) => (
              <table key={i} className="w-full border border-[rgba(58,53,65,0.12)]">
                <thead className="border border-[rgba(58,53,65,0.12)]">
                  <th colSpan={2} className="p-4 text-lg bg-white">
                    {d.title}
                  </th>
                </thead>

                <tbody>
                  <tr>
                    <td className="font-semibold p-4 border border-[rgba(58,53,65,0.12)] bg-[#EBEFFC] w-1/2">
                      Harisenin.com
                    </td>
                    <td className="p-4 border border-[rgba(58,53,65,0.12)] bg-[#EBEFFC] text-sm w-1/2">
                      {d.harisenin === "yes" ? (
                        <FaCheckCircle className="text-green text-2xl" />
                      ) : (
                        d.harisenin
                      )}
                    </td>
                  </tr>

                  <tr className="bg-white">
                    <td className="p-4 border border-[rgba(58,53,65,0.12)] w-1/2">Bootcamp lain</td>
                    <td className="p-4 border border-[rgba(58,53,65,0.12)] text-sm w-1/2">
                      {d.other === "no" ? <FaTimesCircle className="text-red text-2xl" /> : d.other}
                    </td>
                  </tr>
                </tbody>
              </table>
            ))}
          </FlexBox>
        )}
      </Container>
    </section>
  )
}

export default BootcampWhyColumn
