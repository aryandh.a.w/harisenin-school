import { RegularButton } from "@components/buttons"
import { NextImage } from "@components/misc"
import { container } from "@components/wrapper"
import { HARISENIN_PUBLIC_PAGE_ASSETS } from "@constants/pictures"
import clsx from "clsx"
import React from "react"

export const BootcampListBanner = ({
  screenWidth,
  executeScroll,
}: {
  screenWidth: number
  executeScroll: (id: string) => () => void
}) => {
  return (
    <section className={clsx("sm:w-full sm:pt-15 ")}>
      <div
        className={clsx(
          container,
          "sm:flex sm:justify-center sm:h-full sm:my-auto sm:mx-auto items-center sm:gap-9",
          "sm-only:block sm-only:!w-full sm-only:relative"
        )}
      >
        {/* Left section of banner */}
        <div
          className={clsx(
            "sm:w-1/2 h-fit",
            "w-full sm-only:py-10 sm-only:h-auto sm-only:flex sm-only:flex-col sm-only:justify-around sm-only:items-center"
          )}
        >
          {/* Title */}
          <h1 className={clsx("sm:text-[56px] sm:mb-6 font-normal", "text-[32px] mb-3")}>
            Upgrade <span className="font-semibold text-blue">Skill</span> Untuk{" "}
            <span className="font-semibold text-blue">Karier</span> yang Lebih Baik
          </h1>

          {/* Description */}
          <div className={clsx("sm:mb-6 leading-6", "mb-[30px] sm-only:text-sm")}>
            Pengen ningkatin skill di tengah kompetitifnya industri, tapi bingung gimana caranya?
            Mulai perjalanan belajarmu bareng harisenin.com aja! Selama 3–5 bulan, kamu bisa belajar
            langsung dari ahlinya, berkesempatan untuk dapet networking yang luas dan juga beragam
            benefit menarik lainnya.
          </div>

          {/* Button for scrolling*/}
          <RegularButton
            onClick={executeScroll("bootcamp-list")}
            id="btn-regist-class-banner"
            customPadding="sm:px-10 sm:py-3 py-4 px-4"
            className="sm-only:w-full !rounded-none"
          >
            Pilih Bootcamp
          </RegularButton>
        </div>

        {/* Right section of banner */}
        {screenWidth > 576 && (
          <div className={clsx("sm:w-1/2 h-fit")}>
            <NextImage
              size={[1440, 789]}
              src={`${HARISENIN_PUBLIC_PAGE_ASSETS}/bootcamp_banner.png`}
              alt="bootcamp banner"
              className={clsx("sm:h-auto sm:w-4/5 float-right", "sm-only:h-full sm-only:absolute")}
            />
          </div>
        )}
      </div>
    </section>
  )
}
