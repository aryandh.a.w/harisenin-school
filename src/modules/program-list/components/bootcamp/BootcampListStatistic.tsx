import { Container } from "@components/wrapper"
import clsx from "clsx"
import React from "react"

const STATISTIC = [
  {
    value: "99.98%",
    label: "Alumni sudah berhasil bekerja",
  },
  {
    value: "3000+ Orang",
    label: "Memilih belajar di harisenin.com",
  },
  {
    value: "500+ Company",
    label: "Mempercayai dan merekrut alumni kami",
  },
]

export const BootcampListStatistic = ({ screenWidth }: { screenWidth: number }) => {
  return (
    <section className={clsx("bg-[#F7F9FA] sm:py-[30px] sm:mt-20", "py-6 px-5")}>
      <Container className={clsx("grid sm:grid-cols-3")}>
        {STATISTIC.map((s, i) => (
          <div
            key={i}
            className={clsx(
              "grid gap-2.5 sm:py-6 relative",
              "py-6",
              i === 1 && "sm:border-x border-[rgba(58,53,65,0.12)] flex py-5",
              i === 1 &&
                screenWidth < 576 &&
                "before:content-[''] before:h-[1px] before:w-1/2 before:absolute before:bg-[rgba(58,53,65,0.12)] before:left-[calc(25%)] before:top-0",
              i === 1 &&
                screenWidth < 576 &&
                "after:content-[''] after:h-[1px] after:w-1/2 after:absolute after:bg-[rgba(58,53,65,0.12)] after:left-[calc(25%)] after:bottom-0",
              i !== 0 && "sm:pl-[60px]"
            )}
          >
            <div
              className={clsx("sm:text-[32px] font-semibold", "text-[28px] sm-only:text-center")}
            >
              {s.value}
            </div>
            <div className={clsx("text-[#484848]", "sm-only:text-center")}>{s.label}</div>
          </div>
        ))}
      </Container>
    </section>
  )
}
