import { ProgramCard } from "@components/card/SchoolCard"
import { Container, FlexBox } from "@components/wrapper"
import { School } from "@interfaces/school"
import SchoolServices from "@lib/api/services/school.services"
import { useScreenSize } from "@lib/hooks/useScreenSize"
import clsx from "clsx"
import { useRouter } from "next/router"
import { useEffect, useState } from "react"
import { HeaderBootcamp } from "./HeaderBootcamp"

export const BootcampListColumn = ({ data }: { data: School[] }) => {
  const [isLoading, setIsLoading] = useState(false)
  const [schoolData, setSchoolData] = useState<School[]>(data)
  const [currentPage, setCurrentPage] = useState(1)

  const router = useRouter()
  const { isReady, query } = router

  const school = new SchoolServices()
  const { screenWidth } = useScreenSize()

  // Data handler
  useEffect(() => {
    if (!isReady) return
    const getData = async () => {
      try {
        setIsLoading(true)
        const res = await school.getSchoolListV2("BOOTCAMP", {
          limit: 100,
          search: query.search as string,
          page: currentPage,
        })

        if (res.isSuccess) {
          const result = res.getValue()
          setSchoolData(result.data)
        }
        setIsLoading(false)
      } catch (e) {
        // Bugsnag.notify(e);
        setIsLoading(false)
      }
    }

    getData()

    return () => {
      setSchoolData([])
      setCurrentPage(1)
    }
  }, [query])

  // async function handleSeeMore() {
  //   try {
  //     setCurrentPage(currentPage + 1)
  //     setIsSeeMore(true)

  //     const res = await school.getSchoolListV2(isBootcamp ? "BOOTCAMP" : "PRO_CLASS", {
  //       limit: 6,
  //       page: currentPage + 1,
  //       search: query.search as string,
  //     })

  //     if (res.isSuccess) {
  //       const result = res.getValue()
  //       setSchoolData([...schoolData, ...result.data])
  //     }
  //     setIsSeeMore(false)
  //   } catch (e) {
  //     setIsSeeMore(false)
  //   }
  // }

  if (!schoolData || !schoolData.length) {
    return <></>
  }

  return (
    <section className={clsx("bg-[#F7F9FA] sm:py-[60px] py-6 relative")}>
      <div className="absolute top-[-20px]" id="bootcamp-list" />
      <Container>
        <HeaderBootcamp
          title="Program Bootcamp"
          subTitle="Belajar. Dapet kerja. Atau uang kembali up to 110%"
        />

        {isLoading && <FlexBox></FlexBox>}

        <div className={clsx("flex flex-wrap -mx-4 sm-only:gap-[18px]")}>
          {schoolData.map((value, index) => (
            <div
              className={clsx("w-[calc(33.33%-20px)] mx-2 sm:mb-4", "sm-only:w-full")}
              key={index}
            >
              <ProgramCard
                className={clsx("!w-full", "sm-only:mr-0")}
                item={value}
                key={index}
                index={index}
                screenWidth={screenWidth}
              />
            </div>
          ))}
        </div>
      </Container>
    </section>
  )
}
