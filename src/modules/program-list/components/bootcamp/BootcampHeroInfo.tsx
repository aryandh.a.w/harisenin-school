import { NextImage } from "@components/misc"
import { container } from "@components/wrapper"
import { HARISENIN_PUBLIC_PAGE_ASSETS } from "@constants/pictures"
import clsx from "clsx"
import React from "react"

const BootcampHeroInfo = () => {
  return (
    <section
      className={clsx(
        container,
        "sm:my-[60px] flex sm:gap-[80px] sm:items-center",
        "sm-only:flex-col gap-[18px] my-9"
      )}
    >
      <NextImage
        src={`${HARISENIN_PUBLIC_PAGE_ASSETS}/bootcamp_asset_1.png`}
        size={[800, 600]}
        className={clsx("sm:w-2/5")}
      />

      <div className="sm:w-3/5">
        <h2 className="sm:text-[48px] text-2xl font-medium mb-4">
          Dari <span className="text-green font-semibold">2021</span>, Hingga Sekarang
        </h2>
        <div className="sm:text-xl text-[#484848] text-sm">
          Gak terasa harisenin.com telah hadir hingga saat ini. Namun, perjalanan kami gak berhenti
          di sini. Kami akan terus berkomitmen untuk memberikan solusi yang terbaik atas kebutuhan
          kariermu. Karena kami percaya, kamu pun bisa #JadiYangKamuMau
        </div>
      </div>
    </section>
  )
}

export default BootcampHeroInfo
