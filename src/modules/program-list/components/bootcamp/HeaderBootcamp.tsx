import clsx from "clsx"
import React, { ReactNode } from "react"

interface HeaderBootcampProps {
  title: ReactNode
  subTitle: string
}

export const HeaderBootcamp = ({ title, subTitle }: HeaderBootcampProps) => {
  return (
    <div className={clsx("text-center sm:mb-10 mb-[18px]")}>
      <h2 className={clsx("sm:text-[40px] text-2xl font-semibold mb-3")}>{title}</h2>
      <h3 className={clsx("text-[#484848] sm:text-2xl")}>{subTitle}</h3>
    </div>
  )
}
