import { Container } from "@components/wrapper"
import { Disclosure, Transition } from "@headlessui/react"
import { FAQ } from "@interfaces/school"
import SchoolServices from "@lib/api/services/school.services"
import { useIsMounted } from "@lib/hooks/useIsMounted"
import clsx from "clsx"
import { useEffect, useState } from "react"
import { FaChevronDown } from "react-icons/fa"
import { HeaderBootcamp } from "./HeaderBootcamp"

export default function BootcampFaqContainer() {
  const [faqs, setFaqs] = useState<FAQ[]>([])

  const school = new SchoolServices()
  const isMounted = useIsMounted()

  useEffect(() => {
    const getData = async () => {
      try {
        const res = await school.getHomepageFaqs()
        if (res.isSuccess) {
          const result = res.getValue()

          if (result.length) {
            setFaqs(result)
          }
        }
      } catch (e) {
        if (process.env.NODE_ENV === "development") {
          console.log({ e })
        }
      }
    }

    if (isMounted) {
      getData()
    }
  }, [isMounted])

  return (
    <section className="sm:py-[60px] py-9">
      <Container>
        <HeaderBootcamp
          title="Pertanyaan Seputar Bootcamp"
          subTitle="Tenang, kami punya jawabannya"
        />
      </Container>

      <div className="grid gap-4 sm:w-3/5 mx-auto">
        {faqs.map((value, index) => (
          <Disclosure key={index}>
            {({ open }) => (
              <div className="border border-[rgba(58,53,65,0.12)]">
                <Disclosure.Button
                  className={clsx(
                    "flex justify-between bg-[#f7f9fa] p-4 font-medium sm:text-base text-left w-full",
                    "text-sm"
                  )}
                >
                  {value.faq_question}
                  <FaChevronDown className={clsx(open && "rotate-180 transform text-green")} />
                </Disclosure.Button>
                <Transition
                  show={open}
                  enter="transition duration-100 ease-out"
                  enterFrom="transform scale-95 opacity-0"
                  enterTo="transform scale-100 opacity-100"
                  leave="transition duration-75 ease-out"
                  leaveFrom="transform scale-100 opacity-100"
                  leaveTo="transform scale-95 opacity-0"
                >
                  <Disclosure.Panel static>
                    <div
                      className={clsx(
                        "bg-[#f7f9fa] px-4 py-2 sm:text-base text-light-grey",
                        "text-sm"
                      )}
                    >
                      {value.faq_answer}
                    </div>
                  </Disclosure.Panel>
                </Transition>
              </div>
            )}
          </Disclosure>
        ))}
      </div>
    </section>
  )
}
