import { DynamicImage } from "@components/misc"
import { SliderArrow } from "@components/slider"
import { container, FlexBox } from "@components/wrapper"
import { HARISENIN_PUBLIC_PAGE_ASSETS } from "@constants/pictures"
import { StudentTestimonial } from "@interfaces/school"
import MiscServices from "@lib/api/services/misc.services"
import { useIsMounted } from "@lib/hooks/useIsMounted"
import clsx from "clsx"
import Image from "next/image"
import { useEffect, useState } from "react"
import Carousel, { DotProps } from "react-multi-carousel"
import { HeaderBootcamp } from "./HeaderBootcamp"

const TestimonialCard = ({ value }: { value: StudentTestimonial }) => {
  if (!value) {
    return <></>
  }

  return (
    <FlexBox
      direction="col"
      justify="between"
      className={clsx(
        "relative sm:py-[18px] sm:px-[26px] sm:h-[18em] bg-white shadow-[0_2px_15px_0_#9aaacf33] sm:w-auto",
        "w-full px-7 py-7"
      )}
    >
      <div className={clsx("lg:leading-6 sm:h-[9rem] mb-6", "leading-[22px]")}>
        {value.testimony}
      </div>

      <FlexBox justify="between" align="end">
        <FlexBox align="center" className="gap-2.5">
          <div className="rounded w-[60px] h-[60px]">
            <Image
              src={value.testimony_user_picture}
              alt={value.testimony_username}
              width={60}
              height={60}
            />
          </div>
          <div className="text-sm">
            <div className="font-semibold sm:text-lg">{value.testimony_username}</div>
            <div className="text-[#909090] text-sm">{value.testimony_user_as}</div>
          </div>
        </FlexBox>

        <div className="max-w-[35%] h-auto object-contain">
          {value.company && (
            <DynamicImage
              height={60}
              width={40}
              src={value.company.company_picture}
              alt={value.company.company_name}
              className="h-auto max-w-[60px] max-h-[40px] object-contain"
            />
          )}
        </div>
      </FlexBox>
    </FlexBox>
  )
}

const CustomDot = ({ active, onClick }: DotProps) => {
  return (
    <button
      className={clsx(
        "wh-3 rounded-full mx-1.5 border-0 transition-all duration-200",
        active ? "bg-blue" : "bg-grey-c4 w-auto rounded-0"
      )}
      onClick={(e) => {
        if (onClick) {
          onClick()
        }
        e.preventDefault()
      }}
    />
  )
}

export function BootcampTestimonial({ screenWidth }: { screenWidth: number }) {
  // const [isLoading, setIsLoading] = useState(true)
  const [testimonials, setTestimonials] = useState<StudentTestimonial[]>([])

  const misc = new MiscServices()
  const isMounted = useIsMounted()

  useEffect(() => {
    const getData = async () => {
      try {
        const res = await misc.getTestimonials("school")

        if (res.isSuccess) {
          const data = res.getValue()
          setTestimonials(data)
        }

        // setIsLoading(false)
      } catch (e: any) {
        // school.bugsnagNotifier(e)
        // setIsLoading(false)
      }
    }

    if (isMounted) {
      getData()
    }
  }, [isMounted])

  const responsive = {
    superLargeDesktop: {
      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: 3,
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 3,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 1,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
    },
  }

  if (!testimonials.length) {
    return <></>
  }

  return (
    <section className="bg-[#f7f9fa]">
      <div className={clsx("relative lg:py-10 pt-6 pb-[58px]", container)}>
        <div className="absolute sm-only:left-[35%]">
          <Image
            src={`${HARISENIN_PUBLIC_PAGE_ASSETS}/testimonial_quote.png`}
            height={88}
            width={105}
            alt="quote"
          />
        </div>

        <HeaderBootcamp
          title={
            <>
              <span className="text-blue">Testimoni</span> Alumni
            </>
          }
          subTitle="  Yang mereka rasakan selama mengikuti program harisenin.com"
        />

        <Carousel
          responsive={responsive}
          ssr={true} // means to render carousel on server-side.
          arrows={false}
          showDots={screenWidth < 576}
          {...(screenWidth > 576 && {
            customButtonGroup: (
              <SliderArrow
                length={testimonials.length}
                displayedSlide={3}
                leftClassName="-left-[1vw] top-[55%] w-[42px] h-[42px]"
                rightClassName="-right-[1vw] top-[55%] w-[42px] h-[42px]"
              />
            ),
            renderButtonGroupOutside: true,
          })}
          {...(screenWidth < 576 && {
            customDot: <CustomDot />,
            renderDotsOutside: true,
            dotListClass: "absolute !bottom-6 mt-6 mb-2",
            autoPlay: true,
            infinite: true,
            autoPlaySpeed: 3000,
          })}
          itemClass="px-[1.35vw]"
          containerClass="sm:pb-5 -mx-[1.35vw]"
        >
          {testimonials.map((value, index) => (
            <TestimonialCard value={value} key={index} />
          ))}
        </Carousel>
      </div>
    </section>
  )
}
