<<<<<<< HEAD:src/modules/program-list/components/ProgramListBanner.tsx
import { useRouter } from "next/router"
import { useEffect, useState } from "react"
import { FiSearch } from "react-icons/fi"

import { HARISENIN_PUBLIC_PAGE_ASSETS } from "@constants/pictures"
import { NextImage } from "@components/misc"
import clsx from "clsx"
import { container } from "@components/wrapper"

export const ProgramListBanner = () => {
  const [search, setSearch] = useState("")
  const router = useRouter()
  const query = router.query

  async function handleSearch() {
    await router.push(`?search=${search}&type=${query?.type ?? "bootcamp"}#search`)
  }

  async function handleKeyDown(event) {
    if (event.key === "Enter") {
      await router.push(`?search=${search}&type=${query?.type ?? "bootcamp"}#search`)
    }
  }

  useEffect(() => {
    if (query.search !== undefined) {
      setSearch(query.search as string)
    }
  }, [query])

  return (
    <div className={clsx("bg-[#f7f9fa] sm:py-5 px-0", "py-8")}>
      <div className={clsx(container, "flex items-center justify-between", "sm-only:flex-col-reverse")}>
        {/* Left section of Banner */}
        <div className={clsx("sm:w-1/2", "w-full")}>
          <h1
            className={clsx(
              "w-fit sm:text-4xl sm:leading-[52px] sm:mb-4 font-semibold text-black",
              "text-lg leading-[150%] mb-3"
            )}
          >
            Temukan Program yang
            <br />
            Kamu <span className="text-green">Butuhkan</span>
          </h1>
          <div className={clsx("sm:mb-8 leading-[174%]", " leading-[150%] mb-3")}>
            Pelajari keterampilan yang paling banyak dibutuhkan untuk <br />
            pekerjaan hari ini dan masa depan dengan harga kompetitif
          </div>
          <div className={clsx("sm:w-[75%] flex relative", "w-[95%]")}>
            <div className={clsx("flex flex-col justify-center absolute h-full text-grey-c4 px-2")}>
              <FiSearch size="16px" />
            </div>
            <input
              type="text"
              value={search}
              onChange={(e) => setSearch(e.currentTarget.value)}
              className={clsx(
                "sm:py-3 sm:px-11 sm:text-sm rounded w-full leading-5 font-normal placeholder:text-grey-c4 focus:outline-green focus:outline focus:border focus:border-white",
                "py-2 px-9 text-xs"
              )}
              onKeyDown={handleKeyDown}
              placeholder="Cari program yang tepat untukmu"
              id="hms-search-field"
            />
            <button
              onClick={handleSearch}
              type="button"
              id="btn-search-hms"
              className={clsx(
                "sm:text-sm bg-green text-white border-none rounded absolute right-0 leading-5 font-normal h-full py-2 px-4",
                "text-xs"
              )}
            >
              Cari
            </button>
          </div>
        </div>

        {/* Right section of banner, will be hidden on mobile view */}
        <div className={clsx("flex justify-end w-1/2", "sm-only:hidden")}>
          <NextImage
            alt="harisenin-millennial-school"
            // src={`${HARISENIN_PUBLIC_PAGE_ASSETS}/homepage_banner_people-6.png`}
            src={`${HARISENIN_PUBLIC_PAGE_ASSETS}/banner_school-list.png`}
            size={[487, 574]}
            className="w-4/5"
          />
        </div>
      </div>
    </div>
  )
}
=======
import { useEffect, useState } from "react"
import { useRouter } from "next/router"
import { Container } from "react-bootstrap"
import { FiSearch } from "react-icons/fi"

import { HARISENIN_PUBLIC_PAGE_ASSETS } from "../../../constants/pictures"
import style from "./styles/programlist.module.scss"
import Image from "next/image"
import { NextImage } from "../../modules/misc"

const ProgramListBanner = () => {
  const [search, setSearch] = useState("")
  const router = useRouter()
  const query = router.query

  async function handleSearch() {
    await router.push(`?search=${search}&type=${query?.type ?? "bootcamp"}#search`)
  }

  async function handleKeyDown(event) {
    if (event.key === "Enter") {
      await router.push(`?search=${search}&type=${query?.type ?? "bootcamp"}#search`)
    }
  }

  useEffect(() => {
    if (query.search !== undefined) {
      setSearch(query.search as string)
    }
  }, [query])

  return (
    <div className={`${style["landing_page__banner"]} ${style["landing_page__banner--list"]}`}>
      <Container className={style["landing_page__banner__container"]}>
        {/* Left section of Banner */}
        <div
          className={`${style["landing_page__banner__left"]} ${style["landing_page__banner__left--list"]}`}
        >
          <h1 className={`${style["landing_page__banner__left"]} h1`}>
            Temukan Program yang
            <br />
            Kamu <span className="text-green">Butuhkan</span>
          </h1>
          <div className={style["landing_page__banner__left__subtitle"]}>
            Pelajari keterampilan yang paling banyak dibutuhkan untuk <br />
            pekerjaan hari ini dan masa depan dengan harga kompetitif
          </div>
          <div className={style["landing_page__banner__left__search"]}>
            <div className={style["landing_page__banner__left__search__icon"]}>
              <FiSearch size="16px" />
            </div>
            <input
              type="text"
              value={search}
              onChange={(e) => setSearch(e.currentTarget.value)}
              className={style["landing_page__banner__left__search__input"]}
              onKeyDown={handleKeyDown}
              placeholder="Cari program yang tepat untukmu"
              id="hms-search-field"
            />
            <button
              onClick={handleSearch}
              type="button"
              id="btn-search-hms"
              className={style["landing_page__banner__left__search__button"]}
            >
              Cari
            </button>
          </div>
        </div>

        {/* Right section of banner, will be hidden on mobile view */}
        <div
          className={`${style["landing_page__banner__right"]} ${style["landing_page__banner__right--list"]}`}
        >
          <NextImage
            alt="harisenin-millennial-school"
            // src={`${HARISENIN_PUBLIC_PAGE_ASSETS}/homepage_banner_people-6.png`}
            src={`${HARISENIN_PUBLIC_PAGE_ASSETS}/banner_school-list.png`}
            size={[487, 574]}
          />
        </div>
      </Container>
    </div>
  )
}

export default ProgramListBanner
>>>>>>> 94ccec2 (setup fix):src/ui/components/program-list/ProgramListBanner.tsx
