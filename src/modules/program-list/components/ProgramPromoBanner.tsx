import { container } from "@components/wrapper"
import { HARISENIN_PUBLIC_PAGE_ASSETS } from "@constants/pictures"

import clsx from "clsx"
import Image from "next/image"

export const ProgramPromoBanner = () => {
  return (
    <div className={clsx("bg-[#f7f9fa] sm:py-5 px-0", "py-8")}>
      <div
        className={clsx(container, "flex items-center justify-between", "sm-only:flex-col-reverse")}
      >
        {/* Left section of Banner */}
        {/* Left section of Banner */}
        <div className={clsx("sm:w-1/2", "w-full")}>
          {" "}
          <h1
            className={clsx(
              "w-fit sm:text-4xl sm:leading-[52px] sm:mb-4 font-semibold text-black",
              "text-lg leading-[150%] mb-3"
            )}
          >
            Temukan Program yang
            <br />
            Kamu <span className="text-green">Butuhkan</span>
          </h1>
          <div className={clsx("sm:mb-8 leading-[174%]", "sm-only:text-lg leading-[150%] mb-3")}>
            Pelajari keterampilan yang paling banyak dibutuhkan untuk pekerjaan hari ini dan masa
            depan dengan harga kompetitif.
          </div>
        </div>

        {/* Right section of banner, will be hidden on mobile view */}
        <div className={clsx("flex justify-end w-1/2", "sm-only:hidden")}>
          <Image
            alt="harisenin-millennial-school"
            src={`${HARISENIN_PUBLIC_PAGE_ASSETS}/homepage_banner_1.png`}
            width={500}
            height={500}
            // src={`${HARISENIN_PUBLIC_PAGE_ASSETS}/homepage_banner_2.png`}
          />
        </div>
      </div>
    </div>
  )
}
