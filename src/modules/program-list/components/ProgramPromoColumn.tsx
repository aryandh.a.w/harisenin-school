<<<<<<< HEAD
=======
<<<<<<< HEAD:src/modules/program-list/components/ProgramPromoColumn.tsx
import { useRouter } from "next/router"
>>>>>>> 6cd4eae (setup fix)
import { useEffect, useState } from "react"

import { School, SchoolPagination } from "@interfaces/school"
import { RegularButton } from "@components/buttons"
import SchoolServices from "@services/school.services"
import clsx from "clsx"
import { container } from "@components/wrapper"
import { Spinner } from "@components/misc"
import { ProgramCard } from "@components/card/SchoolCard"
import { useScreenSize } from "@lib/hooks/useScreenSize"
import { useIsMounted } from "@lib/hooks/useIsMounted"

export const ProgramPromoColumn = () => {
  const [isLoading, setIsLoading] = useState(true)
  const [isSeeMore, setIsSeeMore] = useState(false)
  const [schoolData, setSchoolData] = useState<School[]>([])
  const [totalData, setTotalData] = useState(0)
  const [currentPage, setCurrentPage] = useState(1)

  const router = useRouter()
  const { id } = router.query
  const school = new SchoolServices()
  const { screenWidth } = useScreenSize()
  const isMounted = useIsMounted()

  // Data handler
  useEffect(() => {
    const getData = async () => {
      try {
        setIsLoading(true)

        if (id) {
          const res = await school.getPromoSchoolList(id as string, { page: currentPage, limit: 5 })

          if (res.isSuccess) {
            const data: SchoolPagination = res.getValue()
            setSchoolData([...schoolData, ...data.data])
            setTotalData(data.totalData)
          }
        }
        setIsLoading(false)
      } catch (e) {
        // Bugsnag.notify(e);
        setIsLoading(false)
      }
    }

    if (router && isMounted) {
      getData()
    }
  }, [router.query, isMounted])

  async function handleSeeMore() {
    try {
      setCurrentPage(currentPage + 1)
      setIsSeeMore(true)

      const res = await school.getPromoSchoolList(id as string, { page: currentPage + 1 })

      if (res.isSuccess) {
        const data: SchoolPagination = res.getValue()
        setSchoolData([...schoolData, ...data.data])
      }
      setIsSeeMore(false)
    } catch (e) {
      setIsSeeMore(false)
    }
  }

  return (
    <div className={clsx("px-0 sm:py-15", "py-8")}>
      <div className={clsx(container, "mb-5")}>
        <h2
          className={clsx(
            "sm:text-2xl font-semibold sm:mb-[10px]  text-[#231f20] text-center",
            "text-base mb-2"
          )}
        >
          Program yang diberikan promo
        </h2>
        {isLoading ? (
          // Loading state component
          <div className="flex justify-center items-center h-[50hv]">
            <Spinner />
          </div>
        ) : (
          <div className={clsx("flex flex-wrap -mx-4")}>
            {schoolData.map((value, index) => (
              <div
                className={clsx("w-[calc(33.33%-20px)] mx-2 sm:mb-4", "sm-only:w-full")}
                key={index}
              >
                <ProgramCard
                  className={clsx("!w-full", "sm-only:mr-0")}
                  item={value}
                  key={index}
                  index={index}
                  screenWidth={screenWidth}
                />
              </div>
            ))}
          </div>
        )}
        {/* Pagination button not appeared when program below 6 */}
        {totalData > 6 && totalData > schoolData.length && (
          <RegularButton
            variant="solid"
            color="green"
            className={clsx(
              "mx-auto sm:2/5 sm:p-3 sm:mt-4",
              "sm-only:w-full p-2 mt-2 sm-only:font-medium sm-only:rounded"
            )}
            onClick={handleSeeMore}
          >
            {isSeeMore ? (
              // Will show loading animation when fetching data
              <Spinner />
            ) : (
              "Muat Lainnya"
            )}
          </RegularButton>
        )}
      </div>
    </div>
  )
}
=======
import { useEffect, useState } from "react"
import { Container, Spinner } from "react-bootstrap"
import { useRouter } from "next/router"

import { School, SchoolPagination } from "../../../constants/interfaces/school"

import ProgramCard from "../../modules/card/ProgramCard"
import { RegularButton } from "../../modules/buttons"
import SchoolServices from "../../../lib/services/school.services"

import style from "./styles/programlist.module.scss"

const ProgramListColumn = () => {
  const [isLoading, setIsLoading] = useState(true)
  const [isSeeMore, setIsSeeMore] = useState(false)
  const [schoolData, setSchoolData] = useState<School[]>([])
  const [totalData, setTotalData] = useState(0)
  const [currentPage, setCurrentPage] = useState(1)

  const router = useRouter()
  const { id } = router.query
  const school = new SchoolServices()

  // Data handler
  useEffect(() => {
    const getData = async () => {
      try {
        setIsLoading(true)

        if (id) {
          const res = await school.getPromoSchoolList(id as string, { page: currentPage })

          if (res.isSuccess) {
            const data: SchoolPagination = res.getValue()
            setSchoolData([...schoolData, ...data.data])
            setTotalData(data.totalData)
          }
        }
        setIsLoading(false)
      } catch (e) {
        // Bugsnag.notify(e);
        setIsLoading(false)
      }
    }

    if (router) {
      getData()
    }
  }, [router.query])

  async function handleSeeMore() {
    try {
      setCurrentPage(currentPage + 1)
      setIsSeeMore(true)

      const res = await school.getPromoSchoolList(id as string, { page: currentPage + 1 })

      if (res.isSuccess) {
        const data: SchoolPagination = res.getValue()
        setSchoolData([...schoolData, ...data.data])
      }
      setIsSeeMore(false)
    } catch (e) {
      school.bugsnagNotifier(e)
      setIsSeeMore(false)
    }
  }

  return (
    <div className={`${style["landing_page__program"]} ${style["landing_page"]}`}>
      <Container className="mb-5">
        <h2 className={`${style["landing_page__program__h2"]}`}>Program yang diberikan promo</h2>
        {isLoading ? (
          // Loading state component
          <div className={style["landing_page__program__column--loading"]}>
            <Spinner animation="border" variant="info" />
          </div>
        ) : (
          <div className={style["landing_page__program__column"]}>
            {schoolData.map((value, index) => (
              <div
                className={`${style["landing_page__program__column__wrapper"]} ${style["landing_page__program__column__wrapper--last"]}`}
                key={index}
              >
                <ProgramCard
                  className={`${style["landing_page__program__column__wrapper__program_card"]}`}
                  data={value}
                  key={index}
                  index={index}
                />
              </div>
            ))}
          </div>
        )}

        {/* Pagination button not appeared when program below 6 */}
        {totalData > 6 && totalData > schoolData.length ? (
          <RegularButton
            type="solid"
            color="green"
            className={`${style["landing_page__program__link"]} mx-auto`}
            onClick={handleSeeMore}
          >
            {isSeeMore ? (
              // Will show loading animation when fetching data
              <Spinner animation="grow" variant="light" />
            ) : (
              "Muat Lainnya"
            )}
          </RegularButton>
        ) : null}
      </Container>
    </div>
  )
}

export default ProgramListColumn
>>>>>>> 94ccec2 (setup fix):src/ui/components/program-list/ProgramPromoColumn.tsx
