<<<<<<< HEAD:src/modules/program-list/pages/ProgramListPage.tsx
import Helmet from "@helpers/Helmet"

import { Header } from "@components/header"

import { ProgramListBanner, ProgramListColumn } from "@modules/program-list/components"

import { School } from "@interfaces/school"
import { HariseninFooter } from "@components/harisenin/HariseninFooter"

export interface ProgramListPage {
  data: School[]
  metaTitle: string
  metaDescription: string
}

const ProgramListPage = ({ data, metaDescription, metaTitle }: ProgramListPage) => {

  return (
    <>
      <Helmet
        title={metaTitle ?? "Harisenin.com: Semua Program"}
        description={metaDescription}
        isIndexed
      />
      <Header />
      <ProgramListBanner />
      <ProgramListColumn data={data} />
      <HariseninFooter />
    </>
  )
}

export default ProgramListPage
=======
import Helmet from "../../../lib/utils/Helmet"

import HeaderHome from "../../modules/header/HeaderHome"

import ProgramListBanner from "./ProgramListBanner"
import ProgramListColumn from "./ProgramListColumn"
import { School } from "../../../constants/interfaces/school"
import { HariseninFooter } from "../../modules/harisenin/HariseninFooter"

export interface ProgramListPage {
  data: School[]
  metaTitle: string
  metaDescription: string
}

const ProgramListPage = ({ data, metaDescription, metaTitle }: ProgramListPage) => {
  return (
    <>
      <Helmet
        title={metaTitle ?? "Harisenin.com: Semua Program"}
        description={metaDescription}
        isIndexed
      />
      <HeaderHome />
      <ProgramListBanner />
      <ProgramListColumn data={data} />
      <HariseninFooter />
    </>
  )
}

export default ProgramListPage
>>>>>>> 94ccec2 (setup fix):src/ui/components/program-list/ProgramListPage.tsx
