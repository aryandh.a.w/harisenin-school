import { HariseninFooter } from "@components/harisenin"
import { Header } from "@components/header"
import { School } from "@interfaces/school"
import Helmet from "@lib/helpers/Helmet"
import { useScreenSize } from "@lib/hooks/useScreenSize"
import BootcampAlumnusCompany from "../components/bootcamp/BootcampAlumnusCompany"
import BootcampCta from "../components/bootcamp/BootcampCta"
import BootcampFaqContainer from "../components/bootcamp/BootcampFaqContainer"
import BootcampHeroInfo from "../components/bootcamp/BootcampHeroInfo"
import { BootcampListBanner } from "../components/bootcamp/BootcampListBanner"
import { BootcampListColumn } from "../components/bootcamp/BootcampListColumn"
import { BootcampListStatistic } from "../components/bootcamp/BootcampListStatistic"
import { BootcampTestimonial } from "../components/bootcamp/BootcampTestimonial"
import BootcampWhyColumn from "../components/bootcamp/BootcampWhyColumn"

export interface ProgramListPage {
  bootcamps: School[]
  metaTitle: string
  metaDescription: string
}

const BootcampListPage = ({ bootcamps, metaDescription, metaTitle }: ProgramListPage) => {
  const { screenWidth } = useScreenSize()

  const executeScroll = (id: string) => () => {
    const ele = document.getElementById(id)

    if (!ele) {
      return
    }

    ele.scrollIntoView({ behavior: "smooth" })
  }

  return (
    <>
      <Helmet
        title={metaTitle ?? "Harisenin.com: Semua Program"}
        description={metaDescription}
        isIndexed
      />

      <Header hasShadow />

      <main>
        <BootcampListBanner screenWidth={screenWidth} executeScroll={executeScroll} />
        <BootcampListStatistic screenWidth={screenWidth} />
        <BootcampHeroInfo />
        <BootcampListColumn data={bootcamps} />
        <BootcampWhyColumn screenWidth={screenWidth} />
        <BootcampAlumnusCompany />
        <BootcampTestimonial screenWidth={screenWidth} />
        <BootcampFaqContainer />
        <BootcampCta />
      </main>

      <HariseninFooter />
    </>
  )
}

export default BootcampListPage
