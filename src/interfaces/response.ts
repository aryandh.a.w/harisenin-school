export interface ErrorMessage {
  message: string
}

export interface RawResponse<T> {
  message: string
  error: boolean
  result: T
  error_code: string
}

export interface ErrorResponse {
  error_code: string
  message: string
  validation?: string
}

export interface ValidationError {
  message: string
  field: string
  validation: string
}

export interface ValidatedResponse<T> {
  error: string
  error_code: string
  result?: T
}
