import { OrderStatus } from "./order"
import { SchoolBatch, SchoolProgram } from "./school"

export interface Checkout {
  id: string
  order_date: Date
  order_number: string
  order_price: number
  order_fee: number
  order_discount: number
  order_total_price: number
  order_status: OrderStatus
  order_clicked: number
  order_fill_requirement_date: Date
  order_finished_date: Date
  order_cart: CheckoutCart[]
  order_certificate: boolean
  order_payment?: CheckoutPayment
  discount?: DiscountCart
  updated_at: Date
}

export interface CheckoutPayment {
  adjusted_received_amount: number
  amount: number
  bank_code: string
  initial_amount: number
  paid_amount: number
  payment_channel: string
  payment_method: PaymentMethod
}

export interface DiscountCart {
  discount: DiscountDetail
  discount_deal: number
}
export interface DiscountDetail {
  created_at: Date
  discount_code: string
  discount_name: string
  discount_percentage: 0.5
  discount_price: 0
}

export interface CheckoutCart {
  id: string
  school_description: string
  school_programs: SchoolProgram[]
  school_slug: string
  school_title: string
  asset: {
    asset_name: string
    asset_path: string
  }
  available_batch: SchoolBatch[]
}

export type PaymentMethod = "BANK_TRANSFER" | "EWALLET" | "CREDIT_CARD"

export interface AvailablePayment {
  BANK_TRANSFER?: PaymentOption[]
  EWALLET?: PaymentOption[]
  CREDIT_CARD?: PaymentOption[]
}

export interface PaymentOption {
  payment_channel: string
  payment_channel_logo: string
  payment_fee: number
  payment_method: PaymentMethod
}

export interface Discount {
  discount_deal: number
  discount_name: string
  discount_description: string
  discount_percentage: number
  discount_price: number
  available_payment: AvailablePayment
}
