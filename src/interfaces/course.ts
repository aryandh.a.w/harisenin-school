export interface CourseCategory {
  id: string
  category_name: string
  category_slug: string
  category_description?: string
}

export interface CourseTeacher {
  id: string
  teacher_name: string
  teacher_slug: string
  teacher_last_job: string
  teacher_picture: string
  teacher_description?: string
  teacher_linkedin?: string
}

interface CourseThumbnail {
  id: string
  course_thumbnail_path: string
}

export interface Course {
  id: string
  course_name: string
  course_slug: string
  course_total_order: number
  course_total_review: number
  course_position: number
  course_price: number
  course_created_date: string
  course_discount_percentage: number
  course_original_price: number
  course_rating: number
  course_status: string
  course_tags: string[]
  course_categories: CourseCategory[]
  course_teacher: CourseTeacher
  course_thumbnail: CourseThumbnail
}
