import { SelectOption } from "./ui"
import { AvailablePromo } from "./promo"
import { Discount, PaymentOption } from "./checkout"
import { SchoolBatch } from "./school"

export type FormStatus = "Mahasiswa" | "Belum Bekerja" | "Karyawan" | ""

export type FormName =
  | "user_job"
  | "user_majors"
  | "user_faculty"
  | "user_fullname"
  | "user_university"
  | "user_desired_job"
  | "user_desired_job_sector"
  | "user_phone_number"
  | "user_batch_request"
  | "user_linkedin_url"

export type FormFileType = "identity_card" | "ijazah" | "student_card" | "last_study_card" | "cv"

export type University = {
  id: number
  university_name: string
}

export interface FormState {
  value: string
  error: string
}

export interface FormFileState {
  id?: number
  name: "File..." | string
  file?: File
  error: string
}

export interface ErrorCheck {
  form: string
  paymentOption: string
}

export interface CheckoutForm {
  user_batch_request: FormState
  user_fullname: FormState
  user_email: FormState
  user_phone_number: FormState
  user_university: FormState
  user_faculty: FormState
  user_majors: FormState
  user_job: FormState
  user_desired_job_sector: FormState
  user_desired_job: FormState
  user_linkedin_url: FormState
}

export interface CheckoutFormFile {
  identity_card: FormFileState
  ijazah: FormFileState
  student_card: FormFileState
  last_study_card: FormFileState
  cv: FormFileState
}

export interface RawCheckoutForm {
  user_job: string
  user_email: string
  user_majors: string
  user_faculty: string
  user_fullname: string
  user_university: string
  user_desired_job: string
  user_phone_number: string
  user_desired_job_sector: string
}

export interface RawCheckoutResponse {
  user_school: RawCheckoutForm
  identity_card: null | FormFileState
  ijazah: null | FormFileState
  student_card: null | FormFileState
  last_study_card: null | FormFileState
  cv: null | FormFileState
}

export interface CheckoutSummary {
  school_id: string
  school_slug: string
  school_title: string
  image: string
  program_id: number
  program_title: string
  program_description: string
  program_price: number
}

export interface CheckoutState {
  //Initial state
  gross_price: number
  bank_method: PaymentOption[]
  e_wallet_method: PaymentOption[]
  credit_card?: PaymentOption
  available_promos: AvailablePromo[]
  //form state
  isLoadingData: boolean
  form: CheckoutForm
  form_file: CheckoutFormFile
  university: SelectOption[]
  user_batch_request: SelectOption[]
  form_status: FormStatus
  form_status_error: string
  //error state
  error_discount: string
  error_submitting: string
  error_admin_fee: string
  error_checking: ErrorCheck
  //state for payment
  admin_fee: number
  selected_method: string
  selected_channel: string
  total_price: number
  modal_open: CheckoutModalMode
  payment_phone_number: string
  //Promo
  promo_code: string
  discount_amount: number
  promo_name: string
  applied_promo_code: string
}

export enum CheckoutActionType {
  PaymentInitializer = "payment_initializer",
  FormInitializer = "form_initializer",
  PaymentOptionInitializer = "payment_option_initializer",
  UniversityInitializer = "university_initializer",
  PromoInitializer = "promo_initializer",
  BatchInitializer = "batch_initializer",
  FormHandler = "form_handler",
  FormTypeHandler = "form_type_handler",
  FormFileHandler = "form_file_handler",
  FormErrorHandler = "form_error_handler",
  ChoosePayment = "choose_payment",
  OpenModal = "open_modal",
  ClosePromoModal = "close_promo_modal",
  InputPromoCode = "input_promo_code",
  ApplyPromo = "apply_promo",
  ErrorDiscount = "error_discount",
  DeleteDiscount = "delete_discount",
  ErrorSubmitting = "error_submitting",
  ErrorChecking = "error_checking",
}

export type CheckoutModalMode = "closed" | "ovo" | "promo" | "qr" | "credit_card"

//payload
export interface PaymentInitializerPayload {
  gross_price: number
  total_price: number
}

export interface FormInitializerPayload {
  form: CheckoutForm
  form_file: CheckoutFormFile
}

export interface PaymentOptionInitializerPayload {
  bank_method: PaymentOption[]
  e_wallet_method: PaymentOption[]
  credit_card: PaymentOption
}

export interface FormHandlerPayload {
  name: FormName
  value: string
}

export interface FormFileHandlerPayload {
  name: FormFileType
  file: File
  id?: number
}

export interface FormErrorPayload {
  error: string
  name: FormName | FormFileType | "form_status"
  type: "file" | "field"
}

export interface ErrorCheckPayment {
  errorType: "form" | "paymentOption"
  message: string
}

export type CheckoutAction =
  | {
      type: CheckoutActionType.ClosePromoModal | CheckoutActionType.DeleteDiscount
      payload?: any
    }
  | {
      type: CheckoutActionType.PaymentInitializer
      payload: PaymentInitializerPayload
    }
  | {
      type: CheckoutActionType.FormInitializer
      payload: FormInitializerPayload
    }
  | {
      type: CheckoutActionType.PaymentOptionInitializer
      payload: PaymentOptionInitializerPayload
    }
  | {
      type: CheckoutActionType.UniversityInitializer
      payload: University[]
    }
  | {
      type: CheckoutActionType.PromoInitializer
      payload: AvailablePromo[]
    }
  | {
      type: CheckoutActionType.BatchInitializer
      payload: SchoolBatch[]
    }
  | { type: CheckoutActionType.FormHandler; payload: FormHandlerPayload }
  | { type: CheckoutActionType.FormTypeHandler; payload: FormStatus }
  | {
      type: CheckoutActionType.FormFileHandler
      payload: FormFileHandlerPayload
    }
  | { type: CheckoutActionType.FormErrorHandler; payload: FormErrorPayload }
  | { type: CheckoutActionType.ChoosePayment; payload: PaymentOption }
  | { type: CheckoutActionType.OpenModal; payload: CheckoutModalMode }
  | {
      type:
        | CheckoutActionType.InputPromoCode
        | CheckoutActionType.ErrorDiscount
        | CheckoutActionType.ErrorSubmitting
      payload: string
    }
  | { type: CheckoutActionType.ApplyPromo; payload: Discount }
  | { type: CheckoutActionType.ErrorChecking; payload: ErrorCheckPayment }
