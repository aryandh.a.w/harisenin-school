export interface CreditCardData {
  id: string
  authentication_id: string
  masked_card_number: string
  status: string
  payer_authentication_url: string
  card_info: CreditCardInfo
}

export interface CreditCardInfo {
  bank: string
  brand: string
  country: string
  fingerprint: string
}

export interface CardAuthData {
  id: string
  payer_authentication_url: string
  status: string
}
