export interface DynamicHelmet {
  title: string
  description: string
}

export interface PaginationData<T> {
  totalData: number
  totalPage: number
  currentPage: number
  nextPage?: number
  prevPage?: number
  data: T[]
}
