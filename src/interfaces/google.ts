import { ReactElement } from "react"

declare global {
  interface Window {
    google: any
    [key: string]: any
  }
}

export interface GoogleInitializeResponse {
  clientId: string
  credential: string
  select_by: string
}

export interface IGoogleEndPointResponse {
  iss: string
  sub: string
  azp: string
  aud: string
  iat: string
  exp: string
  name: string
  email: string
  local: string
  picture: string
  given_name: string
  family_name: string
  email_verified: string
}
