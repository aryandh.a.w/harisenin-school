import { SchoolBatch, SchoolProgram } from "./school"

export type OrderStatus =
  | "PENDING"
  | "SETTLED"
  | "SUBMITTED_PAYMENT"
  | "IN_PROGRESS"
  | "IN_REVISED"
  | "DONE"
  | "CANCELLED"
  | "EXPIRED"

export interface InvoiceDetail {
  expiry_date: Date
  bank_code: string
  account_number: string
  transfer_amount: number
  status: OrderStatus
  invoice_number: string
  paid_at: Date
}

export interface OrderDetail {
  product_id: string
  product_name: string
  product_slug: string
  product_description: string
  school_program: SchoolProgram
  available_batch: SchoolBatch[]
}

export interface PaymentDetail {
  status: OrderStatus
  external_id: string
  paid_at: null | string
  order_number: string
  data: {
    external_id: "484c018ee2e79fa17e1ef701b2521f25"
    status: "PENDING"
    amount: 1005000
    description: string
    expiry_date: Date
    invoice_url: string
    created: Date
    updated: Date
  }
}
