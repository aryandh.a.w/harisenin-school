export type SchoolType = "BOOTCAMP" | "PRO_CLASS"

export interface School {
  id: string
  product_asset: ProductAsset
  product_assets: ProductAsset[]
  product_description: string
  product_name: string
  product_slug: string
  program: SchoolProgram | SchoolProgram[]
  status: number
  school_duration: number
  school_tutor: SchoolTutor
  total_course: number
  total_order: number
  total_tutor: number
  school_batch: string
  school_type: SchoolType
  school_properties: SchoolProperty
  school_discount_percentage: number
  school_original_price: number
  school_start_price: number
}

export interface SchoolPagination {
  currentPage: number
  nextPage?: number
  data: School[]
  totalData: number
  totalPage: number
}

export interface SchoolListQuery {
  page: number
  limit: number
  except?: string
  search?: string
}

export interface SchoolListQueryV2 {
  page: number
  limit: number
  search?: string
}

export interface ProductAsset {
  product_asset_id: string
  product_asset_type: string
  product_image: string
  product_image_alt: string
}

export interface SchoolProgram {
  id: number
  program_description: string
  program_discount_percentage: number
  program_best_seller: number
  program_original_price: number
  program_price: number
  program_qty: number
  program_title: string
  program_theme_color: ProgramTheme
  can_order?: boolean
<<<<<<< HEAD
  program_type_logo?: string
  program_sub_type: ProgramSubType
=======
>>>>>>> 6cd4eae (setup fix)
}

export interface ProgramTheme {
  backgroundColor: string
  bestSellerColor: string
  buttonColor: string
  fontColor: string
  fontColorDiscount: string
  iconColor: string
}

export interface SchoolDetail {
  product_asset: ProductAsset
  product_assets: ProductAsset[]
  product_description: string
  product_id: string
  product_name: string
  product_slug: string
  school_meta_title: string
  school_meta_description: string
  syllabus_status: boolean
  school_banner: SchoolBanner
  school_batch: SchoolBatch[]
  school_courses: SchoolCourse[]
  school_program: SchoolProgram[]
  school_tutors: SchoolTutor[]
  school_rewards: SchoolReward[]
  school_testimonies: StudentTestimonial[]
  school_portfolio: SchoolPortfolio[]
  average_rating: number
  user_picture: string[]
  total_alumni: number
  school_faqs: FAQ[]
<<<<<<< HEAD
  school_tools: SchoolTools[]
  school_type: SchoolType
  school_program_type: SchoolProgramType[]
  school_video_previews: SchoolVideoPreview[]
}

export interface ReviewRating {
  rating_count: number
  rating_value: number
  review_count: number
}

export interface SchoolVideoPreview {
  id: string
  video_title: string
  video_description: string
  video_position: number
  video_thumbnail: string
  video_link: string
}

export interface SchoolLearningExperience {
  id: number
  learning_type: SchoolLearningType
  learning_name: string
  learning_title: string
  learning_description: string
}

export interface SchoolTools {
  tool_id: number
  tool_path: string
  tool_title: string
}

export interface SchoolProgramType {
  program_description: string
  program_type: ProgramType
  program_type_alias: string
  program_type_logo?: string
  school_programs: SchoolProgram[]
=======
>>>>>>> 6cd4eae (setup fix)
}

export interface SchoolBanner {
  banner_image: string
  banner_title: string
  id: number
  school_id: string
  banner_description: { banner_description: string }[]
  banner_sub_title: string
}

export interface SchoolCourse {
  courses_description: string
  courses_tag: string
  courses_title: string
  course_date: string
  id: number
  school_id: string
  tutor: SchoolTutor
}

export interface SchoolTutor {
  id: string
  tutor_as: string
  tutor_description: string
  tutor_linkedin: string
  tutor_name: string
  tutor_picture: string
  tutor_facebook_account?: string
  tutor_instagram_account?: string
  tutor_youtube_account?: string
}

export interface StudentTestimonial {
  id: string
  testimony: string
  testimony_username: string
  testimony_user_picture: string
  testimony_user_as: string
  company?: Company
}

export interface SchoolReward {
  id: number
  school_id: string
  reward_title: string
  reward_content: string
  reward_icon: string
}

export type BatchStatus = "OPEN_REGISTRATION" | "SOLD_OUT"

export interface SchoolBatch {
  id: string
  batch_description: string
  batch: number
  batch_end: string
  batch_start: string
  batch_status: BatchStatus
  close_registration: string
  open_registration: string
  start_learning: string
}

export interface FileRequestForm {
  user_firstname: string
  user_lastname: string
  user_email: string
  user_phone_number: any
  reason_of_download: string
}

export interface Company {
  company_name: string
  company_picture: string
}

export interface FAQ {
  id?: string
  faq_question: string
  faq_answer: string
}

export interface SchoolProperty {
  school_rating: number
  total_order: number
  user_picture: string[]
}

export interface SchoolPortfolio {
  portfolio_id: number
  portfolio_title: string
  portfolio_description: string
  portfolio_path: string
  portfolio_thumbnail: string
}
