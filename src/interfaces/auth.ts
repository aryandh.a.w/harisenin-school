export interface TokenData {
  id: string
  user_email: string
  user_twilio: string
  user_active: number
  user_suspended: number
  user_id: string
  user_firstname: string
  user_lastname: string
  user_phone_number: string
  user_picture: string
  user_interested_courses: string
  user_birthday?: Date
  user_gender?: number
  fullname: string
  completed_profile_percentage: number
  status_completed_profile: boolean
}

export interface RawTokenData {
  exp: number
  iat: number
  uid: string
  data: TokenData
}

export interface RegisterBody {
  user_email: string
  user_password: string
  user_fullname: string
  user_gender: number
  user_birthday: string
  user_phone_number: string
  user_interested_courses: string[]
}

export interface ProfileBody {
  fullname: string
  user_gender: number
  user_phone_number: string
  user_birthday: string
  user_address_1: string
  user_province: string
  user_zip_code: string
  user_city: string
}

export interface SubmitPasswordBody extends Partial<ProfileBody> {
  email: string
  password: string
}

export interface LoginManualBody {
  email: string
  password: string
}

export interface GoogleLoginBody {
  email: string
  fullName: string
  googleId: string
}

export interface LoginResponse {
  token: string
  refreshToken: string
}

export interface GoogleLoginResponse {
  new_user: number
  completed_profile_percentage: number
  status_completed_profile: boolean
  fullname: string
  user_birthday?: Date
  user_phone_number: string
  user_gender: number
}
