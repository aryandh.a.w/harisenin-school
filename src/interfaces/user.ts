<<<<<<< HEAD:src/interfaces/user.ts
export interface TokenData {
  id?: string
  user_email: string
  user_firstname: string
  user_lastname: string
  user_phone_number: string
  user_picture: string
  fullname: string
  user_active: number
  // partner: {
  //   partner_name: string
  //   partner_slug: string
  //   partner_picture: string
  //   partner_level: {
  //     level_name: string
  //   }
  // }
  // is_partner: boolean | number
}

export interface AuthData {
  id: string
  user_email: string
  user_profile: {
    user_id: string
    user_firstname: string
    user_lastname: string
    user_phone_number: string
    user_picture: string
    user_birthday: string | Date
    user_gender: number
    fullname: string
  }
  completed_profile_percentage: number
  status_completed_profile: boolean | number
}
=======
export interface TokenData {
  id?: string
  user_email: string
  user_firstname: string
  user_lastname: string
  user_phone_number: string
  user_picture: string
  fullname: string
  user_active: number
  partner: {
    partner_name: string
    partner_slug: string
    partner_picture: string
    partner_level: {
      level_name: string
    }
  }
  is_partner: boolean | number
}

export interface AuthData {
  id: string
  user_email: string
  user_profile: {
    user_id: string
    user_firstname: string
    user_lastname: string
    user_phone_number: string
    user_picture: string
    user_birthday: string | Date
    user_gender: number
    fullname: string
  }
  completed_profile_percentage: number
  status_completed_profile: boolean | number
}
>>>>>>> 94ccec2 (setup fix):src/constants/interfaces/user.ts
