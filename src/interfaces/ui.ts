export interface HeaderData {
  first_name: string
  full_name: string
  profile_picture: string
}

export interface Time {
  days: number
  hours: number
  minutes: number
  seconds: number
}

export interface SelectOption {
  value: any
  label: string
}

export type ButtonType = "solid" | "transparent"

export type ButtonColor = "blue" | "green" | "white" | "black" | "red" | "grey" | "custom"

export type DynamicObject = {
  [key: string]: any
}

export interface BannerInfo {
  total_alumni: number
  user_picture: string[]
  average_rating: number
}
