export interface Asset {
  asset_name: string
  asset_path: string
  asset_type: string
  id: string
}

export interface RisebinarProgram {
  id: number
  program_description: string
  program_end: Date
  program_price: number
  program_qty: number
  program_start: Date
  program_title: string
  total_order: number
  can_order: boolean | number
  program_certificate_price: number
  program_certificate_required: number
}

export interface Risebinar {
  id: string
  risebinar_asset: Asset
  risebinar_date: Date
  risebinar_slug: string
  risebinar_program: RisebinarProgram
  risebinar_programs: RisebinarProgram[]
  risebinar_description: string
  risebinar_status: number
  risebinar_time: string
  risebinar_title: string
}

export type RisebinarTime =
  | "today"
  | "this_week"
  | "next_week"
  | "this_month"
  | "next_month"
  | "upcoming"

export interface RisebinarQuery {
  time: RisebinarTime
  category: string
  search: string
}
