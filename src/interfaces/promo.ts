export interface Promo {
  id: string
  discount_name: string
  discount_image: string
  discount_type: string
  discount_created: Date
  discount_start_at: Date
  discount_end_at: Date
  discount_date: string
  discount_available: string[]
  discount_except: string[]
  discount_available_for: string[]
  discount_except_for: string[]
}

export interface PromoDetail extends Promo {
  discount_description: string
  discount_terms_and_condition: string
}

export interface AvailablePromo {
  id: string
  discount_name: string
  discount_type: "SCHOOL"
  discount_start_at: Date
  discount_end_at: Date
  discount_available: string[]
  discount_except: string[]
  discount_code: string
  discount_percentage: number
  discount_price: number
}
