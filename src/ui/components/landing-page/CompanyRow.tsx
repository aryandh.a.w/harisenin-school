import { Container } from "react-bootstrap"

import { COMPANY } from "../../../constants/dummy-data"
import { DynamicImage } from "../../modules/misc"

import style from "./styles/landing-page.module.scss"

function CompanyRow({ screenWidth }) {
  if (!screenWidth) {
    return <></>
  }

  return (
    <div className={style["landing_page__company"]}>
      <Container className={style["landing_page__company__container"]}>
        <h2>
          Mentor kami berpengalaman di <br />
          startup teratas & perusahaan multinasional{" "}
        </h2>
        {screenWidth > 576 ? (
          <>
            <div className={style["landing_page__company__column"]}>
              <div
                className={`${style["landing_page__company__row"]} ${style["landing_page__company__row--top"]}`}
              >
                {COMPANY.slice(0, 5).map((value, index) => (
                  <div className={style["landing_page__company__row__img"]} key={index}>
                    <DynamicImage src={value.url} alt={value.alt} />
                  </div>
                ))}
              </div>
              <div className={style["landing_page__company__row"]}>
                {COMPANY.slice(5, 10).map((value, index) => (
                  <div className={style["landing_page__company__row__img"]} key={index}>
                    <DynamicImage src={value.url} alt={value.alt} />
                  </div>
                ))}
              </div>
              <div className={style["landing_page__company__row"]}>
                {COMPANY.slice(10, 15).map((value, index) => (
                  <div className={style["landing_page__company__row__img"]} key={index}>
                    <DynamicImage src={value.url} alt={value.alt} />
                  </div>
                ))}
              </div>
            </div>
          </>
        ) : (
          <>
            <div className={style["landing_page__company__column"]}>
              <div
                className={`${style["landing_page__company__row"]} ${style["landing_page__company__row--three"]}`}
              >
                {COMPANY.slice(0, 4).map((value, index) => (
                  <div className={style["landing_page__company__row__img"]} key={index}>
                    <img src={value.url} alt={value.alt} />
                  </div>
                ))}
              </div>
              <div
                className={`${style["landing_page__company__row"]} ${style["landing_page__company__row--three"]}`}
              >
                {COMPANY.slice(4, 7).map((value, index) => (
                  <div className={style["landing_page__company__row__img"]} key={index}>
                    <img src={value.url} alt={value.alt} />
                  </div>
                ))}
                <div className={style["landing_page__company__row__img"]}>
                  <img src={COMPANY[8].url} alt={COMPANY[8].alt} />
                </div>
              </div>
              <div
                className={`${style["landing_page__company__row"]} ${style["landing_page__company__row--three"]}`}
              >
                <div className={style["landing_page__company__row__img"]}>
                  <img src={COMPANY[7].url} alt={COMPANY[7].alt} />
                </div>
                {COMPANY.slice(9, 12).map((value, index) => (
                  <div className={style["landing_page__company__row__img"]} key={index}>
                    <img src={value.url} alt={value.alt} />
                  </div>
                ))}
              </div>
              <div
                className={`${style["landing_page__company__row"]} ${style["landing_page__company__row--four"]}`}
              >
                {COMPANY.slice(12, 15).map((value, index) => (
                  <div className={style["landing_page__company__row__img"]} key={index}>
                    <img src={value.url} alt={value.alt} />
                  </div>
                ))}
              </div>
            </div>
          </>
        )}
      </Container>
    </div>
  )
}

export default CompanyRow
