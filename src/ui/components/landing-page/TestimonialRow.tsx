import { StudentTestimonial } from "../../../constants/interfaces/school"
import TestimonialCard from "../../modules/card/TestimonialCard"
import React, { useEffect, useState } from "react"
import SchoolServices from "../../../lib/services/school.services"

import style from "./styles/landing-page.module.scss"
import clsx from "clsx"
import Carousel from "react-multi-carousel"
import SliderArrow from "../../modules/sliders/SliderArrow"

const TestimonialRow = ({ screenWidth }: { screenWidth: number }) => {
  const [testimonials, setTestimonials] = useState<StudentTestimonial[]>([])

  const responsive = {
    superLargeDesktop: {
      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: 6,
      slidesToSlide: 6,
      partialVisibilityGutter: 30,
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 3,
      slidesToSlide: 1,
      partialVisibilityGutter: 30,
    },
    tablet: {
      breakpoint: { max: 768, min: 464 },
      items: 1,
      slidesToSlide: 3,
    },
  }

  const school = new SchoolServices()

  useEffect(() => {
    const getData = async () => {
      try {
        const res = await school.getHomepageTestimonials()

        if (res.isSuccess) {
          const result = res.getValue()
          setTestimonials(result)
        }
      } catch (e) {
        school.bugsnagNotifier(e)
      }
    }

    getData()
  }, [])

  if (!testimonials.length) {
    return <></>
  }

  return (
    <div className={style.landing_page__testimonial}>
      <div className={clsx(screenWidth < 576 && "container")}>
        <h2 className="text-center">Apa yang alumni kami katakan?</h2>
      </div>

      {screenWidth > 576 ? (
        <div className={"position-relative container"}>
          <Carousel
            responsive={responsive}
            ssr={true} // means to render carousel on server-side.
            arrows={false}
            containerClass={style.landing_page__testimonial__row}
            customButtonGroup={<SliderArrow length={testimonials.length} displayedSlide={3} />}
            renderButtonGroupOutside
          >
            {testimonials.map((value, index) => (
              <TestimonialCard
                data={value}
                key={index}
                className={style.landing_page__testimonial__card}
              />
            ))}
          </Carousel>
        </div>
      ) : (
        <div className={style.landing_page__testimonial__container}>
          <div className={style.landing_page__testimonial__row}>
            {testimonials.map((value, index) => (
              <TestimonialCard
                data={value}
                key={index}
                className={style.landing_page__testimonial__card}
              />
            ))}
          </div>
        </div>
      )}
    </div>
  )
}

export default TestimonialRow
