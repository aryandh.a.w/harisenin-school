import { Accordion, Container } from "react-bootstrap"

import AccordionArrowHeader from "../../modules/accordion/AccordionArrowHeader"
import { useEffect, useState } from "react"
import { FAQ } from "../../../constants/interfaces/school"
import SchoolServices from "../../../lib/services/school.services"

import style from "./styles/landing-page.module.scss"

const FAQs: FAQ[] = [
  {
    faq_question: "Seberapa besar komitmen waktu yang harus saya berikan?",
    faq_answer:
      "Program ini berjalan selama 4 minggu dengan total 12 pertemuan. Setiap minggu terdapat 3 pertemuan, 2 pertemuan pada hari kerja malam hari dan 1 pertemuan pada Sabtu siang. ",
  },
  {
    faq_question: "Bagaimana bentuk konkrit pertemuan dalam program ini?",
    faq_answer: "Pertemuan berbentuk kelas online, menggunakan media konferensi video.",
  },
  {
    faq_question: "Apa syarat untuk dapat bergabung di program ini?",
    faq_answer: "Kamu cukup mengisi form pendaftaran dan menyediakan scan ijazah",
  },
  {
    faq_question: "Siapa saja yang dapat mengikuti program ini?",
    faq_answer: "Siapapun dengan berbagai latar belakang pendidikan dapat mengikuti program ini",
  },
  {
    faq_question: "Apa syarat kelulusan dari program ini?",
    faq_answer: "Kamu harus menghadiri minimal 8 pertemuan dan mengerjakan final project",
  },
  {
    faq_question: "Bagaimana cara mendapatkan slot di program ini?",
    faq_answer:
      "Bayar secara penuh saat sudah mendaftar atau dengan menggunakan program cicilan sebanyak 3x",
  },
]

const FaqContainer = () => {
  const [faqs, setFaqs] = useState<FAQ[]>(FAQs)
  const [middleSlice, setMiddleSlice] = useState(3)
  const [faqLength, setFaqLength] = useState(6)

  const school = new SchoolServices()

  useEffect(() => {
    const getData = async () => {
      try {
        const res = await school.getHomepageFaqs()
        if (res.isSuccess) {
          const result = res.getValue()

          if (result.length) {
            setFaqs(result)

            const length = result.length
            setFaqLength(length)
            const modulus = length % 2

            if (modulus) {
              setMiddleSlice(length / 2 + 1)
            } else {
              setMiddleSlice(length / 2)
            }
          }
        }
      } catch (e) {
        if (process.env.NODE_ENV === "development") {
          console.log({ e })
        }
      }
    }

    getData()
  }, [])

  return (
    <div className={style["landing_page__faq"]}>
      <h2>Pertanyaan yang sering ditanyakan</h2>
      <Container className={style["landing_page__faq__container"]}>
        {/* Left Accordion */}
        <Accordion className={style["landing_page__faq__accordion"]}>
          {faqs.slice(0, middleSlice).map((value, index) => (
            <div className={style["landing_page__faq__wrapper"]} key={index}>
              <AccordionArrowHeader
                className={style["landing_page__faq__header"]}
                eventKey={`${index + 1}`}
                chevronDirection="down"
                chevronClass="faq-home_chevron"
                textClassName={"text-span"}
              >
                {value.faq_question}
              </AccordionArrowHeader>
              <Accordion.Collapse eventKey={`${index + 1}`}>
                <div className={style["landing_page__faq__body"]}>{value.faq_answer}</div>
              </Accordion.Collapse>
            </div>
          ))}
        </Accordion>

        {/* Right Accordion */}
        <Accordion className={style["landing_page__faq__accordion"]}>
          {faqs.slice(middleSlice, faqLength).map((value, index) => (
            <div className={style["landing_page__faq__wrapper"]} key={index}>
              <AccordionArrowHeader
                className={style["landing_page__faq__header"]}
                eventKey={`${index + 3}`}
                chevronDirection="down"
                chevronClass="faq-home_chevron"
                textClassName={"text-span"}
              >
                {value.faq_question}
              </AccordionArrowHeader>
              <Accordion.Collapse eventKey={`${index + 3}`}>
                <div className={style["landing_page__faq__body"]}>{value.faq_answer}</div>
              </Accordion.Collapse>
            </div>
          ))}
        </Accordion>
      </Container>
    </div>
  )
}

export default FaqContainer
