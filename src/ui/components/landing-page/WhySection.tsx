import React from "react"
import { WHY_LIST, WhyList, WhyValue } from "../../../constants/why-list"
import { FaCheckCircle, FaTimesCircle } from "react-icons/fa"

import style from "./styles/landing-page.module.scss"

const WhyTableRow = ({ data }: { data: WhyList }) => {
  const { hms, other, label, university } = data

  const CellComponent = ({ value }: { value: WhyValue }) => {
    let className
    let component

    switch (value.type) {
      case "yes":
        className = `${style["landing_page__why__table__cell"]} ${style["landing_page__why__table__cell--yes"]}`
        component = <FaCheckCircle />
        break
      case "no":
        className = `${style["landing_page__why__table__cell"]} ${style["landing_page__why__table__cell--no"]}`
        component = <FaTimesCircle />
        break
      case "warning":
        className = `${style["landing_page__why__table__cell"]} ${style["landing_page__why__table__cell--waning"]}`
        component = value.text
        break
      default:
        className = "landing_page__why__table__cell"
        component = value.text
        break
    }

    return <div className={className}>{component}</div>
  }

  return (
    <div className="d-lg-flex d-block">
      <div
        className={`${style["landing_page__why__table__cell"]} ${style["landing_page__why__table__cell--left"]}`}
      >
        {label}
      </div>

      <div className={style["landing_page__why__table__content"]}>
        <div className="w-100 d-lg-none d-flex">
          <div
            className={`${style["landing_page__why__table__cell"]} ${style["landing_page__why__table__cell--hms"]}`}
          >
            HMS
          </div>
          <div
            className={`${style["landing_page__why__table__cell"]} ${style["landing_page__why__table__cell--grey"]}`}
          >
            Other Bootcamp
          </div>
          <div
            className={`${style["landing_page__why__table__cell"]} ${style["landing_page__why__table__cell--grey"]}`}
          >
            University
          </div>
        </div>

        <div className="w-100 d-flex">
          {hms.type === "yes" ? (
            <div
              className={`${style["landing_page__why__table__cell"]} ${style["landing_page__why__table__cell--yes"]}`}
            >
              <FaCheckCircle />
            </div>
          ) : (
            <div
              className={`${style["landing_page__why__table__cell"]} ${style["landing_page__why__table__cell--hms"]}`}
            >
              {hms.text}
            </div>
          )}

          <CellComponent value={other} />
          <CellComponent value={university} />
        </div>
      </div>
    </div>
  )
}

const WhySection = () => {
  return (
    <div className="landing_page__why">
      <h2 className="text-center">Mengapa Bootcamp?</h2>

      <table className="landing_page__why__table">
        <thead className="landing_page__why__table__header d-lg-flex d-none">
          <div className="landing_page__why__table__cell--left landing_page__why__table__cell--grey" />
          <div className="landing_page__why__table__header__cell landing_page__why__table__cell--hms">
            HMS
          </div>
          <div className="landing_page__why__table__header__cell landing_page__why__table__cell--grey">
            Other Bootcamp
          </div>
          <div className="landing_page__why__table__header__cell landing_page__why__table__cell--grey">
            University
          </div>
        </thead>

        {WHY_LIST.map((value, index) => (
          <WhyTableRow data={value} key={index} />
        ))}
      </table>
    </div>
  )
}

export default WhySection
