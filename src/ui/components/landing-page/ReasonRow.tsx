import styles from "./styles/landing-page.module.scss"
import clsx from "clsx"
import { HARISENIN_PUBLIC_ICON } from "../../../constants/pictures"

const list = [
  {
    icon: "icon_webapp-4.png",
    text: "Gaji rata - rata alumni: \nRp 9.5 juta",
  },
  {
    icon: "icon_webapp-5.png",
    text: "365 hari belum bekerja, \n100% investasi kembali",
  },
  {
    icon: "icon_webapp-1.png",
    text: "Kurikulum standar industri & fokus kepada prakteknya",
  },
  {
    icon: "icon_webapp-6.png",
    text: "Lebih terjangkau dibanding bootcamp lain",
  },
]

const ReasonRow = () => {
  return (
    <div className={clsx(styles.landing_page__reason, "container")}>
      <div className={styles.landing_page__reason__row}>
        {list.map((value, index) => (
          <div className={styles.landing_page__reason__card} key={index}>
            <div className={styles.landing_page__reason__card__icon}>
              <img src={`${HARISENIN_PUBLIC_ICON}/${value.icon}`} alt={value.icon} />
            </div>
            <div className={styles.landing_page__reason__card__title}>{value.text}</div>
          </div>
        ))}
      </div>
    </div>
  )
}

export default ReasonRow
