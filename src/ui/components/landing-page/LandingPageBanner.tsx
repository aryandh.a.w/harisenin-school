import { HARISENIN_PUBLIC_PAGE_ASSETS } from "../../../constants/pictures"

import clsx from "clsx"
import { NextImage } from "../../modules/misc"
import style from "./styles/landing-page.module.scss"

const LandingPageBanner = () => {
  return (
    <div className={style.landing_page__banner}>
      {/* Background Image */}
      <NextImage
        alt="harisenin-millennial-school"
        src={`${HARISENIN_PUBLIC_PAGE_ASSETS}/school_banner.png`}
        size={[1440, 807]}
      />

      {/* 1440 × 807 px */}

      {/* Content */}
      <div className={style.landing_page__banner__content}>
        <div className={clsx(style.landing_page__banner__container, "container")}>
          <h1 id="header2">
            Sekolah online yang membantumu menjadi
            <span className="text-green"> Top 7%</span> Talenta Indonesia
          </h1>
          <div className={style.landing_page__banner__container__subtitle}>
            Pelajari keahlian dan rahasia sukses untuk <br />
            mewujudkan karir dan masa depan impianmu
          </div>
        </div>
      </div>
    </div>
  )
}

export default LandingPageBanner
