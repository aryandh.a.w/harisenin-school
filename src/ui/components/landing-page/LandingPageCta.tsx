import { HARISENIN_PUBLIC_PAGE_ASSETS } from "../../../constants/pictures"
import { LinkButton } from "../../modules/buttons"
import { NextImage } from "../../modules/misc"
import style from "./styles/landing-page.module.scss"

function LandingPageCta() {
  return (
    <div className="container">
      <div className={style.landing_page__cta}>
        <div className={style.landing_page__cta__content}>
          <h3 className={style.landing_page__cta__content__title}>
            Masih bingung dengan pilihan karirmu? Konsultasi dengan expert kami
          </h3>
          <LinkButton
            type="solid"
            color="green"
            href="https://api.whatsapp.com/send?phone=6281312117711&text=Saya%20Mau%20ikut%20Harisenin%20Millennial%20School%20di%20harisenin.com%20min"
            isExternal
          >
            Hubungi Kami
          </LinkButton>
        </div>
        <NextImage
          src={`${HARISENIN_PUBLIC_PAGE_ASSETS}/webapp_cta.png`}
          alt="landing page cta"
          size={[1080, 1080]}
        />
      </div>
    </div>
  )
}

export default LandingPageCta
