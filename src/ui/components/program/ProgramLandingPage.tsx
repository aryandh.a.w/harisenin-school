import { FC, useEffect, useState } from "react"
import clsx from "clsx"

import useScreenSize from "../../../lib/utils/hooks/useScreenSize"
import { textShortener } from "../../../lib/utils/method"

import { SchoolDetail } from "../../../constants/interfaces/school"

import Helmet from "../../../lib/utils/Helmet"
import HeaderHome from "../../modules/header/HeaderHome"
import ProgramBanner from "./components/ProgramBanner"
import ProgramExperience from "./components/ProgramExperience"
import ProgramBenefit from "./components/ProgramBenefit"
import ProgramColumn from "./components/ProgramColumn"
import ProgramFaqContainer from "./components/ProgramFaqContainer"
import ProgramCurriculum from "./components/ProgramCurriculum"
import ProgramStudent from "./components/ProgramStudent"
import ProgramTutor from "./components/ProgramTutor"
import ProgramBatch from "./components/ProgramBatch"
import ProgramNavigator from "./components/ProgramNavigator"
import ProgramPortfolio from "./components/ProgramPortfolio"
import RelatedProgram from "./components/RelatedProgram"
import ProgramCta from "./components/ProgramCta"

import style from "./styles/program.module.scss"
import ProgramRegisterFlow from "./components/ProgramRegisterFlow"
import { HariseninFooter } from "../../modules/harisenin/HariseninFooter"

export interface ProgramLandingPageProps {
  schoolDetail?: SchoolDetail
}

const ProgramLandingPage: FC<ProgramLandingPageProps> = ({ schoolDetail }) => {
  const [isTransparent, setIsTransparent] = useState(true)

  const { screenWidth, screenHeight } = useScreenSize()

  useEffect(() => {
    window.addEventListener("scroll", trackScrolling)

    return () => {
      window.removeEventListener("scroll", trackScrolling)
    }
  }, [])

  function isBottom(el: HTMLElement) {
    const cursorPos = document.documentElement.scrollTop

    return cursorPos + 60 >= el.offsetTop
  }

  const trackScrolling = () => {
    const wrappedElement = document.getElementById("header1")
    isBottom(wrappedElement)

    if (isBottom(wrappedElement)) {
      setIsTransparent(false)
    } else {
      setIsTransparent(true)
    }
  }

  const executeScroll = (id: string) => () => {
    const ele = document.getElementById(id.substring(1))

    if (!ele) {
      return
    }

    ele.scrollIntoView({ behavior: "smooth" })
  }

  return (
    <>
      <Helmet
        isIndexed
        title={schoolDetail.school_meta_title || `${schoolDetail?.product_name} - Harisenin.com`}
        description={
          schoolDetail.school_meta_description ||
          textShortener(schoolDetail?.product_description, 280)
        }
      />
      <HeaderHome transparent={isTransparent} isNotSticky />

      <div className={style["program_page"]}>
        {/* Floating button for scrolling*/}
        {/* <button className="program_page__scroll" onClick={scrollToTop}>
          <FaChevronUp />
        </button> */}

        <ProgramBanner
          schoolDetail={schoolDetail}
          scroll={executeScroll("#fee")}
          screenWidth={screenWidth}
        />

        <div className={clsx(style.program_page__body, "container")}>
          {screenWidth > 576 ? (
            <ProgramNavigator
              screenWidth={screenWidth}
              schoolDetail={schoolDetail}
              screenHeight={screenHeight}
            />
          ) : null}

          <div className={style.program_page__content}>
            {/* Will be hidden when there's no school rewards */}
            {schoolDetail?.school_rewards?.length > 0 && (
              <ProgramBenefit detail={schoolDetail} screenWidth={screenWidth} />
            )}

            <ProgramExperience screenWidth={screenWidth} />

            <ProgramCurriculum
              learnMaterial={schoolDetail.school_courses}
              schoolName={schoolDetail.product_name}
              schoolId={schoolDetail.product_id}
            />

            <ProgramTutor screenWidth={screenWidth} tutors={schoolDetail.school_tutors} />

            {/*/!* Jadwal Bootcamp *!/*/}
            <ProgramBatch data={schoolDetail.school_batch} />

            <ProgramRegisterFlow />

            <ProgramColumn
              programs={schoolDetail.school_program}
              schoolDetail={schoolDetail}
              screenWidth={screenWidth}
            />

            {schoolDetail?.school_portfolio && schoolDetail.school_portfolio?.length > 0 && (
              <ProgramPortfolio
                screenWidth={screenWidth}
                portfolio={schoolDetail.school_portfolio}
              />
            )}

            {/*/!* Will be hidden when there's no school testimonies *!/*/}
            <ProgramStudent data={schoolDetail.school_testimonies} screenWidth={screenWidth} />

            {schoolDetail?.school_faqs && schoolDetail?.school_faqs?.length > 0 && (
              <ProgramFaqContainer faqs={schoolDetail.school_faqs} />
            )}
          </div>
        </div>

        <RelatedProgram screenWidth={screenWidth} />

        <ProgramCta />
      </div>

      <HariseninFooter />
    </>
  )
}

export default ProgramLandingPage
