import React, { FC, useState } from "react"

import { createMarkup, priceFormatter } from "../../../../lib/utils/method"
import { SchoolDetail, SchoolProgram } from "../../../../constants/interfaces/school"

import { RegularButton } from "../../../modules/buttons"
import { LoginModal } from "../../../modules/modal/LoginModal"
import { CheckoutSummary } from "../../../../constants/interfaces/checkout-state"
import { useUserData } from "../../../../lib/utils/hooks/useUserData"

import style from "../styles/program.module.scss"
import { useRouter } from "next/router"

export interface ProgramColumnProps {
  programs: SchoolProgram[]
  schoolDetail: SchoolDetail
  screenWidth: number
}

const ProgramColumn: FC<ProgramColumnProps> = ({ programs, schoolDetail, screenWidth }) => {
  const [loginModal, setLoginModal] = useState(false)

  const { isLogin, tokenData } = useUserData()
  const router = useRouter()
  const pathName = router.pathname

  // Function for program button `daftar sekarang`
  const nextPageHandler = (data: SchoolProgram) => () => {
    // setPackageId(index)
    import("react-facebook-pixel")
      .then((module) => module.default)
      .then((ReactPixel) => {
        ReactPixel.track("InitiateCheckout", {
          content_ids: [schoolDetail.product_id, data.id],
          content_category: "hms",
          contents: [
            {
              id: schoolDetail.product_id,
              quantity: 1,
            },
          ],
          currency: "IDR",
          value: data.program_price,
        })
      })

    if (isLogin) {
      try {
        const storage: CheckoutSummary = {
          school_id: schoolDetail.product_id,
          school_slug: schoolDetail.product_slug,
          school_title: schoolDetail.product_name,
          image: schoolDetail.product_asset.product_image,
          program_id: data.id,
          program_description: data.program_description,
          program_title: data.program_title,
          program_price: data.program_price,
        }

        if (tokenData.user_active) {
          const isProClass = pathName.includes("proclass")

          window.localStorage.setItem(
            isProClass ? "hsp_checkout" : "hs_checkout",
            JSON.stringify(storage)
          )
          window.location.assign(`/school/${isProClass ? "/proclass/checkout" : "checkout"}`)
        } else {
          alert(
            "Akun kamu belum diverifikasi. silahkan coba login ulang jika kamu telah melakukan verikasi"
          )
        }
      } catch (e) {
        alert("Terjadi kesalahan. Silahkan coba login ulang dan coba lagi")
      }
    } else {
      setLoginModal(true)
    }
  }

  if (!programs || !programs.length) {
    return <></>
  }

  function ProgramCard({ value, index }: { value: SchoolProgram; index: number }) {
    if (screenWidth > 576) {
      return (
        <div
          className={style.program_page__investment__card}
          style={{ backgroundColor: value.program_theme_color.backgroundColor }}
        >
          {/* Best seller ribbon. Will be shown on best seller program*/}
          {value.program_best_seller ? (
            <div
              className={style["program_page__investment__card__ribbon"]}
              style={{
                backgroundColor: value.program_theme_color.bestSellerColor,
              }}
            >
              Best Seller
            </div>
          ) : null}

          {/* Card Left */}
          <div className={style.program_page__investment__card__left}>
            <div
              className={style.program_page__investment__card__left__title}
              style={{ color: value.program_theme_color.fontColor }}
              id={`investment-title-${index}`}
            >
              {value.program_title}
            </div>

            <div
              dangerouslySetInnerHTML={createMarkup(value.program_description)}
              className={style["program_page__investment__card__left__description"]}
            />
          </div>

          {/* Card Right */}
          <div className={style.program_page__investment__card__right}>
            {/* Price before discount. Will be shown if there's discount*/}
            {value.program_discount_percentage ? (
              <div className={style.program_page__investment__card__right__price__discount}>
                <div
                  className={
                    style["program_page__investment__card__right__price__discount__percentage"]
                  }
                  style={{ backgroundColor: value.program_theme_color.fontColorDiscount }}
                >
                  {Math.round(value.program_discount_percentage * 100)}%
                </div>
                <div
                  className={style["program_page__investment__card__right__price__discount__gross"]}
                >
                  {priceFormatter(value.program_original_price)}
                </div>
              </div>
            ) : null}

            {/* Net price. Red if there's discount, green otherwise*/}
            <div
              className={style["program_page__investment__card__right__price__net"]}
              style={{ color: value.program_theme_color.fontColor }}
            >
              {priceFormatter(value.program_price)}
            </div>

            {/* Button for Checkout */}
            <RegularButton
              type="solid"
              color="custom"
              style={{
                backgroundColor: value.program_theme_color.buttonColor ?? "#0dad8e",
                border: `solid 1px ${value.program_theme_color.buttonColor ?? "#0dad8e"}`,
              }}
              className={style["program_page__investment__card__button"]}
              disabled={!value.can_order || (isLogin && !tokenData.user_active)}
              onClick={nextPageHandler(value)}
              id={`investment-card-${index}`}
            >
              {!value.can_order ? "Sold Out" : "Daftar Sekarang"}
            </RegularButton>
          </div>
        </div>
      )
    } else {
      return (
        <div
          key={index}
          className={style.program_page__investment__card}
          style={{ backgroundColor: value.program_theme_color.backgroundColor }}
        >
          {/* Best seller ribbon. Will be shown on best seller program*/}
          {value.program_best_seller ? (
            <div
              className={style["program_page__investment__card__ribbon"]}
              style={{
                backgroundColor: value.program_theme_color.bestSellerColor,
              }}
            >
              Best Seller
            </div>
          ) : null}

          {/* Card Left */}
          <div className={style.program_page__investment__card__left}>
            <div
              className={style.program_page__investment__card__left__title}
              style={{ color: value.program_theme_color.fontColor }}
              id={`investment-title-${index}`}
            >
              {value.program_title}
            </div>

            {/* Price before discount. Will be shown if there's discount*/}
            {value.program_discount_percentage ? (
              <div className={style.program_page__investment__card__right__price__discount}>
                <div
                  className={
                    style["program_page__investment__card__right__price__discount__percentage"]
                  }
                  style={{ backgroundColor: value.program_theme_color.fontColorDiscount }}
                >
                  {Math.round(value.program_discount_percentage * 100)}%
                </div>
                <div
                  className={style["program_page__investment__card__right__price__discount__gross"]}
                >
                  {priceFormatter(value.program_original_price)}
                </div>
              </div>
            ) : null}

            {/* Net price. Red if there's discount, green otherwise*/}
            <div
              className={style["program_page__investment__card__right__price__net"]}
              style={{ color: value.program_theme_color.fontColor }}
            >
              {priceFormatter(value.program_price)}
            </div>
          </div>

          {/* Card Right */}
          <div className={style.program_page__investment__card__right}>
            <div
              dangerouslySetInnerHTML={createMarkup(value.program_description)}
              className={style["program_page__investment__card__left__description"]}
            />

            {/* Button for Checkout */}
            <RegularButton
              type="solid"
              color="custom"
              style={{
                backgroundColor: value.program_theme_color.buttonColor ?? "#0dad8e",
                border: `solid 1px ${value.program_theme_color.buttonColor ?? "#0dad8e"}`,
              }}
              className={style["program_page__investment__card__button"]}
              disabled={!value.can_order || (isLogin && !tokenData.user_active)}
              onClick={nextPageHandler(value)}
              id={`investment-card-${index}`}
            >
              {!value.can_order ? "Sold Out" : "Daftar Sekarang"}
            </RegularButton>
          </div>
        </div>
      )
    }
  }

  return (
    <div className={style["program_page__investment"]} id="fee1">
      <div className={style["program_page__anchor"]} id="fee" />
      <h2>Biaya Kelas</h2>
      <h3>Proses Belajar Yang Tepat Dengan Harga Terjangkau</h3>

      {/* Program Card map*/}
      {programs.map((value, index) => (
        <ProgramCard value={value} key={index} index={index} />
      ))}

      <LoginModal onHide={() => setLoginModal(false)} show={loginModal} />
    </div>
  )
}

export default ProgramColumn
