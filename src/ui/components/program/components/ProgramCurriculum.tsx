import React, { FC, useState } from "react"
import { Accordion } from "react-bootstrap"

import { createMarkup } from "../../../../lib/utils/method"

import { SchoolCourse } from "../../../../constants/interfaces/school"

import AccordionArrowHeader from "../../../modules/accordion/AccordionArrowHeader"

import style from "../styles/program.module.scss"
import { RegularButton } from "../../../modules/buttons"
import { useUserData } from "../../../../lib/utils/hooks/useUserData"
import ModalFileRequest from "./ModalFileRequest"
import { LoginModal } from "../../../modules/modal/LoginModal"

export interface ProgramLearnProps {
  learnMaterial: SchoolCourse[]
  schoolName: string
  schoolId: string
}

const ProgramCurriculum: FC<ProgramLearnProps> = ({ learnMaterial, schoolName, schoolId }) => {
  const { isLogin, tokenData } = useUserData()

  const [openLogin, setOpenLogin] = useState(false)
  const [openSyllabus, setOpenSyllabus] = useState(false)

  const openModal = () => {
    if (isLogin) {
      setOpenSyllabus(true)
    } else {
      setOpenLogin(true)
    }
  }

  if (!learnMaterial) {
    return <></>
  }

  return (
    <div className={style["program_page__curriculum"]} id="curiculum1">
      <div className={style["program_page__anchor"]} id="curiculum" />
      <h2>Kurikulum</h2>
      <h3>Apa Yang Akan Kamu Pelajari?</h3>

      {/* Accordion */}
      <div className={style["program_page__curriculum__card"]}>
        <Accordion className={style["program_page__curriculum__accordion"]}>
          {learnMaterial.map((value, index) => (
            <div className={style["program_page__curriculum__accordion__wrapper"]} key={index}>
              <AccordionArrowHeader
                className={style["program_page__curriculum__accordion__header"]}
                eventKey={`${value.id}`}
                chevronDirection="down"
              >
                <div
                  className={style["program_page__curriculum__accordion__header__info"]}
                  id={`curriculum-${index}`}
                >
                  {value.courses_title}
                </div>
                {/*<div className="program_page__curriculum__accordion__header__info">*/}
                {/*  /!* This component is hidden on web view*!/*/}
                {/*  <FiCalendar className="d-block d-lg-none" />*/}
                {/*  {value.course_date}*/}
                {/*</div>*/}
                {/*<div className="program_page__curriculum__accordion__header__info">*/}
                {/*  /!* This component is hidden on mobile view*!/*/}
                {/*  <RiUserLine className="d-block d-lg-none" />{" "}*/}
                {/*  {value.tutor.tutor_name}*/}
                {/*</div>*/}
              </AccordionArrowHeader>
              <Accordion.Collapse eventKey={`${value.id}`}>
                <div
                  className={style["program_page__curriculum__accordion__body"]}
                  dangerouslySetInnerHTML={createMarkup(value.courses_description)}
                />
              </Accordion.Collapse>
            </div>
          ))}
        </Accordion>

        <div className={style.program_page__curriculum__syllabus}>
          <div className={style.program_page__curriculum__syllabus__info}>
            Lebih lengkapnya, kamu bisa download silabus {schoolName}{" "}
          </div>
          <RegularButton type="solid" color="green" onClick={openModal} id="btn-download-syllabus">
            Download Silabus
          </RegularButton>
        </div>
      </div>

      <ModalFileRequest
        data={tokenData}
        schoolId={schoolId}
        show={openSyllabus}
        onHide={() => setOpenSyllabus(false)}
        mode="syllabus"
      />

      <LoginModal show={openLogin} onHide={() => setOpenLogin(false)} />
    </div>
  )
}

export default ProgramCurriculum
