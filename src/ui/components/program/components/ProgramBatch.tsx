import React, { FC } from "react"

import style from "../styles/program.module.scss"
import { SchoolBatch } from "../../../../constants/interfaces/school"
import clsx from "clsx"

interface ProgramBatchProps {
  data?: SchoolBatch[]
}

const ProgramBatch: FC<ProgramBatchProps> = ({ data }) => {
  if (!data || !data.length) {
    return <></>
  }

  return (
    <div className={style["program_page__batch"]} id="schedule1">
      <div className={style["program_page__anchor"]} id="schedule" />
      <h2>Jadwal Bootcamp</h2>
      <h3>Jadi Mahir Hanya Dalam 3 - 4 Bulan</h3>

      <div className={style["program_page__batch__wrapper"]}>
        {data.map((value) => (
          <>
            <div key={value.id} className={style.program_page__batch__wrapper__head}>
              <h4
                className={clsx(
                  style.program_page__batch__wrapper__title,
                  value.batch_status === "SOLD_OUT" &&
                    style["program_page__batch__wrapper__title--sold-out"]
                )}
              >
                Batch {value.batch}
              </h4>

              {value.batch_status === "SOLD_OUT" && (
                <div className={style.program_page__batch__wrapper__pill}>Sold Out</div>
              )}
            </div>
            <div className={style["program_page__batch__wrapper__time"]}>
              Kelas mulai: <b>{value.start_learning}</b>
            </div>
            <hr />
          </>
        ))}
        <div className={style["program_page__batch__wrapper__description"]}>
          Peserta kelas akan mengikuti online live class via zoom dengan jadwal yang telah
          ditentukan.
        </div>
      </div>
    </div>
  )
}

export default ProgramBatch
