import React from "react"

import { SchoolDetail } from "../../../../constants/interfaces/school"
import { DynamicImage } from "../../../modules/misc"

import style from "../styles/program.module.scss"
import ProgramShare from "./ProgramShare"

const ProgramBenefit = ({ detail, screenWidth }: { detail: SchoolDetail; screenWidth: number }) => {
  return (
    <div className={style.program_page__benefit} id="benefit1">
      <div className={style["program_page__anchor"]} id="benefit" />
      {screenWidth <= 576 ? (
        <ProgramShare screenWidth={screenWidth} slug={detail.product_slug} />
      ) : null}
      <h2>Benefit</h2>
      <h3>Apa Yang Kamu Dapat Dengan Ikut Kelas Ini </h3>
      <div className={style["program_page__benefit__container"]}>
        {/* Reward point component */}
        {detail?.school_rewards?.map((value, index) => (
          // no margin = last component in row
          <div
            key={index}
            className={`${style["program_page__benefit__wrapper"]} ${
              (index - 2) % 3 === 0 ? `${style["program_page__benefit__wrapper--no-margin"]}` : ""
            }`}
          >
            {/* Reward icon */}
            <div className={style["program_page__benefit__wrapper__image"]}>
              <DynamicImage src={value.reward_icon} alt={value.reward_title} />
            </div>

            {/* Reward text */}
            <div className={style["program_page__benefit__wrapper__content"]}>
              <div className={style["program_page__benefit__wrapper__content__title"]}>
                {value.reward_title}
              </div>
              <div className={style["program_page__benefit__wrapper__content__description"]}>
                {value.reward_content}
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  )
}

export default ProgramBenefit
