import { SchoolTools } from "constants/interfaces/school"
import Image from "next/future/image"
import React from "react"

import style from "../styles/program.module.scss"

export const ProgramTools = ({ tools }: { tools: SchoolTools[] }) => {
  if (!tools || !tools.length) {
    return <></>
  }

  return (
    <div className={style["program_page__curriculum"]} id="tools1">
      <div className={style["program_page__anchor"]} id="tools" />
      <h2>Tools Penunjang Belajarmu</h2>
      <h3>Tools Yang akan Kamu Gunakan Di Kelas</h3>

      <div className={style.program_page__tools}>
        {tools.map((tool, id) => (
          <div key={id} className={style.program_page__tools__column}>
            <Image src={tool.tool_path} alt={tool.tool_title} width={175} height={50} />
          </div>
        ))}
      </div>
    </div>
  )
}
