import { Accordion } from "react-bootstrap"

import AccordionArrowHeader from "../../../modules/accordion/AccordionArrowHeader"
import { FAQ } from "../../../../constants/interfaces/school"
import React, { useEffect, useState } from "react"

import style from "../styles/program.module.scss"

const FAQs: FAQ[] = [
  {
    faq_question: "Seberapa besar komitmen waktu yang harus saya berikan?",
    faq_answer:
      "Program ini berjalan selama 4 minggu dengan total 12 pertemuan. Setiap minggu terdapat 3 pertemuan, 2 pertemuan pada hari kerja malam hari dan 1 pertemuan pada Sabtu siang. ",
  },
  {
    faq_question: "Bagaimana bentuk konkrit pertemuan dalam program ini?",
    faq_answer: "Pertemuan berbentuk kelas online, menggunakan media konferensi video.",
  },
  {
    faq_question: "Apa syarat untuk dapat bergabung di program ini?",
    faq_answer: "Kamu cukup mengisi form pendaftaran dan menyediakan scan ijazah",
  },
  {
    faq_question: "Siapa saja yang dapat mengikuti program ini?",
    faq_answer: "Siapapun dengan berbagai latar belakang pendidikan dapat mengikuti program ini",
  },
  {
    faq_question: "Apa syarat kelulusan dari program ini?",
    faq_answer: "Kamu harus menghadiri minimal 8 pertemuan dan mengerjakan final project",
  },
  {
    faq_question: "Bagaimana cara mendapatkan slot di program ini?",
    faq_answer:
      "Bayar secara penuh saat sudah mendaftar atau dengan menggunakan program cicilan sebanyak 3x",
  },
]

const ProgramFaqContainer = ({ faqs }: { faqs: FAQ[] }) => {
  const [faqData, setFaqData] = useState([])

  useEffect(() => {
    if (faqs && faqs.length) {
      setFaqData(faqs)
    } else {
      setFaqData(FAQs)
    }
  }, [faqs])

  return (
    <div className={style["program_page__faq"]} id="faq1">
      <div className={style["program_page__anchor"]} id="faq" />
      <h2>FAQ</h2>
      <h3>Pertanyaan Seputar Harisenin.com</h3>

      <Accordion className={style["program_page__faq__accordion"]}>
        {faqData.map((value, index) => (
          <div className={style["program_page__faq__wrapper"]} key={index}>
            <AccordionArrowHeader
              className={style["program_page__faq__header"]}
              eventKey={`${index + 1}`}
              chevronDirection="down"
              chevronClass="faq-home_chevron"
              textClassName={"text-span"}
            >
              {value.faq_question}
            </AccordionArrowHeader>
            <Accordion.Collapse eventKey={`${index + 1}`}>
              <div className={style["program_page__faq__body"]}>{value.faq_answer}</div>
            </Accordion.Collapse>
          </div>
        ))}
      </Accordion>
    </div>
  )
}

export default ProgramFaqContainer
