import React, { FC, useEffect, useState } from "react"
import { Modal, ModalProps } from "react-bootstrap"
import * as Yup from "yup"
import { FaTimes } from "react-icons/fa"
import { Form, Formik } from "formik"
import { FormInput } from "../../../modules/input"
import { RegularButton } from "../../../modules/buttons"
import FormikTextArea from "../../../modules/input/FormikTextArea"
import { TokenData } from "../../../../constants/interfaces/user"
import SchoolServices from "../../../../lib/services/school.services"
import { Result } from "../../../../lib/services/response"

import style from "../styles/program.module.scss"
import ManualPhoneNumberInput from "../../../modules/input/ManualPhoneNumberInput"
import clsx from "clsx"
import { FileRequestForm } from "../../../../constants/interfaces/school"

export interface ModalFileRequestProps extends ModalProps {
  data?: TokenData
  schoolId?: string
  mode: "syllabus" | "portfolio"
}

const ModalFileRequest: FC<ModalFileRequestProps> = ({ data, schoolId, mode, ...props }) => {
  const [validation, setValidation] = useState("")
  const [isLoading, setIsLoading] = useState(false)
  const [validationRequired, setValidationRequired] = useState(false)

  const school = new SchoolServices()

  useEffect(() => {
    if (!props.show) {
      setValidation("")
    }
  }, [props.show])

  const validationSchema = Yup.object({
    user_firstname: Yup.string()
      .required("*Wajib diisi")
      .matches(/^[A-Za-z\s]+$/, "Hanya boleh huruf"),
    user_lastname: Yup.string()
      .required("*Wajib diisi")
      .matches(/^[A-Za-z\s]+$/, "Hanya boleh huruf"),
    user_email: Yup.string().email("Email tidak valid").required("*Wajib diisi"),
    user_phone_number: Yup.string()
      .matches(/^\+[\d]+$/, "Format yang kamu masukkan salah")
      .min(12, "*Pastikan nomor diawal +62, dan nomor telepon minimal 10 nomor")
      .max(15, "*Nomor telepon maksimal 13 nomor")
      .required("*Wajib diisi"),
    reason_of_download: Yup.string().min(20).max(300).required("*Wajib diisi"),
  })

  const FormFieldRow = ({ label, name }: { label: string; name: string }) => {
    return (
      <div className={style.program_page__syllabus__form__row}>
        <div className={style.program_page__syllabus__form__row__label}>{label}*</div>
        <FormInput
          placeholder={`Masukkan ${label} Kamu`}
          name={name}
          className={style["program_page__syllabus__form__row__input"]}
        />
      </div>
    )
  }

  const initialValues: FileRequestForm = {
    user_firstname: data?.user_firstname,
    user_lastname: data?.user_lastname,
    user_email: data?.user_email,
    user_phone_number: data?.user_phone_number,
    reason_of_download: "",
  }

  return (
    <Modal
      {...props}
      centered
      className="program_page__syllabus pr-0"
      size="lg"
      contentClassName={style.program_page__syllabus__content}
    >
      <div className={style.program_page__syllabus__times}>
        <FaTimes onClick={props.onHide} />
      </div>

      <div className={style.program_page__syllabus__body}>
        <h2>
          Isi data untuk {mode === "syllabus" ? "mendapatkan silabus" : "melihat Portofolio Alumni"}
        </h2>

        <div className="d-lg-none d-block">
          <hr />
        </div>

        <Formik
          enableReinitialize
          initialValues={initialValues}
          validationSchema={validationSchema}
          validateOnChange={validationRequired}
          validateOnBlur={validationRequired}
          onSubmit={async (value) => {
            setIsLoading(true)
            try {
              let res: Result<any>

              if (mode === "syllabus") {
                res = await school.getSyllabusForm(schoolId, value)
              } else {
                res = await school.getPortfolioForm(schoolId, value)
              }

              if (res && res.isSuccess) {
                const result = res.getValue()
                window.open(result as string, "_blank")
                // props.onHide()
              } else {
                setValidation(res ? res.error.message : "Terjadi kesalahan. Silahkan coba lagi")
              }
            } catch (e: any) {
              if (process.env.NODE_ENV === "development") {
                console.log(e)
              }
              setValidation("Terjadi kesalahan. Silahkan coba lagi")
            }
            setIsLoading(false)
          }}
        >
          {({ values, errors, setFieldValue }) => {
            // const handleChangePhoneNumber = (e: React.ChangeEvent<any>) => {
            //   const value = e.target.value as string
            //
            //   if (value.match(/^\+[\d]+$/)) {
            //     return setFieldValue("user_phone_number", value)
            //   }
            // }

            return (
              <div className={style.program_page__syllabus__form}>
                <Form>
                  <FormFieldRow label="Nama Depan" name="user_firstname" />

                  <FormFieldRow label="Nama Belakang" name="user_lastname" />

                  <FormFieldRow label="Email" name="user_email" />

                  <div className={style.program_page__syllabus__form__row}>
                    <div className={style.program_page__syllabus__form__row__label}>
                      Nomor Telepon*
                    </div>
                    {/*<FormInput*/}
                    {/*  name="user_phone_number"*/}
                    {/*  placeholder={`Masukkan Nomor Telepon Kamu`}*/}
                    {/*  className={style["program_page__syllabus__form__row__input"]}*/}
                    {/*  onChange={handleChangePhoneNumber}*/}
                    {/*/>*/}
                    {/* TODO ini juga udah gw benerin, cuma nambahin typesafety di initialValues nya */}
                    <ManualPhoneNumberInput
                      value={values.user_phone_number}
                      error={errors?.user_phone_number}
                      placeholder="Nomor HP"
                      className={clsx(
                        style.program_page__syllabus__form__row__input,
                        style.program_page__syllabus__form__row__tel
                      )}
                      onChange={(value) => setFieldValue("user_phone_number", value)}
                    />
                  </div>

                  <div className={style.program_page__syllabus__form__row}>
                    <div className={style.program_page__syllabus__form__row__label}>
                      Mengapa {mode === "syllabus" ? "download Silabus" : "melihat portfolio"}*
                    </div>
                    <FormikTextArea
                      className={style.program_page__syllabus__form__row__input}
                      placeholder="Masukkan alasan"
                      name="reason_of_download"
                      rows={3}
                      maxCharacter={300}
                      minCharacter={20}
                    />
                  </div>

                  <div className="text-red">{validation}</div>

                  <RegularButton
                    className={style.program_page__syllabus__form__submit}
                    type="solid"
                    color="green"
                    isSubmitting={isLoading}
                    onClick={() => {
                      setValidationRequired(true)
                    }}
                    isFormik
                  >
                    {mode === "syllabus" ? "Download Silabus" : "Lihat Portofolio"}
                  </RegularButton>
                </Form>
              </div>
            )
          }}
        </Formik>
      </div>
    </Modal>
  )
}

export default ModalFileRequest
