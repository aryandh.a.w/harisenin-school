import React from "react"

import style from "../styles/program.module.scss"
import { LinkButton } from "../../../modules/buttons"

function ProgramCta() {
  return (
    <div className={style["program_page__cta"]}>
      <div className={style["program_page__cta__content"]}>
        <div className={style["program_page__cta__content__title"]}>
          Ada pertanyaan yang ingin kamu tanyakan?
        </div>
        <div className={style["program_page__cta__content__label"]}>
          Konsultasikan bersama Harisenin.com
        </div>
        <LinkButton
          type="solid"
          color="green"
          href="https://api.whatsapp.com/send?phone=6281312117711&text=Saya%20Mau%20ikut%20Harisenin%20Millennial%20School%20di%20harisenin.com%20min"
          className={style["program_page__cta__content__button"]}
          isExternal
        >
          Hubungi Kami
        </LinkButton>
      </div>
    </div>
  )
}

export default ProgramCta
