import React, { FC } from "react"
import { Container } from "react-bootstrap"

import { HARISENIN_PUBLIC_PAGE_ASSETS } from "../../../../constants/pictures"
import { SchoolDetail } from "../../../../constants/interfaces/school"

import { createMarkup, StarRating } from "../../../../lib/utils/method"

import { RegularButton } from "../../../modules/buttons"

import style from "../styles/program.module.scss"
import { DynamicImage, NextImage } from "../../../modules/misc"

export interface ProgramBannerProps {
  schoolDetail: SchoolDetail
  scroll: () => void
  screenWidth: number
}

const ProgramBanner: FC<ProgramBannerProps> = ({ schoolDetail, scroll }) => {
  return (
    <>
      <div className={style.program_page__banner}>
        {/* Background Image */}
        <NextImage
          size={[1440, 789]}
          src={`${HARISENIN_PUBLIC_PAGE_ASSETS}/program_banner_3.png`}
          alt="program banner"
          className={style.program_page__banner__image}
        />
        <div className={style["program_page__banner__content"]}>
          <Container className={style["program_page__banner__container"]}>
            {/* Left section of banner */}
            <div className={style["program_page__banner__container__detail"]}>
              {/* Title */}
              <h1 id="header1">{schoolDetail?.school_banner?.banner_title}</h1>

              {/* Description */}
              {schoolDetail?.school_banner?.banner_sub_title ? (
                <div
                  className={style.program_page__banner__container__detail__description}
                  dangerouslySetInnerHTML={createMarkup(
                    schoolDetail?.school_banner?.banner_sub_title
                  )}
                />
              ) : (
                <div
                  className={style["program_page__banner__container__detail__description--null"]}
                />
              )}

              {/* Button for scrolling*/}
              {!schoolDetail.school_program || !schoolDetail.school_program.length ? null : (
                <RegularButton
                  type={"solid"}
                  color={"green"}
                  onClick={scroll}
                  className="mr-3"
                  id="btn-regist-class-banner"
                >
                  Daftar Kelas
                </RegularButton>
              )}
            </div>

            {schoolDetail?.user_picture && schoolDetail?.user_picture.length > 0 ? (
              <div className={style["program_page__banner__review"]}>
                <div className={style["program_page__banner__review__picture"]}>
                  {schoolDetail?.user_picture.map((picture, index) => {
                    if (index === 0) {
                      return <DynamicImage src={picture} alt="User Picture" key={index} />
                    } else if (index === 1) {
                      return (
                        <DynamicImage
                          className={style["program_page__banner__review__picture--picture_two"]}
                          src={picture}
                          alt="User Picture"
                          key={index}
                        />
                      )
                    } else {
                      return (
                        <DynamicImage
                          className={style["program_page__banner__review__picture--picture_three"]}
                          src={picture}
                          alt="User Picture"
                          key={index}
                        />
                      )
                    }
                  })}
                </div>
                <div className={style["program_page__banner__review__user"]}>
                  <div className={style["program_page__banner__review__user__rating"]}>
                    <StarRating value={schoolDetail.average_rating} />
                  </div>
                  ({schoolDetail.average_rating}/5 dari {schoolDetail.total_alumni} Alumni)
                </div>
              </div>
            ) : (
              ""
            )}
          </Container>
        </div>
      </div>
    </>
  )
}

export default ProgramBanner
