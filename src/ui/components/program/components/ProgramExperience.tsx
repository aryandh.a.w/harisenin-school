import React from "react"

import style from "../styles/program.module.scss"
import { useRouter } from "next/router"
import clsx from "clsx"
import { AiOutlineCaretRight } from "react-icons/ai"

function ProgramExperience({ screenWidth }: { screenWidth: number }) {
  const router = useRouter()
  const { asPath } = router

  const isSchool = asPath.includes("/bootcamp")

  return (
    <div className={style.program_page__experience} id="experience1">
      <div className={style.program_page__anchor} id="experience" />
      <h2>Learning Experience</h2>
      <h3>Fase Pembelajaran yang Akan Kamu Ikuti!</h3>

      <div className={style.program_page__experience__row}>
        <div
          className={clsx(
            style.program_page__experience__column,
            style["program_page__experience__column--left"]
          )}
        >
          <div className={style.program_page__experience__column__title}>RISE Learn</div>
          <div className={style.program_page__experience__column__content}>
            Kuasai <i>technical</i> dan <i>soft skill</i> dalam {isSchool ? "1 - 4" : "1 - 2"} bulan
            secara intensif. Belajar secara kelompok, tugas praktik, dan <i>final project</i>
            {!isSchool && " dengan materi berstandar industri"}. Belajar langsung dengan para ahli
            dibidangnya dari perusahaan Top di Indonesia.
          </div>
        </div>

        {screenWidth < 576 && isSchool && (
          <div
            className={clsx(
              style.program_page__experience__step,
              style["program_page__experience__step--one"]
            )}
          >
            Learning Phase: 1 - 4 Bulan
            <AiOutlineCaretRight />
          </div>
        )}

        <div
          className={clsx(
            style.program_page__experience__column,
            style["program_page__experience__column--right"]
          )}
        >
          <div className={style.program_page__experience__column__title}>
            {isSchool ? "RISE UP*" : "Final Project"}
          </div>
          <div className={style.program_page__experience__column__content}>
            {isSchool ? (
              <>
                Dapatkan pelatihan dan konsultasi karier 1-on-1, plus peluang kerja magang atau
                project-based. Kamu juga akan mendapatkan job-connect dengan hiring partner
                harisenin.com. <br /> <br />
                <i>*Pro/Guarantee Program</i>
              </>
            ) : (
              "Buka peluang karier dan persiapkan portofolio dengan mengerjakan final project yang melalui tahap pitching dan dapatkan feedback dari para ahli di bidangnya."
            )}
          </div>
        </div>

        {screenWidth < 576 && isSchool && (
          <div
            className={clsx(
              style.program_page__experience__step,
              style["program_page__experience__step--two"]
            )}
          >
            Career Pursuit & Job Connect: Life Time
            <AiOutlineCaretRight />
          </div>
        )}

        {screenWidth < 576 && !isSchool && (
          <div
            className={clsx(
              style.program_page__experience__step,
              style["program_page__experience__step--one"]
            )}
          >
            Learning Phase: 1 - 2 Bulan
            <AiOutlineCaretRight />
          </div>
        )}
      </div>

      {screenWidth > 576 && (
        <div className="d-flex relative">
          <div
            className={clsx(
              style.program_page__experience__step,
              style["program_page__experience__step--one"],
              !isSchool && style["program_page__experience__step--pro-class"]
            )}
          >
            Learning Phase: {isSchool ? "1 - 4" : "1 - 2"} Bulan
            <AiOutlineCaretRight />
          </div>
          {isSchool && (
            <div
              className={clsx(
                style.program_page__experience__step,
                style["program_page__experience__step--two"]
              )}
            >
              Career Pursuit & Job Connect: Life Time
              <AiOutlineCaretRight />
            </div>
          )}
        </div>
      )}
    </div>
  )
}

export default ProgramExperience
