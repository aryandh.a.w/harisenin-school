import React from "react"
import { CountdownTimer, pad } from "../../../lib/utils/method"
import dayjs from "dayjs"
import BasicCard from "../../modules/card/BasicCard"
import { InvoiceDetail } from "../../../constants/interfaces/order"

import "dayjs/locale/id"

import style from "./styles/invoice.module.scss"

function CountdownCard({ data }: { data: InvoiceDetail }) {
  dayjs.locale("id")

  if (!data) {
    return <></>
  }

  const countdownTime = CountdownTimer(data.expiry_date)
  return (
    <BasicCard className={style["countdown_card"]} isBordered>
      <div className={style["countdown_card__container"]}>
        {/*<div className={style["countdown_card__wrapper"]}>*/}
        {/*  <div className="countdown_card__wrapper__ticker">*/}
        {/*    {pad(countdownTime.days)}*/}
        {/*  </div>*/}
        {/*  <p>Hari</p>*/}
        {/*</div>*/}
        <div className={style["countdown_card__wrapper"]}>
          <div className={style["countdown_card__wrapper__ticker"]}>{pad(countdownTime.hours)}</div>
          <p>Jam</p>
        </div>
        <div className={style["countdown_card__wrapper"]}>
          <div className={style["countdown_card__wrapper__ticker"]}>
            {pad(countdownTime.minutes)}
          </div>
          <p>Menit</p>
        </div>
        <div className={style["countdown_card__wrapper"]}>
          <div className={style["countdown_card__wrapper__ticker"]}>
            {pad(countdownTime.seconds)}
          </div>
          <p>Detik</p>
        </div>
      </div>
      <div className={style["countdown_card__info"]}>
        (Sebelum <b>{dayjs(data.expiry_date).format("dddd, DD MMMM YYYY HH:mm")} WIB</b>)
      </div>
    </BasicCard>
  )
}

export default CountdownCard
