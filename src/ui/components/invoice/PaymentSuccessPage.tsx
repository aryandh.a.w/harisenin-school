import React, { useEffect, useState } from "react"
import { Container } from "react-bootstrap"
import { FaCheck } from "react-icons/fa"
import { useRouter } from "next/router"
import dayjs from "dayjs"

import Helmet from "../../../lib/utils/Helmet"
import HeaderHome from "../../modules/header/HeaderHome"
import HeaderOrder from "../../modules/header/HeaderOrder"
import BasicCard from "../../modules/card/BasicCard"
import LinkButton from "../../modules/buttons/LinkButton"

import "dayjs/locale/id"
import { PaymentDetail } from "../../../constants/interfaces/order"

import style from "./styles/invoice.module.scss"
import CheckoutService from "../../../lib/services/checkout.services"

const PaymentSuccessPage = ({ invoice }: { invoice: PaymentDetail }) => {
  const [paidDate, setPaidDate] = useState(new Date())
  const [orderNumber, setOrderNumber] = useState("")
  const [programTitle, setProgramTitle] = useState("")

  const router = useRouter()
  const checkout = new CheckoutService()
  const { orderId } = router.query
  dayjs().locale("id").format()

  useEffect(() => {
    const getInvoice = async () => {
      setPaidDate(invoice.data.updated)
      setOrderNumber(invoice.order_number)

      const res = await checkout.getCheckoutDetail(orderId as string)

      if (res.isSuccess) {
        const data = res.getValue()
        setProgramTitle(data.order_cart[0].school_title)
      }
    }
    if (orderId && invoice) {
      getInvoice()
    }
  }, [orderNumber, invoice])

  return (
    <>
      <Helmet title="Harisenin.com: Pembayaran Berhasil" />
      <HeaderHome />
      <HeaderOrder step={3} />

      <div className="grey-background">
        <Container className={style["invoice_page"]}>
          <BasicCard className={style["success_card"]} isBordered>
            <div className={style["success_card__head"]}>
              <div className={style["success_card__checklist"]}>
                <div className={style["success_card__checklist__inner"]}>
                  <FaCheck />
                </div>
              </div>
            </div>
            <div className={style["success_card__body"]}>
              <div className={style["success_card__body__info"]}>
                <div className={style["success_card__body__info__title"]}>
                  Pembayaran Berhasil !
                </div>
                <div className={style["success_card__body__info__content"]}>
                  Terima kasih telah melakukan pembelian <b>{programTitle}</b> harisenin.com! <br />
                  Silahkan cek e-mail kamu untuk pemberitahuan lebih lanjut. Dalam waktu 3 x 24 jam,
                  kamu akan di hubungi dan diundang ke whatsapp group oleh admin harisenin.com atau{" "}
                  <span> hubungi kami</span>
                  jika ada kendala.
                </div>
                <div className={style["success_card__body__info__wrapper"]}>
                  <LinkButton href="/" isExternal id="btn-back-to-homepage">
                    Kembali ke Homepage
                  </LinkButton>
                  <LinkButton
                    href="/dashboard/order?type=school"
                    isExternal
                    id="btn-see-purchase-list"
                  >
                    Lihat Detail Pembelian
                  </LinkButton>
                </div>
              </div>
            </div>
          </BasicCard>
        </Container>
      </div>
    </>
  )
}

export default PaymentSuccessPage
