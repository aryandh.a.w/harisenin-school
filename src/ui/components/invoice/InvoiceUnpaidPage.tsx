import React, { useEffect, useState } from "react"
import { Accordion, Container } from "react-bootstrap"
import copy from "copy-to-clipboard"
import { useRouter } from "next/router"

import { sleep } from "../../../lib/utils/method"
import { HARISENIN_PUBLIC_LOGO } from "../../../constants/pictures"
import Helmet from "../../../lib/utils/Helmet"
import HeaderHome from "../../modules/header/HeaderHome"
import BasicCard from "../../modules/card/BasicCard"
import AccordionArrowHeader from "../../modules/accordion/AccordionArrowHeader"
import LinkButton from "../../modules/buttons/LinkButton"

import { TextButton } from "../../modules/buttons"
import CountdownCard from "./CountdownCard"
import { useBasicWebSocket } from "../../../lib/utils/hooks/useWebsocket"
import { InvoiceDetail, OrderStatus } from "../../../constants/interfaces/order"
import { BCAMethod, BNIMethod, BRIMethod, MandiriMethod, OtherBank } from "../../../constants/const"
import HeaderOrder from "../../modules/header/HeaderOrder"
import InvoiceService from "../../../lib/services/invoice.services"
import style from "./styles/invoice.module.scss"

const InvoicePage = () => {
  const [isWebSocketStart, setIsWebSocketStart] = useState(false)
  const [isLoading, setIsLoading] = useState(true)
  const [invoiceDetail, setInvoiceDetail] = useState<InvoiceDetail | null>(null)

  const [code, setCode] = useState("100700543910")
  const [transactionId, setTransactionId] = useState("")
  const [transferAmount, setTransferAmount] = useState(0)
  const [bank, setBank] = useState("")
  const [method, setMethod] = useState(OtherBank)
  const [status, setStatus] = useState<OrderStatus>("PENDING")

  const [copyAlert, setCopyAlert] = useState(false)

  const router = useRouter()
  const { orderId, productId } = router.query

  const invoice = new InvoiceService()

  useEffect(() => {
    const getInvoice = async () => {
      try {
        const res = await invoice.getInvoiceDetail(orderId as string)
        if (res.isSuccess) {
          const data = res.getValue()
          setInvoiceDetail(data)
          setCode(data.account_number)
          setBank(data.bank_code)
          setTransferAmount(data.transfer_amount)
          setTransactionId(data.invoice_number)
          setStatus(data.status)
          const banks = data.bank_code
          switch (banks) {
            case "BCA":
              setMethod(BCAMethod)
              break
            case "BNI":
              setMethod(BNIMethod)
              break
            case "BRI":
              setMethod(BRIMethod)
              break
            case "MANDIRI":
              setMethod(MandiriMethod)
              break
            default:
              break
          }
        }

        setIsLoading(false)
      } catch (error) {
        setIsLoading(false)
      }
    }
    if (orderId) {
      getInvoice()
      setIsWebSocketStart(true)
    }
  }, [orderId, productId])

  const redirectToSuccessPage = async () => {
    window.location.assign(`/school/invoice/${orderId}/success?productId=${productId}`)
  }

  useBasicWebSocket({
    isSocketStart: isWebSocketStart,
    channel: `invoices-${orderId}`,
    fetcher: redirectToSuccessPage,
  })

  const copyText = async () => {
    copy(code)
    setCopyAlert(true)
    await sleep(2000)
    setCopyAlert(false)
  }

  return (
    <>
      <Helmet title="Harisenin.com: Konfirmasi Pembayaran" />
      <HeaderHome />
      <HeaderOrder step={2} />

      <div className="grey-background">
        <Container className={style["invoice_page"]}>
          {copyAlert ? (
            <div className={style.invoice_card__alert}>Rekening Berhasil Disalin</div>
          ) : null}

          <h1>Konfirmasi Pembayaran</h1>

          <BasicCard className={style["invoice_card"]} isBordered>
            <h5 className="text-center">
              Mohon selesaikan pembayaranmu dengan rincian sebagai berikut:
            </h5>

            {/* Countdown starts here */}
            {status === "SETTLED" || isLoading ? null : <CountdownCard data={invoiceDetail} />}
            {/*<CountdownCard data={invoiceDetail} />*/}
            <div className={style["invoice_card__center"]}>
              {/* Bill number */}
              <div className={style["invoice_card__center__wrapper"]}>
                <h5 className={style["invoice_card__center__wrapper__title"]}>Nomor tagihan</h5>
                <p className={style["invoice_card__center__wrapper__content"]}>{transactionId}</p>
              </div>

              {/* Money amount */}
              <div className={style["invoice_card__center__wrapper"]}>
                <h5 className={style["invoice_card__center__wrapper__title"]}>
                  Jumlah tagihan kamu
                </h5>
                <h5 className={style["invoice_card__center__wrapper__price"]}>
                  Rp {Intl.NumberFormat("id-ID").format(transferAmount)}{" "}
                </h5>
              </div>

              {/* Virtual account number */}
              <div className={style["invoice_card__center__wrapper"]}>
                <h5 className={style["invoice_card__center__wrapper__title"]}>Virtual Account</h5>
                <div
                  className={`${style["invoice_card__center__wrapper__content"]} ${style["invoice_card__center__wrapper__copy-text"]}`}
                >
                  {code}
                  <TextButton onClick={copyText}>Salin</TextButton>
                </div>
              </div>
            </div>

            {/* bank logo */}
            {bank ? (
              <div className={style["invoice_card__bank"]}>
                <img
                  src={`${HARISENIN_PUBLIC_LOGO}/logo_${bank?.toLowerCase()}.png`}
                  alt={bank?.toLowerCase()}
                />
                {bank} Virtual Account
              </div>
            ) : null}

            {status === "SETTLED" ? null : (
              <div className={style["invoice_card__change"]}>
                <p>Kamu ingin mengubah metode pembayaran?</p>
                <LinkButton
                  className={style["invoice_card__change__button"]}
                  isExternal={true}
                  href={`/school/checkout/${orderId}`}
                  id="btn-change-payment"
                >
                  Ganti Metode Pembayaran
                </LinkButton>
              </div>
            )}

            <div className={style["invoice_card__bottom"]}>
              <div className={style["invoice_card__bottom__wrapper"]}>
                <h5>Metode Pembayaran</h5>
                <Accordion>
                  {method.map((item, i) => (
                    <div className="method-accordion" key={i}>
                      <AccordionArrowHeader
                        eventKey={`${item.id}`}
                        chevronDirection="down"
                        className={style["invoice_card__bottom__accordion__toggle"]}
                      >
                        {item.method}
                      </AccordionArrowHeader>
                      <Accordion.Collapse
                        eventKey={`${item.id}`}
                        className={style["invoice_card__bottom__accordion__body"]}
                      >
                        <ol>
                          {item.step.map((list) => (
                            <li key={list}>{list}</li>
                          ))}
                        </ol>
                      </Accordion.Collapse>
                    </div>
                  ))}
                </Accordion>
              </div>
            </div>
            <p className={style["invoice_card__warning"]}>
              Pembelian akan otomatis dibatalkan apabila kamu tidak melakukan pembayaran lebih dari
              3 (tiga) jam setelah orderan ini dibuat
            </p>
          </BasicCard>
        </Container>
      </div>
    </>
  )
}

export default InvoicePage
