import { Container } from "react-bootstrap"

import { HARISENIN_PUBLIC_PAGE_ASSETS } from "../../../constants/pictures"

import style from "./styles/programlist.module.scss"

const ProgramPromoBanner = () => {
  return (
    <div className={`${style["landing_page__banner"]} ${style["landing_page__banner--promo"]}`}>
      <Container className={style["landing_page__banner__container"]}>
        {/* Left section of Banner */}
        <div
          className={`${style["landing_page__banner__left"]} ${style["landing_page__banner__left--promo"]}`}
        >
          <h1 className={`${style["landing_page__banner__left"]} h1`}>
            Temukan Program yang
            <br />
            Kamu <span className="text-green">Butuhkan</span>
          </h1>
          <div className={style["landing_page__banner__left__subtitle"]}>
            Pelajari keterampilan yang paling banyak dibutuhkan untuk pekerjaan hari ini dan masa
            depan dengan harga kompetitif.
          </div>
        </div>

        {/* Right section of banner, will be hidden on mobile view */}
        <div
          className={`${style["landing_page__banner__right"]} ${style["landing_page__banner__right--promo"]}`}
        >
          <img
            alt="harisenin-millennial-school"
            src={`${HARISENIN_PUBLIC_PAGE_ASSETS}/homepage_banner_1.png`}
            // src={`${HARISENIN_PUBLIC_PAGE_ASSETS}/homepage_banner_2.png`}
          />
        </div>
      </Container>
    </div>
  )
}

export default ProgramPromoBanner
