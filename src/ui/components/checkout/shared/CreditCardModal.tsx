import { Form, Formik } from "formik"
import React, { FC, useEffect, useState } from "react"
import { Modal, ModalProps, Spinner } from "react-bootstrap"
import { FaTimes } from "react-icons/fa"
import { PROVINCE, REGENCIES } from "../../../../constants/const"
import { CreditCardData } from "../../../../constants/interfaces/credit-card"
import { SelectOption } from "../../../../constants/interfaces/ui"
import { ISO_3166 } from "../../../../constants/ISO_3166"
import { HARISENIN_PUBLIC_LOGO } from "../../../../constants/pictures"
import CheckoutService from "../../../../lib/services/checkout.services"
import XenditService from "../../../../lib/services/XenditNode"
import { useUserData } from "../../../../lib/utils/hooks/useUserData"
import { priceFormatter, creditCardTypeV2 } from "../../../../lib/utils/method"
import { RegularButton } from "../../../modules/buttons"
import { FormInput, PasswordInput } from "../../../modules/input"
import FormikCreditCardInput from "../../../modules/input/FormikCreditCard"
import FormikDate from "../../../modules/input/FormikDate"
import FormikSelectInput from "../../../modules/input/FormikSelectInput"
import FormikTextArea from "../../../modules/input/FormikTextArea"
import ManualPhoneNumberInput from "../../../modules/input/ManualPhoneNumberInput"
import { creditCardValidation } from "../FormValidation"

import style from "../styles/checkout.module.scss"
import clsx from "clsx"

export interface CreditCardModalProps extends ModalProps {
  checkoutId?: string
  price?: number
  productId?: string
}

interface CreditFormData {
  given_names: string
  surname: string
  email: string
  country: string
  postal_code: string
  account_number: string
  mobile_number: string
  phone_number: string
  exp_date: string
  card_cvn: string
  province_state: string
  street_line1: string
  street_line2: string
  city: string
}

const CreditCardModal: FC<CreditCardModalProps> = ({
  checkoutId,
  productId,
  price,
  show,
  onHide,
}) => {
  const [modalCredit, setModalCredit] = useState(false)
  const [modal3Ds, setModal3Ds] = useState(false)
  const [modalListening, setModalListening] = useState(false)

  const [validationRequired, setValidationRequired] = useState(false)
  const [authLink, setAuthLink] = useState("")
  // @ts-ignore
  const [tokenData, setTokenData] = useState<CreditCardData>(null)
  const [formData, setFormData] = useState<CreditFormData>({
    given_names: "",
    surname: "",
    email: "",
    country: "",
    postal_code: "",
    account_number: "",
    mobile_number: "",
    phone_number: "",
    exp_date: "",
    card_cvn: "",
    province_state: "",
    street_line1: "",
    street_line2: "",
    city: "",
  })
  // const [tokenId, setTokenId] = useState("")

  const [isListening, setIsListening] = useState(false)
  const [error3Ds, setError3Ds] = useState("")

  const { tokenData: userData } = useUserData()

  const xendit = new XenditService()
  const checkout = new CheckoutService()

  useEffect(() => {
    setModalCredit(show)
  }, [show])

  function countryOption(): SelectOption[] {
    return ISO_3166.map((value) => {
      return { label: value.name, value: value["alpha-2"] }
    })
  }

  function CityOptionMaker(value?: string): SelectOption[] {
    if (!value) {
      return []
    }

    const province = PROVINCE.filter((item) => item.label === value)

    return REGENCIES.filter((item) => item.province_id === province[0].value).map((item) => {
      return { value: item.id, label: item.regency_name }
    })
  }

  const credits = ["visa", "mastercard", "jcb"]

  const hideCreditModal = () => {
    setModalCredit(false)
    onHide()
  }

  const handleSubmitPayment = async (tokenData: CreditCardData, formData: CreditFormData) => {
    setModal3Ds(false)
    setModalListening(true)

    const res = await checkout.submitCreditCardPayment({
      checkoutId,
      body: {
        payment_method: "CREDIT_CARD",
        payment_channel: creditCardTypeV2(formData.account_number, true),
        tokenID: tokenData.id,
        authID: tokenData.authentication_id,
        cardCvn: formData.card_cvn,
        billing_details: {
          given_names: formData.given_names,
          surname: formData.surname,
          email: formData.email,
          mobile_number: formData.mobile_number,
          phone_number: formData.phone_number,
          address: {
            street_line1: formData.street_line1,
            street_line2: formData.street_line2,
            city: formData.city,
            province: formData.province_state,
            zip_code: formData.postal_code,
            country: formData.country,
          },
        },
      },
    })

    if (res.isSuccess) {
      window.location.assign(`/school/invoice/${checkoutId}/success?productId=${productId}`)
    } else {
      setModal3Ds(false)
      setModalCredit(true)
      setError3Ds(
        "Terjadi kesalahan saat memverifikasi kartu kamu. Pastikan kembali info yang kamu masukkan sudah benar"
      )
    }
  }

  useEffect(() => {
    window.addEventListener(
      "message",
      async (ev) => {
        if (ev.data && tokenData?.id && formData?.card_cvn) {
          const data = JSON.parse(ev.data)

          if (data.status) {
            const status = data.status

            if (status === "VERIFIED" && tokenData.id && formData.card_cvn) {
              await handleSubmitPayment(tokenData, formData)
            } else {
              setModal3Ds(false)
              setModalCredit(true)
              setError3Ds(
                "Terjadi kesalahan saat memverifikasi kartu kamu. Pastikan kembali info yang kamu masukkan sudah benar"
              )
            }
          } else {
            setModal3Ds(false)
            setModalCredit(true)
            setError3Ds(
              "Terjadi kesalahan saat memverifikasi kartu kamu. Pastikan kembali info yang kamu masukkan sudah benar"
            )
          }
        }
      },
      false
    )
  }, [modal3Ds, tokenData])

  return (
    <>
      <Modal
        show={modalCredit}
        onHide={hideCreditModal}
        contentClassName={style.modal_credit}
        className="pl-0"
        size="lg"
      >
        <div className={clsx(style.modal_credit__times, "d-lg-none d-flex")}>
          <FaTimes onClick={hideCreditModal} />
        </div>

        <h1>Pembayaran dengan Kartu Debit / Kredit</h1>
        <div className={style["modal_credit__image"]}>
          {credits.map((value, index) => (
            <img src={`${HARISENIN_PUBLIC_LOGO}/logo_${value}.png`} alt="" key={index} />
          ))}
        </div>

        <Formik
          initialValues={{
            given_names: userData?.user_firstname,
            surname: userData?.user_lastname,
            email: userData?.user_email,
            country: "",
            postal_code: "",
            account_number: "",
            mobile_number: userData?.user_phone_number,
            phone_number: "",
            exp_date: "",
            card_cvn: "",
            province_state: "",
            street_line1: "",
            street_line2: "",
            city: "",
          }}
          enableReinitialize
          validationSchema={creditCardValidation}
          validateOnChange={validationRequired}
          validateOnBlur={validationRequired}
          onSubmit={async (values) => {
            try {
              setIsListening(true)
              const expireDate = values.exp_date.split("/")
              const cardNumber = values.account_number.replace(/ /g, "")

              const body = {
                amount: Math.floor(price).toString(),
                card_cvn: values.card_cvn,
                card_exp_month: expireDate[0],
                card_exp_year: expireDate[1],
                card_number: cardNumber,
              }

              const res = await xendit.tokenize(body)

              if (res.isValidated) {
                setModalCredit(false)
                // setTokenId(res.tokenData?.id)
                setTokenData(res.tokenData)
                setFormData(values)

                if (res.tokenData?.status === "VERIFIED") {
                  await handleSubmitPayment(res.tokenData, values)
                } else {
                  setAuthLink(res.tokenData?.payer_authentication_url)
                  setModal3Ds(true)
                }
              } else {
                setModalCredit(true)
                setModal3Ds(false)
                setError3Ds(
                  "Terjadi kesalahan saat memverifikasi kartu kamu. Pastikan kembali info yang kamu masukkan sudah benar"
                )
              }

              setIsListening(false)
            } catch (e) {
              setModalCredit(true)
              setIsListening(false)
            }
          }}
        >
          {({ values, setFieldValue, errors }) => (
            <Form className={style["modal_credit__form"]}>
              {/*<pre>{JSON.stringify(values, null, 2)}</pre>*/}
              {/*<pre>{JSON.stringify(errors, null, 2)}</pre>*/}
              <div className={style["modal_credit__form__name"]}>
                <FormInput
                  name="given_names"
                  placeholder="Nama Depan"
                  className={style["modal_credit__form__name__input"]}
                />
                <FormInput
                  name="surname"
                  placeholder="Nama Belakang"
                  className={style["modal_credit__form__name__input"]}
                />
              </div>

              <FormInput name="email" placeholder="Email" />

              <div className={style["modal_credit__form__info"]}>
                <FormikSelectInput
                  setField={setFieldValue}
                  name="country"
                  options={countryOption()}
                  placeholder="Negara"
                  className={style["modal_credit__form__info__select"]}
                />
                <FormInput
                  name="postal_code"
                  placeholder="Kode Pos"
                  type="number"
                  className={style["modal_credit__form__info__zip"]}
                />
              </div>

              <div className={`${style["modal_credit__form__name"]} mb-4`}>
                <FormikSelectInput
                  setField={setFieldValue}
                  name="province_state"
                  valueType={"province"}
                  options={PROVINCE}
                  placeholder="Provinsi"
                  className={style["modal_credit__form__name__input"]}
                />
                <FormikSelectInput
                  setField={setFieldValue}
                  name="city"
                  valueType="label"
                  options={CityOptionMaker(values.province_state)}
                  placeholder="Kota/Kabupaten"
                  className={style["modal_credit__form__name__input"]}
                />
              </div>

              <FormikCreditCardInput placeholder="Nomor Kartu" name="account_number" />

              <div className={style["modal_credit__form__name"]}>
                <FormikDate name="exp_date" className={style["modal_credit__form__name__input"]} />
                <PasswordInput
                  max={999}
                  name="card_cvn"
                  placeholder="CVC"
                  type="number"
                  className={style["modal_credit__form__name__input"]}
                />
              </div>

              <FormikTextArea name="street_line1" placeholder="Alamat Billing" maxCharacter={600} />
              <FormikTextArea
                name="street_line2"
                placeholder="Alamat Billing 2 (optional)"
                maxCharacter={600}
              />

              <div className={style["modal_credit__form__name"]}>
                {/*TODO benerin dlu ini, typesafety nya berantakan*/}
                <ManualPhoneNumberInput
                  // value={values.mobile_number}
                  error={errors?.mobile_number}
                  placeholder="Nomor HP"
                  className={style["modal_credit__form__name__input"]}
                  onChange={(value) => setFieldValue("mobile_number", value)}
                />
                {/*TODO benerin dlu ini, typesafety nya berantakan*/}
                <ManualPhoneNumberInput
                  // value={values.phone_number}
                  error={errors?.phone_number}
                  placeholder="Nomor telepon (Opsional)"
                  className={style["modal_credit__form__name__input"]}
                  onChange={(value) => setFieldValue("mobile_number", value)}
                />
              </div>

              <div className={style["modal_credit__bill"]}>
                <div className={style["modal_credit__bill__label"]}>Jumlah Tagihan Kamu</div>
                <div className={style["modal_credit__bill__price"]}>{priceFormatter(price)}</div>
              </div>

              <RegularButton
                type="solid"
                color="green"
                isSubmitting={isListening}
                className={style["modal_credit__form__submit"]}
                isFormik
                onClick={() => setValidationRequired(true)}
              >
                Bayar Sekarang
              </RegularButton>

              <div className={style["modal_credit__note"]}>
                *Kami bekerja sama dengan xendit untuk memastikan bahwa informasi kartu kredit Kamu
                tetap terlindungi. Harisenin.com tidak akan mengakses info kartu kredit Kamu.
              </div>

              {error3Ds ? (
                <div className={`${style["modal_credit__note"]} text-red`}>{error3Ds}</div>
              ) : null}

              <div className={style["modal_credit__xendit"]}>
                Powered by <img src={`${HARISENIN_PUBLIC_LOGO}/logo_xendit.png`} alt="" />
              </div>
            </Form>
          )}
        </Formik>
      </Modal>

      <Modal show={modal3Ds} backdrop="static">
        <iframe
          src={authLink}
          height={500}
          frameBorder="none"
          className={style["modal_credit__iframe"]}
        />
      </Modal>

      <Modal show={modalListening} backdrop="static" contentClassName={style.modal_listening}>
        <Spinner animation="grow" variant="info" />
        <div className={style["modal_listening__text"]}>Memproses pembayaran</div>
      </Modal>
    </>
  )
}

export default CreditCardModal
