import React, { useEffect, useState } from "react"
import Helmet from "../../../lib/utils/Helmet"
import HeaderHome from "../../modules/header/HeaderHome"
import HeaderOrder from "../../modules/header/HeaderOrder"

import style from "./styles/checkout.module.scss"
import { Container, Spinner } from "react-bootstrap"
import BasicCard from "../../modules/card/BasicCard"
import { priceFormatter } from "../../../lib/utils/method"
import { RegularButton } from "../../modules/buttons"
import { useRouter } from "next/router"
import CheckoutService from "../../../lib/services/checkout.services"
import {
  AvailablePayment,
  CheckoutCart,
  DiscountDetail,
  PaymentOption,
} from "../../../constants/interfaces/checkout"
import clsx from "clsx"
import { HARISENIN_PUBLIC_LOGO } from "../../../constants/pictures"
import OvoModal from "./shared/OvoModal"
import QrModal from "./shared/QrModal"
import CreditCardModal from "./shared/CreditCardModal"
import { isBrowser } from "react-device-detect"
import { useEWalletPaymentSocket } from "../../../lib/utils/hooks/useWebsocket"

type ModalType = "none" | "qr" | "ovo" | "credit_card"

function ChangePaymentPage() {
  const [isFetching, setIsFetching] = useState(true)
  const [cart, setCart] = useState<null | CheckoutCart>(null)
  const [productId, setProductId] = useState("")
  const [discount, setDiscount] = useState<null | DiscountDetail>(null)
  const [discountDeal, setDiscountDeal] = useState(0)
  const [paymentOptions, setPaymentOptions] = useState<AvailablePayment | null>(null)

  const [adminFee, setAdminFee] = useState(0)
  const [selectedChannel, setSelectedChannel] = useState("")
  const [selectedMethod, setSelectedMethod] = useState("")
  const [totalPrice, setTotalPrice] = useState(0)
  const [qrCode, setQrCode] = useState("")

  const [modalOpen, setModalOpen] = useState<ModalType>("none")

  const [isListening, setIsListening] = useState(false)
  const [isSubmittingPayment, setIsSubmittingPayment] = useState(false)
  const [errorMessage, setErrorMessage] = useState("")

  const checkout = new CheckoutService()
  const router = useRouter()
  const { id } = router.query

  useEffect(() => {
    const getData = async () => {
      try {
        const res = await checkout.getCheckoutDetail(id as string)

        if (res.isSuccess) {
          const data = res.getValue()
          setCart(data.order_cart[0])
          setAdminFee(data.order_fee)
          setTotalPrice(data.order_payment.initial_amount)
          setSelectedChannel(data.order_payment.payment_channel)
          setSelectedMethod(data.order_payment.payment_method)
          setProductId(data.order_cart[0].id)
          if (data.discount) {
            setDiscount(data.discount.discount)
            setDiscountDeal(data.discount.discount_deal)
          }

          const res2 = await checkout.getAvailablePayment(data.order_cart[0].school_programs[0].id)

          if (res2.isSuccess) {
            const data2 = res2.getValue()
            setPaymentOptions(data2)
          }
        }

        setIsFetching(false)
      } catch (e) {
        setIsFetching(false)
      }
    }

    if (id) {
      getData()
    }
  }, [router])

  const handleCloseModal = () => {
    setModalOpen("none")
    setIsSubmittingPayment(false)
  }

  const credits = ["visa", "mastercard", "jcb"]

  // Loading state
  if (isFetching) {
    return (
      <BasicCard className={style["checkout__form--loading"]}>
        <Spinner animation="border" variant="info" />
      </BasicCard>
    )
  }

  const handleSelectPayment = (option: PaymentOption) => () => {
    if (cart) {
      setTotalPrice(cart.school_programs[0].program_price + option.payment_fee)
      setAdminFee(option.payment_fee)
      setSelectedChannel(option.payment_channel)
      setSelectedMethod(option.payment_method)
    }
  }

  const handleErrorListening = () => {
    setErrorMessage("Terjadi Kesalahan saat proses pembayaran. Silahkan coba lagi")
    setIsSubmittingPayment(false)
    setIsListening(false)
  }

  const handleSubmitPayment = async () => {
    // this is host url for redirect url
    const host = window.location.origin

    if (selectedChannel === "ID_OVO") {
      return setModalOpen("ovo")
    }

    if (selectedChannel === "CREDIT_CARD") {
      return setModalOpen("credit_card")
    }

    // try catch for payment
    try {
      setIsSubmittingPayment(true)
      const resPayment = await checkout.submitPayment({
        checkoutId: id as string,
        body: {
          payment_method: selectedMethod,
          payment_channel: selectedChannel,
          ...(selectedMethod === "EWALLET" && {
            payment_attributes: {
              success_redirect_url: `${host}/school/invoice/${id}/success`,
            },
          }),
        },
      })

      if (resPayment.isSuccess) {
        // Will activate websocket for e-wallet payment callback
        if (selectedMethod === "EWALLET") {
          const data = resPayment.getValue()
          const payment = data.payment
          const checkoutUrl =
            payment.actions.desktop_web_checkout_url ??
            payment.actions.mobile_web_checkout_url ??
            payment.actions.mobile_deeplink_checkout_url

          if (selectedChannel === "ID_SHOPEEPAY" && isBrowser) {
            setQrCode(payment.actions.qr_checkout_string)
            setModalOpen("qr")
            setIsSubmittingPayment(false)
          }

          window.open(checkoutUrl, "_blank")

          // eslint-disable-next-line react-hooks/rules-of-hooks
          useEWalletPaymentSocket({
            checkoutId: id,
            productId: productId,
            handleError: handleErrorListening,
          })
        } else {
          // Will redirect to invoice page
          window.location.assign(`/school/invoice/${id}/unpaid?productId=${productId}`)
        }
      } else {
        // catch error for payment process
        setIsSubmittingPayment(false)
        setErrorMessage(
          "Terjadi kesalahan saat proses pembayaran. Refresh ulang halaman dan Silahkan coba lagi"
        )
      }
    } catch (e) {
      // catch error for payment process
      setIsSubmittingPayment(false)
      setErrorMessage(
        "Terjadi kesalahan saat proses pembayaran. Refresh ulang halaman dan Silahkan coba lagi"
      )
    }
  }

  return (
    <>
      <Helmet title="Harisenin.com: Ubah Metode pembayaran" />
      <HeaderHome />
      <HeaderOrder step={1} />
      <div className="grey-background">
        <Container className={`${style["checkout"]} pb-2`}>
          <h1>Pembayaran</h1>

          <BasicCard className={style["checkout__payment"]}>
            <div className={style.checkout__payment__column}>
              <h4>Pilih Metode pembayaran</h4>

              <div className={style.checkout__payment__option__title}>Transfer Bank</div>
              <div className={style.checkout__payment__option}>
                {paymentOptions?.BANK_TRANSFER ? (
                  <>
                    {paymentOptions?.BANK_TRANSFER?.map((value, index) => (
                      <button
                        className={clsx(
                          style.checkout__payment__option__button,
                          selectedChannel === value.payment_channel &&
                            `${style["checkout__payment__option__button--selected"]}`
                        )}
                        key={index}
                        onClick={handleSelectPayment(value)}
                      >
                        <img src={value.payment_channel_logo} alt="" />
                      </button>
                    ))}
                  </>
                ) : null}
              </div>

              {/*/!* Will be hidden if e-wallet method is disabled on back-end *!/*/}
              {paymentOptions?.EWALLET ? (
                <>
                  <div className={style.checkout__payment__option__title}>E-Wallet</div>
                  <div className={style.checkout__payment__option}>
                    {paymentOptions?.EWALLET.map((value, index) => (
                      <button
                        className={clsx(
                          style.checkout__payment__option__button,
                          selectedChannel === value.payment_channel &&
                            `${style["checkout__payment__option__button--selected"]}`
                        )}
                        key={index}
                        onClick={handleSelectPayment(value)}
                      >
                        <img src={value.payment_channel_logo} alt="" />
                      </button>
                    ))}
                  </div>
                </>
              ) : null}

              {paymentOptions?.CREDIT_CARD ? (
                <>
                  <div className={style.checkout__payment__option__title}>Kartu Kredit/Debit</div>
                  <div className={style.checkout__payment__option}>
                    <button
                      className={clsx(
                        style.checkout__payment__option__button,
                        selectedMethod === "CREDIT_CARD" &&
                          style["checkout__payment__option__button--selected"]
                      )}
                      onClick={handleSelectPayment(paymentOptions?.CREDIT_CARD[0])}
                    >
                      {credits.map((value, index) => (
                        <img
                          src={`${HARISENIN_PUBLIC_LOGO}/logo_${value}.png`}
                          alt=""
                          key={index}
                          className={style.checkout__payment__option__button__cc}
                        />
                      ))}
                    </button>
                  </div>
                </>
              ) : null}
            </div>

            <div className={style.checkout__payment__column}>
              <h4>Total Pembayaran</h4>

              <div className={style.checkout__payment__detail}>
                <div className={style.checkout__payment__detail__label}>
                  {cart?.school_title} : <br />
                  {cart?.school_programs && cart?.school_programs[0]?.program_title}
                </div>
                <div className={style.checkout__payment__detail__amount}>x1</div>
                <div className={style.checkout__payment__detail__price}>
                  {priceFormatter(
                    cart?.school_programs ? cart?.school_programs[0]?.program_price : 0
                  )}
                </div>
              </div>

              {adminFee > 0 ? (
                <div className={style.checkout__payment__detail}>
                  <div className={style.checkout__payment__detail__label}>Biaya Admin</div>
                  <div className={style.checkout__payment__detail__price}>
                    {priceFormatter(adminFee)}
                  </div>
                </div>
              ) : null}

              {/* Will be appeared when there is promo applied */}
              {discountDeal ? (
                <div className={style.checkout__payment__detail}>
                  <div className={style.checkout__payment__detail__label}>
                    {discount.discount_name}
                  </div>
                  <div className={clsx(style.checkout__payment__detail__error, "text-red")}>
                    -{priceFormatter(discountDeal)}
                  </div>
                </div>
              ) : null}

              <hr />

              <div
                className={clsx(
                  style.checkout__payment__detail,
                  style["checkout__payment__detail--total"]
                )}
              >
                <div className={style.checkout__payment__detail__label}>Total</div>
                <div>{priceFormatter(totalPrice)}</div>
              </div>
            </div>

            {/* Listening animation when waiting callback on e-wallet payment */}
            {isListening ? (
              <div className="listening-component d-flex align-items-center">
                <Spinner animation="grow" variant="info" />
                <div className="ml-3">Menunggu respon dari pembayaran</div>
              </div>
            ) : null}

            {/* Button for submitting */}
            <div className={style["checkout__payment__button-row"]}>
              <RegularButton
                color="green"
                type="solid"
                onClick={handleSubmitPayment}
                isSubmitting={isSubmittingPayment}
              >
                Bayar Sekarang
              </RegularButton>
            </div>

            {/* Error text */}
            <div className={`${style["checkout__payment__error"]} mt-4`}>{errorMessage}</div>
          </BasicCard>
        </Container>
      </div>

      <OvoModal
        show={modalOpen === "ovo"}
        onHide={handleCloseModal}
        checkoutId={id as string}
        productId={productId}
        price={totalPrice}
      />

      <QrModal
        show={modalOpen === "qr"}
        onHide={handleCloseModal}
        checkoutId={id as string}
        productId={productId}
        price={totalPrice}
        qrString={qrCode}
      />

      <CreditCardModal
        show={modalOpen === "credit_card"}
        onHide={handleCloseModal}
        checkoutId={id as string}
        productId={productId}
        price={totalPrice}
      />
    </>
  )
}

export default ChangePaymentPage
