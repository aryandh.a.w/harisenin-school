import React from "react"
import clsx from "clsx"
import style from "./styles/checkbox.module.scss"

export const Checkbox: React.FC<React.InputHTMLAttributes<HTMLInputElement>> = ({
  className,
  id,
  children,
  ...props
}) => {
  return (
    <div className={clsx(style.checkbox, className)}>
      <input type="checkbox" id={id} {...props} />
      <label htmlFor={id}>{children}</label>
    </div>
  )
}
