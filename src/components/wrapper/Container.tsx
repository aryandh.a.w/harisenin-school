import React from "react"
import clsx from "clsx"

export const Container: React.FC<React.HTMLAttributes<HTMLDivElement>> = ({
  children,
  className,
  ...props
}) => {
  return (
    <div className={clsx(container, className)} {...props}>
      {children}
    </div>
  )
}

export const container = "mx-auto lg:w-[90%] px-4 w-full"
