import { ReactNode } from "react"
import { TriggerType } from "react-popper-tooltip/dist/types"

export type PopoverPlacement =
  | "auto"
  | "top"
  | "top-start"
  | "top-end"
  | "auto-start"
  | "auto-end"
  | "bottom"
  | "bottom-start"
  | "bottom-end"
  | "right"
  | "right-start"
  | "right-end"
  | "left"
  | "left-start"
  | "left-end"

export interface PopoverProps {
  arrow?: boolean
  arrowClassName?: string
  tooltipClassName?: string
  triggerClassName?: string
  children: ReactNode
  popover: ReactNode
  show?: boolean
  placement?: PopoverPlacement
  trigger?: TriggerType
  triggerId?: string
}
