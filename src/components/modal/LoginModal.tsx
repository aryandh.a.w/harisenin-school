import React, { FC, useEffect, useState } from "react"
import { Modal, ModalProps } from "react-bootstrap"
import { Form, Formik } from "formik"
import * as Yup from "yup"
import { FaTimes } from "react-icons/fa"
import { useRouter } from "next/router"
import { GoogleLoginButton, RegularButton } from "../buttons"
import { FormInput, PasswordInput } from "../input"
import AuthServices from "../../../lib/services/auth.services"

import style from "./styles/modal.module.scss"
import { setCookie } from "nookies"
import { domainChecker } from "../../../lib/utils/method"
import { RawTokenData } from "../../../constants/interfaces/auth"

export interface LoginModalProps {
  redirect?: string
}

export const LoginModal: FC<LoginModalProps & ModalProps> = ({ onHide, redirect, ...props }) => {
  const [validation, setValidation] = useState("")
  const [isLoading, setIsLoading] = useState(false)
  const [validationRequired, setValidationRequired] = useState(false)

  const router = useRouter()
  const auth = new AuthServices()

  useEffect(() => {
    if (!props.show) {
      setValidation("")
    }
  }, [props.show])

  const validationSchema = Yup.object({
    email: Yup.string().email("Email tidak valid").required("Email harus diisi"),
    password: Yup.string().required("Password harus diisi"),
  })

  function errorMessageGenerator(msg: string) {
    setValidation(msg)
  }

  return (
    <>
      <Modal
        {...props}
        centered
        contentClassName={style[`modal-register-login__content`]}
        dialogClassName={style["modal-register-login__dialog"]}
        onHide={onHide}
      >
        <div className={`${style["modal-times"]} d-lg-none d-flex`}>
          <FaTimes onClick={onHide} />
        </div>
        <Modal.Header className={`${style["modal-register-login__header"]} mx-auto pb-0 border-0`}>
          <Modal.Title className={style["modal-register-login__title"]}>Login</Modal.Title>
        </Modal.Header>
        <Formik
          initialValues={{ email: "", password: "" }}
          validationSchema={validationSchema}
          validateOnChange={validationRequired}
          validateOnBlur={validationRequired}
          onSubmit={async (value) => {
            setValidation("")
            setIsLoading(true)
            try {
              const res = await auth.manualLogin(value)

              if (!res || res.isFailure) {
                const err = res.error
                setValidation(err.message)
                setIsLoading(false)
              } else {
                const data = res.getValue() as RawTokenData

                if (data.data.status_completed_profile) {
                  if (!redirect) {
                    router.reload()
                  } else {
                    window.location.assign(redirect)
                  }
                } else {
                  setCookie(null, "HS_TEMP", JSON.stringify(data), {
                    path: "/",
                    domain: domainChecker(),
                  })

                  window.location.assign("/biodata")
                }
              }
            } catch (e: any) {
              if (e.message) {
                setValidation(e.message)
              } else {
                setValidation("Terjadi kesalahan. Cek kembali data yang diinput dan coba lagi")
              }
              setIsLoading(false)
            }
          }}
        >
          {() => (
            <Modal.Body className={style["modal-register-login__body"]}>
              <GoogleLoginButton
                handleError={errorMessageGenerator}
                redirect={redirect}
                id="btn-login-with-google"
              />
              <div className={style["modal-register-login__body__divider"]}>
                <span>atau</span>
              </div>

              <Form>
                <FormInput
                  placeholder="Masukkan e-mail kamu"
                  name="email"
                  id="login-email-field"
                  className={style["modal-register-login__body__form"]}
                />
                <PasswordInput
                  placeholder="Masukkan password kamu"
                  name="password"
                  className={style["modal-register-login__body__form"]}
                  id="login-email-password"
                  showButtonId="hide-show-password-login"
                />
                <RegularButton
                  className={style["modal-register-login__body__submit-button"]}
                  type="solid"
                  color="green"
                  isSubmitting={isLoading}
                  onClick={() => setValidationRequired(true)}
                  isFormik
                  id="btn-login-modal"
                >
                  Masuk
                </RegularButton>
                <div className={style["modal-register-login__body__validation-text"]}>
                  {validation}
                </div>
                <div className={style["modal-register-login__body__footer"]}>
                  <a href={`/lupa-password`} id="btn-forget-password-modal">
                    Lupa Password?
                  </a>
                </div>
              </Form>
            </Modal.Body>
          )}
        </Formik>
      </Modal>
    </>
  )
}
