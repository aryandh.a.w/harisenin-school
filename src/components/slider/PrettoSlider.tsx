import React from "react"
import Slider, { SliderProps, SliderTooltip } from "rc-slider"

const handle = (props: any) => {
  const { value, index, ...restProps } = props
  return (
    <SliderTooltip prefixCls="slider-tooltip" overlay={value} placement="top" key={index}>
      <Slider.Handle value={value} {...restProps} />
    </SliderTooltip>
  )
}

interface PrettoSliderProps extends SliderProps {
  value: number
}

export const PrettoSlider: React.FC<PrettoSliderProps> = ({ value, ...props }) => {
  return <Slider {...props} handle={handle} prefixCls="slider" value={value} />
}
