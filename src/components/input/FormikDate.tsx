import React, { FC } from "react"
import Cleave from "cleave.js/react"
import { useField } from "formik"

import style from "./styles/input.module.scss"

interface FormikDateProps {
  name: string
  className?: string
}

const FormikDate: FC<FormikDateProps> = ({ className, name }) => {
  const [field, meta] = useField(name)
  const errorText = meta.error

  return (
    <div className={`${style['text_input']} ${className ?? ""}`}>
      <Cleave
        options={{
          date: true,
          datePattern: ["m", "Y"],
          delimiter: "/",
        }}
        {...field}
        placeholder="MM/YYYY"
        className={`${style['text_input__field']} ${
          field.value ? `${style['text_input__field--focus']}` : errorText ? `${style['text_input__field--error']}` : ""
        }`}
      />
      <small className="text-red">{errorText}</small>
    </div>
  )
}

export default FormikDate
