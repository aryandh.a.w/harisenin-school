import { forwardRef, InputHTMLAttributes } from "react"

const phoneInput = (props: InputHTMLAttributes<any>, ref: any) => {
  return <input ref={ref} id="phone-number-field" {...props} />
}

export default forwardRef(phoneInput)
