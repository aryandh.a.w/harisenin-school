import React, { FC, useEffect, useState } from "react"
import { NamedProps } from "react-select"
import CreatableSelect from "react-select/creatable"
import { SelectOption } from "../../../constants/interfaces/ui"

import style from "./styles/input.module.scss"

export interface CreatableSelectInputProps extends NamedProps {
  error?: string
  defaultOption?: SelectOption
}

const CreatableSelectInput: FC<CreatableSelectInputProps> = ({
  onChange,
  options,
  placeholder,
  value,
  error,
  defaultOption,
  className,
}) => {
  const [selectValue, setSelectValue] = useState<SelectOption>()

  useEffect(() => {
    if (defaultOption) {
      setSelectValue(defaultOption)
    }
  }, [defaultOption])

  return (
    <div
      className={`${style["select_input"]} ${error ? `${style["select_input--error"]}` : ""} ${
        className ?? ""
      }`}
    >
      <CreatableSelect
        key={`my_unique_select_key__${JSON.stringify(selectValue)}`}
        options={options}
        placeholder={placeholder}
        onChange={onChange}
        value={value ?? selectValue}
        className="select-container"
        classNamePrefix="select-prefix"
      />
      <small>{error}</small>
    </div>
  )
}

export default CreatableSelectInput
