import React, { FC, TextareaHTMLAttributes, useEffect, useState } from "react"
import { useField } from "formik"

import style from "./styles/input.module.scss"

export interface FormikTextAreaProps extends TextareaHTMLAttributes<HTMLTextAreaElement> {
  errorText?: string
  maxCharacter?: number
  minCharacter?: number
  name: string
}

const FormikTextArea: FC<FormikTextAreaProps> = ({
  placeholder,
  rows,
  minCharacter,
  maxCharacter,
  name,
  className,
  ...props
}) => {
  const [field, meta] = useField(name)
  const errorText = meta.error
  const val = field.value as string

  const [infoText, setInfoText] = useState(`Minimal ${minCharacter} Karakter`)

  useEffect(() => {
    if (!minCharacter || val.length > maxCharacter) {
      setInfoText(`Maksimal ${maxCharacter} Karakter`)
    } else {
      setInfoText(`Minimal ${minCharacter} Karakter`)
    }
  }, [val])

  return (
    <div className={`${style["text_input"]} ${className ?? ""}`}>
      <textarea
        rows={rows}
        placeholder={placeholder}
        maxLength={maxCharacter ?? 300}
        {...field}
        {...props}
        className={`${style["text_input__field"]} ${
          field.value
            ? `${style["text_input__field--focus"]}`
            : errorText
            ? `${style["text_input__field--error"]}`
            : ""
        }`}
      />
      <div className={style["text_input__footer"]}>
        <small className={errorText ? "text-red" : ""}>
          {infoText}{" "}
          {maxCharacter ? ` (${val.length}/${maxCharacter})` : ` (${val.length} karakter)`}
        </small>
        {/*<small className="">{errorText}</small>*/}
      </div>
    </div>
  )
}

export default FormikTextArea
