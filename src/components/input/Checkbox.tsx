import React from "react"
import clsx from "clsx"

export interface CheckboxProps extends React.InputHTMLAttributes<HTMLInputElement> {
  className?: string
  id: string
}

export const Checkbox: React.FC<CheckboxProps> = ({ className, id, name, children, ...props }) => {
  return (
    <div className={clsx("checkbox", className)}>
      <input type="checkbox" id={id} name={name} {...props} />
      <label htmlFor={id}>{children}</label>
    </div>
  )
}
