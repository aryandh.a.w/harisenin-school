import { textShortener } from "../../../lib/utils/method"
import {
  CheckoutAction,
  CheckoutActionType,
  CheckoutState,
  FormFileType,
} from "../../../constants/interfaces/checkout-state"
import { Dispatch } from "react"
import useScreenSize from "../../../lib/utils/hooks/useScreenSize"

import style from "./styles/input.module.scss"

export interface FilePickerProps {
  name: FormFileType
  className?: string
  dispatch: Dispatch<CheckoutAction>
  state: CheckoutState
}

export default function FilePicker({ name, className, dispatch, state }: FilePickerProps) {
  const { screenWidth } = useScreenSize()

  function handleUploadFile(name, e) {
    const file = e.target.files[0] as File

    let errorText
    let condition

    switch (true) {
      case !file:
        errorText = "Terjadi kesalahan"
        condition = "not uploaded"
        break
      case file.size > 5242880:
        errorText = "File maksimal 5MB"
        condition = "too big"
        break
      case file.type === "application/pdf":
      case file.type === "image/jpeg":
      case file.type === "image/png":
        condition = "success"
        break
      default:
        errorText = "Jenis file yang diperbolehkan: PDF, PNG, JPG, JPEG"
        condition = "Wrong type"
        break
    }

    if (condition !== "success") {
      dispatch({
        type: CheckoutActionType.FormErrorHandler,
        payload: { name, type: "file", error: errorText },
      })
    } else {
      dispatch({
        type: CheckoutActionType.FormFileHandler,
        payload: { name, file },
      })
    }
  }

  return (
    <div className={`${style["file_picker"]} ${className ?? ""}`}>
      <input
        type="file"
        hidden
        accept="application/pdf, image/*"
        id={`file_picker-${name}`}
        onChange={(event) => handleUploadFile(name, event)}
      />
      <label htmlFor={`file_picker-${name}`} className={style["file_picker__column"]}>
        <div className={style["file_picker__column__name"]}>
          {textShortener(state.form_file[name].name, screenWidth > 576 ? 45 : 25)}
        </div>
        <div className={style["file_picker__column__button"]}>Pilih File</div>
      </label>
      <div className={style.file_picker__error}>{state.form_file[name].error}</div>
    </div>
  )
}
