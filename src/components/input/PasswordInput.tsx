import React, { FC, InputHTMLAttributes, SyntheticEvent, useState } from "react"
import { useField } from "formik"
import { AiOutlineEye, AiOutlineEyeInvisible } from "react-icons/ai"

import style from "./styles/input.module.scss"

interface FocusEvent<T = Element> extends SyntheticEvent<T> {
  relatedTarget: EventTarget | null
  target: EventTarget & T
}

interface PasswordInputProps extends InputHTMLAttributes<HTMLInputElement> {
  showButtonId?: string
}

const PasswordInput: FC<PasswordInputProps> = ({ showButtonId, ...props }) => {
  // @ts-ignore
  const [field, meta] = useField(props.name)

  const [showPassword, setShowPassword] = useState(false)
  const [isFocused, setIsFocused] = useState(false)

  const errorText = meta.error

  const passwordToggle = () => {
    setShowPassword(!showPassword)
  }

  const handleBlur = async (e: FocusEvent<HTMLInputElement>) => {
    field.onBlur(e)
    setIsFocused(false)
  }

  function classNameSwitcher(): string {
    switch (true) {
      case isFocused:
      case field.value && !errorText:
        return `${style["text_input__password"]} ${style["text_input__password--focus"]}`
      case errorText !== undefined:
        return `${style["text_input__password"]} ${style["text_input__password--error"]}`
      default:
        return `${style["text_input__password"]}`
    }
  }

  return (
    <div className={`${style["text_input"]} ${props.className ?? ""}`}>
      <div className={classNameSwitcher()}>
        <input
          {...field}
          type={showPassword ? "text" : "password"}
          placeholder={props.placeholder}
          onFocus={() => setIsFocused(true)}
          onBlur={handleBlur}
        />
        <div
          className={style["text_input__password__eye-icon"]}
          onClick={passwordToggle}
          id={showButtonId ?? "show-password"}
        >
          {showPassword ? <AiOutlineEyeInvisible /> : <AiOutlineEye />}
        </div>
      </div>
      <small className="text-red">{errorText}</small>
    </div>
  )
}

export default PasswordInput
