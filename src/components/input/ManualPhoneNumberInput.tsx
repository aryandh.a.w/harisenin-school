import React, { FC } from "react"
import PhoneInput, { Props, DefaultInputComponentProps } from "react-phone-number-input"

import style from "./styles/input.module.scss"
import forwardInput from "./forwardInput"

export interface ManualPhoneNumberInput extends Props<DefaultInputComponentProps> {
  error?: any
}

const ManualPhoneNumberInput: FC<ManualPhoneNumberInput> = ({
  value,
  placeholder,
  error,
  onChange,
  className,
}) => {
  return (
    <div className={`${style["phone_input"]} ${className ?? ""}`}>
      <PhoneInput
        inputComponent={forwardInput}
        placeholder={placeholder}
        value={value}
        onChange={onChange}
        defaultCountry="ID"
        className={`${style["text_field"]} ${error ? `${style["phone_input--error"]}` : ""}`}
      />
      <small className="text-red">{error}</small>
    </div>
  )
}

export default ManualPhoneNumberInput
