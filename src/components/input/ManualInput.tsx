import React, { FC, InputHTMLAttributes } from "react"

import style from "./styles/input.module.scss"

export interface ManualInputProps extends InputHTMLAttributes<any> {
  errorText?: string
}

const ManualInput: FC<ManualInputProps> = ({ errorText, value, className, onChange, ...props }) => {
  return (
    <div className={`${style["text_input"]} ${className ?? ""}`}>
      <input
        {...props}
        onChange={onChange}
        value={value}
        className={`${style["text_input__field"]} ${
          errorText
            ? `${style["text_input__field--error"]}`
            : value
            ? `${style["text_input__field--focus"]}`
            : ""
        } `}
      />
      <small className="text-red">{errorText}</small>
    </div>
  )
}

export default ManualInput
