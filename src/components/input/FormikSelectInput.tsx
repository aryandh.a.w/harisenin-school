import React, { FC, useEffect, useState } from "react"
import Select, { NamedProps } from "react-select"
import { useField } from "formik"
import { SelectOption } from "../../../constants/interfaces/ui"

import style from "./styles/input.module.scss"

export interface FormikSelectInputProps extends NamedProps {
  defaultOption?: SelectOption | null
  setField: (field: string, value: any, shouldValidate?: boolean) => void
  valueType?: "label" | "province"
  name: string
}

const FormikSelectInput: FC<FormikSelectInputProps> = ({
  setField,
  valueType,
  name,
  options,
  placeholder,
  value,
  defaultOption,
  className,
}) => {
  const [selectValue, setSelectValue] = useState<SelectOption>()

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [field, meta] = useField(name)
  const errorText = meta.error

  useEffect(() => {
    if (defaultOption) {
      setSelectValue(defaultOption)
    }
  }, [defaultOption])

  const handleChange = (value: SelectOption | null) => {
    if (!value) {
      return
    }

    switch (true) {
      case valueType === "label":
        setField(`${name}`, value.label)
        setSelectValue(value)
        break
      case valueType === "province":
        setField("province_state", value.label)
        setField("city", "")
        setSelectValue(value)
        break
      case value.value === "":
        // @ts-ignore
        setSelectValue(null)
        break
      default:
        setField(`${name}`, value.value)
        setSelectValue(value)
        break
    }
  }

  return (
    <div className={`${style['select_input']} ${errorText ? `${style['select_input--error']}` : ""} ${className ?? ""}`}>
      <Select
        key={`my_unique_select_key__${JSON.stringify(selectValue)}`}
        options={options}
        placeholder={placeholder}
        onChange={(value1) => handleChange(value1)}
        value={value ?? selectValue}
        className="select-container"
        classNamePrefix="select-prefix"
      />
      <small>{errorText}</small>
    </div>
  )
}

export default FormikSelectInput
