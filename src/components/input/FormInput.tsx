import clsx from "clsx"
import { useField } from "formik"
import { FC, InputHTMLAttributes } from "react"

export interface FormInputProps {
  placeholder?: string
  isNumber?: boolean
  name: string
  className?: string
  inputClassName?: string
  padding?: string
}

const FormInput: FC<FormInputProps & InputHTMLAttributes<HTMLInputElement>> = ({
  placeholder,
  name,
  isNumber,
  className,
  inputClassName,
  padding,
  ...props
}) => {
  const [field, meta] = useField(name)
  const errorText = meta.error

  return (
    <div className={clsx("flex flex-col mb-5 relative", className ?? "")}>
      <input
        type={isNumber ? "number" : "text"}
        placeholder={placeholder}
        {...field}
        {...props}
        className={clsx(
          inputClassName,
          padding ?? "py-2.5 px-3",
          "rounded w-full border-2 mb-1 border-grey-ec lg:text-base",
          "focus:outline-none focus:shadow-outline focus:border-green active:shadow-outline active:border-green text-sm",
          field.value && !errorText && "border-2 !border-green",
          errorText && "!border-red border"
        )}
      />
      <small className="text-red">{errorText}</small>
    </div>
  )
}

export default FormInput
