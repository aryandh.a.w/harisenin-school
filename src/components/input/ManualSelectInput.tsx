import { FC, useEffect, useState } from "react"
import Select, { Props } from "react-select"
import { SelectOption } from "@interfaces/ui"

export interface ManualSelectInputProps extends Props {
  error?: string
  defaultOption?: SelectOption
}

const ManualSelectInput: FC<ManualSelectInputProps> = ({
  onChange,
  options,
  placeholder,
  value,
  error,
  defaultOption,
  className,
}) => {
  const [selectValue, setSelectValue] = useState<SelectOption>()

  useEffect(() => {
    if (defaultOption) {
      setSelectValue(defaultOption)
    }
  }, [defaultOption])

  return (
    <div className={`relative text-sm ${error ? `border border-red` : ""} ${className ?? ""}`}>
      <Select
        key={`my_unique_select_key__${JSON.stringify(selectValue)}`}
        options={options}
        placeholder={placeholder}
        onChange={onChange}
        value={value ?? selectValue}
        className="select-container"
        classNamePrefix="select-prefix"
      />
      <small className="absolute text-red -bottom-5">{error}</small>
    </div>
  )
}

export default ManualSelectInput
