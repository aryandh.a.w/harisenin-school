import clsx from "clsx"
import { useField } from "formik"
import { FC, InputHTMLAttributes } from "react"

export interface MaterialFormProps {
  placeholder?: string
  label: string
  isNumber?: boolean
  name: string
  className?: string
  inputClassName?: string
  padding?: string
}

const MaterialFormInput: FC<MaterialFormProps & InputHTMLAttributes<HTMLInputElement>> = ({
  placeholder,
  name,
  isNumber,
  className,
  label,
  ...props
}) => {
  const [field, meta] = useField(name)
  const errorText = meta.error

  return (
    <div className={clsx("material_input relative", className ?? "")}>
      <input
        type={isNumber ? "number" : "text"}
        placeholder={placeholder}
        {...field}
        {...props}
        id={name}
        className={clsx(field.value && !errorText && "active", errorText && "error")}
      />
      <label className="label" htmlFor={name}>
        <div className="text">{label}</div>
      </label>
      <small className="text-red absolute left-0 bottom-[-18px]">{errorText}</small>
    </div>
  )
}

export default MaterialFormInput
