import React, { FC, TextareaHTMLAttributes, useEffect, useState } from "react"
import { useField } from "formik"
import clsx from "clsx"

export interface MaterialTextAreaInputProps extends TextareaHTMLAttributes<HTMLTextAreaElement> {
  errorText?: string
  maxCharacter?: number
  minCharacter?: number
  name: string
  label: string
}

const MaterialTextAreaInput: FC<MaterialTextAreaInputProps> = ({
  placeholder,
  rows,
  minCharacter,
  maxCharacter,
  name,
  className,
  label,
  ...props
}) => {
  const [field, meta] = useField(name)
  const errorText = meta.error
  const val = field.value as string

  const [infoText, setInfoText] = useState(`Minimal ${minCharacter} Karakter`)

  useEffect(() => {
    if (!minCharacter || val.length > maxCharacter) {
      setInfoText(`Maksimal ${maxCharacter} Karakter`)
    } else {
      setInfoText(`Minimal ${minCharacter} Karakter`)
    }
  }, [val])

  return (
    <div className={clsx("material_textarea relative", className ?? "")}>
      <textarea
        rows={rows}
        placeholder={placeholder}
        maxLength={maxCharacter ?? 300}
        {...field}
        {...props}
        className={clsx(
          field.value && !errorText && "active",
          errorText && "error",
          field.value && "filled"
        )}
        id={name}
      />
      <label className="label" htmlFor={name}>
        <div className="text">{label}</div>
      </label>
      <small
        className={clsx(
          "absolute left-0 -bottom-4",
          field.value && !errorText && "text-green",
          errorText && "text-red"
        )}
      >
        {infoText} {maxCharacter ? ` (${val.length}/${maxCharacter})` : ` (${val.length} karakter)`}
      </small>
    </div>
  )
}

export default MaterialTextAreaInput
