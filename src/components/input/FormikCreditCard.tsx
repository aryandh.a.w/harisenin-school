import React, { FC, InputHTMLAttributes, SyntheticEvent, useState } from "react"
import { useField } from "formik"
import Cleave from "cleave.js/react"
import { creditCardTypeV2 } from "../../../lib/utils/method/checker"
import { BsCreditCard } from "react-icons/bs"

import style from "./styles/input.module.scss"

interface FocusEvent<T = Element> extends SyntheticEvent<T> {
  relatedTarget: EventTarget | null
  target: EventTarget & T
}

const FormikCreditCardInput: FC<InputHTMLAttributes<HTMLInputElement>> = (props) => {
  const [field, meta] = useField(props.name)

  const [isFocused, setIsFocused] = useState(false)

  const errorText = meta.error

  const handleBlur = async (e: FocusEvent<HTMLInputElement>) => {
    field.onBlur(e)
    setIsFocused(false)
  }

  function classNameSwitcher(): string {
    switch (true) {
      case isFocused:
      case field.value && !errorText:
        return `${style['text_input__cc']} ${style['text_input__cc--focus']}`
      case errorText !== undefined:
        return `${style['text_input__cc']} ${style['text_input__cc--error']}`
      default:
        return `${style['text_input__cc']}`
    }
  }

  function creditImage() {
    if (field.value && creditCardTypeV2(field.value)) {
      return <img src={creditCardTypeV2(field.value)} alt="" />
    } else {
      return <BsCreditCard />
    }
  }

  return (
    <div className={`${style['text_input']} ${props.className ?? ""}`}>
      <div className={classNameSwitcher()}>
        <Cleave
          {...field}
          options={{ creditCard: true }}
          placeholder={props.placeholder}
          onFocus={() => setIsFocused(true)}
          onBlur={handleBlur}
        />

        <div className={style['text_input__cc__card']}>{creditImage()}</div>
      </div>
      <small className="text-red">{errorText}</small>
    </div>
  )
}

export default FormikCreditCardInput
