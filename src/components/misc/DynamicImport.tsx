import dynamic from "next/dynamic"

export const DynamicImage = dynamic(async () => {
  const misc = await import("./Image")
  return misc.BasicImage
})
