import React, { FC, HTMLAttributes } from "react"

import style from "./styles/card.module.scss"
import clsx from "clsx"

interface BasicCardProps extends HTMLAttributes<HTMLDivElement> {
  isBordered?: boolean
  small?: boolean
}

const BasicCard: FC<BasicCardProps> = ({ className, children, onClick, isBordered, small }) => {
  return (
    <div
      className={clsx(
        style.basic_card,
        small && style["basic_card--small"],
        isBordered && style["basic_card--bordered"],
        className
      )}
      onClick={onClick}
    >
      {children}
    </div>
  )
}

export default BasicCard
