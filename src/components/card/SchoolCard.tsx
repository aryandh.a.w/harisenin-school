import { FlexBox } from "@components/wrapper"
import { HARISENIN_PLACEHOLDER } from "@constants/pictures"
import { School, SchoolType } from "@interfaces/school"
import { priceFormatter } from "@lib/functions"
import clsx from "clsx"
import Image from "next/image"
import { BiTimeFive } from "react-icons/bi"
import { FaRegCalendar } from "react-icons/fa"

interface ProgramCardProps {
  item: School
  index: number
  screenWidth: number
  className?: string
}

export const ProgramCard = ({ item, index, screenWidth }: ProgramCardProps) => {
  function idMaker(component: "card" | "title") {
    if (!item.status) {
      return `item-${index}`
    }

    switch (item.school_type) {
      case "BOOTCAMP":
        return `program-${component}-bootcamp-${index}`
      case "PRO_CLASS":
        return `program-${component}-proclass-${index}`
      case "CODING_CAMP":
        return `program-${component}-coding-camp-${index}`

      default:
        return ""
    }
  }

  const PriceComponent = () => {
    if (!item.status) {
      return (
        <>
          <div className={clsx("sm:text-lg font-semibold text-red uppercase", "text-sm")}>
            Coming Soon
          </div>
        </>
      )
    }

    return (
      <>
        {item.status > 0 && (
          <FlexBox justify="between" className="gap-1">
            {item.school_discount_percentage ? (
              <>
                <FlexBox align="center">
                  {/* Discount percentage */}
                  <div className="w-fit bg-red text-xs text-white py-0.5 px-1 rounded mr-1.5 h-fit">
                    {item.school_discount_percentage}%
                  </div>

                  {/* Strikethrough price */}
                  <div className={clsx("text-[#b3b3b3] line-through sm-only:text-xs")}>
                    {priceFormatter(item.school_original_price)}
                  </div>
                </FlexBox>

                {/* Net Price */}
                <div className={clsx("sm:text-lg  text-green font-bold", "text-sm")}>
                  {priceFormatter(item.school_start_price)}
                </div>
              </>
            ) : (
              <div className={clsx("sm:text-lg font-semibold text-green", "text-sm")}>
                {priceFormatter(item.school_start_price)}
              </div>
            )}
          </FlexBox>
        )}
      </>
    )
  }

  const InfoBox = () => {
    return (
      <FlexBox
        className={clsx(
          "relative border border-[#dfe0e1] sm:flex-col bg-white",
          !item.status && "cursor-not-allowed",
          "sm-only:items-start",
          "!hover:text-black"
        )}
        id={idMaker("card")}
      >
        {/* Overlay when program is not active */}
        {!item.status && (
          <FlexBox
            justify="center"
            align="center"
            className={clsx(
              "absolute text-center top-0 w-full sm:h-[17.2vw] text-white rounded z-[5] bg-[rgba(0,0,0,0.4)] font-semibold sm:text-lg",
              "h-[100px] text-xs"
            )}
          >
            Coming Soon
          </FlexBox>
        )}

        <Image
          width={600}
          height={600}
          src={item.product_asset?.product_image ?? HARISENIN_PLACEHOLDER}
          alt={item.product_name}
          className={clsx("w-full object-cover ", "sm:w-full sm:h-[17.2vw] w-[100px] h-[100px]")}
        />

        <div className="sm:p-3 p-2.5">
          <div>
            {screenWidth > 576 && (
              <div className="uppercase text-blue sm:mb-3 tracking-widest text-sm">
                {item.school_type === "BOOTCAMP" ? "bootcamp" : "proclass"}
              </div>
            )}

            <h5
              id={idMaker("title")}
              className={clsx("font-semibold sm:mb-3", "sm:text-lg text-sm mb-2")}
            >
              {item.product_name}
            </h5>

            {screenWidth < 576 && (
              <FlexBox align="center" className="mb-2 gap-3 flex-wrap text-sm">
                <span className="flex items-center gap-2">
                  <BiTimeFive className="text-lg" />
                  {item.school_duration} Bulan
                </span>
                <span className="flex items-center gap-2">
                  <FaRegCalendar className="text-lg" />
                  Mei 2023
                </span>
              </FlexBox>
            )}

            {screenWidth > 576 && (
              <FlexBox align="center" className="mb-2 gap-3 text-sm ">
                <div className="w-1/2 ">
                  <div className="capitalize text-[#b3b3b3]">Durasi</div>
                  <div className="font-semibold ">{item.school_duration} Minggu</div>
                </div>

                <div className="w-1/2 ">
                  <div className="capitalize  text-[#b3b3b3]">jadwal</div>
                  <div className="font-semibold ">Mei 2023</div>
                </div>
              </FlexBox>
            )}
          </div>

          <PriceComponent />

          {/* {Boolean(item.school_batches) && (
            <div className="mt-3 text-sm font-medium">
              <div className="uppercase text-[#b3b3b3] mb-1">batch tersedia</div>
              <FlexBox wrap="wrap" className="gap-2">
                {item.school_batches.map((b, i) => (
                  <FlexBox
                    key={i}
                    align="center"
                    justify="center"
                    className={clsx(
                      "rounded-[2px] px-2 py-1 ",
                      b.is_sold_out
                        ? "text-[#B3B3B3] bg-[rgba(58,53,65,0.12)]"
                        : "bg-[#E4F2FE] text-[#3362E5]"
                    )}
                  >
                    Batch {b.batch_number}
                  </FlexBox>

                  {/* Net Price */}
                  <div className={clsx("sm:text-lg  text-green", "text-sm")}>
                    {priceFormatter(item.school_start_price)}
                  </div>
                </>
              ) : (
                <div className={clsx("sm:text-lg font-semibold text-green", "text-sm")}>
                  {priceFormatter(item.school_start_price)}
                </div>
              )}
            </FlexBox>
          ) : (
            <div className={clsx("sm:text-lg font-medium text-red uppercase", "text-sm")}>
              Coming Soon
            </div>
          )} */}
        </div>
      </FlexBox>
    )
  }

  function linkTypeGenerator(type: SchoolType) {
    switch (type) {
      case "BOOTCAMP":
        return "bootcamp"
      case "PRO_CLASS":
        return "proclass"
      case "CODING_CAMP":
        return "coding-camp"

      default:
        return ""
    }
  }

  if (!item.status) {
    return <InfoBox />
  } else {
    return (
      <a
        href={`/school/${linkTypeGenerator(item.school_type)}/${item.product_slug}`}
        id={`program-card-${linkTypeGenerator(item.school_type)}-${index}`}
      >
        <InfoBox />
      </a>
    )
  }
}
