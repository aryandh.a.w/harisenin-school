import { LoginModal } from "@components/modal"
import { container, FlexBox } from "@components/wrapper"
import { HARISENIN_LOGO_COLORED, HARISENIN_LOGO_WHITE } from "@constants/pictures"
import { useUserData } from "@lib/context/ComponentContext"
import { useScreenSize } from "@lib/hooks/useScreenSize"
import AuthServices from "@services/auth.services"
import clsx from "clsx"
import Image from "next/image"
import { useEffect, useState } from "react"
import { FaCheckCircle } from "react-icons/fa"
import { HeaderComponent } from "./components"
import HeaderMobile from "./components/HeaderMobile"
import { PopupProvider, usePopup } from "./components/popover/PopupContext"
import Link from "next/link"
import { BasicLink } from "@components/typography"

interface HeaderProps {
  transparent?: boolean
  isNotSticky?: boolean
  bgOnly?: boolean
  hasShadow?: boolean
}

export function Header({ transparent, isNotSticky, bgOnly, hasShadow }: HeaderProps) {
  const [loginModal, setLoginModal] = useState(false)
  const [unverifiedStatus, setUnverifiedStatus] = useState(false)
  const [isToastOpen, setIsToastOpen] = useState(false)

  const { deviceType, screenHeight } = useScreenSize()
  const { tokenData, isLogin } = useUserData()
  const auth = new AuthServices()

  useEffect(() => {
    if (!tokenData?.user_active && isLogin) {
      setUnverifiedStatus(true)
    }

    if (isLogin) {
      window.dataLayer?.push({
        user_id: tokenData?.id,
        user_email: tokenData?.user_email,
        user_phone: tokenData?.user_phone_number,
      })
    }
  }, [tokenData])

  const handleOpenLogin = () => {
    setLoginModal(true)
  }
  const handleCloseLogin = () => {
    setLoginModal(false)
  }

  const sendEmail = async () => {
    try {
      await auth.resendVerificationEmail(tokenData?.user_email as string)
      setIsToastOpen(true)

      setTimeout(() => setIsToastOpen(false), 3000)
    } catch (e) {
      if (process.env.NODE_ENV === "development") {
        console.log(e)
      }
    }
  }

  const HeaderBar = () => {
    const { openPopup } = usePopup()

    if (deviceType === "web") {
      return (
        <div
          className={clsx(
            "top-0 z-[1000]",
            openPopup && "bg-white text-black-48",
            isNotSticky ? "fixed" : "sticky",
            transparent && !bgOnly && "text-white",
            transparent ? `w-full fixed transition ease-in delay-150 ` : "bg-white w-full",
            hasShadow && "shadow-[0px_2px_10px_rgba(58,53,65,0.1)]"
          )}
        >
          {/* Header Component */}
          <div className="static">
            <FlexBox align="center" justify="between" className={clsx(container, "py-4")}>
              {/* Logo */}
              <FlexBox align="center">
                <BasicLink href="/" isExternal>
                  <Image
                    src={
                      transparent && !bgOnly && !openPopup
                        ? HARISENIN_LOGO_WHITE
                        : HARISENIN_LOGO_COLORED
                    }
                    alt="harisenin.com logo"
                    className="h-[25px] w-auto"
                    width={100}
                    height={25}
                  />
                </BasicLink>
              </FlexBox>

              <HeaderComponent
                loginModalHandler={handleOpenLogin}
                isLogin={isLogin}
                tokenData={tokenData}
                transparent={transparent}
              />
            </FlexBox>
          </div>

          {unverifiedStatus && (
            <div className="bg-yellow px-7.5 py-2.5 text-center p-2.5">
              Akun kamu belum diverifikasi. Silahkan cek email untuk memverifikasi akun atau klik{" "}
              <u className="cursor-pointer" onClick={sendEmail}>
                disini
              </u>{" "}
              bila email verifikasi belum masuk di inbox kamu. <br />
              Bila kamu telah melakukan verifikasi dan notifikasi belum hilang, silahkan coba login
              ulang
            </div>
          )}

          <FlexBox
            justify="center"
            align="center"
            className={clsx(
              "z-[1000] fixed top-[50px] left-1/2 w-[300px] ml-[-150px] bg-green text-white rounded text-center p-3 lg:text-base text-sm gap-2",
              isToastOpen ? "visible show_toast" : "invisible"
            )}
          >
            <FaCheckCircle /> Email Verifikasi berhasil dikirim
          </FlexBox>
        </div>
      )
    } else {
      return (
        <HeaderMobile
          loginModalHandler={handleOpenLogin}
          isLogin={isLogin}
          tokenData={tokenData}
          screenHeight={screenHeight}
        />
      )
    }
  }

  return (
    <PopupProvider>
      <HeaderBar />
      <LoginModal show={loginModal} onHide={handleCloseLogin} />
    </PopupProvider>
  )
}
