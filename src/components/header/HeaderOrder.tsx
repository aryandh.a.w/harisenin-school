import React, { HtmlHTMLAttributes } from "react"
import clsx from "clsx"
import { FlexBox } from "../wrapper"

interface HeaderOrderProps extends HtmlHTMLAttributes<HTMLDivElement> {
  step: 1 | 2 | 3
}

export const HeaderOrder = ({ step, className }: HeaderOrderProps) => {
  const orderSteps = ["Detail Orderan", "Konfirmasi Pembayaran", "Pembayaran Berhasil"]

  return (
    <div className={clsx(className)}>
      <div className="py-3 max-w-[700px] mx-auto sm-only:hidden">
        <FlexBox className="gap-4">
          {orderSteps.map((item, index) => (
            <li key={index} className="flex items-center gap-2">
              <FlexBox
                align="center"
                justify="center"
                className={clsx(
                  "h-6 w-6 rounded-full text-sm text-white",
                  index < step ? "bg-green" : "bg-grey-97 "
                )}
              >
                {index + 1}
              </FlexBox>
              <div className={clsx(index < step ? "text-green" : "text-grey-97 ")}>{item}</div>
            </li>
          ))}
        </FlexBox>
      </div>
    </div>
  )
}
