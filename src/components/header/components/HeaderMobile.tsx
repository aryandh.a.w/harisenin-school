import React, { useEffect, useRef, useState } from "react"
import { AiOutlineMenu } from "react-icons/ai"
import { FaCheckCircle, FaTimes } from "react-icons/fa"
import clsx from "clsx"
import { useRouter } from "next/router"

import { TokenData } from "@interfaces/user"
import AuthServices from "@services/auth.services"
import { container, Container, FlexBox } from "@components/wrapper"
import { HARISENIN_LOGO_COLORED } from "@constants/pictures"
import { Avatar } from "@components/misc"
import { LinkButton, RegularButton } from "@components/buttons"
import { CategoryAccordion } from "./mobile/CategoryAccordion"
import { BasicLink } from "@components/typography"
import Image from "next/image"

interface HeaderMobileProps {
  loginModalHandler: () => void
  isLogin?: boolean | number
  tokenData?: TokenData
  screenHeight: number
}

const LinkHead = ({ children, link }: { link: string; children: any }) => {
  return (
    <a
      href={link}
      className="py-2.5 bg-primary-grey rounded font-semibold text-sm w-full px-4 flex mb-3"
    >
      {children}
    </a>
  )
}
const HeaderMobile: React.FC<HeaderMobileProps> = ({ loginModalHandler, isLogin, tokenData }) => {
  const [menuOpen, setMenuOpen] = useState(false)
  // const [menuHeight, setMenuHeight] = useState("0px")

  // const [searchOpen, setSearchOpen] = useState(false)
  // const [searchHeight, setSearchHeight] = useState("0px")

  const [unverifiedStatus, setUnverifiedStatus] = useState(false)
  const [isToastOpen, setIsToastOpen] = useState(false)

  const auth = new AuthServices()
  const router = useRouter()
  const contentSpace = useRef<HTMLDivElement>(null)
  // const searchSpace = useRef<HTMLDivElement>(null)

  const links = [
    {
      label: "Profil",
      slug: "profil",
    },
    {
      label: "Daftar Pembelian",
      slug: "order",
    },
    {
      label: "Referral Program",
      slug: "referral",
    },
  ]

  useEffect(() => {
    if (!tokenData?.user_active && isLogin) {
      setUnverifiedStatus(true)
    }
  }, [tokenData])

  const sendEmail = async () => {
    try {
      await auth.resendVerificationEmail(tokenData?.user_email as string)
      setIsToastOpen(true)

      setTimeout(() => setIsToastOpen(false), 3000)
    } catch (e) {
      if (process.env.NODE_ENV === "development") {
        console.log(e)
      }
    }
  }

  function handleMenu() {
    const content = contentSpace.current

    if (!contentSpace || !content) {
      return
    }

    setMenuOpen(!menuOpen)

    // if (menuOpen) {
    //   setMenuHeight("0px")
    // } else {
    //   const currentHeight = screenHeight - 57
    //   const contentHeight = content.scrollHeight
    //
    //   setMenuHeight(currentHeight < contentHeight ? `${currentHeight}px` : `${contentHeight}px`)
    // }
  }

  // function handleSearchButton() {
  //   const content = searchSpace.current
  //
  //   if (!searchSpace || !content) {
  //     return
  //   }
  //
  //   setSearchOpen(!searchOpen)
  //
  //   if (searchOpen) {
  //     setSearchHeight("0px")
  //   } else {
  //     const currentHeight = screenHeight - 57
  //     const contentHeight = content.scrollHeight
  //
  //     setSearchHeight(currentHeight < contentHeight ? `${currentHeight}px` : `${contentHeight}px`)
  //   }
  // }

  async function logout() {
    await auth.logout()
    router.reload()
  }

  return (
    <>
      <Container className="flex justify-center py-4 bg-primary-grey sticky top-0 z-[10] sm:hidden">
        <a href="/">
          <Image
            src={HARISENIN_LOGO_COLORED}
            alt="harisenin.com logo"
            className="h-3.5"
            width={200}
            height={14}
          />
        </a>

        <FlexBox className="absolute right-[15px]">
          {/*<button onClick={handleSearchButton}>*/}
          {/*  {searchOpen ? <FaTimes className="text-xl" /> : <FaSearch className="text-md" />}*/}
          {/*</button>*/}
          {/*<div className="w-4" />*/}
          <button onClick={handleMenu}>
            {menuOpen ? <FaTimes className="text-xl " /> : <AiOutlineMenu className="text-xl " />}
          </button>
        </FlexBox>
      </Container>

      {unverifiedStatus ? (
        <div className="bg-yellow  text-center p-2.5 text-sm">
          Akun kamu belum diverifikasi. Silahkan cek email untuk memverifikasi akun atau klik{" "}
          <u onClick={sendEmail} className="cursor-pointer">
            disini
          </u>
          . Bila kamu telah melakukan verifikasi dan notifikasi belum hilang, silahkan coba login
          ulang
        </div>
      ) : null}

      <FlexBox
        justify="center"
        align="center"
        className={clsx(
          "z-[1000] fixed top-[50px] left-1/2 w-[300px] ml-[-150px] bg-green text-white rounded text-center p-3 text-sm",
          isToastOpen ? "visible show_toast" : "invisible"
        )}
      >
        <FaCheckCircle className="mr-2" /> Email Verifikasi berhasil dikirim
      </FlexBox>

      {/* Search Box*/}
      {/*<div*/}
      {/*  ref={searchSpace}*/}
      {/*  style={{ maxHeight: searchOpen ? `${searchHeight}` : "0px" }}*/}
      {/*  className={clsx(*/}
      {/*    container,*/}
      {/*    "shadow-lg overflow-hidden transition-max-menuHeight duration-700 ease-in-out sticky top-[55px] z-[10] bg-white"*/}
      {/*  )}*/}
      {/*>*/}
      {/*  <div className="h-[18px]" />*/}
      {/*  <FlexBox className="bg-primary-grey rounded p-2.5" align="center">*/}
      {/*    <FaSearch className="text-green mr-3 text-sm" />*/}
      {/*    <input*/}
      {/*      type="text"*/}
      {/*      placeholder="Cari"*/}
      {/*      className="focus:outline-none focus:shadow-outline text-sm bg-transparent"*/}
      {/*    />*/}
      {/*  </FlexBox>*/}
      {/*  <div className="h-[18px]" />*/}
      {/*</div>*/}

      {/* Menu Box */}
      <div
        ref={contentSpace}
        className={clsx(
          container,
          "shadow-lg overflow-auto transition-max-height duration-700 ease-in-out sticky top-[55px] z-[10] bg-white"
        )}
        style={{
          height: menuOpen ? "initial" : "0px",
          maxHeight: menuOpen ? "calc(100vh - 54px)" : 0,
        }}
      >
        <div className="h-3" />
        {isLogin ? (
          <FlexBox justify="center" direction="col" align="center">
            <Avatar
              alt="avatar"
              src={tokenData?.user_picture ?? ""}
              size={1000}
              className="wh-20 mb-4 mt-9"
            />
            <div className="text-sm font-semibold">{tokenData?.fullname}</div>
          </FlexBox>
        ) : (
          <FlexBox justify="between">
            <LinkButton href={"/register"} variant="transparent" className="w-[47%]" isExternal>
              DAFTAR
            </LinkButton>
            <RegularButton className="w-[47%]" onClick={loginModalHandler}>
              MASUK
            </RegularButton>
          </FlexBox>
        )}
        <div className="pt-8">
          <CategoryAccordion />
          {isLogin ? (
            <FlexBox direction="col">
              {links.map((value, index) => (
                <LinkHead key={index} link={`/dashboard/${value.slug}`}>
                  {value.label}
                </LinkHead>
              ))}

              <BasicLink
                href="/learning/dashboard/progress"
                className="py-2.5 bg-primary-grey rounded font-semibold text-sm w-full px-4 flex mb-3"
              >
                Kelas Video Course
              </BasicLink>

              <button
                className="py-2.5 bg-primary-grey rounded font-semibold text-sm text-center text-red"
                onClick={logout}
              >
                Logout
              </button>
            </FlexBox>
          ) : null}

          <div className="h-5" />
        </div>
      </div>
    </>
  )
}

export default HeaderMobile
