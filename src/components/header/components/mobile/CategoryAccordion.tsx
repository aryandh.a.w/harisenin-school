import React from "react"
import { FlexBox } from "@components/wrapper"
import { useSchools } from "@hooks/data/useSchoolData"
import { Spinner } from "@components/misc"
import { useCourse } from "@lib/hooks/data/useCourse"
import { Disclosure, Transition } from "@headlessui/react"
import { AiOutlineDown} from "react-icons/ai"

export const ChildItem = ({ children }) => {
  return (
    <Transition
      enter="transition duration-100 ease-out"
      enterFrom="transform scale-95 opacity-0"
      enterTo="transform scale-100 opacity-100"
      leave="transition duration-75 ease-out"
      leaveFrom="transform scale-100 opacity-100"
      leaveTo="transform scale-95 opacity-0"
    >
      {children}
    </Transition>
  )
}

export const CategoryAccordion = () => {
  const { data: courses, isLoading: isFetchingCourses } = useCourse()
  const { data: schools, isLoading: isFetchingSchools } = useSchools("BOOTCAMP")
  const { data: proClass, isLoading: isFetchingProClass } = useSchools("PRO_CLASS")

  const LoadingData = () => {
    return (
      <FlexBox justify="center" align="center" className="w-full h-[200px]">
        <Spinner />
      </FlexBox>
    )
  }

  return (
    <>
      <Disclosure>
        {({ open }) => (
          <div className="bg-primary-grey pl-4 pr-3 py-2 mb-3.5">
            <Disclosure.Button
              className={`flex justify-between items-center font-medium w-full text-left`}
            >
              Bootcamp
              <AiOutlineDown className={`${open ? "rotate-180 transform" : ""} md:hidden`} />
            </Disclosure.Button>
            <ChildItem>
              {isFetchingSchools ? (
                <LoadingData />
              ) : (
                schools.map((value, index) => (
                  <div className="my-2" key={index}>
                    <a
                      href={`/school/bootcamp/${value.product_slug}`}
                      className="font-semibold text-green text-sm mt-2.5"
                    >
                      {value.product_name}
                    </a>
                  </div>
                ))
              )}
            </ChildItem>
          </div>
        )}
      </Disclosure>

      <Disclosure>
        {({ open }) => (
          <div className="bg-primary-grey pl-4 pr-3 py-2 mb-3.5">
            <Disclosure.Button
              className={`flex justify-between items-center font-medium w-full text-left`}
            >
              ProClass
              <AiOutlineDown className={`${open ? "rotate-180 transform" : ""} md:hidden`} />
            </Disclosure.Button>
            <ChildItem>
              {isFetchingProClass ? (
                <LoadingData />
              ) : (
                proClass.map((value, index) => (
                  <div className="my-2" key={index}>
                    <a
                      href={`/school/proclass/${value.product_slug}`}
                      className="font-semibold text-green text-sm mt-2.5"
                    >
                      {value.product_name}
                    </a>
                  </div>
                ))
              )}
            </ChildItem>
          </div>
        )}
      </Disclosure>
      <Disclosure>
        {({ open }) => (
          <div className="bg-primary-grey pl-4 pr-3 py-2 mb-3.5">
            <Disclosure.Button
              className={`flex justify-between items-center font-medium w-full text-left`}
            >
              Video Course
              <AiOutlineDown className={`${open ? "rotate-180 transform" : ""} md:hidden`} />
            </Disclosure.Button>
            <ChildItem>
              {isFetchingCourses ? (
                <LoadingData />
              ) : (
                courses.map((value, index) => (
                  <div className="my-2" key={index}>
                    <a
                      href={`/learning/category/${value.category_slug}`}
                      className="font-semibold text-green text-sm mt-2.5"
                    >
                      {value.category_name}
                    </a>
                  </div>
                ))
              )}
            </ChildItem>
          </div>
        )}
      </Disclosure>

      <div className="bg-primary-grey px-4 py-2 mb-3.5">
        <a
          href="/risebinar"
          className={`flex justify-between items-center font-medium w-full text-left`}
        >
          Event & Workshop
        </a>
      </div>
    </>
  )
}
