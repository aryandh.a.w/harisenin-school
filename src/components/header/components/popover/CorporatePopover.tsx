import React from "react"

import HeaderPopup from "./HeaderPopup"
import { container } from "@components/wrapper"
import clsx from "clsx"
import { LinkHead } from "../HeaderComponent"

export function CorporatePopover() {
  const PopoverComponent = () => {
    return (
      <div className={clsx(container)}>
        <div className="mt-4 text-2xl font-medium text-black ">Corporate</div>
        <div className="flex gap-12 mt-4 text-black">
          <div className="w-2/5">
            Jadi perusahaan yang terdepan dengan peningkatan sesuai dengan kebutuhan dan tujuan anda
          </div>
          <div className="grid grid-cols-2 gap-4 w-1/2">
            <LinkHead link="/hire-graduates">Hire Graduates</LinkHead>
            <LinkHead link="/corporate-training">Corporate Training</LinkHead>
          </div>
        </div>
      </div>
    )
  }

  return <HeaderPopup panel={<PopoverComponent />} buttonLabel="Corporate" />
}
