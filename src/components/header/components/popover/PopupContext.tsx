import { createContext, useContext, useState } from "react"

interface PopupState {
  openPopup: boolean
  setOpenPopup: (v: boolean) => void
}

const PopupContext = createContext<Partial<PopupState>>({ openPopup: false })

export const PopupProvider = ({ children }: { children: any }) => {
  const [openPopup, setOpenPopup] = useState(false)

  return (
    <PopupContext.Provider value={{ openPopup, setOpenPopup }}>{children}</PopupContext.Provider>
  )
}

export const usePopup = (): Partial<PopupState> => useContext(PopupContext)
