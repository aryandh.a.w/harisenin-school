import React from "react"

import HeaderPopup from "./HeaderPopup"
import { container } from "@components/wrapper"
import clsx from "clsx"
import { LinkHead } from "../HeaderComponent"

export function AboutPopover({ setIsHover }: any) {
  const PopoverComponent = () => {
    return (
      <div className={clsx(container)}>
        <div className="mt-4 text-2xl font-medium text-black">About</div>
        <div className="flex gap-12 mt-4 text-black">
          <div className="w-2/5">
            Karena kesuksesan ialah hak setiap anak bangsa, kami berambisi untuk memberikan akses
            pendidikan yang berkualitas pada generasi muda Indonesia
          </div>
          <div className="grid grid-cols-2 gap-4 w-1/2 text-black">
            <LinkHead link="/tentang-kami">Tentang Kami</LinkHead>
            <LinkHead link="/alumni">Alumni</LinkHead>
          </div>
        </div>
      </div>
    )
  }

  return <HeaderPopup setIsHover={setIsHover} panel={<PopoverComponent />} buttonLabel="About" />
}
