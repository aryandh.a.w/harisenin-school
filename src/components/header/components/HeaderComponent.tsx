import { LinkButton, RegularButton } from "@components/buttons"
import { FlexBox } from "@components/wrapper"
import { TokenData } from "@interfaces/user"
import React from "react"
import { AboutPopover, AccountPopover, CorporatePopover, ServiceDropdown } from "./popover"

export interface HeaderComponentProps {
  loginModalHandler: () => void
  isLogin?: boolean | number
  tokenData?: TokenData
  transparent?: boolean
}

export const LinkHead = ({ children, link }: { link: string; children: any }) => {
  return (
    <a href={link} className="hover:underline underline-offset-8 hover:font-semibold text-black">
      {children}
    </a>
  )
}

export const HeaderComponent: React.FC<HeaderComponentProps> = ({
  loginModalHandler,
  isLogin,
  tokenData,
  transparent,
}) => {
  return (
    <nav className="flex items-center">
      <FlexBox align="center" justify="between" className="gap-8">
        <FlexBox align="center" className="gap-8">
          <ServiceDropdown />
          {/* <VideoPopover /> */}
          <CorporatePopover />
          <AboutPopover />
          {/* <a href="/prakerja" className="flex gap-2 font-medium">
            <Image
              src={`${HARISENIN_PUBLIC_LOGO}/logo_kartu-prakerja.png`}
              width={100}
              height={25}
              className="h-[25px] w-auto"
            />
          </a> */}
        </FlexBox>

        {isLogin ? (
          <AccountPopover userData={tokenData as TokenData} />
        ) : (
          <FlexBox justify="between" align="center" className="gap-3">
            <LinkButton
              href="/register"
              variant={transparent ? "white-green" : "transparent"}
              id="btn-regist"
              isExternal
              className="!rounded-none"
            >
              Daftar
            </LinkButton>
            <RegularButton onClick={loginModalHandler} id="btn-login" className="!rounded-none">
              Masuk
            </RegularButton>
          </FlexBox>
        )}
      </FlexBox>
    </nav>
  )
}
