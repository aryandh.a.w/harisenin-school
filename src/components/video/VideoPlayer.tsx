import clsx from "clsx"
import React, { useEffect, useRef, useState } from "react"
import ReactPlayer from "react-player/lazy"
import screenfull from "screenfull"

import { LoadingBox } from "@components/wrapper"
import { sleep } from "@lib/functions"
import Image from "next/image"
import { useVideoPlayer } from "./useVideoPlayer"
import { useVideoProgress } from "./useVideoProgress"
import { DurationJump, VideoActionType, VideoState } from "./video"
import VideoControl from "./VideoControl"
import { HARISENIN_PUBLIC_S3 } from "@constants/pictures"
interface VideoPlayerProps {
  url: string
  videoTitle: string
  className?: string
  customWidth?: string
  customHeight?: string
  onProgressPlayed?: (played: VideoState, totalDuration: number) => void
  onNext?: () => void
  hasNextButton?: boolean
  thumbnail?: string
  isPrakerja?: boolean
}

const VideoPlayer: React.FC<VideoPlayerProps> = ({
  url,
  videoTitle,
  className,
  onProgressPlayed,
  hasNextButton,
  onNext,
  thumbnail = "",
  isPrakerja,
}) => {
  /* Video Handler */
  const [controlVisible, setControlVisible] = useState<"hidden" | "visible">("visible")

  const controlsRef = useRef<HTMLElement>(null)
  const playerRef = useRef<ReactPlayer>(null)
  const playerContainerRef = useRef<HTMLDivElement>(null)

  const { videoState, videoDispatch } = useVideoPlayer()
  const { isPlaying, isMuted, volume, isFullScreen, playbackRate } = videoState
  const { videoDispatch: progressDispatch, videoState: progressState } = useVideoProgress()
  const { totalDuration } = progressState

  useEffect(() => {
    window.addEventListener("keydown", handleKeyDown)

    return () => {
      window.removeEventListener("keydown", handleKeyDown)
    }
  }, [videoState, isPrakerja])

  const handleKeyDown = async (e: KeyboardEvent) => {
    const keyboardKey = e.code
    const eventTarget = e.target

    //@ts-ignore
    if (eventTarget && eventTarget.type) {
      return
    }

    switch (keyboardKey) {
      case "Space":
        if (isPrakerja) {
          return
        }
        if (videoState.isPlaying === true) {
          videoDispatch({
            type: VideoActionType.HANDLE_PLAY,
            payload: false,
          })
          setControlVisible("visible")
        } else {
          videoDispatch({
            type: VideoActionType.HANDLE_PLAY,
            payload: true,
          })

          setControlVisible("hidden")
        }
        return e.preventDefault()

      case "KeyM":
        videoDispatch({ type: VideoActionType.HANDLE_MUTE, payload: !isMuted })
        return e.preventDefault()
      case "ArrowLeft":
        if (isPrakerja) {
          return
        }
        handleRewind("previous")()
        return e.preventDefault()
      case "ArrowRight":
        if (isPrakerja) {
          return
        }
        handleRewind("next")()
        return e.preventDefault()
      case "ArrowUp": {
        const newVolume = videoState.volume + 5
        if (newVolume > 100) {
          videoDispatch({ type: VideoActionType.HANDLE_VOLUME, payload: 100 })
        } else {
          videoDispatch({ type: VideoActionType.HANDLE_VOLUME, payload: newVolume })
        }
        return e.preventDefault()
      }
      case "ArrowDown": {
        const newVolume = videoState.volume - 5
        if (newVolume < 0) {
          videoDispatch({ type: VideoActionType.HANDLE_VOLUME, payload: 0 })
        } else {
          videoDispatch({ type: VideoActionType.HANDLE_VOLUME, payload: newVolume })
        }
        return e.preventDefault()
      }

      default:
        break
    }
  }

  const handleRewind = (type: DurationJump) => () => {
    if (!playerRef || !playerRef.current) {
      return
    }

    if (type === "next") {
      playerRef.current.seekTo(playerRef.current.getCurrentTime() + 10)
    } else {
      playerRef.current.seekTo(playerRef.current.getCurrentTime() - 10)
    }
  }

  const handleSeekChange = (val: number) => {
    if (!playerRef || !playerRef.current) {
      return
    }

    playerRef.current.seekTo(val / 100, "fraction")
  }

  const handleReplay = () => {
    if (!playerRef || !playerRef.current) {
      return
    }

    playerRef.current.seekTo(0)
    videoDispatch({ type: VideoActionType.HANDLE_PLAY, payload: true })
  }

  const handleBuffer = (val: boolean) => () => {
    videoDispatch({ type: VideoActionType.HANDLE_BUFFER, payload: val })
  }

  const toggleFullScreen = async () => {
    if (!playerContainerRef || !playerContainerRef.current) {
      return
    }

    if (screenfull && screenfull.isEnabled) {
      videoDispatch({ type: VideoActionType.HANDLE_FULL_SCREEN, payload: !isFullScreen })
      await screenfull.toggle(playerContainerRef.current)
    }
  }

  const toggleVisibility = (visibility: string) => () => {
    if (!controlsRef || !controlsRef.current) {
      return
    }

    if (!videoState.isPlaying || videoState.isEnded || videoState.openNext) {
      controlsRef.current.style.visibility = "visible"
    } else {
      controlsRef.current.style.visibility = visibility
    }
  }

  const mouseIn = async (e: React.MouseEvent) => {
    const target = e.target

    // @ts-ignore
    if (!videoState.isPlaying || target.id) {
      return setControlVisible("visible")
    }

    setControlVisible("visible")
    await sleep(4000)
    setControlVisible("hidden")
  }

  const mouseOut = () => {
    if (videoState.isPlaying) {
      setControlVisible("hidden")
    } else {
      setControlVisible("visible")
    }
  }

  return (
    <div
      className={clsx("relative mt-2.5 sm:mt-0", className)}
      ref={playerContainerRef}
      onMouseMove={mouseIn}
      onMouseLeave={mouseOut}
    >
      <ReactPlayer
        ref={playerRef}
        url={url}
        width="100%"
        height="100%"
        className="react-player"
        playing={isPlaying}
        volume={volume / 100}
        playbackRate={playbackRate}
        onBuffer={handleBuffer(true)}
        onBufferEnd={handleBuffer(false)}
        muted={isMuted}
        onProgress={(progress: VideoState) => {
          if (onProgressPlayed) {
            onProgressPlayed(progress, totalDuration)
          }

          progressDispatch({
            type: VideoActionType.HANDLE_DURATION,
            payload: {
              loaded: progress.loaded,
              percentagePlayed: progress.played,
              secondsPlayed: progress.playedSeconds,
            },
          })

          if (totalDuration - progress.playedSeconds <= 5) {
            setControlVisible("visible")
            videoDispatch({ type: VideoActionType.OPEN_NEXT, payload: true })
          } else {
            videoDispatch({ type: VideoActionType.OPEN_NEXT, payload: false })
          }

          if (progress.played === 1) {
            videoDispatch({ type: VideoActionType.HANDLE_END })
          }
        }}
        onEnded={() => {
          videoDispatch({ type: VideoActionType.HANDLE_PLAY, payload: false })
          setControlVisible("visible")
        }}
        onDuration={(duration) => {
          progressDispatch({
            type: VideoActionType.INIT_DURATION,
            payload: duration,
          })
        }}
        fallback={<LoadingBox />}
      />

      {thumbnail && progressState.percentagePlayed <= 0 && (
        <div className="absolute top-0 left-0 wh-full z-[1]">
          <Image
            src={thumbnail}
            height={720}
            width={1280}
            className="wh-full z-[99] object-cover"
            priority
            alt="thumbnail"
          />
        </div>
      )}

      {!thumbnail ? (
        <div className="absolute top-0 left-0 wh-full z-[99] bg-white">
          <Image
            src={`${HARISENIN_PUBLIC_S3}/placeholders/placeholder_video-loading-4.gif`}
            height={450}
            width={800}
            className="wh-full z-[99] object-cover"
            alt="loading"
            priority
          />
        </div>
      ) : (
        <VideoControl
          videoTitle={videoTitle}
          ref={controlsRef}
          state={videoState}
          progressState={progressState}
          dispatch={videoDispatch}
          progressDispatch={progressDispatch}
          onJump={handleRewind}
          onSeek={handleSeekChange}
          onFullScreen={toggleFullScreen}
          handleVisibility={toggleVisibility}
          handleReplay={handleReplay}
          hasNextButton={hasNextButton}
          onNext={onNext}
          controlVisible={controlVisible}
          isPrakerja={isPrakerja}
        />
      )}
    </div>
  )
}

export default VideoPlayer
