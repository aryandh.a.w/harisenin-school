import { Reducer, useReducer } from "react"
import { VideoAction, VideoActionType, VideoInitialState } from "./video"

const initialState: VideoInitialState = {
  isPlaying: false,
  isBuffering: false,
  isMuted: false,
  playbackRate: 1,
  volume: 100,
  isFullScreen: false,
  isEnded: false,
  openNext: false,
}

const reducer: Reducer<VideoInitialState, VideoAction> = (state, action) => {
  const { type, payload } = action

  switch (type) {
    case VideoActionType.HANDLE_PLAYBACK_RATE:
    case VideoActionType.HANDLE_VOLUME: {
      const numberPayload = payload as number

      return {
        ...state,
        ...(type === VideoActionType.HANDLE_PLAYBACK_RATE && {
          playbackRate: numberPayload,
        }),
        ...(type === VideoActionType.HANDLE_VOLUME && {
          volume: numberPayload,
          isMuted: numberPayload < 1,
        }),
      }
    }

    case VideoActionType.HANDLE_PLAY:
    case VideoActionType.HANDLE_FULL_SCREEN:
    case VideoActionType.HANDLE_MUTE:
    case VideoActionType.OPEN_NEXT:
    case VideoActionType.HANDLE_BUFFER: {
      const booleanPayload = payload as boolean
      return {
        ...state,
        ...(type === VideoActionType.HANDLE_PLAY && { isPlaying: booleanPayload }),
        ...(type === VideoActionType.HANDLE_MUTE && { isMuted: booleanPayload }),
        ...(type === VideoActionType.HANDLE_BUFFER && { isBuffering: booleanPayload }),
        ...(type === VideoActionType.HANDLE_FULL_SCREEN && { isFullScreen: booleanPayload }),
        ...(type === VideoActionType.OPEN_NEXT && { openNext: booleanPayload }),
      }
    }

    case VideoActionType.HANDLE_END: {
      return {
        ...state,
        isPlaying: false,
        isEnded: true,
      }
    }

    default: {
      return state
    }
  }
}

export const useVideoPlayer = () => {
  const [videoState, videoDispatch] = useReducer(reducer, initialState)

  return { videoState, videoDispatch }
}
