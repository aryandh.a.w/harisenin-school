import React, { Dispatch, forwardRef } from "react"
import { IoPlay, IoReload } from "react-icons/io5"
import { IoIosPause, IoMdVolumeHigh, IoMdVolumeLow, IoMdVolumeOff } from "react-icons/io"
import { MdOutlineFullscreen, MdOutlineFullscreenExit } from "react-icons/md"
import {
  DurationJump,
  VideoAction,
  VideoActionType,
  VideoInitialState,
  VideoProgressAction,
  VideoProgressInitialState,
} from "./video"
import clsx from "clsx"
import { flex_center } from "@constants/styles"
import { timeConverter } from "@lib/functions"
import { Spinner } from "@components/misc"
import { FlexBox } from "@components/wrapper"
import { PrettoSlider } from "@components/slider"

interface VideoControlProps {
  state: VideoInitialState
  progressState: VideoProgressInitialState
  dispatch: Dispatch<VideoAction>
  progressDispatch: Dispatch<VideoProgressAction>
  onJump: (type: DurationJump) => () => void
  onSeek: (val: number) => void
  onFullScreen: () => void
  handleReplay: () => void
  onNext?: () => void
  videoTitle: string
  handleVisibility: (type: string) => () => void
  hasNextButton?: boolean
  controlVisible?: "hidden" | "visible"
  isPrakerja?: boolean
}

const VideoControl = forwardRef<any, VideoControlProps>(
  (
    {
      state,
      progressState,
      dispatch,
      // progressDispatch,
      onSeek,
      onFullScreen,
      handleVisibility,
      hasNextButton,
      handleReplay,
      onNext,
      controlVisible,
    },
    ref
  ) => {
    const {
      isPlaying,
      isMuted,
      volume,
      isFullScreen,
      isBuffering,
      openNext,
      isEnded,
      // playbackRate,
    } = state
    const { percentageLoaded, percentagePlayed, secondsPlayed, totalDuration } = progressState

    // const handleChangePlayRate = (val: number) => () => {
    //   dispatch({ type: VideoActionType.HANDLE_PLAYBACK_RATE, payload: val })
    // }

    // const PlaybackRate = () => {
    //   return (
    //     <div className="relative">
    //       <Menu as="div" className="relative">
    //         <Menu.Button className="text-sm mr-4">{playbackRate}x</Menu.Button>
    //         <Transition
    //           as={Fragment}
    //           enter="transition ease-out duration-100"
    //           enterFrom="transform opacity-0 scale-95"
    //           enterTo="transform opacity-100 scale-100"
    //           leave="transition ease-in duration-75"
    //           leaveFrom="transform opacity-100 scale-100"
    //           leaveTo="transform opacity-0 scale-95"
    //         >
    //           <Menu.Items className="absolute rounded focus:outline-none bg-[#373737] -top-[155px] right-0">
    //             {[1.5, 1.25, 1, 0.25, 0.5].map((value, index) => (
    //               <Menu.Item key={index}>
    //                 {({ active }) => (
    //                   <button
    //                     className={clsx(
    //                       "px-3 py-1.5 text-sm cursor-pointer",
    //                       active && "text-green"
    //                     )}
    //                     onClick={handleChangePlayRate(value)}
    //                   >
    //                     {value}x
    //                   </button>
    //                 )}
    //               </Menu.Item>
    //             ))}
    //           </Menu.Items>
    //         </Transition>
    //       </Menu>
    //     </div>
    //   )
    // }

    const handlePlay = (val: boolean) => () => {
      dispatch({ type: VideoActionType.HANDLE_PLAY, payload: val })
      if (!val) {
        handleVisibility("visible")()
      }
    }

    const CenterButton = () => {
      switch (true) {
        case isBuffering: {
          return <Spinner size="lg" />
        }

        case isPlaying: {
          return (
            <>
              {/* {!isPrakerja && (
                <button className={flex_center} onClick={onJump("previous")}>
                  <MdOutlineReplay10 className="text-3xl" />
                </button>
              )} */}
              <button className={clsx(flex_center, "sm:mx-4 mx-6")} onClick={handlePlay(false)}>
                <IoIosPause className="text-3xl" />
              </button>
              {/* {!isPrakerja && (
                <button className={flex_center} onClick={onJump("next")}>
                  <MdOutlineForward10 className="text-3xl" />
                </button>
              )} */}
            </>
          )
        }

        case isEnded: {
          return (
            <button
              className={clsx("rounded-full bg-white w-[48px] h-[48px]", flex_center)}
              onClick={handleReplay}
            >
              <IoReload className="sm:text-3xl" />
            </button>
          )
        }

        default: {
          return (
            <button
              className={clsx("rounded-full bg-white w-[48px] h-[48px]", flex_center)}
              onClick={handlePlay(true)}
            >
              <IoPlay className="sm:text-3xl" />
            </button>
          )
        }
      }
    }

    return (
      <div
        className="absolute top-0 left-0 wh-full bg-[rgba(0,0,0,0.3)] rounded-xl flex-col flex justify-between z-[5] transition-all"
        ref={ref}
        style={{ visibility: controlVisible }}
        id="video-ctr"
      >
        <FlexBox direction="col" justify="between" className="sm:p-5 p-4">
          <FlexBox align="center" justify="between">
            {/* <h5 className="font-semibold sm:text-lg text-base">{videoTitle}</h5> */}
          </FlexBox>
        </FlexBox>

        <FlexBox align="center" justify="center" className="sm:p-4 w-fit mx-auto">
          <FlexBox
            align="center"
            justify="center"
            style={{
              borderRadius: "50%",
              width: "64px",
              height: "64px",
              background: "rgba(217, 217, 217, 0.9)",
              backgroundBlendMode: "soft-light",
              backdropFilter: "blur(3.55036px)",
            }}
          >
            <CenterButton />
          </FlexBox>
        </FlexBox>

        <div className="relative">
          {openNext && hasNextButton && (
            <button
              className={clsx(
                "absolute text-white right-0 sm:-top-[60px] sm:px-5 sm:py-2 border-y-2 border-white border-l-2 font-semibold sm:text-base",
                "text-xs -top-[44px] py-2 px-3"
              )}
              onClick={() => {
                if (hasNextButton && onNext) {
                  onNext()
                }
              }}
            >
              Selanjutnya {">>"}
            </button>
          )}

          <div className="w-full px-3 mb-3 relative" />

          <div className="w-full px-3 h-3.5 relative">
            <PrettoSlider
              value={percentagePlayed}
              min={0}
              max={100}
              onChange={onSeek}
              className="absolute"
              trackStyle={{
                height: "8px",
              }}
              railStyle={{
                height: "8px",
              }}
              handleStyle={{
                marginTop: "-4px",
              }}
            />

            <div
              className={clsx("bg-black-48 h-2 top-[5px] absolute")}
              style={{ width: `calc(${percentageLoaded}% - 1.5rem)` }}
            />
          </div>

          <FlexBox
            justify="between"
            className={clsx("sm:px-5 sm:pt-2.5 sm:pb-5 text-white", "px-4 pb-4")}
          >
            <FlexBox align="center">
              <button className="mr-2 text-lg" onClick={handlePlay(!isPlaying)}>
                {isPlaying ? <IoIosPause /> : <IoPlay />}
              </button>
              <button
                className="text-lg mr-2.5"
                onClick={() => dispatch({ type: VideoActionType.HANDLE_MUTE, payload: !isMuted })}
              >
                {isMuted ? (
                  <IoMdVolumeOff />
                ) : volume < 0.5 ? (
                  <IoMdVolumeLow />
                ) : (
                  <IoMdVolumeHigh />
                )}
              </button>
              <div className="sm:w-[4vw] w-[12vw] mr-3">
                <PrettoSlider
                  value={volume}
                  min={0}
                  max={100}
                  className="small-slider"
                  onChange={(value) => {
                    dispatch({ type: VideoActionType.HANDLE_VOLUME, payload: value })
                  }}
                />
              </div>
              <div className="text-sm">
                {timeConverter(secondsPlayed)} / {timeConverter(totalDuration)}
              </div>
            </FlexBox>

            <FlexBox className="relative">
              {/* {!isPrakerja && <PlaybackRate />} */}
              <button className="text-lg" onClick={onFullScreen}>
                {isFullScreen ? <MdOutlineFullscreenExit /> : <MdOutlineFullscreen />}
              </button>
            </FlexBox>
          </FlexBox>
        </div>
      </div>
    )
  }
)

VideoControl.displayName = "VideoControl"

export default VideoControl
