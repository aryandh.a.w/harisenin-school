export interface VideoInitialState {
  isPlaying: boolean
  isMuted: boolean
  isBuffering: boolean
  playbackRate: number
  volume: number
  isFullScreen: boolean
  openNext: boolean
  isEnded: boolean
}
export interface VideoProgressInitialState {
  totalDuration: number
  secondsPlayed: number
  percentagePlayed: number
  percentageLoaded: number
}

export interface VideoState {
  played: number
  playedSeconds: number
  loaded: number
  loadedSeconds: number
}

export enum VideoActionType {
  INIT_DURATION = "init_duration",
  HANDLE_PLAY = "handle_play",
  HANDLE_MUTE = "handle_mute",
  HANDLE_DURATION = "handle_duration",
  HANDLE_SEEK_DURATION = "handle_seek_duration",
  HANDLE_VOLUME = "handle_volume",
  HANDLE_BUFFER = "handle_buffer",
  HANDLE_FULL_SCREEN = "handle_full_screen",
  HANDLE_PLAYBACK_RATE = "handle_playback_rate",
  OPEN_NEXT = "open_next",
  HANDLE_END = "handle_end",
  PLAY_AGAIN = "play_again",
}

export type VideoAction =
  | {
      type:
        | VideoActionType.HANDLE_PLAY
        | VideoActionType.HANDLE_MUTE
        | VideoActionType.HANDLE_BUFFER
        | VideoActionType.HANDLE_FULL_SCREEN
        | VideoActionType.OPEN_NEXT
      payload: boolean
    }
  | {
      type: VideoActionType.HANDLE_VOLUME | VideoActionType.HANDLE_PLAYBACK_RATE
      payload: number
    }
  | { type: VideoActionType.HANDLE_END | VideoActionType.PLAY_AGAIN; payload?: any }

export type VideoProgressAction =
  | {
      type: VideoActionType.HANDLE_DURATION
      payload: PercentagePayload
    }
  | {
      type: VideoActionType.INIT_DURATION | VideoActionType.HANDLE_SEEK_DURATION
      payload: number
    }

export interface PercentagePayload {
  percentagePlayed: number
  loaded: number
  secondsPlayed: number
}

export type DurationJump = "previous" | "next"
