import { Reducer, useReducer } from "react"
import {
  PercentagePayload,
  VideoActionType,
  VideoProgressAction,
  VideoProgressInitialState,
} from "./video"

const initialState: VideoProgressInitialState = {
  totalDuration: 0,
  percentageLoaded: 0,
  percentagePlayed: 0,
  secondsPlayed: 0,
}

const reducer: Reducer<VideoProgressInitialState, VideoProgressAction> = (state, action) => {
  const { type, payload } = action

  switch (type) {
    case VideoActionType.INIT_DURATION:
    case VideoActionType.HANDLE_SEEK_DURATION: {
      const numberPayload = payload as number

      return {
        ...state,
        ...(type === VideoActionType.INIT_DURATION && { totalDuration: numberPayload }),
        ...(type === VideoActionType.HANDLE_SEEK_DURATION && {
          percentagePlayed: numberPayload,
        }),
      }
    }

    case VideoActionType.HANDLE_DURATION: {
      const percentagePayload = payload as PercentagePayload

      return {
        ...state,
        percentagePlayed: percentagePayload.percentagePlayed * 100,
        percentageLoaded: percentagePayload.loaded * 100,
        secondsPlayed: percentagePayload.secondsPlayed,
        isEnded: percentagePayload.percentagePlayed >= 1,
      }
    }

    default: {
      return state
    }
  }
}

export const useVideoProgress = () => {
  const [videoState, videoDispatch] = useReducer(reducer, initialState)

  return { videoState, videoDispatch }
}
