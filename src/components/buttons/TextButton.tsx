import React, { ButtonHTMLAttributes, FC } from "react"

import style from "./styles/button.module.scss"

const TextButton: FC<ButtonHTMLAttributes<HTMLButtonElement>> = ({
  children,
  className,
  ...props
}) => {
  return (
    <button {...props} className={`${style['text_button']} ${className || ""}`}>
      {children}
    </button>
  )
}

export default TextButton
