import { ButtonHTMLAttributes, FC, useState } from "react"
import { Spinner } from "react-bootstrap"
import GoogleLogin from "react-google-login"
import { useRouter } from "next/router"
import AuthServices from "../../../lib/services/auth.services"

import style from "./styles/button.module.scss"

export interface GoogleLoginProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  redirect?: string
  handleError: (a: string) => void
}

export const GoogleLoginButton: FC<GoogleLoginProps> = ({ redirect, handleError, ...props }) => {
  const [isSubmitting, setIsSubmitting] = useState(false)

  const router = useRouter()
  const auth = new AuthServices()

  const handleGoogleLogin = async (response) => {
    const userProfile = response.profileObj
    const userFullname = userProfile.name
    const userEmail = userProfile.email
    const googleId = userProfile.googleId

    setIsSubmitting(true)

    try {
      handleError("")

      const res = await auth.googleLogin({
        email: userEmail,
        fullName: userFullname,
        googleId,
      })

      if (res.isSuccess) {
        const result = res.getValue()

        if (result.errorCode === "SUCCESS") {
          if (!redirect) {
            router.reload()
          } else {
            window.location.assign(redirect)
          }
        } else {
          window.location.assign("/biodata")
        }
      } else {
        handleError("Terjadi kesalahan. Silahkan coba lagi")
        setIsSubmitting(false)
      }
    } catch (error: any) {
      setIsSubmitting(false)

      if (error.message) {
        handleError(error.message)
      } else {
<<<<<<< HEAD
        return
=======
        auth.bugsnagNotifier(error)
>>>>>>> 6cd4eae (setup fix)
      }
    }
  }

  const handleGoogleError = async (res) => {
    console.log(res)
  }

  return (
    <>
      <GoogleLogin
        clientId={process.env.GOOGLE_CLIENT_ID || process.env.HARISENIN_GOOGLE_CLIENT_ID || ""}
        render={(renderProps) => (
          <button
            {...props}
            disabled={isSubmitting}
<<<<<<< HEAD
            className={clsx(
            disabled={isSubmitting}
            className={clsx(
              flex_center,
              "relative px-4 py-[22px] h-[50px] text-grey-69 w-full border border-grey-69 rounded"
            )}
=======
            className={style["btn_google"]}
>>>>>>> 6cd4eae (setup fix)
            onClick={renderProps.onClick}
          >
            <>
              {isSubmitting ? (
<<<<<<< HEAD
                <Spinner />
                <Spinner />
              ) : (
                <>
                  <Image
                    src="https://pngimg.com/uploads/google/google_PNG19635.png"
                    alt="google"
                    className="h-7 absolute sm:left-[10%] left-2.5"
                    width={28}
                    height={28}
                  />
                  <img
                    src="https://pngimg.com/uploads/google/google_PNG19635.png"
                    alt="google"
                    className="h-7 absolute sm:left-[10%] left-2.5"
                  />
=======
                <Spinner animation="border" variant="secondary" />
              ) : (
                <>
                  <img src="https://pngimg.com/uploads/google/google_PNG19635.png" alt="google" />
>>>>>>> 6cd4eae (setup fix)
                  <span>Lanjutkan dengan Google</span>
                </>
              )}
            </>
          </button>
        )}
        buttonText="Login"
        onSuccess={handleGoogleLogin}
        onFailure={handleGoogleError}
        cookiePolicy={"single_host_origin"}
      />
    </>
  )
}
