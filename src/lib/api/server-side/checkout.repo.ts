<<<<<<< HEAD:src/lib/api/server-side/checkout.repo.ts
import GotMethod from "./GotMethod"

export default class CheckoutRepo extends GotMethod {
  async getRawInvoice(token: any, orderNumber: any) {
    return await this.getProtectedData(token, {
      url: `/invoice/${orderNumber}`,
    })
  }

  async getRawCheckoutDetail(token: string, orderNumber: string) {
    return this.getProtectedData(token, { version: "v3", url: `/checkout/order/${orderNumber}` })
  }

  async getRawPayments(orderNumber: any, token: any) {
    return await this.getProtectedData(token, {
      url: `/get_payment/${orderNumber}`,
    })
  }
}
=======
import GotMethod from "./GotMethod"

export default class CheckoutRepo extends GotMethod {
  async getRawInvoice(orderNumber: any, token: any) {
    return await this.getProtectedData(token, {
      url: `/invoice/${orderNumber}`,
    })
  }

  async getRawPayments(orderNumber: any, token: any) {
    return await this.getProtectedData(token, {
      url: `/get_payment/${orderNumber}`,
    })
  }
}
>>>>>>> 94ccec2 (setup fix):src/lib/apis/checkout.repo.ts
