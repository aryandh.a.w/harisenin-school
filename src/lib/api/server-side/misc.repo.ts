import GotMethod from "../server-side/GotMethod"

export default class MiscRepo extends GotMethod {
  async getMetaTag(page: string) {
    return this._getData(`/meta-tag?type=${page}`)
  }
}
