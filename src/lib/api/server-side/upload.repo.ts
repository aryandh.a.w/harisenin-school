<<<<<<< HEAD:src/lib/api/server-side/upload.repo.ts
import nookies from "nookies"
import jwt from "jsonwebtoken"
import { JWT_SECRET } from "@constants/const"

function tokenChecker() {
  const cookies = nookies.get({})

  const token = cookies["HSTOKENID"]

  if (!token) {
    return null
  }

  jwt.verify(token, JWT_SECRET, (err) => {
    if (err) {
      console.log(err)
      return null
    }
  })

  return token
}

export default class UploadRepo {
  token: string
  url: string
  headers: any

  constructor() {
    this.token = tokenChecker()
    this.url = process.env.API_ENDPOINT || process.env.HARISENIN_API_ENDPOINT || ""
    this.headers = {
      headers: {
        Authorization: "Bearer " + this.token,
      },
    }
  }

  async uploadFile({
    method = "post",
    file,
    url,
    version = "v2",
  }: {
    method: "post" | "put"
    file: FormData
    url: string
    version: "v2" | "v3"
  }) {
    try {
      const res = await fetch(`${this.url}/${version}${url}`, {
        method: method,
        body: file,
        headers: {
          Authorization: "Bearer " + this.token,
        },
      })

      if (res.ok) {
        const json = await res.json()
        return {
          response: res,
          data: json,
          ok: res.ok,
        }
      } else {
        if (res) {
          const json = await res.json()
          return {
            data: json ?? {},
            ok: false,
            statusCode: res.status,
          }
        } else {
          return {
            ok: false,
            statusCode: 500,
            data: {},
          }
        }
      }
    } catch (e) {
      return {
        data: [],
        ok: false,
      }
    }
  }
}
=======
import nookies from "nookies"
import jwt from "jsonwebtoken"
import { JWT_SECRET } from "../../constants/const"

function tokenChecker() {
  const cookies = nookies.get({})

  const token = cookies["HSTOKENID"]

  if (!token) {
    return null
  }

  jwt.verify(token, JWT_SECRET, (err) => {
    if (err) {
      console.log(err)
      return null
    }
  })

  return token
}

export default class UploadRepo {
  token: string
  url: string
  headers: any

  constructor() {
    this.token = tokenChecker()
    this.url = process.env.API_ENDPOINT || process.env.HARISENIN_API_ENDPOINT || ""
    this.headers = {
      headers: {
        Authorization: "Bearer " + this.token,
      },
    }
  }

  async uploadFile({
    method = "post",
    file,
    url,
    version = "v2",
  }: {
    method: "post" | "put"
    file: FormData
    url: string
    version: "v2" | "v3"
  }) {
    try {
      const res = await fetch(`${this.url}/${version}${url}`, {
        method: method,
        body: file,
        headers: {
          Authorization: "Bearer " + this.token,
        },
      })

      if (res.ok) {
        const json = await res.json()
        return {
          response: res,
          data: json,
          ok: res.ok,
        }
      } else {
        if (res) {
          const json = await res.json()
          return {
            data: json ?? {},
            ok: false,
            statusCode: res.status,
          }
        } else {
          return {
            ok: false,
            statusCode: 500,
            data: {},
          }
        }
      }
    } catch (e) {
      return {
        data: [],
        ok: false,
      }
    }
  }
}
>>>>>>> 94ccec2 (setup fix):src/lib/apis/upload.repo.ts
