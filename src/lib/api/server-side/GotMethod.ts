<<<<<<< HEAD:src/lib/api/server-side/GotMethod.ts
import got, { Got, Method } from "got"
import jwt from "jsonwebtoken"

import { JWT_SECRET } from "@constants/const"
import { NextApiRequest, NextApiResponse } from "next"
import { destroyCookie, parseCookies } from "nookies"
// import Bugsnag from "../../functions/bugsnag"

export default class GotMethod {
  errorMessage: string
  instance: Got

  constructor() {
    this.errorMessage = "Internal server error"
    this.instance = got.extend({
      prefixUrl: process.env.API_ENDPOINT || process.env.HARISENIN_API_ENDPOINT,
    })
  }

  /**
   * Checks if token is valid or not
   */
  tokenChecker(req: NextApiRequest, res: NextApiResponse) {
    const parsedCookies = parseCookies({ req })
    const token = parsedCookies.HSTOKENID

    if (!token) {
      return null
    }

    jwt.verify(token, JWT_SECRET, (err) => {
      if (err) {
        destroyCookie({ res }, "HSTOKENID")
        return null
      }
    })

    return token
  }

  // bugsnagNotifier(error: any) {
  //   Bugsnag.notify(error)
  // }

  async _getData(url: string, query: any = {}) {
    try {
      return await this.instance.get("v2" + url, {
        searchParams: query,
      })
    } catch (error: any) {
      const errResponse = error.response
      console.log({ errResponse })
      throw {
        errorCode: errResponse.statusCode,
        data: errResponse.body,
      }
    }
  }

  async _crudMethod(url: string, body: any = {}, method: Method = "POST") {
    try {
      return await this.instance({
        method,
        url: "v2" + url,
        ...(body && { json: body }),
      })
    } catch (error: any) {
      const errResponse = error.response
      // console.log({ error })
      throw {
        errorCode: errResponse.statusCode,
        data: errResponse.body,
      }
    }
  }

  /**
   * GET method for protected API
   */
  async getProtectedData(
    token: string | null,
    options: {
      version?: "v2" | "v3"
      url: string
      query?: any
    }
  ) {
    try {
      const version = options.version
      return await this.instance.get((version ?? "v2") + options.url, {
        searchParams: options.query ?? {},
        headers: {
          Authorization: "Bearer " + token,
        },
      })
    } catch (error: any) {
      const errResponse = error.response
      throw {
        errorCode: errResponse.statusCode ?? "",
        data: errResponse.body ?? "",
      }
    }
  }

  /**
   * CRUD method for protected API
   */
  async protectedCrudMethod(
    token: string | null,
    options: {
      url: string
      body?: any
      version?: "v2" | "v3"
      query?: any
      method: Method
      bodyType?: "form-data" | "json"
    }
  ) {
    try {
      const version = options.version
      const type = options.bodyType ?? "json"
      // console.log({ res });
      return await this.instance((version ?? "v2") + options.url, {
        searchParams: options.query ?? {},
        headers: {
          Authorization: "Bearer " + token,
        },
        ...(type === "form-data" && { body: options.body }),
        ...(type === "json" && { json: options?.body ?? {} }),
        method: options.method,
      })
    } catch (error: any) {
      const errResponse = error.response
      throw {
        errorCode: errResponse.statusCode ?? "",
        data: errResponse.body ?? "",
      }
    }
  }
}
=======
import got, { Got, Method } from "got"
import jwt from "jsonwebtoken"

import { JWT_SECRET } from "../../constants/const"
import { NextApiRequest, NextApiResponse } from "next"
import { destroyCookie, parseCookies } from "nookies"
import Bugsnag from "../utils/method/bugsnag"

export default class GotMethod {
  errorMessage: string
  instance: Got

  constructor() {
    this.errorMessage = "Internal server error"
    this.instance = got.extend({
      prefixUrl: process.env.API_ENDPOINT || process.env.HARISENIN_API_ENDPOINT,
    })
  }

  /**
   * Checks if token is valid or not
   */
  tokenChecker(req: NextApiRequest, res: NextApiResponse) {
    const parsedCookies = parseCookies({ req })
    const token = parsedCookies.HSTOKENID

    if (!token) {
      return null
    }

    jwt.verify(token, JWT_SECRET, (err) => {
      if (err) {
        destroyCookie({ res }, "HSTOKENID")
        return null
      }
    })

    return token
  }

  bugsnagNotifier(error: any) {
    Bugsnag.notify(error)
  }

  /**
   * Get method for public API
   */
  async getPublicData(url: string, version: "v2" | "v3" = "v2") {
    try {
      return await this.instance.get(version + url)
    } catch (error: any) {
      const errResponse = error.response
      throw {
        errorCode: errResponse.statusCode ?? "",
        data: errResponse.body ?? "",
      }
    }
  }

  /**
   * CRUD method for public API
   */
  async CrudPublicMethod(
    url: string,
    body: any = {},
    options: {
      method: Method
      version: "v2" | "v3"
    } = {
      method: "POST",
      version: "v2",
    }
  ) {
    try {
      const version = options.version
      return await this.instance({
        method: options.method,
        url: version + url,
        json: body,
      })
    } catch (error: any) {
      const errResponse = error.response
      throw {
        errorCode: errResponse.statusCode ?? "",
        data: errResponse.body ?? "",
      }
    }
  }

  /**
   * GET method for public API which have pagination structure data
   */
  async getPaginationData(
    url: string,
    query: any = {},
    options: { version: "v2" | "v3" } = { version: "v2" }
  ) {
    try {
      const version = options.version
      return await this.instance.get(version + url, {
        searchParams: query,
      })
    } catch (error: any) {
      const errResponse = error.response
      throw {
        errorCode: errResponse.statusCode ?? "",
        data: errResponse.body ?? "",
      }
    }
  }

  /**
   * GET method for protected API
   */
  async getProtectedData(
    token: string | null,
    options: {
      version?: "v2" | "v3"
      url: string
      query?: any
    }
  ) {
    try {
      const version = options.version
      return await this.instance.get((version ?? "v2") + options.url, {
        searchParams: options.query ?? {},
        headers: {
          Authorization: "Bearer " + token,
        },
      })
    } catch (error: any) {
      const errResponse = error.response
      throw {
        errorCode: errResponse.statusCode ?? "",
        data: errResponse.body ?? "",
      }
    }
  }

  /**
   * CRUD method for protected API
   */
  async protectedCrudMethod(
    token: string | null,
    options: {
      url: string
      body?: any
      version?: "v2" | "v3"
      query?: any
      method: Method
      bodyType?: "form-data" | "json"
    }
  ) {
    try {
      const version = options.version
      const type = options.bodyType ?? "json"
      // console.log({ res });
      return await this.instance((version ?? "v2") + options.url, {
        searchParams: options.query ?? {},
        headers: {
          Authorization: "Bearer " + token,
        },
        ...(type === "form-data" && { body: options.body }),
        ...(type === "json" && { json: options?.body ?? {} }),
        method: options.method,
      })
    } catch (error: any) {
      const errResponse = error.response
      throw {
        errorCode: errResponse.statusCode ?? "",
        data: errResponse.body ?? "",
      }
    }
  }
}
>>>>>>> 94ccec2 (setup fix):src/lib/apis/GotMethod.ts
