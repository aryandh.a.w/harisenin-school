import { SchoolType } from "@interfaces/school"
import GotMethod from "./GotMethod"

export default class SchoolRepo extends GotMethod {
  async schoolList(type: SchoolType) {
    return this._getData(`/schools`, {
      type: type.toLocaleLowerCase(),
    })
  }

  async schoolDetail(slug: string, type: string) {
    return this._getData(`/product/detail/${slug}`, { type })
  }
}
