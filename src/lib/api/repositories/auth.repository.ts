<<<<<<< HEAD:src/lib/api/repositories/auth.repository.ts
import KyMethod from "./KyMethod"

export default class AuthRepo extends KyMethod {
  protected async rawLogin({
    user_email,
    user_password,
  }: {
    user_email: string
    user_password: string
  }) {
    return this._crudMethod("/auth/login", { user_email, user_password })
  }

  protected async rawGoogleLogin({
    googleId,
    userFullname,
    email,
  }: {
    googleId: string
    userFullname: string
    email: string
  }) {
    return this._crudMethod("/auth/google", {
      googleId,
      userFullname,
      email,
    })
  }

  protected async rawShowUserLogin() {
    return this._getProtectedData("/auth")
  }

  protected async rawResendVerification(email: string) {
    return this._protectedCrudMethod("/user/resend-verification", {
      method: "post",
      body: {
        user_email: email,
      },
    })
  }

  protected async rawLogout() {
    return this._protectedCrudMethod("/auth/logout")
  }
}
=======
import KyMethod from "./KyMethod"

export default class AuthRepo extends KyMethod {
  protected async rawLogin({
    user_email,
    user_password,
  }: {
    user_email: string
    user_password: string
  }) {
    return this._crudPublicMethod("/auth/login", { user_email, user_password })
  }

  protected async rawGoogleLogin({
    googleId,
    userFullname,
    email,
  }: {
    googleId: string
    userFullname: string
    email: string
  }) {
    return this._crudPublicMethod("/auth/google", {
      googleId,
      userFullname,
      email,
    })
  }

  protected async rawShowUserLogin() {
    return this._getProtectedData("/auth")
  }

  protected async rawResendVerification(email: string) {
    return this._protectedCrudMethod("/user/resend-verification", {
      method: "post",
      body: {
        user_email: email,
      },
    })
  }

  protected async rawLogout() {
    return this._protectedCrudMethod("/auth/logout")
  }
}
>>>>>>> 94ccec2 (setup fix):src/lib/repositories/auth.repository.ts
