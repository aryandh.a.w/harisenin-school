<<<<<<< HEAD:src/lib/api/repositories/school.repository.ts
import { FileRequestForm, SchoolListQuery, SchoolListQueryV2, SchoolType } from "@interfaces/school"
import KyMethod from "./KyMethod"

interface SchoolLinkBody {
  alias?: string
  type: "direct"
  password?: null
  active: true
  expires_at?: Date
  activates_at?: Date
  utm?: string
  domain_id?: null
  title?: string
  description?: string
  pixels?: any[]
  groups?: any[]
  rules?: any[]
  long_url: string
}

export default class SchoolRepository extends KyMethod {
  protected async getRawSchoolList({ page = 1, limit = 12, except, search }: SchoolListQuery) {
    return this._getData(`/product/list/school`, {
      page,
      limit,
      ...(search && { search }),
      ...(except && { except }),
    })
  }

  protected async getRawSchoolListV2(
    type: SchoolType,
    { page = 1, limit = 12, search }: SchoolListQueryV2
  ) {
    return this._getData(`/schools`, {
      type: type ? type.toLocaleLowerCase() : "school",
      page,
      limit,
      ...(search && { search }),
    })
  }

  protected async getRawTestimonials() {
    return this._getData(`/testimonies`, { type: "school" })
  }

  protected async getRawFaqs() {
    return this._getData("/faqs", { type: "school" })
  }

  protected async getRawPromoSchool(id: string, { page = 1, limit = 6 }) {
    return this._getData(`/discounts/${id}`, {
      q: "list-school",
      page,
      limit,
    })
  }

  protected async submitRawSyllabusForm(schoolId: string, form: FileRequestForm) {
    return this._protectedCrudMethod(`/school/${schoolId}/syllabus`, {
      body: form,
      method: "post",
    })
  }

  protected async getRawSchoolLink(body: SchoolLinkBody) {
    return this._protectedExternalCrudMethod("https://harisenin.link/api/v1/link", {
      body,
      method: "post",
      token: process.env.LINK_TOKEN || process.env.HARISENIN_LINK_TOKEN,
    })
  }

  protected async submitRawPortfolio(portfolioId: string, form: FileRequestForm) {
    return this._protectedCrudMethod(`/school/${portfolioId}/portfolio`, {
      body: form,
      method: "post",
    })
  }
}
=======
import KyMethod from "./KyMethod"
import {
  FileRequestForm,
  SchoolListQuery,
  SchoolListQueryV2,
  SchoolType,
} from "../../constants/interfaces/school"

interface SchoolLinkBody {
  alias?: string
  type: "direct"
  password?: null
  active: true
  expires_at?: Date
  activates_at?: Date
  utm?: string
  domain_id?: null
  title?: string
  description?: string
  pixels?: any[]
  groups?: any[]
  rules?: any[]
  long_url: string
}

export default class SchoolRepository extends KyMethod {
  protected async getRawSchoolList({ page = 1, limit = 12, except, search }: SchoolListQuery) {
    return this._getPaginationData(`/product/list/school`, {
      page,
      limit,
      ...(search && { search }),
      ...(except && { except }),
    })
  }

  protected async getRawSchoolListV2(
    type: SchoolType,
    { page = 1, limit = 12, search }: SchoolListQueryV2
  ) {
    return this._getPaginationData(`/schools`, {
      type: type ? type.toLocaleLowerCase() : "school",
      page,
      limit,
      ...(search && { search }),
    })
  }

  protected async getRawTestimonials() {
    return this._getPublicData(`/testimonials?type=school`, "v3")
  }

  protected async getRawFaqs() {
    return this._getPublicData("/faqs?type=school", "v3")
  }

  protected async getRawPromoSchool(id: string, { page = 1, limit = 6 }) {
    return this._getPaginationData(`/discounts/${id}`, {
      q: "list-school",
      page,
      limit,
    })
  }

  protected async submitRawSyllabusForm(schoolId: string, form: FileRequestForm) {
    return this._protectedCrudMethod(`/school/${schoolId}/syllabus`, {
      body: form,
      method: "post",
    })
  }

  protected async getRawSchoolLink(body: SchoolLinkBody) {
    return this._protectedExternalCrudMethod("https://harisenin.link/api/v1/link", {
      body,
      method: "post",
      token: process.env.LINK_TOKEN || process.env.HARISENIN_LINK_TOKEN,
    })
  }

  protected async submitRawPortfolio(portfolioId: string, form: FileRequestForm) {
    return this._protectedCrudMethod(`/school/${portfolioId}/portfolio`, {
      body: form,
      method: "post",
    })
  }
}
>>>>>>> 94ccec2 (setup fix):src/lib/repositories/school.repository.ts
