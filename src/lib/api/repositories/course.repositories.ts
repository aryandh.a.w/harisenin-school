import KyMethod from "../repositories/KyMethod"

export default class CourseRepositories extends KyMethod {
  async getRawCategories() {
    return this._getData("/categories", { type: "learning" })
  }

  async getRawCourses(query?: any) {
    return this._getData("/courses", query)
  }
}
