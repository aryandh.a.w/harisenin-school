<<<<<<< HEAD:src/lib/api/repositories/checkout.repository.ts
import { RawCheckoutForm } from "@interfaces/checkout-state"
import { RawCreditCardForm } from "@services/checkout.services"
import KyMethod from "./KyMethod"

export default class CheckoutRepo extends KyMethod {
  protected async getRawCheckoutDetail(slug: string, programId: number) {
    return this._getProtectedData("/product/order/detail", {
      query: {
        type: "school",
        s: slug,
        p: programId,
      },
    })
  }

  protected async getRawCheckoutDetailV2(orderNumber: string) {
    return this._getProtectedData(`/checkout/order/${orderNumber}`, { version: "v3" })
  }

  protected async getRawCheckoutForm(programId: number) {
    return this._getProtectedData(`/service/${programId}/user`, {
      version: "v3",
    })
  }

  protected async getRawAvailablePayment(programId: number) {
    return this._getProtectedData(`/service/${programId}/payment`, {
      version: "v3",
      query: { key: "school" },
    })
  }

  protected async checkRawDiscount({ programId, code }: { programId: number; code: string }) {
    return this._protectedCrudMethod(`/service/${programId}/discount`, {
      version: "v3",
      method: "post",
      body: {
        discount_code: code,
        service_type: "SCHOOL",
      },
    })
  }

  protected async checkRawDiscountOrReferral({
    programId,
    code,
  }: {
    programId: number
    code: string
  }) {
    return this._protectedCrudMethod(`/service/${programId}/code`, {
      version: "v3",
      method: "post",
      body: {
        discount_code: code,
        service_type: "SCHOOL",
      },
    })
  }

  protected async rawCheckoutOrder({
    productId,
    serviceId,
  }: {
    productId: string
    serviceId: number
  }) {
    return this._protectedCrudMethod(`/checkout/order`, {
      version: "v3",
      method: "post",
      body: {
        cart: [
          {
            type: "SCHOOL",
            product: {
              id: productId,
              service_id: serviceId,
            },
          },
        ],
      },
    })
  }

  protected async submitRawDiscount({ checkoutId, code }: { checkoutId: string; code: string }) {
    return this._protectedCrudMethod(`/checkout/order/${checkoutId}/discount`, {
      version: "v3",
      method: "post",
      body: {
        discount_code: code,
      },
    })
  }

  protected async submitRawDiscountOrReferral({
    checkoutId,
    code,
  }: {
    checkoutId: string
    code: string
  }) {
    return this._protectedCrudMethod(`/checkout/order/${checkoutId}/discount_or_referral`, {
      version: "v3",
      method: "post",
      body: {
        discount_code: code,
        service_type: "SCHOOL",
      },
    })
  }

  protected async submitRawSchoolForm({
    checkoutId,
    form,
  }: {
    checkoutId: string
    form: RawCheckoutForm
  }) {
    return this._protectedCrudMethod(`/checkout/order/${checkoutId}/registration`, {
      version: "v3",
      method: "post",
      body: {
        type: "SCHOOL",
        form,
      },
    })
  }

  protected async submitRawPayment({ checkoutId, body }: { checkoutId: string; body: any }) {
    return this._protectedCrudMethod(`/checkout/order/${checkoutId}/payment`, {
      version: "v3",
      method: "post",
      body,
    })
  }

  protected async submitRawCreditCardPayment({
    checkoutId,
    body,
  }: {
    checkoutId: string
    body: RawCreditCardForm
  }) {
    return this._protectedCrudMethod(`/checkout/order/${checkoutId}/payment`, {
      version: "v3",
      method: "post",
      body,
    })
  }

  protected async submitRawPayLaterPayment({
    checkoutId,
    channel_code,
  }: {
    checkoutId: string
    channel_code: string
  }) {
    return this._protectedCrudMethod(`/checkout/order/${checkoutId}/paylater/plans`, {
      version: "v3",
      method: "post",
      body: { channel_code },
    })
  }
}
=======
import { RawCheckoutForm } from "../../constants/interfaces/checkout-state"
import { RawCreditCardForm } from "../services/checkout.services"
import KyMethod from "./KyMethod"

export default class CheckoutRepo extends KyMethod {
  protected async getRawUniversities() {
    return this._getPublicData("/university")
  }

  protected async getRawCheckoutDetail(slug: string, programId: number) {
    return this._getProtectedData("/product/order/detail", {
      query: {
        type: "school",
        s: slug,
        p: programId,
      },
    })
  }

  protected async getRawCheckoutDetailV2(orderNumber: string) {
    return this._getProtectedData(`/checkout/order/${orderNumber}`, { version: "v3" })
  }

  protected async getRawCheckoutForm(programId: number) {
    return this._getProtectedData(`/service/${programId}/user`, {
      version: "v3",
    })
  }

  protected async getRawAvailablePayment(programId: number) {
    return this._getProtectedData(`/service/${programId}/payment`, {
      version: "v3",
      query: { key: "school" },
    })
  }

  protected async checkRawDiscount({ programId, code }: { programId: number; code: string }) {
    return this._protectedCrudMethod(`/service/${programId}/discount`, {
      version: "v3",
      method: "post",
      body: {
        discount_code: code,
        service_type: "SCHOOL",
      },
    })
  }

  protected async checkRawDiscountOrReferral({
    programId,
    code,
  }: {
    programId: number
    code: string
  }) {
    return this._protectedCrudMethod(`/service/${programId}/code`, {
      version: "v3",
      method: "post",
      body: {
        discount_code: code,
        service_type: "SCHOOL",
      },
    })
  }

  protected async rawCheckoutOrder({
    productId,
    serviceId,
  }: {
    productId: string
    serviceId: number
  }) {
    return this._protectedCrudMethod(`/checkout/order`, {
      version: "v3",
      method: "post",
      body: {
        cart: [
          {
            type: "SCHOOL",
            product: {
              id: productId,
              service_id: serviceId,
            },
          },
        ],
      },
    })
  }

  protected async submitRawDiscount({ checkoutId, code }: { checkoutId: string; code: string }) {
    return this._protectedCrudMethod(`/checkout/order/${checkoutId}/discount`, {
      version: "v3",
      method: "post",
      body: {
        discount_code: code,
      },
    })
  }

  protected async submitRawDiscountOrReferral({
    checkoutId,
    code,
  }: {
    checkoutId: string
    code: string
  }) {
    return this._protectedCrudMethod(`/checkout/order/${checkoutId}/discount_or_referral`, {
      version: "v3",
      method: "post",
      body: {
        discount_code: code,
        service_type: "SCHOOL",
      },
    })
  }

  protected async submitRawSchoolForm({
    checkoutId,
    form,
  }: {
    checkoutId: string
    form: RawCheckoutForm
  }) {
    return this._protectedCrudMethod(`/checkout/order/${checkoutId}/registration`, {
      version: "v3",
      method: "post",
      body: {
        type: "SCHOOL",
        form,
      },
    })
  }

  protected async submitRawPayment({ checkoutId, body }: { checkoutId: string; body: any }) {
    return this._protectedCrudMethod(`/checkout/order/${checkoutId}/payment`, {
      version: "v3",
      method: "post",
      body,
    })
  }

  protected async submitRawCreditCardPayment({
    checkoutId,
    body,
  }: {
    checkoutId: string
    body: RawCreditCardForm
  }) {
    return this._protectedCrudMethod(`/checkout/order/${checkoutId}/payment`, {
      version: "v3",
      method: "post",
      body,
    })
  }
}
>>>>>>> 94ccec2 (setup fix):src/lib/repositories/checkout.repository.ts
