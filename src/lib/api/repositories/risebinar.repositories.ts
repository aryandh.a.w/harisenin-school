import KyMethod from "../repositories/KyMethod"

export default class RisebinarRepositories extends KyMethod {
  protected async getRawRisebinarList(query?: any) {
    return this._getData("/risebinars", query)
  }
}
