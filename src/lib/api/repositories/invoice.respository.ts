import KyMethod from "./KyMethod"

export default class InvoiceRepo extends KyMethod {
  protected async getRawInvoice(orderNumber: string) {
    return this._getProtectedData(`/invoice/${orderNumber}`)
  }
}
