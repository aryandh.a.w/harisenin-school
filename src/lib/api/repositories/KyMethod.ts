<<<<<<< HEAD:src/lib/api/repositories/KyMethod.ts
import ky from "ky"
import { destroyCookie, parseCookies } from "nookies"
import jwt from "jsonwebtoken"
import { JWT_SECRET } from "@constants/const"
// import Bugsnag from "../../functions/bugsnag"
import { HttpMethod } from "ky/distribution/types/options"

export default class KyMethod {
  private readonly errorMessage: string
  private readonly instance: typeof ky

  constructor() {
    this.errorMessage = "Terjadi kesalahan. Silahkan coba lagi"
    this.instance = ky.extend({
      prefixUrl: process.env.API_ENDPOINT || process.env.HARISENIN_API_ENDPOINT,
      throwHttpErrors: false,
      timeout: false,
    })
  }

  _tokenChecker() {
    const parsedCookies = parseCookies()
    const token = parsedCookies.HSTOKENID

    if (!token) {
      return null
    }

    jwt.verify(token, JWT_SECRET, (err) => {
      if (err) {
        destroyCookie(null, "HSTOKENID")
        return null
      }
    })

    return token
  }

  // bugsnagNotifier(error: any) {
  //   if (error instanceof Error) {
  //     Bugsnag.notify(error)
  //   }
  // }

  protected async _getRawResult<T>(res: Response): Promise<T> {
    const data = await res.json()

    return data.result as T
  }

  protected _getResult<T>(data: any): T {
    const result = data.result
    return result as T
  }

  protected _sendError(message: string) {
    throw {
      statusCode: 400,
      message,
    }
  }

  protected async _getErrorObject(error: any, options?: { consoleResponse?: boolean }) {
    if (options?.consoleResponse) {
      console.log(error)
    }

    const errResponse = error.response

    if (errResponse) {
      const json = await errResponse.json()
      return {
        errorCode: errResponse.status,
        data: json,
      }
    } else {
      return {
        data: { message: this.errorMessage },
      }
    }
  }

  protected async _getData(url: string, query: any = {}, version = "v2") {
    return this.instance.get(version + url, {
      searchParams: query,
    })
  }

  /**
   * CRUD method for API
   */
  protected async _crudMethod(
    url: string,
    body: any = {},
    options?: {
      method: HttpMethod
      bodyType?: "form-data" | "json"
    }
  ) {
    const type = options?.bodyType ?? "json"

    return this.instance("v2" + url, {
      method: options?.method ?? "post",
      ...(type === "form-data" && { body }),
      ...(type === "json" && { json: body }),
    })
  }

  /**
   * GET method for protected API
   */
  protected async _getProtectedData(
    url: string,
    options?: {
      version?: "v2" | "v3"
      query?: any
    }
  ) {
    const token = this._tokenChecker()

    if (token) {
      return this.instance.get((options?.version ?? "v2") + url, {
        ...(options?.query && { searchParams: options.query }),
        headers: {
          Authorization: "Bearer " + token,
        },
      })
    } else {
      throw {
        statusCode: 401,
        message: "Sesi login telah habis. Silahkan login ulang",
      }
    }
  }

  /**
   * CRUD method for protected API
   */
  protected async _protectedCrudMethod(
    url: string,
    options?: {
      body?: any
      version?: "v2" | "v3"
      query?: any
      method?: HttpMethod
      bodyType?: "form-data" | "json"
    }
  ) {
    const type = options?.bodyType ?? "json"
    const token = this._tokenChecker()

    if (token) {
      return this.instance((options?.version ?? "v2") + url, {
        ...(options?.query && { searchParams: options.query }),
        headers: {
          Authorization: "Bearer " + token,
        },
        ...(type === "form-data" && { body: options?.body }),
        ...(type === "json" && { json: options?.body ?? {} }),
        method: options?.method ?? "post",
      })
    } else {
      throw {
        statusCode: 401,
        message: "Sesi login telah habis. Silahkan login ulang",
      }
    }
  }

  /**
   * CRUD method for protected API
   */
  protected async _protectedExternalCrudMethod(
    url: string,
    options?: {
      body?: any
      query?: any
      method?: HttpMethod
      bodyType?: "form-data" | "json"
      token: string
    }
  ) {
    const type = options?.bodyType ?? "json"

    if (options?.token) {
      return ky(url, {
        ...(options?.query && { searchParams: options.query }),
        headers: {
          Authorization: "Bearer " + options?.token,
        },
        ...(type === "form-data" && { body: options?.body }),
        ...(type === "json" && { json: options?.body ?? {} }),
        method: options?.method ?? "post",
      })
    } else {
      throw {
        statusCode: 401,
        message: "Sesi login telah habis. Silahkan login ulang",
      }
    }
  }
}
=======
import ky from "ky"
import { destroyCookie, parseCookies } from "nookies"
import jwt from "jsonwebtoken"
import { JWT_SECRET } from "../../constants/const"
import Bugsnag from "../utils/method/bugsnag"
import { HttpMethod } from "ky/distribution/types/options"

export default class KyMethod {
  private readonly errorMessage: string
  private readonly instance: typeof ky

  constructor() {
    this.errorMessage = "Terjadi kesalahan. Silahkan coba lagi"
    this.instance = ky.extend({
      prefixUrl: process.env.API_ENDPOINT || process.env.HARISENIN_API_ENDPOINT,
      throwHttpErrors: false,
      timeout: false,
    })
  }

  _tokenChecker() {
    const parsedCookies = parseCookies()
    const token = parsedCookies.HSTOKENID

    if (!token) {
      return null
    }

    jwt.verify(token, JWT_SECRET, (err) => {
      if (err) {
        destroyCookie(null, "HSTOKENID")
        return null
      }
    })

    return token
  }

  bugsnagNotifier(error: any) {
    if (error instanceof Error) {
      Bugsnag.notify(error)
    }
  }

  protected async _getRawResult<T>(res: Response): Promise<T> {
    const data = await res.json()

    return data.result as T
  }

  protected _getResult<T>(data: any): T {
    const result = data.result
    return result as T
  }

  protected _sendError(message: string) {
    throw {
      statusCode: 400,
      message,
    }
  }

  protected async _getErrorObject(error: any, options?: { consoleResponse?: boolean }) {
    if (options?.consoleResponse) {
      console.log(error)
    }

    const errResponse = error.response

    if (errResponse) {
      const json = await errResponse.json()
      return {
        errorCode: errResponse.status,
        data: json,
      }
    } else {
      return {
        data: { message: this.errorMessage },
      }
    }
  }

  /**
   * Get method for public API
   */
  protected async _getPublicData(url: string, version?: "v2" | "v3") {
    return this.instance.get((version ?? "v2") + url)
  }

  /**
   * CRUD method for public API
   */
  protected async _crudPublicMethod(
    url: string,
    body: any = {},
    options?: {
      method: HttpMethod
      version: "v2" | "v3"
    }
  ) {
    return this.instance((options?.version ?? "v2") + url, {
      method: options?.method ?? "post",
      json: body,
    })
  }

  /**
   * GET method for public API which have pagination structure data
   */
  protected async _getPaginationData(
    url: string,
    query: any = {},
    options: { version: "v2" | "v3" } = { version: "v2" }
  ) {
    const version = options.version
    return this.instance.get(version + url, {
      searchParams: query,
    })
  }

  /**
   * GET method for protected API
   */
  protected async _getProtectedData(
    url: string,
    options?: {
      version?: "v2" | "v3"
      query?: any
    }
  ) {
    const token = this._tokenChecker()

    if (token) {
      return this.instance.get((options?.version ?? "v2") + url, {
        ...(options?.query && { searchParams: options.query }),
        headers: {
          Authorization: "Bearer " + token,
        },
      })
    } else {
      throw {
        statusCode: 401,
        message: "Sesi login telah habis. Silahkan login ulang",
      }
    }
  }

  /**
   * CRUD method for protected API
   */
  protected async _protectedCrudMethod(
    url: string,
    options?: {
      body?: any
      version?: "v2" | "v3"
      query?: any
      method?: HttpMethod
      bodyType?: "form-data" | "json"
    }
  ) {
    const type = options?.bodyType ?? "json"
    const token = this._tokenChecker()

    if (token) {
      return this.instance((options?.version ?? "v2") + url, {
        ...(options?.query && { searchParams: options.query }),
        headers: {
          Authorization: "Bearer " + token,
        },
        ...(type === "form-data" && { body: options?.body }),
        ...(type === "json" && { json: options?.body ?? {} }),
        method: options?.method ?? "post",
      })
    } else {
      throw {
        statusCode: 401,
        message: "Sesi login telah habis. Silahkan login ulang",
      }
    }
  }

  /**
   * CRUD method for protected API
   */
  protected async _protectedExternalCrudMethod(
    url: string,
    options?: {
      body?: any
      query?: any
      method?: HttpMethod
      bodyType?: "form-data" | "json"
      token: string
    }
  ) {
    const type = options?.bodyType ?? "json"

    if (options?.token) {
      return ky(url, {
        ...(options?.query && { searchParams: options.query }),
        headers: {
          Authorization: "Bearer " + options?.token,
        },
        ...(type === "form-data" && { body: options?.body }),
        ...(type === "json" && { json: options?.body ?? {} }),
        method: options?.method ?? "post",
      })
    } else {
      throw {
        statusCode: 401,
        message: "Sesi login telah habis. Silahkan login ulang",
      }
    }
  }
}
>>>>>>> 94ccec2 (setup fix):src/lib/repositories/KyMethod.ts
