import KyMethod from "../repositories/KyMethod"

export default class MiscRepo extends KyMethod {
  protected async getRawUniversities() {
    return this._getData("/university")
  }

  protected async getRawTestimonials(type: string) {
    return this._getData(`/testimonials`, { type }, "v3")
  }
}
