import { PaginationData } from "@interfaces/misc"
import { Risebinar, RisebinarQuery } from "@interfaces/risebinar"
import RisebinarRepositories from "../repositories/risebinar.repositories"
import { Result } from "./response"

export default class RisebinarServices extends RisebinarRepositories {
  async getRisebinarList(query?: Partial<RisebinarQuery>) {
    try {
      const res = await this.getRawRisebinarList(query)

      if (res.ok) {
        const response = await res.json()
        return Result.ok<PaginationData<Risebinar>>(response.result)
      } else {
        return Result.fail<PaginationData<Risebinar>>()
      }
    } catch (e) {
      return Result.fail<PaginationData<Risebinar>>()
    }
  }
}
