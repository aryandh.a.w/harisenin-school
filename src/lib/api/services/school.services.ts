<<<<<<< HEAD:src/lib/api/services/school.services.ts
import {
  FAQ,
  FileRequestForm,
  SchoolListQuery,
  SchoolListQueryV2,
  SchoolPagination,
  SchoolType,
  StudentTestimonial,
} from "@interfaces/school"
import SchoolRepository from "@repositories/school.repository"
import { ErrorMessage, Result } from "./response"

interface LinkResponse {
  status: string
  link: {
    id: number
    title: string
    hash: string
    alias: string
    long_url: string
    short_url: string
    disabled: false
    expires_at: Date
    description: string
    type: string
    clicks_count: number
    expiration_clicks: number
    utm: string
    has_password: boolean
  }
}

export default class SchoolServices extends SchoolRepository {
  async getSchoolList({ page = 1, limit = 12, except, search }: SchoolListQuery) {
    try {
      const res = await this.getRawSchoolList({ page, limit, except, search })
      if (res.ok) {
        const result = await this._getRawResult<SchoolPagination>(res)

        if (result) {
          return Result.ok<SchoolPagination>(result)
        } else {
          return Result.ok<SchoolPagination>({
            data: [],
            totalPage: 1,
            totalData: 0,
            currentPage: 1,
            nextPage: null,
          })
        }
      }
    } catch (e) {
      throw { message: "Terjadi kesalahan. Silahkan coba lagi" }
    }
  }

  async getSchoolListV2(type: SchoolType, { page = 1, limit = 12, search }: SchoolListQueryV2) {
    try {
      const res = await this.getRawSchoolListV2(type, { page, limit, search })
      if (res.ok) {
        const result = await this._getRawResult<SchoolPagination>(res)

        if (result) {
          return Result.ok<SchoolPagination>(result)
        } else {
          return Result.ok<SchoolPagination>({
            data: [],
            totalPage: 1,
            totalData: 0,
            currentPage: 1,
            nextPage: null,
          })
        }
      }
    } catch (e) {
      throw { message: "Terjadi kesalahan. Silahkan coba lagi" }
    }
  }

  async getHomepageTestimonials() {
    try {
      const res = await this.getRawTestimonials()
      if (res.ok) {
        const result = await this._getRawResult<StudentTestimonial[]>(res)

        if (result) {
          return Result.ok<StudentTestimonial[]>(result)
        } else {
          return Result.ok<StudentTestimonial[]>([])
        }
      } else {
        return Result.fail<StudentTestimonial[]>()
      }
    } catch (e) {
      throw { message: "Terjadi kesalahan. Silahkan coba lagi" }
    }
  }

  async getHomepageFaqs() {
    try {
      const res = await this.getRawFaqs()

      if (res.ok) {
        const result = await this._getRawResult<FAQ[]>(res)

        if (result) {
          return Result.ok<FAQ[]>(result)
        } else {
          return Result.ok<FAQ[]>([])
        }
      } else {
        return Result.fail<FAQ[]>()
      }
    } catch (e) {
      throw { message: "Terjadi kesalahan. Silahkan coba lagi" }
    }
  }

  async getPromoSchoolList(id: string, { page = 1, limit = 12 }) {
    try {
      const res = await this.getRawPromoSchool(id, { page, limit })
      if (res.ok) {
        const result = await this._getRawResult<SchoolPagination>(res)

        if (result) {
          return Result.ok<SchoolPagination>(result)
        } else {
          return Result.ok<SchoolPagination>({
            data: [],
            totalPage: 1,
            totalData: 0,
            currentPage: 1,
            nextPage: null,
          })
        }
      }
    } catch (e) {
      throw { message: "Terjadi kesalahan. Silahkan coba lagi" }
    }
  }

  async getSyllabusForm(schoolId: string, form: FileRequestForm) {
    try {
      const res = await this.submitRawSyllabusForm(schoolId, form)

      if (res.ok) {
        const body = await res.json()
        if (body.result) {
          return Result.ok<string>(body.result)
        } else {
          return Result.fail<ErrorMessage>("Terjadi kesalahan. Silahkan coba lagi")
        }
      } else {
        return Result.fail<ErrorMessage>("Terjadi kesalahan. Silahkan coba lagi")
      }
    } catch (e) {
      throw { message: "Terjadi kesalahan. Silahkan coba lagi" }
    }
  }

  async getSchoolShareLink(schoolSlug: string, isSchool: boolean) {
    try {
      const res = await this.getRawSchoolLink({
        long_url: `${process.env.HOME_URL}/school/${
          isSchool ? "bootcamp" : "proclass"
        }/${schoolSlug}`,
        type: "direct",
        active: true,
      })

      if (res.ok) {
        const body = (await res.json()) as LinkResponse
        return Result.ok<string>(body.link.short_url)
      } else {
        return Result.fail<string>()
      }
    } catch (e) {
      throw { message: "Terjadi kesalahan. Silahkan coba lagi" }
    }
  }

  async getPortfolioForm(portfolioId: string, form: FileRequestForm) {
    try {
      const res = await this.submitRawPortfolio(portfolioId, form)

      if (res.ok) {
        const body = await res.json()
        if (body.result) {
          return Result.ok<string>(body.result)
        } else {
          return Result.fail<ErrorMessage>("Terjadi kesalahan. Silahkan coba lagi")
        }
      } else {
        return Result.fail<ErrorMessage>("Terjadi kesalahan. Silahkan coba lagi")
      }
    } catch (e) {
      throw { message: "Terjadi kesalahan. Silahkan coba lagi" }
    }
  }
}
=======
import SchoolRepository from "../repositories/school.repository"
import {
  FAQ,
  SchoolPagination,
  StudentTestimonial,
  FileRequestForm,
  SchoolListQuery,
  SchoolType,
  SchoolListQueryV2,
} from "../../constants/interfaces/school"
import { ErrorMessage, Result } from "./response"

interface LinkResponse {
  status: string
  link: {
    id: number
    title: string
    hash: string
    alias: string
    long_url: string
    short_url: string
    disabled: false
    expires_at: Date
    description: string
    type: string
    clicks_count: number
    expiration_clicks: number
    utm: string
    has_password: boolean
  }
}

export default class SchoolServices extends SchoolRepository {
  async getSchoolList({ page = 1, limit = 12, except, search }: SchoolListQuery) {
    try {
      const res = await this.getRawSchoolList({ page, limit, except, search })
      if (res.ok) {
        const result = await this._getRawResult<SchoolPagination>(res)

        if (result) {
          return Result.ok<SchoolPagination>(result)
        } else {
          return Result.ok<SchoolPagination>({
            data: [],
            totalPage: 1,
            totalData: 0,
            currentPage: 1,
            nextPage: null,
          })
        }
      }
    } catch (e) {
      this.bugsnagNotifier(e)
      throw { message: "Terjadi kesalahan. Silahkan coba lagi" }
    }
  }

  async getSchoolListV2(type: SchoolType, { page = 1, limit = 12, search }: SchoolListQueryV2) {
    try {
      const res = await this.getRawSchoolListV2(type, { page, limit, search })
      if (res.ok) {
        const result = await this._getRawResult<SchoolPagination>(res)

        if (result) {
          return Result.ok<SchoolPagination>(result)
        } else {
          return Result.ok<SchoolPagination>({
            data: [],
            totalPage: 1,
            totalData: 0,
            currentPage: 1,
            nextPage: null,
          })
        }
      }
    } catch (e) {
      this.bugsnagNotifier(e)
      throw { message: "Terjadi kesalahan. Silahkan coba lagi" }
    }
  }

  async getHomepageTestimonials() {
    try {
      const res = await this.getRawTestimonials()
      if (res.ok) {
        const result = await this._getRawResult<StudentTestimonial[]>(res)

        if (result) {
          return Result.ok<StudentTestimonial[]>(result)
        } else {
          this.bugsnagNotifier(res)
          return Result.ok<StudentTestimonial[]>([])
        }
      } else {
        this.bugsnagNotifier(res)
        return Result.fail<StudentTestimonial[]>()
      }
    } catch (e) {
      this.bugsnagNotifier(e)
      throw { message: "Terjadi kesalahan. Silahkan coba lagi" }
    }
  }

  async getHomepageFaqs() {
    try {
      const res = await this.getRawFaqs()

      if (res.ok) {
        const result = await this._getRawResult<FAQ[]>(res)

        if (result) {
          return Result.ok<FAQ[]>(result)
        } else {
          this.bugsnagNotifier(res)
          return Result.ok<FAQ[]>([])
        }
      } else {
        this.bugsnagNotifier(res)
        return Result.fail<FAQ[]>()
      }
    } catch (e) {
      this.bugsnagNotifier(e)
      throw { message: "Terjadi kesalahan. Silahkan coba lagi" }
    }
  }

  async getPromoSchoolList(id: string, { page = 1, limit = 12 }) {
    try {
      const res = await this.getRawPromoSchool(id, { page, limit })
      if (res.ok) {
        const result = await this._getRawResult<SchoolPagination>(res)

        if (result) {
          return Result.ok<SchoolPagination>(result)
        } else {
          return Result.ok<SchoolPagination>({
            data: [],
            totalPage: 1,
            totalData: 0,
            currentPage: 1,
            nextPage: null,
          })
        }
      }
    } catch (e) {
      this.bugsnagNotifier(e)
      throw { message: "Terjadi kesalahan. Silahkan coba lagi" }
    }
  }

  async getSyllabusForm(schoolId: string, form: FileRequestForm) {
    try {
      const res = await this.submitRawSyllabusForm(schoolId, form)

      if (res.ok) {
        const body = await res.json()
        if (body.result) {
          return Result.ok<string>(body.result)
        } else {
          return Result.fail<ErrorMessage>("Terjadi kesalahan. Silahkan coba lagi")
        }
      } else {
        return Result.fail<ErrorMessage>("Terjadi kesalahan. Silahkan coba lagi")
      }
    } catch (e) {
      throw { message: "Terjadi kesalahan. Silahkan coba lagi" }
    }
  }

  async getSchoolShareLink(schoolSlug: string, isSchool: boolean) {
    try {
      const res = await this.getRawSchoolLink({
        long_url: `${process.env.HOME_URL}/school/${
          isSchool ? "bootcamp" : "proclass"
        }/${schoolSlug}`,
        type: "direct",
        active: true,
      })

      if (res.ok) {
        const body = (await res.json()) as LinkResponse
        return Result.ok<string>(body.link.short_url)
      } else {
        return Result.fail<string>()
      }
    } catch (e) {
      throw { message: "Terjadi kesalahan. Silahkan coba lagi" }
    }
  }

  async getPortfolioForm(portfolioId: string, form: FileRequestForm) {
    try {
      const res = await this.submitRawPortfolio(portfolioId, form)

      if (res.ok) {
        const body = await res.json()
        if (body.result) {
          return Result.ok<string>(body.result)
        } else {
          return Result.fail<ErrorMessage>("Terjadi kesalahan. Silahkan coba lagi")
        }
      } else {
        return Result.fail<ErrorMessage>("Terjadi kesalahan. Silahkan coba lagi")
      }
    } catch (e) {
      throw { message: "Terjadi kesalahan. Silahkan coba lagi" }
    }
  }
}
>>>>>>> 94ccec2 (setup fix):src/lib/services/school.services.ts
