import { University } from "@interfaces/checkout-state"
import { StudentTestimonial } from "@interfaces/school"
import { Result } from "@lib/api/services/response"
import MiscRepo from "../repositories/misc.repository"

export interface RawCreditCardForm {
  payment_method: string
  payment_channel: string
  tokenID: string
  authID: string
  cardCvn: string
  billing_details: {
    given_names: string
    surname: string
    email: string
    mobile_number: string
    phone_number: string
    address: {
      street_line1: string
      street_line2: string
      city: string
      province: string
      zip_code: string
      country: string
    }
  }
}

export default class MiscService extends MiscRepo {
  async getUniversities() {
    try {
      const res = await this.getRawUniversities()

      if (res.ok) {
        const result = await this._getRawResult<University[]>(res)

        return Result.ok<University[]>(result)
      } else {
        return Result.fail<University[]>()
      }
    } catch (error: any) {
      throw { message: "Terjadi kesalahan. Silahkan coba lagi" }
    }
  }

  async getTestimonials(type: string) {
    try {
      const res = await this.getRawTestimonials(type)
      const response = await this._getRawResult<StudentTestimonial[]>(res)

      if (!response) {
        return Result.fail<StudentTestimonial[]>()
      }

      return Result.ok<StudentTestimonial[]>(response)
    } catch (e) {
      return Result.fail<StudentTestimonial[]>()
    }
  }
}
