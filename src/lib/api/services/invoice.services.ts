<<<<<<< HEAD:src/lib/api/services/invoice.services.ts
import { InvoiceDetail } from "@interfaces/order"
import InvoiceRepo from "@repositories/invoice.respository"
import { Result } from "./response"

export default class InvoiceService extends InvoiceRepo {
  async getInvoiceDetail(orderNumber: string) {
    try {
      const res = await this.getRawInvoice(orderNumber)

      if (res.ok) {
        const data = await res.json()

        if (data.status) {
          return Result.fail<InvoiceDetail>()
        } else {
          const result = data.result
          const invoice: InvoiceDetail = {
            account_number: result.available_banks[0].bank_account_number,
            invoice_number: result.order_number,
            status: result.status,
            bank_code: result.available_banks[0].bank_code,
            expiry_date: result.expiry_date,
            transfer_amount: result.amount,
            paid_at: result.paid_at,
            checkout_url: result.invoice_url,
          }
          return Result.ok<InvoiceDetail>(invoice)
        }
      } else {
        return Result.fail<InvoiceDetail>()
      }
    } catch (error: any) {
      throw { message: "Terjadi kesalahan. Silahkan coba lagi" }
    }
  }
}
=======
import { InvoiceDetail } from "../../constants/interfaces/order"
import InvoiceRepo from "../repositories/invoice.respository"
import { Result } from "./response"

export default class InvoiceService extends InvoiceRepo {
  async getInvoiceDetail(orderNumber: string) {
    try {
      const res = await this.getRawInvoice(orderNumber)

      if (res.ok) {
        const data = await res.json()

        if (data.status) {
          return Result.fail<InvoiceDetail>()
        } else {
          const result = data.result
          const invoice: InvoiceDetail = {
            account_number: result.available_banks[0].bank_account_number,
            invoice_number: result.order_number,
            status: result.status,
            bank_code: result.available_banks[0].bank_code,
            expiry_date: result.expiry_date,
            transfer_amount: result.amount,
            paid_at: result.paid_at,
          }
          return Result.ok<InvoiceDetail>(invoice)
        }
      } else {
        return Result.fail<InvoiceDetail>()
      }
    } catch (error: any) {
      this.bugsnagNotifier(error)
      throw { message: "Terjadi kesalahan. Silahkan coba lagi" }
    }
  }
}
>>>>>>> 94ccec2 (setup fix):src/lib/services/invoice.services.ts
