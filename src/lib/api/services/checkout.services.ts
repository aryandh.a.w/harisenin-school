<<<<<<< HEAD:src/lib/api/services/checkout.services.ts
import { AvailablePayment, Checkout, Discount } from "@interfaces/checkout"
import { RawCheckoutForm, RawCheckoutResponse } from "@interfaces/checkout-state"
import { OrderDetail } from "@interfaces/order"
import CheckoutRepo from "../repositories/checkout.repository"
import { Result } from "./response"

interface CheckoutId {
  message: "Success"
  checkout_id: string
}

export interface RawCreditCardForm {
  payment_method: string
  payment_channel: string
  tokenID: string
  authID: string
  cardCvn: string
  billing_details: {
    given_names: string
    surname: string
    email: string
    mobile_number: string
    phone_number: string
    address: {
      street_line1: string
      street_line2: string
      city: string
      province: string
      zip_code: string
      country: string
    }
  }
}

export default class CheckoutService extends CheckoutRepo {
  async getAvailablePayment(programId: number) {
    try {
      const res = await this.getRawAvailablePayment(programId)

      if (res.ok) {
        const result = await res.json()
        return Result.ok<AvailablePayment>(result)
      } else {
        return Result.fail<AvailablePayment>()
      }
    } catch (error: any) {
      this.bugsnagNotifier(error)
      throw { message: "Terjadi kesalahan. Silahkan coba lagi" }
    }
  }

  async getCheckoutForm(programId: number) {
    try {
      const res = await this.getRawCheckoutForm(programId)

      if (res.ok) {
        const data = await res.json()
        const status = data.status
        const result = data.result

        if (status) {
          return Result.fail<RawCheckoutResponse | null>()
        } else {
          return Result.ok<RawCheckoutResponse | null>(result)
        }
      } else {
        return Result.fail<RawCheckoutResponse | null>()
      }
    } catch (error: any) {
      this.bugsnagNotifier(error)
      throw { message: "Terjadi kesalahan. Silahkan coba lagi" }
    }
  }

  async getOrderDetail(programId: number, slug: string) {
    try {
      const res = await this.getRawCheckoutDetail(slug, programId)

      if (res.ok) {
        const data = await res.json()
        const status = data.status
        const errorCode = data.error_code
        const result = data.result

        if (status || errorCode) {
          return Result.fail<OrderDetail>()
        } else {
          return Result.ok<OrderDetail>(result)
        }
      } else {
        return Result.fail<OrderDetail>()
      }
    } catch (error: any) {
      this.bugsnagNotifier(error)
      throw { message: "Terjadi kesalahan. Silahkan coba lagi" }
    }
  }

  async applyPromoCode(programId: number, { code }: { code: string }) {
    try {
      // const res = await this.checkRawDiscount({ programId, code })
      const res = await this.checkRawDiscountOrReferral({ programId, code })

      if (res.ok) {
        const data = await res.json()
        const status = data.status

        if (status) {
          switch (status) {
            case "DISCOUNT_NOT_FOUND_OR_EXPIRED":
              return Result.fail<Discount>("Kode promo tidak ditemukan / sudah kaduluarsa")
            case "CAN_NOT_BE_APPLIED":
              return Result.fail<Discount>("Kode Promo tidak bisa diterapkan.")
            case "DISCOUNT_NOT_AVAILABLE_FOR_THIS_PRODUCT":
            case "DISCOUNT_EXCEPTED_FOR_THIS_PRODUCT":
              return Result.fail<Discount>("Kode Promo tidak bisa diterapkan.")
            case "OWN_REFERRAL_CODE":
              return Result.fail<Discount>("Kode referral sendiri tidak dapat digunakan.")
            case "DISCOUNT_CAN_NOT_USED":
            case "CAN_NOT_USED":
              return Result.fail<Discount>(data.message)
            default:
              return Result.fail<Discount>()
          }
        } else {
          return Result.ok<Discount>({
            discount_deal: data.discount_deal,
            discount_name: data.discount_name,
            discount_description: data.discount_description,
            discount_percentage: data.discount_percentage ? data.discount_percentage * 100 : 0,
            discount_price: data.discount_price ?? 0,
            available_payment: data.available_payment,
          })
        }
      } else {
        return Result.fail<Discount>()
      }
    } catch (error: any) {
      this.bugsnagNotifier(error)
      throw { message: "Terjadi kesalahan. Silahkan coba lagi" }
    }
  }

  async submitCheckout({ productId, serviceId }: { productId: string; serviceId: number }) {
    try {
      const res = await this.rawCheckoutOrder({ productId, serviceId })

      if (res.ok) {
        const data = await res.json()
        if (data.status || data.error_code) {
          if (data.error_code === "ERR_MAX") {
            return Result.fail<CheckoutId>("Kuota program sudah penuh.")
          } else {
            return Result.fail<CheckoutId>(
              "Terjadi kesalahan pada proses checkout. Silahkan coba lagi"
            )
          }
        } else {
          return Result.ok<CheckoutId>({
            message: "Success",
            checkout_id: data.id,
          })
        }
      } else {
        return Result.fail<CheckoutId>("Terjadi kesalahan pada proses checkout. Silahkan coba lagi")
      }
    } catch (error: any) {
      return Result.fail<CheckoutId>("Terjadi kesalahan pada proses checkout. Silahkan coba lagi")
    }
  }

  async submitDiscount({ checkoutId, code }: { checkoutId: string; code: string }) {
    try {
      // const res = await this.submitRawDiscount({ checkoutId, code })
      const res = await this.submitRawDiscountOrReferral({ checkoutId, code })

      if (res.ok) {
        const data = await res.json()
        if (data.status) {
          return Result.fail<any>()
        } else {
          return Result.ok<any>({
            message: "Success",
          })
        }
      } else {
        return Result.fail<any>()
      }
    } catch (error: any) {
      this.bugsnagNotifier(error)
      throw { message: "Terjadi kesalahan. Silahkan coba lagi" }
    }
  }

  async submitForm({ checkoutId, form }: { checkoutId: string; form: RawCheckoutForm }) {
    try {
      const res = await this.submitRawSchoolForm({ checkoutId, form })

      if (res.ok) {
        const data = await res.json()
        if (data.status) {
          return Result.fail<any>()
        } else {
          return Result.ok<any>({
            message: "Success",
          })
        }
      } else {
        return Result.fail<any>()
      }
    } catch (error: any) {
      this.bugsnagNotifier(error)
      throw { message: "Terjadi kesalahan. Silahkan coba lagi" }
    }
  }

  async submitPayment({ checkoutId, body }: { checkoutId: string; body: any }) {
    try {
      const res = await this.submitRawPayment({ checkoutId, body })

      if (res.ok) {
        const data = await res.json()
        const payment = data.order_payment

        if (data.status || data.error_code) {
          if (data.error_code === "ERR_MAX") {
            return Result.fail<any>()
          } else {
            return Result.fail<any>()
          }
        } else {
          return Result.ok<any>({
            data,
            payment,
          })
        }
      } else {
        return Result.fail<any>()
      }
    } catch (error: any) {
      this.bugsnagNotifier(error)
      throw { message: "Terjadi kesalahan. Silahkan coba lagi" }
    }
  }

  async submitCreditCardPayment({
    checkoutId,
    body,
  }: {
    checkoutId: string
    body: RawCreditCardForm
  }) {
    try {
      const res = await this.submitRawCreditCardPayment({ checkoutId, body })

      if (res.ok) {
        const data = await res.json()

        if (data.status) {
          return Result.fail<any>()
        } else {
          return Result.ok<any>(data)
        }
      } else {
        return Result.fail<any>()
      }
    } catch (error: any) {
      this.bugsnagNotifier(error)
      throw { message: "Terjadi kesalahan. Silahkan coba lagi" }
    }
  }

  async submitPayLaterPayment({
    checkoutId,
    channelCode,
  }: {
    checkoutId: string
    channelCode: string
  }) {
    try {
      const res = await this.submitRawPayLaterPayment({ checkoutId, channel_code: channelCode })

      if (res.ok) {
        const data = await res.json()

        if (data.status) {
          return Result.fail<any>()
        } else {
          return Result.ok<any>(data)
        }
      } else {
        return Result.fail<any>()
      }
    } catch (error: any) {
      throw { message: "Terjadi kesalahan. Silahkan coba lagi" }
    }
  }

  async getCheckoutDetail(orderNumber: string) {
    try {
      const res = await this.getRawCheckoutDetailV2(orderNumber)

      if (res.ok) {
        const data = await res.json()

        if (data.status) {
          return Result.fail<Checkout>()
        } else {
          return Result.ok<Checkout>(data)
        }
      } else {
        return Result.fail<Checkout>()
      }
    } catch (error: any) {
      this.bugsnagNotifier(error)
      throw { message: "Terjadi kesalahan. Silahkan coba lagi" }
    }
  }
}
=======
import {
  RawCheckoutForm,
  RawCheckoutResponse,
  University,
} from "../../constants/interfaces/checkout-state"
import { OrderDetail } from "../../constants/interfaces/order"
import CheckoutRepo from "../repositories/checkout.repository"
import { Result } from "./response"
import { AvailablePayment, Checkout, Discount } from "../../constants/interfaces/checkout"

interface CheckoutId {
  message: "Success"
  checkout_id: string
}

export interface RawCreditCardForm {
  payment_method: string
  payment_channel: string
  tokenID: string
  authID: string
  cardCvn: string
  billing_details: {
    given_names: string
    surname: string
    email: string
    mobile_number: string
    phone_number: string
    address: {
      street_line1: string
      street_line2: string
      city: string
      province: string
      zip_code: string
      country: string
    }
  }
}

export default class CheckoutService extends CheckoutRepo {
  async getUniversities() {
    try {
      const res = await this.getRawUniversities()

      if (res.ok) {
        const result = await this._getRawResult<University[]>(res)

        return Result.ok<University[]>(result)
      } else {
        return Result.fail<University[]>()
      }
    } catch (error: any) {
      this.bugsnagNotifier(error)
      throw { message: "Terjadi kesalahan. Silahkan coba lagi" }
    }
  }

  async getAvailablePayment(programId: number) {
    try {
      const res = await this.getRawAvailablePayment(programId)

      if (res.ok) {
        const result = await res.json()
        return Result.ok<AvailablePayment>(result)
      } else {
        return Result.fail<AvailablePayment>()
      }
    } catch (error: any) {
      this.bugsnagNotifier(error)
      throw { message: "Terjadi kesalahan. Silahkan coba lagi" }
    }
  }

  async getCheckoutForm(programId: number) {
    try {
      const res = await this.getRawCheckoutForm(programId)

      if (res.ok) {
        const data = await res.json()
        const status = data.status
        const result = data.result

        if (status) {
          return Result.fail<RawCheckoutResponse | null>()
        } else {
          return Result.ok<RawCheckoutResponse | null>(result)
        }
      } else {
        return Result.fail<RawCheckoutResponse | null>()
      }
    } catch (error: any) {
      this.bugsnagNotifier(error)
      throw { message: "Terjadi kesalahan. Silahkan coba lagi" }
    }
  }

  async getOrderDetail(programId: number, slug: string) {
    try {
      const res = await this.getRawCheckoutDetail(slug, programId)

      if (res.ok) {
        const data = await res.json()
        const status = data.status
        const errorCode = data.error_code
        const result = data.result

        if (status || errorCode) {
          return Result.fail<OrderDetail>()
        } else {
          return Result.ok<OrderDetail>(result)
        }
      } else {
        return Result.fail<OrderDetail>()
      }
    } catch (error: any) {
      this.bugsnagNotifier(error)
      throw { message: "Terjadi kesalahan. Silahkan coba lagi" }
    }
  }

  async applyPromoCode(programId: number, { code }: { code: string }) {
    try {
      // const res = await this.checkRawDiscount({ programId, code })
      const res = await this.checkRawDiscountOrReferral({ programId, code })

      if (res.ok) {
        const data = await res.json()
        const status = data.status

        if (status) {
          switch (status) {
            case "DISCOUNT_NOT_FOUND_OR_EXPIRED":
              return Result.fail<Discount>("Kode promo tidak ditemukan / sudah kaduluarsa")
            case "CAN_NOT_BE_APPLIED":
              return Result.fail<Discount>("Kode Promo tidak bisa diterapkan.")
            case "DISCOUNT_NOT_AVAILABLE_FOR_THIS_PRODUCT":
            case "DISCOUNT_EXCEPTED_FOR_THIS_PRODUCT":
              return Result.fail<Discount>("Kode Promo tidak bisa diterapkan.")
            case "OWN_REFERRAL_CODE":
              return Result.fail<Discount>("Kode referral sendiri tidak dapat digunakan.")
            case "DISCOUNT_CAN_NOT_USED":
            case "CAN_NOT_USED":
              return Result.fail<Discount>(data.message)
            default:
              return Result.fail<Discount>()
          }
        } else {
          return Result.ok<Discount>({
            discount_deal: data.discount_deal,
            discount_name: data.discount_name,
            discount_description: data.discount_description,
            discount_percentage: data.discount_percentage ? data.discount_percentage * 100 : 0,
            discount_price: data.discount_price ?? 0,
            available_payment: data.available_payment,
          })
        }
      } else {
        return Result.fail<Discount>()
      }
    } catch (error: any) {
      this.bugsnagNotifier(error)
      throw { message: "Terjadi kesalahan. Silahkan coba lagi" }
    }
  }

  async submitCheckout({ productId, serviceId }: { productId: string; serviceId: number }) {
    try {
      const res = await this.rawCheckoutOrder({ productId, serviceId })

      if (res.ok) {
        const data = await res.json()
        if (data.status || data.error_code) {
          if (data.error_code === "ERR_MAX") {
            return Result.fail<CheckoutId>("Kuota program sudah penuh.")
          } else {
            return Result.fail<CheckoutId>(
              "Terjadi kesalahan pada proses checkout. Silahkan coba lagi"
            )
          }
        } else {
          return Result.ok<CheckoutId>({
            message: "Success",
            checkout_id: data.id,
          })
        }
      } else {
        return Result.fail<CheckoutId>("Terjadi kesalahan pada proses checkout. Silahkan coba lagi")
      }
    } catch (error: any) {
      return Result.fail<CheckoutId>("Terjadi kesalahan pada proses checkout. Silahkan coba lagi")
    }
  }

  async submitDiscount({ checkoutId, code }: { checkoutId: string; code: string }) {
    try {
      // const res = await this.submitRawDiscount({ checkoutId, code })
      const res = await this.submitRawDiscountOrReferral({ checkoutId, code })

      if (res.ok) {
        const data = await res.json()
        if (data.status) {
          return Result.fail<any>()
        } else {
          return Result.ok<any>({
            message: "Success",
          })
        }
      } else {
        return Result.fail<any>()
      }
    } catch (error: any) {
      this.bugsnagNotifier(error)
      throw { message: "Terjadi kesalahan. Silahkan coba lagi" }
    }
  }

  async submitForm({ checkoutId, form }: { checkoutId: string; form: RawCheckoutForm }) {
    try {
      const res = await this.submitRawSchoolForm({ checkoutId, form })

      if (res.ok) {
        const data = await res.json()
        if (data.status) {
          return Result.fail<any>()
        } else {
          return Result.ok<any>({
            message: "Success",
          })
        }
      } else {
        return Result.fail<any>()
      }
    } catch (error: any) {
      this.bugsnagNotifier(error)
      throw { message: "Terjadi kesalahan. Silahkan coba lagi" }
    }
  }

  async submitPayment({ checkoutId, body }: { checkoutId: string; body: any }) {
    try {
      const res = await this.submitRawPayment({ checkoutId, body })

      if (res.ok) {
        const data = await res.json()
        const payment = data.order_payment

        if (data.status || data.error_code) {
          if (data.error_code === "ERR_MAX") {
            return Result.fail<any>()
          } else {
            return Result.fail<any>()
          }
        } else {
          return Result.ok<any>({
            data,
            payment,
          })
        }
      } else {
        return Result.fail<any>()
      }
    } catch (error: any) {
      this.bugsnagNotifier(error)
      throw { message: "Terjadi kesalahan. Silahkan coba lagi" }
    }
  }

  async submitCreditCardPayment({
    checkoutId,
    body,
  }: {
    checkoutId: string
    body: RawCreditCardForm
  }) {
    try {
      const res = await this.submitRawCreditCardPayment({ checkoutId, body })

      if (res.ok) {
        const data = await res.json()

        if (data.status) {
          return Result.fail<any>()
        } else {
          return Result.ok<any>(data)
        }
      } else {
        return Result.fail<any>()
      }
    } catch (error: any) {
      this.bugsnagNotifier(error)
      throw { message: "Terjadi kesalahan. Silahkan coba lagi" }
    }
  }

  async getCheckoutDetail(orderNumber: string) {
    try {
      const res = await this.getRawCheckoutDetailV2(orderNumber)

      if (res.ok) {
        const data = await res.json()

        if (data.status) {
          return Result.fail<Checkout>()
        } else {
          return Result.ok<Checkout>(data)
        }
      } else {
        return Result.fail<Checkout>()
      }
    } catch (error: any) {
      this.bugsnagNotifier(error)
      throw { message: "Terjadi kesalahan. Silahkan coba lagi" }
    }
  }
}
>>>>>>> 94ccec2 (setup fix):src/lib/services/checkout.services.ts
