<<<<<<< HEAD:src/lib/api/services/XenditNode.ts
import { sleep } from "@lib/functions"
import { CardAuthData, CreditCardData } from "@interfaces/credit-card"

export interface TokenizeBody {
  amount: string
  card_number: string
  card_exp_month: string
  card_exp_year: string
  card_cvn: string
}

interface AuthenticateBody {
  amount: string
  token_id: string
}

export default class XenditService {
  private readonly API_KEY: string
  private _tokenData: CreditCardData
  private _authData: CardAuthData

  constructor() {
    this.API_KEY = process.env.XENDIT_KEY || process.env.HARISENIN_XENDIT_KEY
  }

  async tokenize(body: TokenizeBody) {
    try {
      // eslint-disable-next-line @typescript-eslint/no-var-requires
      const Xendit = require("@harisenin/xendit-js-node")
      Xendit.setPublishableKey(this.API_KEY)

      const tokenData = {
        ...body,
        is_multiple_use: false,
        should_authenticate: true,
      }

      const cn = Xendit.card.validateCardNumber(body.card_number)
      const exp = Xendit.card.validateExpiry(body.card_exp_month, body.card_exp_year)
      const cvn = Xendit.card.validateCvn(body.card_cvn)

      if (!cn || !exp || !cvn) {
        return {
          tokenData: null,
          isValidated: false,
        }
      }

      Xendit.card.createToken(tokenData, (err, token) => {
        if (err) {
          alert("Terjadi kesalahan. Silahkan coba lagi")

          return err
        }

        switch (token.status) {
          case "FAILED":
            return alert("Terjadi kesalahan. Silahkan coba lagi")
          case "APPROVED":
          case "VERIFIED":
          case "IN_REVIEW":
            this._tokenData = token
            return {
              tokenData: this._tokenData,
              isValidated: true,
            }
          default:
            return alert("Terjadi kesalahan. Silahkan coba lagi")
        }
      })

      await sleep(3000)

      return {
        tokenData: this._tokenData,
        isValidated: true,
      }
    } catch (e) {
      if (process.env.NODE_ENV === "development") {
        console.log({ e })
      }
    }
  }

  async authenticate(body: AuthenticateBody) {
    try {
      // eslint-disable-next-line @typescript-eslint/no-var-requires
      const Xendit = require("@harisenin/xendit-js-node")
      Xendit.setPublishableKey(this.API_KEY)

      Xendit.card.createAuthentication(body, (err, token) => {
        if (err) {
          alert("Terjadi kesalahan. Silahkan coba lagi")

          return err
        }

        switch (token.status) {
          case "FAILED":
            return alert("Terjadi kesalahan. Silahkan coba lagi")
          case "APPROVED":
          case "VERIFIED":
          case "IN_REVIEW":
            this._authData = token
            return {
              tokenData: this._authData,
              isValidated: true,
            }
          default:
            return alert("Terjadi kesalahan. Silahlan coba lagi")
        }
      })

      await sleep(2000)

      return {
        tokenData: this._authData,
        isValidated: true,
      }
    } catch (e) {
      if (process.env.NODE_ENV === "development") {
        console.log({ e })
      }
    }
  }
}
=======
import { sleep } from "../utils/method"
import { CardAuthData, CreditCardData } from "../../constants/interfaces/credit-card"

export interface TokenizeBody {
  amount: string
  card_number: string
  card_exp_month: string
  card_exp_year: string
  card_cvn: string
}

interface AuthenticateBody {
  amount: string
  token_id: string
}

export default class XenditService {
  private readonly API_KEY: string
  private _tokenData: CreditCardData
  private _authData: CardAuthData

  constructor() {
    this.API_KEY = process.env.XENDIT_KEY || process.env.HARISENIN_XENDIT_KEY
  }

  async tokenize(body: TokenizeBody) {
    try {
      // eslint-disable-next-line @typescript-eslint/no-var-requires
      const Xendit = require("@harisenin/xendit-js-node")
      Xendit.setPublishableKey(this.API_KEY)

      const tokenData = {
        ...body,
        is_multiple_use: false,
        should_authenticate: true,
      }

      const cn = Xendit.card.validateCardNumber(body.card_number)
      const exp = Xendit.card.validateExpiry(body.card_exp_month, body.card_exp_year)
      const cvn = Xendit.card.validateCvn(body.card_cvn)

      if (!cn || !exp || !cvn) {
        return {
          tokenData: null,
          isValidated: false,
        }
      }

      Xendit.card.createToken(tokenData, (err, token) => {
        if (err) {
          alert("Terjadi kesalahan. Silahkan coba lagi")

          return err
        }

        switch (token.status) {
          case "FAILED":
            return alert("Terjadi kesalahan. Silahkan coba lagi")
          case "APPROVED":
          case "VERIFIED":
          case "IN_REVIEW":
            this._tokenData = token
            return {
              tokenData: this._tokenData,
              isValidated: true,
            }
          default:
            return alert("Terjadi kesalahan. Silahkan coba lagi")
        }
      })

      await sleep(3000)

      return {
        tokenData: this._tokenData,
        isValidated: true,
      }
    } catch (e) {
      if (process.env.NODE_ENV === "development") {
        console.log({ e })
      }
    }
  }

  async authenticate(body: AuthenticateBody) {
    try {
      // eslint-disable-next-line @typescript-eslint/no-var-requires
      const Xendit = require("@harisenin/xendit-js-node")
      Xendit.setPublishableKey(this.API_KEY)

      Xendit.card.createAuthentication(body, (err, token) => {
        if (err) {
          alert("Terjadi kesalahan. Silahkan coba lagi")

          return err
        }

        switch (token.status) {
          case "FAILED":
            return alert("Terjadi kesalahan. Silahkan coba lagi")
          case "APPROVED":
          case "VERIFIED":
          case "IN_REVIEW":
            this._authData = token
            return {
              tokenData: this._authData,
              isValidated: true,
            }
          default:
            return alert("Terjadi kesalahan. Silahlan coba lagi")
        }
      })

      await sleep(2000)

      return {
        tokenData: this._authData,
        isValidated: true,
      }
    } catch (e) {
      if (process.env.NODE_ENV === "development") {
        console.log({ e })
      }
    }
  }
}
>>>>>>> 94ccec2 (setup fix):src/lib/services/XenditNode.ts
