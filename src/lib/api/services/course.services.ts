import { Course, CourseCategory } from "@interfaces/course"
import { PaginationData } from "@interfaces/misc"
import { Result } from "./response"
import CourseRepositories from "../repositories/course.repositories"

export default class CourseServices extends CourseRepositories {
  async getCourseList(query: any) {
    try {
      const res = await this.getRawCourses(query)

      if (res.ok) {
        const response = await res.json()

        const result = response.result

        if (result) {
          return Result.ok<PaginationData<Course>>(result)
        } else {
          return Result.ok<PaginationData<Course>>({
            data: [],
            currentPage: 1,
            totalData: 0,
            totalPage: 1,
          })
        }
      } else {
        return Result.fail<PaginationData<Course>>()
      }
    } catch (e) {
      return Result.fail<PaginationData<Course>>()
    }
  }

  async getCategoryList() {
    try {
      const res = await this.getRawCategories()

      if (res.ok) {
        const response = await res.json()
        return Result.ok<CourseCategory[]>(response.result)
      } else {
        return Result.fail<CourseCategory[]>()
      }
    } catch (e) {
      return Result.fail<CourseCategory[]>()
    }
  }
}
