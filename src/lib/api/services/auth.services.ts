<<<<<<< HEAD:src/lib/api/services/auth.services.ts
import { ErrorObject, Result } from "./response"
import AuthRepo from "@repositories/auth.repository"
import { destroyCookie, setCookie } from "nookies"
import { domainChecker } from "@lib/functions"
import {
  GoogleLoginBody,
  GoogleLoginResponse,
  LoginManualBody,
  LoginResponse,
  RawTokenData,
} from "@interfaces/auth"
import { deleteTokenCookies, setTokenCookies } from "@lib/functions/auth"

export default class AuthServices extends AuthRepo {
  async manualLogin({ email, password }: LoginManualBody) {
    try {
      const res = await this.rawLogin({ user_email: email, user_password: password })

      if (res.ok) {
        const data = await res.json()
        const errorCode = data.error_code

        switch (errorCode) {
          case "REQUIRED":
            this._sendError("Terjadi Kesalahan. Silahkan coba lagi")
            break
          case "WRONG_PASSWORD":
            this._sendError("Password yang kamu masukkan salah")
            break
          case "NOT_FOUND":
            this._sendError("Email belum terdaftar")
            break

          default: {
            const result = this._getResult<LoginResponse>(data)

            const tokenData = setTokenCookies(result)

            if (!tokenData) {
              return Result.fail<{ message: string }>(
                "Terjadi Kesalahan. Pastikan form terisi dengan benar dan silahkan coba lagi"
              )
            }

            if (tokenData.data?.status_completed_profile) {
              destroyCookie(null, "HS_TEMP")
            }

            return Result.ok<RawTokenData>(tokenData)
          }
        }
      } else {
        return Result.fail("Terjadi Kesalahan. Silahkan coba lagi")
      }
    } catch (e: any) {
      if (e.statusCode) {
        throw e
      }

      const errorData = await this._getErrorObject(e)
      const errorCode = errorData.data.error_code

      switch (errorCode) {
        case "NOT_VERIFIED":
          throw {
            message: "Akun ini belum diaktifkan. Silahkan cek email kamu untuk mengaktifkan akun",
          }
        case "DEACTIVATED":
          throw {
            message: "Akun ini sudah dinonaktifkan",
          }
        case "SUSPENDED":
          throw {
            message: "Akun ini telah ditangguhkan",
          }

        default: {
          throw { message: "Terjadi kesalahan. Silahkan coba lagi" }
        }
      }
    }
  }

  async googleLogin({ email, fullName, googleId }: GoogleLoginBody) {
    try {
      const res = await this.rawGoogleLogin({ userFullname: fullName, googleId, email })

      if (res.ok) {
        const result = await this._getRawResult<GoogleLoginResponse | LoginResponse>(res)

        const tokenData = setTokenCookies(result as LoginResponse)

        if (!tokenData) {
          const loginResult = result as GoogleLoginResponse

          if (loginResult.new_user) {
            const resultData = { ...loginResult, user_email: email }

            setCookie(null, "HS_TEMP", JSON.stringify(resultData), {
              path: "/",
              domain: domainChecker(),
            })
            return Result.ok<ErrorObject>({
              message: "Isi password dan biodata lengkap",
              errorCode: "NEW_USER",
            })
          } else if (!loginResult.status_completed_profile) {
            setCookie(null, "HS_TEMP", JSON.stringify(loginResult), {
              path: "/",
              domain: domainChecker(),
            })
            return Result.ok<ErrorObject>({
              message: "Isi biodata lengkap",
              errorCode: "NOT_COMPLETED",
            })
          } else {
            return Result.fail<ErrorObject>("Terjadi Kesalahan. Silahkan coba lagi")
          }
        } else {
          destroyCookie(null, "HS_TEMP")
          return Result.ok<ErrorObject>({ message: "Login Success", errorCode: "SUCCESS" })
        }
      } else {
        return Result.fail<ErrorObject>("Terjadi Kesalahan. Silahkan coba lagi")
      }
    } catch (e: any) {
      const errorData = await this._getErrorObject(e)
      const errorCode = errorData.data.error_code

      switch (errorCode) {
        case "ACCOUNT_HAS_BEEN_DEACTIVATED":
          throw {
            message: "Akun ini sudah dinonaktifkan",
          }
        case "USER_ACCOUNT_SUSPENDED":
          throw {
            message: "Akun ini telah ditangguhkan",
          }
        case "USER_ALREADY_EXISTS":
          throw {
            message: "Email sudah terdaftar, silahkan cek email dan verifikasi akun kamu.",
          }

        default: {
          throw { message: "Terjadi kesalahan. Silahkan coba lagi" }
        }
      }
    }
  }

  async resendVerificationEmail(email: string) {
    try {
      const res = await this.rawResendVerification(email)

      if (res.ok) {
        const result = await this._getRawResult<any>(res)
        return Result.ok<any>(result)
      } else {
        return Result.fail<any>()
      }
    } catch (e: any) {
      if (process.env.NODE_ENV === "development") {
        console.log(e)
      }

      throw e
    }
  }

  async logout() {
    deleteTokenCookies()

    try {
      await this.rawLogout()
    } catch (e: any) {
      return e
    }
  }
}
=======
import { ErrorObject, Result } from "./response"
import AuthRepo from "../repositories/auth.repository"
import { destroyCookie, setCookie } from "nookies"
import { domainChecker } from "../utils/method"
import {
  GoogleLoginBody,
  GoogleLoginResponse,
  LoginManualBody,
  LoginResponse,
  RawTokenData,
} from "../../constants/interfaces/auth"
import { deleteTokenCookies, setTokenCookies } from "../utils/method/auth"

export default class AuthServices extends AuthRepo {
  async manualLogin({ email, password }: LoginManualBody) {
    try {
      const res = await this.rawLogin({ user_email: email, user_password: password })

      if (res.ok) {
        const data = await res.json()
        const errorCode = data.error_code

        switch (errorCode) {
          case "REQUIRED":
            this._sendError("Terjadi Kesalahan. Silahkan coba lagi")
            break
          case "WRONG_PASSWORD":
            this._sendError("Password yang kamu masukkan salah")
            break
          case "NOT_FOUND":
            this._sendError("Email belum terdaftar")
            break

          default: {
            const result = this._getResult<LoginResponse>(data)

            const tokenData = setTokenCookies(result)

            if (!tokenData) {
              return Result.fail<{ message: string }>(
                "Terjadi Kesalahan. Pastikan form terisi dengan benar dan silahkan coba lagi"
              )
            }

            if (tokenData.data?.status_completed_profile) {
              destroyCookie(null, "HS_TEMP")
            }

            return Result.ok<RawTokenData>(tokenData)
          }
        }
      } else {
        this.bugsnagNotifier(res)
        return Result.fail("Terjadi Kesalahan. Silahkan coba lagi")
      }
    } catch (e: any) {
      if (e.statusCode) {
        throw e
      }

      const errorData = await this._getErrorObject(e)
      const errorCode = errorData.data.error_code

      switch (errorCode) {
        case "NOT_VERIFIED":
          throw {
            message: "Akun ini belum diaktifkan. Silahkan cek email kamu untuk mengaktifkan akun",
          }
        case "DEACTIVATED":
          throw {
            message: "Akun ini sudah dinonaktifkan",
          }
        case "SUSPENDED":
          throw {
            message: "Akun ini telah ditangguhkan",
          }

        default: {
          this.bugsnagNotifier(e)
          throw { message: "Terjadi kesalahan. Silahkan coba lagi" }
        }
      }
    }
  }

  async googleLogin({ email, fullName, googleId }: GoogleLoginBody) {
    try {
      const res = await this.rawGoogleLogin({ userFullname: fullName, googleId, email })

      if (res.ok) {
        const result = await this._getRawResult<GoogleLoginResponse | LoginResponse>(res)

        const tokenData = setTokenCookies(result as LoginResponse)

        if (!tokenData) {
          const loginResult = result as GoogleLoginResponse

          if (loginResult.new_user) {
            const resultData = { ...loginResult, user_email: email }

            setCookie(null, "HS_TEMP", JSON.stringify(resultData), {
              path: "/",
              domain: domainChecker(),
            })
            return Result.ok<ErrorObject>({
              message: "Isi password dan biodata lengkap",
              errorCode: "NEW_USER",
            })
          } else if (!loginResult.status_completed_profile) {
            setCookie(null, "HS_TEMP", JSON.stringify(loginResult), {
              path: "/",
              domain: domainChecker(),
            })
            return Result.ok<ErrorObject>({
              message: "Isi biodata lengkap",
              errorCode: "NOT_COMPLETED",
            })
          } else {
            return Result.fail<ErrorObject>("Terjadi Kesalahan. Silahkan coba lagi")
          }
        } else {
          destroyCookie(null, "HS_TEMP")
          return Result.ok<ErrorObject>({ message: "Login Success", errorCode: "SUCCESS" })
        }
      } else {
        this.bugsnagNotifier(res)
        return Result.fail<ErrorObject>("Terjadi Kesalahan. Silahkan coba lagi")
      }
    } catch (e: any) {
      const errorData = await this._getErrorObject(e)
      const errorCode = errorData.data.error_code

      switch (errorCode) {
        case "ACCOUNT_HAS_BEEN_DEACTIVATED":
          throw {
            message: "Akun ini sudah dinonaktifkan",
          }
        case "USER_ACCOUNT_SUSPENDED":
          throw {
            message: "Akun ini telah ditangguhkan",
          }
        case "USER_ALREADY_EXISTS":
          throw {
            message: "Email sudah terdaftar, silahkan cek email dan verifikasi akun kamu.",
          }

        default: {
          this.bugsnagNotifier(e)
          throw { message: "Terjadi kesalahan. Silahkan coba lagi" }
        }
      }
    }
  }

  async resendVerificationEmail(email: string) {
    try {
      const res = await this.rawResendVerification(email)

      if (res.ok) {
        const result = await this._getRawResult<any>(res)
        return Result.ok<any>(result)
      } else {
        return Result.fail<any>()
      }
    } catch (e: any) {
      if (process.env.NODE_ENV === "development") {
        console.log(e)
      }

      throw e
    }
  }

  async logout() {
    deleteTokenCookies()

    try {
      await this.rawLogout()
    } catch (e: any) {
      this.bugsnagNotifier(e)
    }
  }
}
>>>>>>> 94ccec2 (setup fix):src/lib/services/auth.services.ts
