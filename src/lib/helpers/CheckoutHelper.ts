<<<<<<< HEAD:src/lib/helpers/CheckoutHelper.ts
import {
  CheckoutForm,
  CheckoutFormFile,
  FormFileState,
  RawCheckoutForm,
  RawCheckoutResponse,
} from "../../interfaces/checkout-state"

const blankFormFileState: FormFileState = {
  name: "File...",
  error: "",
}

export function checkoutFormHelper(data: RawCheckoutResponse) {
  const rawFilledForm: RawCheckoutForm = data.user_school
  const filledForm = {}
  const filledFileForm = {}

  for (const rawFilledFormKey in rawFilledForm) {
    // if (rawFilledForm.hasOwnProperty(rawFilledFormKey)) {
    if (Object.prototype.hasOwnProperty.call(rawFilledForm, rawFilledFormKey)) {
      filledForm[rawFilledFormKey] = {
        value: rawFilledForm[rawFilledFormKey] ?? "",
        error: "",
      }
    }
  }

  filledFileForm["identity_card"] = data.identity_card
    ? {
        name: data.identity_card.name,
        error: "",
        id: data.identity_card.id,
      }
    : blankFormFileState
  filledFileForm["ijazah"] = data.ijazah
    ? {
        name: data.ijazah.name,
        error: "",
        id: data.ijazah.id,
      }
    : blankFormFileState
  filledFileForm["student_card"] = data.student_card
    ? {
        name: data.student_card.name,
        error: "",
        id: data.student_card.id,
      }
    : blankFormFileState
  filledFileForm["last_study_card"] = data.last_study_card
    ? {
        name: data.last_study_card.name,
        error: "",
        id: data.last_study_card.id,
      }
    : blankFormFileState
  filledFileForm["cv"] = data.cv
    ? {
        name: data.cv.name,
        error: "",
        id: data.cv.id,
      }
    : blankFormFileState

  let status
  switch (true) {
    case rawFilledForm.user_job?.length > 0:
      status = "Karyawan"
      break
    case rawFilledForm.user_university?.length > 0:
      status = "Mahasiswa"
      break
    default:
      status = "Belum Bekerja"
      break
  }

  return {
    filledForm: filledForm as CheckoutForm,
    filledFileForm: filledFileForm as CheckoutFormFile,
    status,
  }
}
=======
import {
  CheckoutForm,
  CheckoutFormFile,
  FormFileState,
  RawCheckoutForm,
  RawCheckoutResponse,
} from "../../../constants/interfaces/checkout-state"

const blankFormFileState: FormFileState = {
  name: "File...",
  error: "",
}

export function checkoutFormHelper(data: RawCheckoutResponse) {
  const rawFilledForm: RawCheckoutForm = data.user_school
  const filledForm = {}
  const filledFileForm = {}

  for (const rawFilledFormKey in rawFilledForm) {
    // if (rawFilledForm.hasOwnProperty(rawFilledFormKey)) {
    if (Object.prototype.hasOwnProperty.call(rawFilledForm, rawFilledFormKey)) {
      filledForm[rawFilledFormKey] = {
        value: rawFilledForm[rawFilledFormKey] ?? "",
        error: "",
      }
    }
  }

  filledFileForm["identity_card"] = data.identity_card
    ? {
        name: data.identity_card.name,
        error: "",
        id: data.identity_card.id,
      }
    : blankFormFileState
  filledFileForm["ijazah"] = data.ijazah
    ? {
        name: data.ijazah.name,
        error: "",
        id: data.ijazah.id,
      }
    : blankFormFileState
  filledFileForm["student_card"] = data.student_card
    ? {
        name: data.student_card.name,
        error: "",
        id: data.student_card.id,
      }
    : blankFormFileState
  filledFileForm["last_study_card"] = data.last_study_card
    ? {
        name: data.last_study_card.name,
        error: "",
        id: data.last_study_card.id,
      }
    : blankFormFileState
  filledFileForm["cv"] = data.cv
    ? {
        name: data.cv.name,
        error: "",
        id: data.cv.id,
      }
    : blankFormFileState

  let status
  switch (true) {
    case rawFilledForm.user_job?.length > 0:
      status = "Karyawan"
      break
    case rawFilledForm.user_university?.length > 0:
      status = "Mahasiswa"
      break
    default:
      status = "Belum Bekerja"
      break
  }

  return {
    filledForm: filledForm as CheckoutForm,
    filledFileForm: filledFileForm as CheckoutFormFile,
    status,
  }
}
>>>>>>> 94ccec2 (setup fix):src/lib/utils/helper/CheckoutHelper.ts
