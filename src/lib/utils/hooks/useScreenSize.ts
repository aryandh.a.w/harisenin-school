import { useState, useEffect } from "react";

function useScreenSize() {
  const [screenWidth, setScreenWidth] = useState(0);
  const [screenHeight, setScreenHeight] = useState(0);

  useEffect(() => {
    setScreenWidth(window.innerWidth);
    setScreenHeight(window.innerHeight);
    window.addEventListener("resize", handleViewPort);
    return () => {
      window.removeEventListener("resize", handleViewPort);
    };
  }, [screenWidth, screenHeight]);

  const handleViewPort = () => {
    setScreenWidth(window.innerWidth);
    setScreenHeight(window.innerHeight);
  };

  return {
    screenWidth,
    screenHeight,
  };
}

export default useScreenSize;
