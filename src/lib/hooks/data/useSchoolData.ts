import { School, SchoolType } from "@interfaces/school"
import SchoolServices from "@services/school.services"
import useData from "./useData"

const fetcher = (type: SchoolType) => async () => {
  const school = new SchoolServices()
  try {
    const res = await school.getSchoolListV2(type, { page: 1, limit: 12 })

    if (res.isSuccess) {
      const result = res.getValue()
      return result.data
    } else {
      return []
    }
  } catch (e) {
    return []
  }
}

export function useSchools(type: SchoolType) {
  const { data, isLoading, isError, swrResponse } = useData<School[]>(
    `/school/${type}`,
    fetcher(type)
  )

  return { data: data ?? [], isLoading, mutate: swrResponse.mutate, isError }
}
