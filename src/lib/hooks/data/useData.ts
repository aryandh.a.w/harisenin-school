import useSWR, { Key, SWRResponse } from "swr"

interface DataResponse<T> {
  data?: T
  isLoading: boolean
  isError: any
  swrResponse: SWRResponse<T, any>
}

function useData<T>(key: Key, fetcher: (args?: any) => Promise<T>): DataResponse<T> {
  const response = useSWR<T>(key, fetcher)

  return {
    data: response.data,
    isLoading: !response.data && !response.error,
    isError: response.error,
    swrResponse: response,
  }
}

export default useData
