import { CourseCategory } from "@interfaces/course"
import CourseServices from "@lib/api/services/course.services"
import useData from "./useData"

const fetcher = async () => {
  const course = new CourseServices()

  try {
    const res = await course.getCategoryList()

    if (res.isSuccess) {
      return res.getValue()
    } else {
      return []
    }
  } catch (e) {
    return []
  }
}

export function useCourse() {
  const { data, isLoading, isError, swrResponse } = useData<CourseCategory[]>(`/course`, fetcher)

  return { data: data ?? [], isLoading, mutate: swrResponse.mutate, isError }
}
