import { Risebinar } from "@interfaces/risebinar"
import RisebinarServices from "@lib/api/services/risebinar.services"
import useData from "./useData"

const fetcher = async () => {
  const risebinar = new RisebinarServices()
  try {
    const res = await risebinar.getRisebinarList()

    if (res.isSuccess) {
      const result = res.getValue()
      return result.data
    } else {
      return []
    }
  } catch (e) {
    return []
  }
}

export function useRisebinar() {
  const { data, isLoading, isError, swrResponse } = useData<Risebinar[]>(`/risebinar`, fetcher)

  return { data: data ?? [], isLoading, mutate: swrResponse.mutate, isError }
}
