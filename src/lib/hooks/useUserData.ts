<<<<<<< HEAD:src/lib/hooks/useUserData.ts
import { useEffect, useState } from "react"
import jwt from "jsonwebtoken"
import { JWT_SECRET } from "@constants/const"
import nookies from "nookies"
import { TokenData } from "@interfaces/user"
import { useRouter } from "next/router"
import AuthServices from "@services/auth.services"
import { useIsMounted } from "./useIsMounted"

export interface CookiesData {
  uid: string
  data: TokenData
}

export function useUserData() {
  const [loading, setLoading] = useState(true)
  const [isLogin, setIsLogin] = useState(false)
  const [isVerified, setIsVerified] = useState(false)
  const [tokenData, setTokenData] = useState<TokenData | undefined>(undefined)
  const [uid, setUid] = useState("")

  const cookies = nookies.get({})
  const router = useRouter()
  const auth = new AuthServices()
  const isMounted = useIsMounted()

  useEffect(() => {
    async function loadUserFromCookies() {
      setLoading(true)
      const token = cookies.HSTOKENID

      if (token) {
        setIsLogin(true)
        jwt.verify(token, JWT_SECRET, {}, async (err, decoded) => {
          if (err) {
            await auth.logout()
            setIsVerified(false)
          } else {
            const data: CookiesData = decoded as CookiesData
            setIsVerified(true)
            setTokenData(data.data)
            setUid(data.uid)
            // setIsPartner(data?.data?.is_partner)
          }
        })
      } else {
        setIsLogin(false)
      }

      setLoading(false)
    }

    if (cookies && loading && isMounted) {
      loadUserFromCookies()
    }
  }, [cookies, router, isMounted])

  return {
    isLogin,
    tokenData,
    isVerified,
    uid,
  }
}
=======
import { useEffect, useState } from "react"
import jwt from "jsonwebtoken"
import { JWT_SECRET } from "../../../constants/const"
import nookies from "nookies"
import { TokenData } from "../../../constants/interfaces/user"
import { useRouter } from "next/router"
import AuthServices from "../../services/auth.services"

export interface CookiesData {
  uid: string
  data: TokenData
}

export function useUserData() {
  const [loading, setLoading] = useState(true)
  const [isLogin, setIsLogin] = useState(false)
  const [isVerified, setIsVerified] = useState(false)
  const [isPartner, setIsPartner] = useState<boolean | number>(false)
  const [tokenData, setTokenData] = useState<TokenData | undefined>(undefined)
  const [uid, setUid] = useState("")

  const cookies = nookies.get({})
  const router = useRouter()
  const auth = new AuthServices()

  useEffect(() => {
    async function loadUserFromCookies() {
      setLoading(true)
      const token = cookies.HSTOKENID

      if (token) {
        setIsLogin(true)
        jwt.verify(token, JWT_SECRET, {}, async (err, decoded) => {
          if (err) {
            await auth.logout()
            setIsVerified(false)
          } else {
            const data: CookiesData = decoded as CookiesData
            setIsVerified(true)
            setTokenData(data.data)
            setUid(data.uid)
            setIsPartner(data?.data?.is_partner)
          }
        })
      } else {
        setIsLogin(false)
      }

      setLoading(false)
    }

    if (cookies && loading) {
      loadUserFromCookies()
    }
  }, [cookies, router])

  return {
    isLogin,
    isPartner,
    tokenData,
    isVerified,
    uid,
  }
}
>>>>>>> 94ccec2 (setup fix):src/lib/utils/hooks/useUserData.ts
