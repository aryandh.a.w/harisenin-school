import { useEffect } from "react"

export interface BasicWebsocketProps {
  channel: string
  fetcher: () => any
  isSocketStart: boolean
}

export interface EWalletPaymentSocketProps {
  checkoutId?: string | string[]
  handleError: () => void
  productId: string
}

export function useBasicWebSocket({ channel, fetcher, isSocketStart }: BasicWebsocketProps) {
  useEffect(() => {
    if (isSocketStart) {
      // eslint-disable-next-line @typescript-eslint/no-var-requires
      const Ws = require("@adonisjs/websocket-client")
      const ws = Ws(`${process.env.WSS_ENDPOINT || process.env.HARISENIN_WSS_ENDPOINT}`, {
        path: "v2/ws",
      })
      ws.connect()
      const subs = ws.subscribe(`socket:${channel}`)
      subs.on("message", async () => {
        try {
          fetcher()
        } catch (error) {
          console.log(error)
        }
      })
      return () => {
        subs.on("close", () => null)
        subs.close()
        ws.close()
      }
    }
  }, [isSocketStart])
}

export function useEWalletPaymentSocket({
  checkoutId,
  productId,
  handleError,
}: EWalletPaymentSocketProps) {
  // eslint-disable-next-line @typescript-eslint/no-var-requires
  const Ws = require("@adonisjs/websocket-client")
  const ws = Ws(`${process.env.WSS_ENDPOINT || process.env.HARISENIN_WSS_ENDPOINT}`, {
    path: "v2/ws",
  })
  ws.connect()

  const subs = ws.subscribe(`socket:ewallet-${checkoutId}`)
  subs.on("message", async (res: any) => {
    if (res === "SUCCEEDED") {
      subs.on("close", () => null)
      subs.close()
      ws.close()

      window.location.assign(`/school/invoice/${checkoutId}/success?productId=${productId}`)
    } else {
      subs.on("close", () => null)
      handleError()
      subs.close()
      ws.close()
    }
  })
}
