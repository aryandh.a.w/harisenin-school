<<<<<<< HEAD:src/lib/hooks/useCheckout.ts
import dayjs from "dayjs"
import "dayjs/locale/id"
import { Reducer, useEffect, useReducer, useState } from "react"
import {
  CheckoutAction,
  CheckoutActionType,
  CheckoutForm,
  CheckoutFormFile,
  CheckoutModalMode,
  CheckoutState,
  CheckoutSummary,
  ErrorCheckPayment,
  FormErrorPayload,
  FormFileHandlerPayload,
  FormFileState,
  FormHandlerPayload,
  FormInitializerPayload,
  FormState,
  FormStatus,
  OtherOptionState,
  PaymentInitializerPayload,
  PaymentOptionInitializerPayload,
  University,
} from "@interfaces/checkout-state"
import { AvailablePromo } from "@interfaces/promo"
import { SelectOption } from "@interfaces/ui"
import CheckoutService from "@services/checkout.services"
import { checkoutFormHelper } from "../helpers/CheckoutHelper"
import { useUserData } from "./useUserData"
import { SchoolBatch } from "@interfaces/school"
import { Discount, PaymentOption } from "@interfaces/checkout"
import MiscService from "@lib/api/services/misc.services"

const blankFormState: FormState = {
  value: "",
  error: "",
}

const blankFormFileState: FormFileState = {
  name: "File...",
  error: "",
}

const form = {
  user_job: blankFormState,
  user_email: blankFormState,
  user_majors: blankFormState,
  user_faculty: blankFormState,
  user_fullname: blankFormState,
  user_university: blankFormState,
  user_linkedin_url: blankFormState,
  user_desired_job: blankFormState,
  user_phone_number: blankFormState,
  user_desired_job_sector: blankFormState,
  user_batch_request: blankFormState,
}

const form_file = {
  identity_card: blankFormFileState,
  ijazah: blankFormFileState,
  student_card: blankFormFileState,
  last_study_card: blankFormFileState,
  cv: blankFormFileState,
}

const initialState: CheckoutState = {
  //Initial state
  gross_price: 0,
  bank_method: [],
  e_wallet_method: [],
  pay_later_method: [],
  available_promos: [],

  //form state
  isLoadingData: true,
  form: form,
  form_file: form_file,
  university: [],
  user_batch_request: [],
  form_status: "",
  form_status_error: "",

  //error state
  error_discount: "",
  error_submitting: "",
  error_admin_fee: "",
  error_checking: {
    form: "",
    paymentOption: "",
  },

  //state for payment
  admin_fee: 0,
  selected_method: "",
  selected_channel: "",
  total_price: 0,
  modal_open: "closed",
  payment_phone_number: "",

  //Promo
  promo_code: "",
  discount_amount: 0,
  promo_name: "",
  applied_promo_code: "",
}

const reducer: Reducer<CheckoutState, CheckoutAction> = (state, action) => {
  const { type, payload } = action

  switch (type) {
    // initializer
    case CheckoutActionType.PaymentInitializer: {
      const initPayload = payload as PaymentInitializerPayload
      return {
        ...state,
        gross_price: initPayload.gross_price,
        total_price: initPayload.total_price,
      }
    }

    case CheckoutActionType.FormInitializer: {
      const formPayload = payload as FormInitializerPayload
      return {
        ...state,
        form: formPayload.form,
        form_file: formPayload.form_file,
      }
    }

    case CheckoutActionType.UniversityInitializer: {
      const uniPayload = payload as University[]

      const uni: SelectOption[] = uniPayload.map((value) => {
        return {
          value: value.university_name,
          label: value.university_name,
        }
      })

      return {
        ...state,
        university: uni,
      }
    }

    case CheckoutActionType.PromoInitializer: {
      const promoPayload = payload as AvailablePromo[]

      return {
        ...state,
        available_promos: promoPayload,
      }
    }

    case CheckoutActionType.BatchInitializer: {
      const batchPayload = payload as SchoolBatch[]

      const batch: SelectOption[] = batchPayload.map((value) => {
        const formattedDate = (date: string) => dayjs(date).format("DD MMM")

        return {
          value: `${value.batch}`,
          label: `${value.batch} (Program dimulai: ${formattedDate(value.batch_start)}) `,
        }
      })

      return {
        ...state,
        user_batch_request: batch,
      }
    }

    case CheckoutActionType.PaymentOptionInitializer: {
      const paymentInitPayload = payload as PaymentOptionInitializerPayload
      return {
        ...state,
        bank_method: paymentInitPayload.bank_method,
        e_wallet_method: paymentInitPayload.e_wallet_method,
        credit_card: paymentInitPayload.credit_card,
        pay_later_method: paymentInitPayload.pay_later_method,
      }
    }

    //  Form Handler
    case CheckoutActionType.FormHandler: {
      const formHandlerPayload = payload as FormHandlerPayload
      const name = formHandlerPayload.name

      return {
        ...state,
        form: {
          ...state.form,
          [name]: {
            value: formHandlerPayload.value,
            error: "",
          },
        },
      }
    }

    case CheckoutActionType.FormOptionHandler: {
      const formPayload = payload as string

      let currentOptions = [...state.form.info_from.value]

      const findOption = currentOptions.find((c) => c === formPayload)

      if (findOption) {
        currentOptions = currentOptions.filter((c) => c !== findOption)
      } else {
        currentOptions = [...currentOptions, formPayload]
      }

      return {
        ...state,
        form: {
          ...state.form,
          info_from: {
            value: currentOptions,
            error: "",
          },
        },
      }
    }

    case CheckoutActionType.OtherOptionHandler: {
      const otherOptionPayload = payload as OtherOptionState

      let currentOptions = [...state.form.info_from.value]

      if (!otherOptionPayload.isSelected) {
        const findOption = currentOptions.find((c) => c === otherOptionPayload.value)

        if (findOption) {
          currentOptions = currentOptions.filter((c) => c !== findOption)
        }
      } else {
        currentOptions = [...state.form.info_from.value, otherOptionPayload.value]
      }

      return {
        ...state,
        other_option: payload,
        form: {
          ...state.form,
          info_from: {
            value: currentOptions,
            error: "",
          },
        },
      }
    }

    case CheckoutActionType.FormTypeHandler:
      return {
        ...state,
        form_status: payload as FormStatus,
        form_status_error: "",
      }

    case CheckoutActionType.FormFileHandler: {
      const formFilePayload = payload as FormFileHandlerPayload
      const fileTypeName = formFilePayload.name

      return {
        ...state,
        form_file: {
          ...state.form_file,
          [fileTypeName]: {
            ...state.form_file[fileTypeName],
            file: formFilePayload.file,
            error: "",
            name: formFilePayload.file.name,
          },
        },
      }
    }

    case CheckoutActionType.FormErrorHandler: {
      const formErrorPayload = payload as FormErrorPayload
      const formErrorName = formErrorPayload.name

      let errorForm = {}

      switch (true) {
        case formErrorPayload.type === "file":
          errorForm = {
            form_file: {
              ...state.form_file,
              [formErrorName]: {
                ...state.form_file[formErrorName],
                error: formErrorPayload.error,
              },
            },
          }
          break
        case formErrorPayload.type === "field" && formErrorName !== "form_status":
          errorForm = {
            form: {
              ...state.form,
              [formErrorName]: {
                ...state.form[formErrorName],
                error: formErrorPayload.error,
              },
            },
          }
          break
        default:
          errorForm = {
            form_status_error: formErrorPayload.error,
          }
          break
      }

      return {
        ...state,
        ...errorForm,
      }
    }

    //  Payment Handler
    case CheckoutActionType.ChoosePayment: {
      const selectOption = payload as PaymentOption
      return {
        ...state,
        total_price: state.gross_price + selectOption.payment_fee - state.discount_amount,
        admin_fee: selectOption.payment_fee,
        selected_channel: selectOption.payment_channel,
        selected_method: selectOption.payment_method,
      }
    }

    case CheckoutActionType.OpenModal: {
      const open = payload as CheckoutModalMode
      return {
        ...state,
        modal_open: open,
      }
    }

    case CheckoutActionType.ClosePromoModal:
      return {
        ...state,
        modal_open: "closed",
        error_discount: "",
        promo_code: "",
        applied_promo_code: "",
      }

    case CheckoutActionType.InputPromoCode:
      return {
        ...state,
        promo_code: action.payload as string,
        applied_promo_code: action.payload as string,
      }

    case CheckoutActionType.ApplyPromo: {
      const promoPayload = payload as Discount

      return {
        ...state,
        modal_open: "closed",
        promo_code: "",
        error_discount: "",
        discount_amount: promoPayload.discount_deal,
        promo_name: promoPayload.discount_name,
        total_price: state.gross_price + state.admin_fee - promoPayload.discount_deal,
      }
    }

    case CheckoutActionType.ErrorDiscount:
      return {
        ...state,
        error_discount: payload as string,
      }

    case CheckoutActionType.DeleteDiscount:
      return {
        ...state,
        discount_amount: 0,
        promo_name: "",
        total_price: state.gross_price + state.admin_fee,
      }

    case CheckoutActionType.ErrorSubmitting:
      return {
        ...state,
        error_submitting: payload as string,
      }

    case CheckoutActionType.ErrorChecking: {
      const errorCheck = payload as ErrorCheckPayment

      return {
        ...state,
        error_checking: {
          ...state.error_checking,
          [errorCheck.errorType]: errorCheck.message,
        },
      }
    }

    default:
      return state
  }
}

const useCheckout = (programId: number, summary: CheckoutSummary) => {
  const [checkoutState, checkoutDispatch] = useReducer(reducer, initialState)
  const [isLoading, setIsLoading] = useState(true)
  dayjs.locale("id")

  const { tokenData } = useUserData()
  const user_email = tokenData?.user_email
  const checkout = new CheckoutService()
  const misc = new MiscService()

  useEffect(() => {
    const getCheckoutData = async () => {
      checkoutDispatch({
        type: CheckoutActionType.PaymentInitializer,
        payload: {
          gross_price: summary?.program_price,
          total_price: summary?.program_price,
        },
      })

      try {
        const res = await checkout.getAvailablePayment(programId)
        if (res.isSuccess) {
          const data = res.getValue()

          checkoutDispatch({
            type: CheckoutActionType.PaymentOptionInitializer,
            payload: {
              bank_method: data.BANK_TRANSFER,
              e_wallet_method: data.EWALLET,
              ...(data.CREDIT_CARD && {
                credit_card: {
                  payment_method: "CREDIT_CARD",
                  payment_fee: data.CREDIT_CARD[0].payment_fee,
                  payment_channel: "",
                  payment_channel_logo: "",
                },
              }),
              pay_later_method: data.PAY_LATER,
            },
          })
        }
      } catch (e) {
        if (process.env.NODE_ENV === "development") {
          console.log(e)
        }
      }
    }

    const getFormData = async () => {
      const university = misc.getUniversities()
      const formRes = checkout.getCheckoutForm(programId)
      const orderData = checkout.getOrderDetail(programId, summary.school_slug)

      const [res1, res2, res3] = await Promise.all([university, formRes, orderData])

      if (res1.isSuccess) {
        checkoutDispatch({
          type: CheckoutActionType.UniversityInitializer,
          payload: res1.getValue(),
        })
      }

      if (res2.isSuccess) {
        const result = res2.getValue()

        if (!result) {
          checkoutDispatch({
            type: CheckoutActionType.FormInitializer,
            payload: {
              form: {
                ...form,
                user_email: {
                  value: user_email,
                  error: "",
                },
                user_fullname: {
                  value: tokenData.fullname ?? "",
                  error: "",
                },
                user_phone_number: {
                  value: tokenData.user_phone_number ?? "",
                  error: "",
                },
                user_batch_request: {
                  value: "",
                  error: "",
                },
              },
              form_file,
            },
          })
        } else {
          const { filledFileForm, filledForm, status } = checkoutFormHelper(result)

          checkoutDispatch({
            type: CheckoutActionType.FormInitializer,
            payload: {
              form: {
                ...(filledForm as CheckoutForm),
                user_linkedin_url: filledForm.user_linkedin_url ?? { value: "", error: "" },
                user_batch_request: { value: "", error: "" },
              },
              form_file: filledFileForm as CheckoutFormFile,
            },
          })

          checkoutDispatch({
            type: CheckoutActionType.FormTypeHandler,
            payload: status,
          })
        }
      }

      // if (res3.ok) {
      //   checkoutDispatch({
      //     type: CheckoutActionType.PromoInitializer,
      //     payload: res3.data as AvailablePromo[],
      //   })
      // }

      if (res3.isSuccess) {
        const res4Data = res3.getValue()

        checkoutDispatch({
          type: CheckoutActionType.BatchInitializer,
          payload: res4Data.available_batch,
        })

        checkoutDispatch({
          type: CheckoutActionType.FormHandler,
          payload: {
            name: "user_batch_request",
            value: res4Data.available_batch[0].batch.toString(),
          },
        })
      }

      setIsLoading(false)
    }

    if (programId) {
      getCheckoutData()
    }

    if (programId && tokenData && summary) {
      getFormData()
    }
  }, [programId, tokenData, summary])

  return {
    checkoutState,
    checkoutDispatch,
    isLoading,
  }
}
export default useCheckout
=======
import dayjs from "dayjs"
import "dayjs/locale/id"
import { Reducer, useEffect, useReducer, useState } from "react"
import {
  CheckoutAction,
  CheckoutActionType,
  CheckoutForm,
  CheckoutFormFile,
  CheckoutModalMode,
  CheckoutState,
  CheckoutSummary,
  ErrorCheckPayment,
  FormErrorPayload,
  FormFileHandlerPayload,
  FormFileState,
  FormHandlerPayload,
  FormInitializerPayload,
  FormState,
  FormStatus,
  PaymentInitializerPayload,
  PaymentOptionInitializerPayload,
  University,
} from "../../../constants/interfaces/checkout-state"
import { AvailablePromo } from "../../../constants/interfaces/promo"
import { SelectOption } from "../../../constants/interfaces/ui"
import CheckoutService from "../../services/checkout.services"
import { checkoutFormHelper } from "../helper/CheckoutHelper"
import { useUserData } from "./useUserData"
import { SchoolBatch } from "../../../constants/interfaces/school"
import { Discount, PaymentOption } from "../../../constants/interfaces/checkout"

const blankFormState: FormState = {
  value: "",
  error: "",
}

const blankFormFileState: FormFileState = {
  name: "File...",
  error: "",
}

const form = {
  user_job: blankFormState,
  user_email: blankFormState,
  user_majors: blankFormState,
  user_faculty: blankFormState,
  user_fullname: blankFormState,
  user_university: blankFormState,
  user_linkedin_url: blankFormState,
  user_desired_job: blankFormState,
  user_phone_number: blankFormState,
  user_desired_job_sector: blankFormState,
  user_batch_request: blankFormState,
}

const form_file = {
  identity_card: blankFormFileState,
  ijazah: blankFormFileState,
  student_card: blankFormFileState,
  last_study_card: blankFormFileState,
  cv: blankFormFileState,
}

const initialState: CheckoutState = {
  //Initial state
  gross_price: 0,
  bank_method: [],
  e_wallet_method: [],
  available_promos: [],

  //form state
  isLoadingData: true,
  form: form,
  form_file: form_file,
  university: [],
  user_batch_request: [],
  form_status: "",
  form_status_error: "",

  //error state
  error_discount: "",
  error_submitting: "",
  error_admin_fee: "",
  error_checking: {
    form: "",
    paymentOption: "",
  },

  //state for payment
  admin_fee: 0,
  selected_method: "",
  selected_channel: "",
  total_price: 0,
  modal_open: "closed",
  payment_phone_number: "",

  //Promo
  promo_code: "",
  discount_amount: 0,
  promo_name: "",
  applied_promo_code: "",
}

const reducer: Reducer<CheckoutState, CheckoutAction> = (state, action) => {
  const { type, payload } = action

  switch (type) {
    // initializer
    case CheckoutActionType.PaymentInitializer: {
      const initPayload = payload as PaymentInitializerPayload
      return {
        ...state,
        gross_price: initPayload.gross_price,
        total_price: initPayload.total_price,
      }
    }

    case CheckoutActionType.FormInitializer: {
      const formPayload = payload as FormInitializerPayload
      return {
        ...state,
        form: formPayload.form,
        form_file: formPayload.form_file,
      }
    }

    case CheckoutActionType.UniversityInitializer: {
      const uniPayload = payload as University[]

      const uni: SelectOption[] = uniPayload.map((value) => {
        return {
          value: value.university_name,
          label: value.university_name,
        }
      })

      return {
        ...state,
        university: uni,
      }
    }

    case CheckoutActionType.PromoInitializer: {
      const promoPayload = payload as AvailablePromo[]

      return {
        ...state,
        available_promos: promoPayload,
      }
    }

    case CheckoutActionType.BatchInitializer: {
      const batchPayload = payload as SchoolBatch[]

      const batch: SelectOption[] = batchPayload.map((value) => {
        const formattedDate = (date: string) => dayjs(date).format("DD MMM")

        return {
          value: `${value.batch}`,
          label: `${value.batch} (Program dimulai: ${formattedDate(value.batch_start)}) `,
        }
      })

      return {
        ...state,
        user_batch_request: batch,
      }
    }

    case CheckoutActionType.PaymentOptionInitializer: {
      const paymentInitPayload = payload as PaymentOptionInitializerPayload
      return {
        ...state,
        bank_method: paymentInitPayload.bank_method,
        e_wallet_method: paymentInitPayload.e_wallet_method,
        credit_card: paymentInitPayload.credit_card,
      }
    }

    //  Form Handler
    case CheckoutActionType.FormHandler: {
      const formHandlerPayload = payload as FormHandlerPayload
      const name = formHandlerPayload.name

      return {
        ...state,
        form: {
          ...state.form,
          [name]: {
            value: formHandlerPayload.value,
            error: "",
          },
        },
      }
    }

    case CheckoutActionType.FormTypeHandler:
      return {
        ...state,
        form_status: payload as FormStatus,
        form_status_error: "",
      }

    case CheckoutActionType.FormFileHandler: {
      const formFilePayload = payload as FormFileHandlerPayload
      const fileTypeName = formFilePayload.name

      return {
        ...state,
        form_file: {
          ...state.form_file,
          [fileTypeName]: {
            ...state.form_file[fileTypeName],
            file: formFilePayload.file,
            error: "",
            name: formFilePayload.file.name,
          },
        },
      }
    }

    case CheckoutActionType.FormErrorHandler: {
      const formErrorPayload = payload as FormErrorPayload
      const formErrorName = formErrorPayload.name

      let errorForm = {}

      switch (true) {
        case formErrorPayload.type === "file":
          errorForm = {
            form_file: {
              ...state.form_file,
              [formErrorName]: {
                ...state.form_file[formErrorName],
                error: formErrorPayload.error,
              },
            },
          }
          break
        case formErrorPayload.type === "field" && formErrorName !== "form_status":
          errorForm = {
            form: {
              ...state.form,
              [formErrorName]: {
                ...state.form[formErrorName],
                error: formErrorPayload.error,
              },
            },
          }
          break
        default:
          errorForm = {
            form_status_error: formErrorPayload.error,
          }
          break
      }

      return {
        ...state,
        ...errorForm,
      }
    }

    //  Payment Handler
    case CheckoutActionType.ChoosePayment: {
      const selectOption = payload as PaymentOption
      return {
        ...state,
        total_price: state.gross_price + selectOption.payment_fee - state.discount_amount,
        admin_fee: selectOption.payment_fee,
        selected_channel: selectOption.payment_channel,
        selected_method: selectOption.payment_method,
      }
    }

    case CheckoutActionType.OpenModal: {
      const open = payload as CheckoutModalMode
      return {
        ...state,
        modal_open: open,
      }
    }

    case CheckoutActionType.ClosePromoModal:
      return {
        ...state,
        modal_open: "closed",
        error_discount: "",
        promo_code: "",
        applied_promo_code: "",
      }

    case CheckoutActionType.InputPromoCode:
      return {
        ...state,
        promo_code: action.payload as string,
        applied_promo_code: action.payload as string,
      }

    case CheckoutActionType.ApplyPromo: {
      const promoPayload = payload as Discount

      return {
        ...state,
        modal_open: "closed",
        promo_code: "",
        error_discount: "",
        discount_amount: promoPayload.discount_deal,
        promo_name: promoPayload.discount_name,
        total_price: state.gross_price + state.admin_fee - promoPayload.discount_deal,
      }
    }

    case CheckoutActionType.ErrorDiscount:
      return {
        ...state,
        error_discount: payload as string,
      }

    case CheckoutActionType.DeleteDiscount:
      return {
        ...state,
        discount_amount: 0,
        promo_name: "",
        total_price: state.gross_price + state.admin_fee,
      }

    case CheckoutActionType.ErrorSubmitting:
      return {
        ...state,
        error_submitting: payload as string,
      }

    case CheckoutActionType.ErrorChecking: {
      const errorCheck = payload as ErrorCheckPayment

      return {
        ...state,
        error_checking: {
          ...state.error_checking,
          [errorCheck.errorType]: errorCheck.message,
        },
      }
    }

    default:
      return state
  }
}

const useCheckout = (programId: number, summary: CheckoutSummary) => {
  const [checkoutState, checkoutDispatch] = useReducer(reducer, initialState)
  const [isLoading, setIsLoading] = useState(true)
  dayjs.locale("id")

  const { tokenData } = useUserData()
  const user_email = tokenData?.user_email
  const checkout = new CheckoutService()

  useEffect(() => {
    const getCheckoutData = async () => {
      checkoutDispatch({
        type: CheckoutActionType.PaymentInitializer,
        payload: {
          gross_price: summary?.program_price,
          total_price: summary?.program_price,
        },
      })

      try {
        const res = await checkout.getAvailablePayment(programId)
        if (res.isSuccess) {
          const data = res.getValue()

          checkoutDispatch({
            type: CheckoutActionType.PaymentOptionInitializer,
            payload: {
              bank_method: data.BANK_TRANSFER,
              e_wallet_method: data.EWALLET,
              ...(data.CREDIT_CARD && {
                credit_card: {
                  payment_method: "CREDIT_CARD",
                  payment_fee: data.CREDIT_CARD[0].payment_fee,
                  payment_channel: "",
                  payment_channel_logo: "",
                },
              }),
            },
          })
        }
      } catch (e) {
        if (process.env.NODE_ENV === "development") {
          console.log(e)
        }
      }
    }

    const getFormData = async () => {
      const university = checkout.getUniversities()
      const formRes = checkout.getCheckoutForm(programId)
      const orderData = checkout.getOrderDetail(programId, summary.school_slug)

      const [res1, res2, res3] = await Promise.all([university, formRes, orderData])

      if (res1.isSuccess) {
        checkoutDispatch({
          type: CheckoutActionType.UniversityInitializer,
          payload: res1.getValue(),
        })
      }

      if (res2.isSuccess) {
        const result = res2.getValue()

        if (!result) {
          checkoutDispatch({
            type: CheckoutActionType.FormInitializer,
            payload: {
              form: {
                ...form,
                user_email: {
                  value: user_email,
                  error: "",
                },
                user_fullname: {
                  value: tokenData.fullname ?? "",
                  error: "",
                },
                user_phone_number: {
                  value: tokenData.user_phone_number ?? "",
                  error: "",
                },
                user_batch_request: {
                  value: "",
                  error: "",
                },
              },
              form_file,
            },
          })
        } else {
          const { filledFileForm, filledForm, status } = checkoutFormHelper(result)

          checkoutDispatch({
            type: CheckoutActionType.FormInitializer,
            payload: {
              form: {
                ...(filledForm as CheckoutForm),
                user_linkedin_url: filledForm.user_linkedin_url ?? { value: "", error: "" },
                user_batch_request: { value: "", error: "" },
              },
              form_file: filledFileForm as CheckoutFormFile,
            },
          })

          checkoutDispatch({
            type: CheckoutActionType.FormTypeHandler,
            payload: status,
          })
        }
      }

      // if (res3.ok) {
      //   checkoutDispatch({
      //     type: CheckoutActionType.PromoInitializer,
      //     payload: res3.data as AvailablePromo[],
      //   })
      // }

      if (res3.isSuccess) {
        const res4Data = res3.getValue()

        checkoutDispatch({
          type: CheckoutActionType.BatchInitializer,
          payload: res4Data.available_batch,
        })

        checkoutDispatch({
          type: CheckoutActionType.FormHandler,
          payload: {
            name: "user_batch_request",
            value: res4Data.available_batch[0].batch.toString(),
          },
        })
      }

      setIsLoading(false)
    }

    if (programId) {
      getCheckoutData()
    }

    if (programId && tokenData && summary) {
      getFormData()
    }
  }, [programId, tokenData, summary])

  return {
    checkoutState,
    checkoutDispatch,
    isLoading,
  }
}
export default useCheckout
>>>>>>> 94ccec2 (setup fix):src/lib/utils/hooks/useCheckout.ts
