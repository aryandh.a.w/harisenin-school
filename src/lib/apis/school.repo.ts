import GotMethod from "./GotMethod"
import { SchoolType } from "../../constants/interfaces/school"

export default class SchoolRepo extends GotMethod {
  async schoolList(type: SchoolType) {
    return this.getPublicData(`/schools?type=${type.toLocaleLowerCase()}`)
  }
  async schoolDetail(slug) {
    return this.getPublicData(`/product/detail/${slug}?type=school`)
  }
}
