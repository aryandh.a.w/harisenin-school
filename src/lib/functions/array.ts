export function chunk(arr: any[], chunkSize = 1) {
  const tmp = [...arr]
  const cache = []

  if (arr.length < chunkSize) {
    return arr
  }

  while (tmp.length) cache.push(tmp.splice(0, chunkSize))
  return cache
}
