const path = require("path")
const withTM = require("next-transpile-modules")(["ky"])
const withPWA = require("next-pwa")
const runtimeCaching = require("next-pwa/cache")
const {
  BugsnagBuildReporterPlugin,
  BugsnagSourceMapUploaderPlugin,
} = require("webpack-bugsnag-plugins")
const packageJson = require("./package.json")

module.exports = withTM(
  withPWA({
    compiler: {
      removeConsole: process.env.NODE_ENV === "production",
    },
    generateEtags: false,
    poweredByHeader: false,
    pwa: {
      dest: "public",
      disable: process.env.NODE_ENV === "development",
      buildExcludes: [/middleware-manifest.json$/],
      runtimeCaching,
    },
    experimental: {
      images: {
        layoutRaw: true,
      },
    },
    images: {
      domains: [
        "cdn-beta.harisenin.com",
        "cdn.harisenin.com",
        "ui-avatars.com",
        "https://harisenin-storage-dev.s3.ap-southeast-1.amazonaws.com",
        "harisenin-storage-dev.s3.ap-southeast-1.amazonaws.com",
        "singlecolorimage.com",
      ],
      dangerouslyAllowSVG: true,
    },
    basePath: "/school",
    maximumFileSizeToCacheInBytes: 5000000,
    productionBrowserSourceMaps: true,
    rewrites: async () => {
      return [
        {
          source: "/program",
          destination: "/bootcamp",
        },
        {
          source: "/program/:slug",
          destination: "/bootcamp/:slug",
        },
        {
          source: "/prog",
          destination: "/bootcamp",
        },
        {
          source: "/proclass/proclass-testing-6n9ig",
          destination: "/proclass",
        },
        {
          source: "/proclass/proclass-professio-htzc9",
          destination: "/proclass",
        },
        {
          source: "/bootcamp/human-resources-iw6pf",
          destination: "/bootcamp/human-resources",
        },
      ]
    },
    webpack: (config) => {
      config.module.rules
        .filter((rule) => !!rule.oneOf)
        .forEach((rule) => {
          rule.oneOf
            .filter((oneOfRule) => {
              return oneOfRule.test
                ? oneOfRule.test.toString().includes("sass") &&
                    Array.isArray(oneOfRule.use) &&
                    oneOfRule.use.some((use) => use.loader.includes("sass-loader"))
                : false
            })
            .forEach((rule) => {
              rule.use = rule.use.map((useRule) => {
                if (useRule.loader.includes("sass-loader")) {
                  return {
                    ...useRule,
                    options: {
                      ...useRule.options,
                      additionalData: `$cdnUrl: '${process.env.HARISENIN_CDN_URL}';`,
                    },
                  }
                }
                return useRule
              })
            })
        })

<<<<<<< HEAD
    // if (process.env.HARISENIN_BUGSNAG_RELEASE_STAGE !== "local") {
    //   config.plugins.push(
    //     new BugsnagBuildReporterPlugin(
    //       {
    //         apiKey: process.env.HARISENIN_BUGSNAG_SERVER_API_KEY,
    //         appVersion: packageJson.version,
    //         releaseStage: process.env.HARISENIN_BUGSNAG_RELEASE_STAGE,
    //         autoAssignRelease: true,
    //       },
    //       {
    //         /* opts */
    //       }
    //     ),
    //     new BugsnagSourceMapUploaderPlugin({
    //       apiKey: process.env.HARISENIN_BUGSNAG_SERVER_API_KEY,
    //       publicPath: ".next",
    //       overwrite: true,
    //       ignoredBundleExtensions: [".css"],
    //     })
    //   )
    // }

    return config
  },
  sassOptions: {
    includePaths: [path.join(__dirname, "styles")],
  },
  env: {
    API_ENDPOINT: process.env.HARISENIN_API_ENDPOINT,
    PUBLIC_API_ENDPOINT: process.env.HARISENIN_PUBLIC_API_ENDPOINT,
    WSS_ENDPOINT: process.env.HARISENIN_WSS_ENDPOINT,
    HOME_URL: process.env.HARISENIN_HOME_URL,
    SCHOOL_URL: process.env.HARISENIN_SCHOOL_URL,
    RISEBINAR_URL: process.env.HARISENIN_RISEBINAR_URL,
    CDN_URL: process.env.HARISENIN_CDN_URL,
    SITEMAP_URL: process.env.HARISENIN_SITEMAP_URL,
    XENDIT_KEY: process.env.HARISENIN_XENDIT_KEY,
    LINK_TOKEN: process.env.HARISENIN_LINK_TOKEN,
    GOOGLE_CLIENT_ID: process.env.HARISENIN_GOOGLE_CLIENT_ID,
    COOKIES_DOMAIN: process.env.HARISENIN_COOKIES_DOMAIN,
    FACEBOOK_PIXEL: process.env.HARISENIN_FACEBOOK_PIXEL,
    GTM_ID: process.env.HARISENIN_GTM_ID,
    GA_TRACKING_ID: process.env.HARISENIN_GA_TRACKING_ID,
    GA4_TRACKING_ID: process.env.HARISENIN_GA4_TRACKING_ID,
    ANALYZE: process.env.HARISENIN_ANALYZE,
    GOOGLE_ADSENSE_ID: process.env.HARISENIN_GOOGLE_ADSENSE_ID,
  },
}
=======
      if (process.env.HARISENIN_BUGSNAG_RELEASE_STAGE !== "local") {
        config.plugins.push(
          new BugsnagBuildReporterPlugin(
            {
              apiKey: process.env.HARISENIN_BUGSNAG_SERVER_API_KEY,
              appVersion: packageJson.version,
              releaseStage: process.env.HARISENIN_BUGSNAG_RELEASE_STAGE,
              autoAssignRelease: true,
            },
            {
              /* opts */
            }
          ),
          new BugsnagSourceMapUploaderPlugin({
            apiKey: process.env.HARISENIN_BUGSNAG_SERVER_API_KEY,
            publicPath: ".next",
            overwrite: true,
            ignoredBundleExtensions: [".css"],
          })
        )
      }

      return config
    },
    sassOptions: {
      includePaths: [path.join(__dirname, "styles")],
    },
    serverRuntimeConfig: {
      // Will only be available on the server side
      API_ENDPOINT: process.env.HARISENIN_API_ENDPOINT,
      WSS_ENDPOINT: process.env.HARISENIN_WSS_ENDPOINT,
      HOME_URL: process.env.HARISENIN_HOME_URL,
      SCHOOL_URL: process.env.HARISENIN_SCHOOL_URL,
      RISEBINAR_URL: process.env.HARISENIN_RISEBINAR_URL,
      CDN_URL: process.env.HARISENIN_CDN_URL,
      SITEMAP_URL: process.env.HARISENIN_SITEMAP_URL,
      XENDIT_KEY: process.env.HARISENIN_XENDIT_KEY,
      BUGSNAG_API_KEY: process.env.HARISENIN_BUGSNAG_SERVER_API_KEY,
      BUGSNAG_CAPTURE_SESSIONS: process.env.HARISENIN_BUGSNAG_CAPTURE_SESSIONS === "true",
      BUGSNAG_RELEASE_STAGE: process.env.HARISENIN_BUGSNAG_RELEASE_STAGE,
      BUGSNAG_SEND_CODE: process.env.HARISENIN_BUGSNAG_SEND_CODE === "true",
      LINK_TOKEN: process.env.HARISENIN_LINK_TOKEN,
      PASSWORD_PROTECT: process.env.HARISENIN_PASSWORD_PROTECT === "true",
      GOOGLE_CLIENT_ID: process.env.HARISENIN_GOOGLE_CLIENT_ID,
      COOKIES_DOMAIN: process.env.HARISENIN_COOKIES_DOMAIN,
      FACEBOOK_PIXEL: process.env.HARISENIN_FACEBOOK_PIXEL,
      GTM_ID: process.env.HARISENIN_GTM_ID,
      GA_TRACKING_ID: process.env.HARISENIN_GA_TRACKING_ID,
      ANALYZE: process.env.HARISENIN_ANALYZE,
    },
    env: {
      API_ENDPOINT: process.env.HARISENIN_API_ENDPOINT,
      WSS_ENDPOINT: process.env.HARISENIN_WSS_ENDPOINT,
      HOME_URL: process.env.HARISENIN_HOME_URL,
      SCHOOL_URL: process.env.HARISENIN_SCHOOL_URL,
      RISEBINAR_URL: process.env.HARISENIN_RISEBINAR_URL,
      CDN_URL: process.env.HARISENIN_CDN_URL,
      SITEMAP_URL: process.env.HARISENIN_SITEMAP_URL,
      XENDIT_KEY: process.env.HARISENIN_XENDIT_KEY,
      BUGSNAG_API_KEY: process.env.HARISENIN_BUGSNAG_SERVER_API_KEY,
      BUGSNAG_CAPTURE_SESSIONS: process.env.HARISENIN_BUGSNAG_CAPTURE_SESSIONS,
      BUGSNAG_RELEASE_STAGE: process.env.HARISENIN_BUGSNAG_RELEASE_STAGE,
      BUGSNAG_SEND_CODE: process.env.HARISENIN_BUGSNAG_SEND_CODE,
      LINK_TOKEN: process.env.HARISENIN_LINK_TOKEN,
      PASSWORD_PROTECT: process.env.HARISENIN_PASSWORD_PROTECT,
      GOOGLE_CLIENT_ID: process.env.HARISENIN_GOOGLE_CLIENT_ID,
      COOKIES_DOMAIN: process.env.HARISENIN_COOKIES_DOMAIN,
      FACEBOOK_PIXEL: process.env.HARISENIN_FACEBOOK_PIXEL,
      GTM_ID: process.env.HARISENIN_GTM_ID,
      GA_TRACKING_ID: process.env.HARISENIN_GA_TRACKING_ID,
      ANALYZE: process.env.HARISENIN_ANALYZE,
    },
  })
)
>>>>>>> 6cd4eae (setup fix)
