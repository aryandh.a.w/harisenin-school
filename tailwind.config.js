const plugin = require("tailwindcss/plugin")

module.exports = {
  // @see https://tailwindcss.com/docs/upcoming-changes
  future: {
    removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true,
  },
  // important: true,
  theme: {
    colors: {
      white: "#fff",
      black: "#000",
      "secondary-black": "#231F20",
      "black-48": "#484848",
      transparent: "transparent",
      green: "#0DAD8E",
      "green-50": "rgba(13, 173, 142, 0.5)",
      blue: "#0038DE",
      red: "#ef2547",
      "red-50": "rgba(239, 37, 71, 0.5)",
      "red-20": "rgba(239, 37, 71, 0.2)",
      yellow: "#FCBC2A",
      orange: "#DE4632",
      "primary-grey": "#F6F7F7", // HSL
      "primary-blue": "#00295F", // Navy
      "secondary-blue": "#0D015C", // Secondary Navy
      "oxford-blue": "#253238", // dark-grey
      maroon: "#BF3B53",
      "secondary-orange": "#FFBF9D",
      "black-22": "#222325",
      "grey-c4": "#C4C4C4",
      "grey-a7": "#a7a7a7",
      "grey-ec": "#ECECEC",
      "grey-90": "#909090",
      "grey-97": "#979797",
      "grey-f7": "#f7f7f7",
      "grey-f9": "#F9FAFC",
      "grey-69": "#696969",
      "grey-normal": "rgba(151, 151, 151, 0.1)",
      "grey-68": "rgba(58, 53, 65, 0.68)",
      "light-grey": "#909090",
      banner: "#00295FB2",
      overlay: "rgba(246, 247, 247, 0.2)",
      backdrop: "rgba(35, 31, 32, 0.7)",
      facebook: "#4267b2",
      whatsapp: "#4ac959",
      linkedin: "#2867b2",
      twitter: "#1da1f2",
      telegram: "#0088cc",
    },
    fontFamily: {
      sans: ["DM Sans"],
    },
    // fontSize: {
    // xxs: "8px",
    // xs: "10px",
    // sm: "12px",
    // base: "14px",
    // md: "16px",
    // lg: "18px",
    // xl: "20px",
    // "1.5xl": "22px",
    // "2xl": "24px",
    // "2.5xl": "28px",
    // "3xl": "32px",
    // "3.5xl": "34px",
    // "4xl": "36px",
    // "4.5xl": "38px",
    // title: "42px",
    // },
    borderWidth: {
      DEFAULT: "1px",
      0: "0",
      2: "2px",
      3: "3px",
      4: "4px",
      6: "6px",
      8: "8px",
    },
    borderRadius: {
      none: "0",
      DEFAULT: "4px",
      sm: "2px",
      md: "0.375rem",
      lg: "8px",
      10: "10px",
      "2xl": "16px",
      "3xl": "24px",
      full: "9999px",
      large: "12px",
    },
    transitionProperty: {
      height: "height",
      "max-height": "max-height",
      padding: "padding",
      margin: "margin",
      spacing: "margin, padding",
    },
    extend: {
      spacing: {
        4.5: "18px",
        15: "60px",
      },
    },
  },
  variants: {},
  plugins: [
    require("tailwindcss"),
    require("autoprefixer"),
    require("@ky-is/tailwindcss-plugin-width-height")({ variants: ["responsive"] }),
    plugin(function ({ addBase, theme }) {
      addBase({
        body: {
          color: theme("colors.black-48"),
        },
      })
    }),
    plugin(function ({ addVariant }) {
      addVariant("sm-only", "@media screen and (max-width: theme('screens.sm'))") // instead of hard-coded 640px use sm breakpoint value from config. Or anything
    }),
  ],
  content: ["./src/**/*.{js,ts,jsx,tsx}"],
}
