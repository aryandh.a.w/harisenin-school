const normalizeSrc = src => {
  return src.startsWith('/') ? src.slice(1) : src;
};

export default function cloudflareLoader ({ src, width, quality }) {
  const encodedURI = src.startsWith('http') ? encodeURIComponent(src) : src 
  const params = [`width=${width}`];
  if (quality) {
    params.push(`quality=${quality}`);
  }
  const paramsString = params.join(',');

  if (process.env.NODE_ENV !== "production") {
    return `${process.env.HOME_URL}/cdn-cgi/image/${paramsString}/${normalizeSrc(encodedURI)}`;
  }

  return `/cdn-cgi/image/${paramsString}/${normalizeSrc(encodedURI)}`;
};