#!/bin/sh

# setup PATH
export PATH="$HOME/.nodenv/shims:$PATH"
export PATH="$HOME/.phpenv/shims:$PATH"

# clean first
rm -rf standalone
rm -rf dist

# install package.json
if [ -f yarn.lock ]; then yarn --frozen-lockfile; \
elif [ -f package-lock.json ]; then npm ci; \
elif [ -f pnpm-lock.yaml ]; then yarn global add pnpm && pnpm i; \
else echo "Lockfile not found." && exit 1; \
fi

# build
yarn build

# copy standalone resource to dist folder
cp -r .next/standalone ./
# copy .next/static
cp -r .next/static standalone/.next/
# copy public folder
cp -r public standalone/

# install postmark
yarn add postmark
yarn add dotenv

# cleanup
rm -rf .next
rm -rf standalone/tmp
mkdir standalone/tmp
touch standalone/tmp/restart.txt

# notify via email
node notify.js