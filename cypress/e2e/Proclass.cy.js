describe("proclass", () => {
  it("validasi komponen", () => {
    cy.viewport(1920, 1080)
    cy.visit("https://www.harisenin.net/school/proclass")
    cy.get("#password").type("gwsudahbahagia{enter}")
    cy.contains("Program").should("exist")
    cy.contains("Menjadi Tutor").should("exist")
    cy.get("#btn-regist").should("exist")
    cy.get("#btn-login").should("exist")

    cy.get("#hms-search-field").should("exist")
    cy.get("#btn-search-hms").should("exist")

    cy.get("button").contains("BOOTCAMP").should("exist")
    cy.get("button").contains("PROCLASS").should("exist")
    cy.get("#program-card-proclass-1").should("exist")

    cy.get("footer").should("exist")
    cy.contains("PT.Karya Kaya Bahagia. 2020").should("exist")
  })

  it("proses login", () => {
    cy.viewport(1920, 1080)
    cy.visit("https://www.harisenin.net/")
    cy.get("#password").type("gwsudahbahagia{enter}")
    cy.get("#btn-login").click()

    cy.get('input[type="text"]')
      .type("vanessa.angelica@student.umn.ac.id")
      .should("have.value", "vanessa.angelica@student.umn.ac.id")
    cy.get('input[type="password"]').type("vannes88")
    cy.get("#btn-login-modal").click()
  })

  it("masuk ke proclass", () => {
    cy.viewport(1920, 1080)
    cy.visit("https://www.harisenin.net/")
    cy.get("#password").type("gwsudahbahagia{enter}")
    cy.get("#btn-all-program-proclass").click().wait(500)
    cy.visit("https://www.harisenin.net/school/proclass")
  })

  it("pencarian proclass", () => {
    cy.viewport(1920, 1080)
    cy.get("#hms-search-field").click().type("Human")
    cy.get("#btn-search-hms").click()
    cy.get("#program-card-proclass-0").click()

    // tidak ditemukan bootcamp
    cy.visit("https://www.harisenin.net/school/proclass")
    cy.get("#password").type("gwsudahbahagia{enter}")
    cy.get("#hms-search-field").click().type("abcde")
    cy.get("#btn-search-hms").click()
  })

  it("BOOTCAMP", () => {
    cy.viewport(1920, 1080)
    cy.visit("https://www.harisenin.net/school/proclass")
    cy.get("#password").type("gwsudahbahagia{enter}")
    cy.get("button").contains("BOOTCAMP").click()
  })

  it("testing proclass", () => {
    cy.viewport(1920, 1080)
    cy.visit("https://www.harisenin.net/school/proclass")
    cy.get("#password").type("gwsudahbahagia{enter}")
    cy.get("#program-card-proclass-1").click()
    cy.get("#btn-regist-class-banner").click()

    cy.get("#btn-share-hms").click()
    cy.get(".program_program_page__share__row__YB7SB").should("exist")
    cy.get(".program_program_page__share__link__XRxzJ").should("exist")
    cy.visit("https://www.harisenin.net/school/proclass/human-resources-development")

    // Kurikulum
    cy.get("#curriculum-0").click().wait(1000)
    // cy.get("#curriculum-1").click()
    // cy.get("#curriculum-2").click()
    // cy.get("#curriculum-3").click()
    // cy.get("#curriculum-4").click()
    // cy.get("#curriculum-5").click()
    // cy.get("#curriculum-6").click()
    // cy.get("#curriculum-7").click()
    // cy.get("#curriculum-8").click()
    // cy.get("#curriculum-9").click()

    // download silabus
    cy.get("#btn-login").click()
    cy.get('input[type="text"]')
      .type("vanessa.angelica@student.umn.ac.id")
      .should("have.value", "vanessa.angelica@student.umn.ac.id")
    cy.get('input[type="password"]').type("vannes88")
    cy.get("#btn-login-modal").click().wait(1000)
    cy.get("#btn-download-syllabus").click().wait(1000)
    cy.get('input[type="text"]').eq(1).click().clear().type("angelica")
    cy.get("textarea").type("testing download silabus bootcamp harisenin")
    cy.get("button").contains("Download Silabus").click({ force: true })

    // slider tutor
    // cy.visit("https://www.harisenin.net/school/proclass/proclass-testing-6n9ig")
    // cy.get(".slider_custom_carousel_button__next-arrow__78WKM").first().click()
    // cy.get(".slider_custom_carousel_button__prev-arrow__vpINq").first().click()

    // pilihan kelas
    cy.get("#investment-card-0").click({ force: true })
    cy.visit("https://www.harisenin.net/school/proclass/human-resources-development")

    cy.contains("Hubungi Kami").should("exist")
    cy.get("footer").should("exist")
    cy.contains("PT.Karya Kaya Bahagia. 2020").should("exist")
  })
})
