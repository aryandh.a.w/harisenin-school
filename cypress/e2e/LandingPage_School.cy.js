describe("Landing Page School", () => {
  it("validasi komponen", () => {
    cy.viewport(1920, 1080)
    cy.visit("https://www.harisenin.net/school").wait(800)
    cy.get("#password").type("gwsudahbahagia{enter}")
    cy.contains("Program").should("exist")
    cy.contains("Menjadi Tutor").should("exist")
    cy.get("#btn-regist").should("exist")
    cy.get("#btn-login").should("exist")

    cy.get("#header2").should("exist")
    cy.get("div")
      .contains(
        "Pelajari keahlian dan rahasia sukses untuk mewujudkan karir dan masa depan impianmu"
      )
      .should("exist")
    cy.get(".landing-page_landing_page__company__column__0DxoV").should("exist")
    cy.get(".landing-page_landing_page__testimonial__O_PVE").should("exist")

    cy.get("footer").should("exist")
    cy.contains("PT.Karya Kaya Bahagia. 2020").should("exist")
  })

  it("masuk ke school", () => {
    cy.viewport(1920, 1080)
    cy.visit("https://www.harisenin.net")
    cy.get("#password").type("gwsudahbahagia{enter}")
    cy.contains("Temukan Bootcamp Yang Tepat Untukmu").click({ force: true })
    cy.go("back")
  })

  it("testing fitur navbar", () => {
    cy.viewport(1920, 1080)
    cy.visit("https://www.harisenin.net/school")
    cy.get("#password").type("gwsudahbahagia{enter}")
    cy.contains("Program").click()
    cy.get("#dropdown-hms").should("exist").click()
    cy.go("back")
    cy.contains("Program").click()
    cy.get("#dropdown-proclass").should("exist").click().wait(500)
    cy.go("back")
    cy.contains("Program").click()
    cy.get("#dropdown-event").should("exist").click().wait(500)
    cy.go("back")

    cy.get("a").contains("Menjadi Tutor").should("exist").click()
  })

  it("proses login", () => {
    cy.viewport(1920, 1080)
    cy.visit("https://www.harisenin.net/")
    cy.get("#password").type("gwsudahbahagia{enter}")
    cy.get("#btn-login").click()

    cy.get('input[type="text"]')
      .type("vanessa.angelica@student.umn.ac.id")
      .should("have.value", "vanessa.angelica@student.umn.ac.id")
    cy.get('input[type="password"]').type("vannes88")
    cy.get("#btn-login-modal").click()
  })

  it("testing bootcamp", () => {
    cy.viewport(1920, 1080).wait(500)
    cy.get("#password").type("gwsudahbahagia{enter}")
    cy.get("#program-card-bootcamp-0").should("exist").click()
    cy.visit("https://www.harisenin.net/school")
    cy.get("#program-card-bootcamp-1").should("exist").click()
    cy.visit("https://www.harisenin.net/school")
    cy.get("#program-card-bootcamp-2").should("exist").click()

    cy.visit("https://www.harisenin.net/school")
    cy.get(".slider_custom_carousel_button__next-arrow__78WKM ").eq(0).click()
    cy.get(".slider_custom_carousel_button__next-arrow__78WKM ").eq(0).click().wait(300)
    cy.get(".slider_custom_carousel_button__next-arrow__78WKM ").eq(0).click().wait(300)
    cy.get(".slider_custom_carousel_button__next-arrow__78WKM ").eq(0).click().wait(300)

    cy.get("#program-card-bootcamp-3").should("exist").click()
    cy.visit("https://www.harisenin.net/school")
    cy.get("#program-card-bootcamp-4").should("exist").click()
    cy.visit("https://www.harisenin.net/school")
    cy.get("#program-card-bootcamp-5").should("exist").click()

    cy.visit("https://www.harisenin.net/school")
    cy.get(".slider_custom_carousel_button__next-arrow__78WKM ").eq(0).click().wait(300)
    cy.get(".slider_custom_carousel_button__next-arrow__78WKM ").eq(0).click().wait(300)
    cy.get("#program-card-bootcamp-6").should("exist").click()
    cy.visit("https://www.harisenin.net/school")
    cy.get("#program-card-bootcamp-7").should("exist").click()
    cy.visit("https://www.harisenin.net/school")
    cy.get(".slider_custom_carousel_button__next-arrow__78WKM ").eq(0).click().wait(300)
    cy.get(".slider_custom_carousel_button__prev-arrow__vpINq")
      .eq(0)
      .click({ force: true })
      .wait(300)

    cy.get("#btn-all-program-bootcamp").click()
  })

  it("testing proclass", () => {
    cy.viewport(1920, 1080)
    cy.visit("https://www.harisenin.net/school")
    cy.get("#password").type("gwsudahbahagia{enter}").wait(800)
    // cy.get("#program-card-proclass-0").should("exist").click()
    cy.visit("https://www.harisenin.net/school")
    cy.get("#program-card-proclass-1").should("exist").click()

    cy.get("#btn-all-program-proclass").click()
  })

  it("testing faq", () => {
    cy.viewport(1920, 1080)
    cy.visit("https://www.harisenin.net/school")
    cy.get("#password").type("gwsudahbahagia{enter}")
    cy.get(".faq-home_chevron").click({ multiple: true })
  })

  it("validasi hubungi kami", () => {
    cy.viewport(1920, 1080)
    cy.visit("https://www.harisenin.net/school")
    cy.get("#password").type("gwsudahbahagia{enter}")
    cy.get("a").contains("Hubungi Kami").should("exist")
  })
})
