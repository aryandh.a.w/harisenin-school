describe("Testing kelas bootcamp 3", () => {
  it("testing bootcamp Full-Stack Web Developer", () => {
    cy.viewport(1920, 1080)
    cy.visit("https://www.harisenin.net/school/bootcamp")
    cy.get("#password").type("gwsudahbahagia{enter}").wait(500)
    cy.get("#program-card-bootcamp-5").click()
    cy.get("#btn-regist-class-banner").click()

    cy.get("#btn-share-hms").click()
    cy.get(".program_program_page__share__row__YB7SB").should("exist")
    cy.get(".program_program_page__share__link__XRxzJ").should("exist")
    cy.visit("https://www.harisenin.net/school/bootcamp/full-stack-web-developer")

    // Kurikulum
    cy.get("#curriculum-0").click().wait(1000)
    cy.get("#curriculum-1").click()
    cy.get("#curriculum-2").click()
    cy.get("#curriculum-3").click()
    cy.get("#curriculum-4").click()
    cy.get("#curriculum-5").click()
    cy.get("#curriculum-6").click()
    cy.get("#curriculum-7").click()
    cy.get("#curriculum-8").click()
    cy.get("#curriculum-9").click()

    // download silabus
    cy.get("#btn-login").click()
    cy.get('input[type="text"]')
      .type("vanessa.angelica@student.umn.ac.id")
      .should("have.value", "vanessa.angelica@student.umn.ac.id")
    cy.get('input[type="password"]').type("vannes88")
    cy.get("#btn-login-modal").click().wait(1000)
    cy.get("#btn-download-syllabus").click()
    cy.get('input[type="text"]').eq(1).click().clear().type("angelica")
    cy.get("textarea").type("testing download silabus bootcamp harisenin")
    cy.get("button").contains("Download Silabus").click({ force: true })

    // slider tutor
    cy.visit("https://www.harisenin.net/school/bootcamp/full-stack-web-developer")
    cy.get(".slider_custom_carousel_button__next-arrow__78WKM").first().click()
    cy.get(".slider_custom_carousel_button__prev-arrow__vpINq").first().click()

    // pilihan kelas
    cy.get("#investment-card-0").click({ force: true })
    cy.visit("https://www.harisenin.net/school/bootcamp/full-stack-web-developer")
    cy.get("#investment-card-1").click({ force: true })
    cy.visit("https://www.harisenin.net/school/bootcamp/full-stack-web-developer")
    cy.get("#investment-card-2").click({ force: true })
    cy.visit("https://www.harisenin.net/school/bootcamp/full-stack-web-developer")
    cy.get("#investment-card-3").click({ force: true })
    cy.visit("https://www.harisenin.net/school/bootcamp/full-stack-web-developer")
    // cy.get("#investment-card-4").click({ force: true })
    // cy.visit("https://www.harisenin.net/school/bootcamp/full-stack-web-developer")

    cy.get(".slider_custom_carousel_button__next-arrow__78WKM").eq(1).click({ force: true })
    cy.get(".slider_custom_carousel_button__prev-arrow__vpINq").eq(1).click({ force: true })

    cy.get(".faq-home_chevron").click({ multiple: true })
    cy.get(".program_program_page__related__card__j40K0").should("exist")

    cy.get(".slider_custom_carousel_button__next-arrow__78WKM").eq(2).click()
    cy.get(".slider_custom_carousel_button__prev-arrow__vpINq").eq(2).click()

    cy.contains("Hubungi Kami").should("exist")
    cy.get("footer").should("exist")
    cy.contains("PT.Karya Kaya Bahagia. 2020").should("exist")
  })

  it("testing bootcamp Big 4 Auditor Financial Analyst", () => {
    cy.viewport(1920, 1080)
    cy.visit("https://www.harisenin.net/school/bootcamp")
    cy.get("#password").type("gwsudahbahagia{enter}").wait(1000)
    cy.contains("Muat Lainnya").click()
    cy.get("#program-card-bootcamp-6").click()
    cy.get("#btn-regist-class-banner").click()

    cy.get("#btn-share-hms").click()
    cy.get(".program_program_page__share__row__YB7SB").should("exist")
    cy.get(".program_program_page__share__link__XRxzJ").should("exist")
    cy.visit("https://www.harisenin.net/school/bootcamp/big-4-auditor-financial-analyst")

    // Kurikulum
    cy.get("#curriculum-0").click().wait(1000)
    cy.get("#curriculum-1").click()
    cy.get("#curriculum-2").click()
    cy.get("#curriculum-3").click()
    cy.get("#curriculum-4").click()
    cy.get("#curriculum-5").click()
    cy.get("#curriculum-6").click()
    cy.get("#curriculum-7").click()
    cy.get("#curriculum-8").click()

    // download silabus
    cy.get("#btn-login").click()
    cy.get('input[type="text"]')
      .type("vanessa.angelica@student.umn.ac.id")
      .should("have.value", "vanessa.angelica@student.umn.ac.id")
    cy.get('input[type="password"]').type("vannes88")
    cy.get("#btn-login-modal").click().wait(1000)
    cy.get("#btn-download-syllabus").click().wait(1000)
    cy.get('input[type="text"]').eq(1).click().clear().type("angelica")
    cy.get("textarea").type("testing download silabus bootcamp harisenin")
    cy.get("button").contains("Download Silabus").click({ force: true })

    // slider tutor
    cy.visit("https://www.harisenin.net/school/bootcamp/big-4-auditor-financial-analyst")
    cy.get(".slider_custom_carousel_button__next-arrow__78WKM").first().click()
    cy.get(".slider_custom_carousel_button__prev-arrow__vpINq").first().click()

    // pilihan kelas
    cy.get("#investment-card-0").click({ force: true })
    cy.visit("https://www.harisenin.net/school/bootcamp/big-4-auditor-financial-analyst")
    cy.get("#investment-card-1").click({ force: true })
    cy.visit("https://www.harisenin.net/school/bootcamp/big-4-auditor-financial-analyst")
    cy.get("#investment-card-2").click({ force: true })
    cy.visit("https://www.harisenin.net/school/bootcamp/big-4-auditor-financial-analyst")
    cy.get("#investment-card-3").click({ force: true })
    cy.visit("https://www.harisenin.net/school/bootcamp/big-4-auditor-financial-analyst")
    cy.get("#investment-card-4").click({ force: true })
    cy.visit("https://www.harisenin.net/school/bootcamp/big-4-auditor-financial-analyst")

    cy.get(".slider_custom_carousel_button__next-arrow__78WKM").eq(1).click({ force: true })
    cy.get(".slider_custom_carousel_button__prev-arrow__vpINq").eq(1).click({ force: true })

    cy.get(".faq-home_chevron").click({ multiple: true })
    cy.get(".program_program_page__related__card__j40K0").should("exist")

    cy.get(".slider_custom_carousel_button__next-arrow__78WKM").eq(2).click()
    cy.get(".slider_custom_carousel_button__prev-arrow__vpINq").eq(2).click()

    cy.contains("Hubungi Kami").should("exist")
    cy.get("footer").should("exist")
    cy.contains("PT.Karya Kaya Bahagia. 2020").should("exist")
  })

  it("testing bootcamp Professional Sales", () => {
    cy.viewport(1920, 1080)
    cy.visit("https://www.harisenin.net/school/bootcamp")
    cy.get("#password").type("gwsudahbahagia{enter}").wait(500)
    cy.contains("Muat Lainnya").click()
    cy.get("#program-card-bootcamp-7").click()
    cy.get("#btn-regist-class-banner").click()

    cy.get("#btn-share-hms").click()
    cy.get(".program_program_page__share__row__YB7SB").should("exist")
    cy.get(".program_program_page__share__link__XRxzJ").should("exist")
    cy.visit("https://www.harisenin.net/school/bootcamp/professional-sales")

    // Kurikulum
    cy.get("#curriculum-0").click().wait(1000)
    cy.get("#curriculum-1").click()
    cy.get("#curriculum-2").click()
    cy.get("#curriculum-3").click()
    cy.get("#curriculum-4").click()
    cy.get("#curriculum-5").click()
    cy.get("#curriculum-6").click()
    cy.get("#curriculum-7").click()
    cy.get("#curriculum-8").click()
    cy.get("#curriculum-9").click()

    // download silabus
    cy.get("#btn-login").click()
    cy.get('input[type="text"]')
      .type("vanessa.angelica@student.umn.ac.id")
      .should("have.value", "vanessa.angelica@student.umn.ac.id")
    cy.get('input[type="password"]').type("vannes88")
    cy.get("#btn-login-modal").click().wait(1000)
    cy.get("#btn-download-syllabus").click().wait(1000)
    cy.get('input[type="text"]').eq(1).click().clear().type("angelica")
    cy.get("textarea").type("testing download silabus bootcamp harisenin")
    cy.get("button").contains("Download Silabus").click({ force: true })

    cy.visit("https://www.harisenin.net/school/bootcamp/professional-sales")
    cy.get(".slider_custom_carousel_button__next-arrow__78WKM").first().click({ force: true })
    cy.get(".slider_custom_carousel_button__prev-arrow__vpINq").first().click({ force: true })

    // pilihan kelas
    cy.get("#investment-card-0").click({ force: true })
    cy.visit("https://www.harisenin.net/school/bootcamp/professional-sales")
    cy.get("#investment-card-1").click({ force: true })
    cy.visit("https://www.harisenin.net/school/bootcamp/professional-sales")
    cy.get("#investment-card-2").click({ force: true })
    cy.visit("https://www.harisenin.net/school/bootcamp/professional-sales")
    cy.get("#investment-card-3").click({ force: true })
    cy.visit("https://www.harisenin.net/school/bootcamp/professional-sales")
    cy.get("#investment-card-4").click({ force: true })
    cy.visit("https://www.harisenin.net/school/bootcamp/professional-sales")

    cy.get(".slider_custom_carousel_button__next-arrow__78WKM").eq(1).click({ force: true })
    cy.get(".slider_custom_carousel_button__prev-arrow__vpINq").eq(1).click({ force: true })

    cy.get(".faq-home_chevron").click({ multiple: true })
    cy.get(".program_program_page__related__card__j40K0").should("exist")

    // cy.get(".slider_custom_carousel_button__next-arrow__78WKM").eq(2).click()
    // cy.get(".slider_custom_carousel_button__prev-arrow__vpINq").eq(2).click()

    cy.contains("Hubungi Kami").should("exist")
    cy.get("footer").should("exist")
    cy.contains("PT.Karya Kaya Bahagia. 2020").should("exist")
  })
})
