describe("TC_PEMBAYARAN", () => {
  // bootcamp
  it("proses login", () => {
    cy.viewport(1920, 1080)
    cy.visit("https://www.harisenin.net/")
    cy.get("#password").type("gwsudahbahagia{enter}")
    cy.get("#btn-login").click()

    cy.get('input[type="text"]')
      .type("vanessa.angelica@student.umn.ac.id")
      .should("have.value", "vanessa.angelica@student.umn.ac.id")
    cy.get('input[type="password"]').type("vannes88")
    cy.get("#btn-login-modal").click()
  })

  it("proses checkout Human Resource", () => {
    cy.viewport(1920, 1080)
    cy.visit("https://www.harisenin.net/school/")
    cy.get("#password").type("gwsudahbahagia{enter}")
    cy.contains("Human Resource").click()
    cy.contains("Daftar Sekarang").click({ force: true })
  })

  it("isi form checkout", () => {
    cy.viewport(1920, 1080)
    cy.get("body").then(($body) => {
      if ($body.find("#btn-login").length > 0) {
        cy.get('input[type="text"]')
          .type("vanessa.angelica@student.umn.ac.id")
          .should("have.value", "vanessa.angelica@student.umn.ac.id")
        cy.get('input[type="password"]').type("vannes88")
        cy.get("#btn-login-modal").click()
        cy.get("#password").type("gwsudahbahagia{enter}")
        cy.contains("Daftar Sekarang").click({ force: true })
      }
    })
    cy.get("#name-field").clear().type("Vanessa Angelica")
    cy.get("#phone-number-field").clear().type("081212255555")
    cy.contains("Karyawan").click().type("Belum Bekerja{enter}")
    cy.get("#job-sector-field")
      .clear()
      .type("Information Technology{enter}")
      .type("Human Resource Manager")
    cy.get("#link-linkedin-field")
      .clear()
      .type("https://www.linkedin.com/in/vanessa-angelica-7662a6162/")
  })

  it("validasi upload file", () => {
    cy.viewport(1920, 1080)
    cy.contains("Scan Ijazah Terakhir").should("exist")
    cy.contains("Scan KTP/KTM*").should("exist")
    cy.contains("CV").should("exist")
  })

  it("TC_PEMBAYARAN_001", () => {
    cy.viewport(1920, 1080)
    cy.get("#btn-bca-hms").click()
    cy.get("#hms-promo-field").should("exist")
    cy.get("#btn-buy-now-hms").click()
  })

  it("TC_PEMBAYARAN_002", () => {
    cy.viewport(1920, 1080)
    cy.get("#btn-bni-hms").click()
    cy.get("#hms-promo-field").should("exist")
    cy.get("#btn-buy-now-hms").click()
  })

  it("TC_PEMBAYARAN_003", () => {
    cy.viewport(1920, 1080)
    cy.get("#btn-bri-hms").click()
    cy.get("#hms-promo-field").should("exist")
    cy.get("#btn-buy-now-hms").click()
  })

  it("TC_PEMBAYARAN_004", () => {
    cy.viewport(1920, 1080)
    cy.get("#btn-mandiri-hms").click()
    cy.get("#hms-promo-field").should("exist")
    cy.get("#btn-buy-now-hms").click()
  })

  it("TC_PEMBAYARAN_005", () => {
    cy.viewport(1920, 1080)
    cy.get("#btn-id_ovo-hms").click()
    cy.get("#hms-promo-field").should("exist")
    cy.get("#btn-buy-now-hms").click()
  })

  it("TC_PEMBAYARAN_006", () => {
    cy.viewport(1920, 1080)
    cy.get("#btn-id_dana-hms").click()
    cy.get("#hms-promo-field").should("exist")
    cy.get("#btn-buy-now-hms").click()
  })

  it("TC_PEMBAYARAN_007", () => {
    cy.viewport(1920, 1080)
    cy.get("#btn-id_linkaja-hms").click()
    cy.get("#hms-promo-field").should("exist")
    cy.get("#btn-buy-now-hms").click()
  })

  it("TC_PEMBAYARAN_008", () => {
    cy.viewport(1920, 1080)
    cy.get("#btn-id_shopeepay-hms").click()
    cy.get("#hms-promo-field").should("exist")
    cy.get("#btn-buy-now-hms").click()
  })

  it("TC_PEMBAYARAN_009", () => {
    cy.viewport(1920, 1080)
    cy.get("#btn-cc-hms").click()
    cy.get("#hms-promo-field").should("exist")
    cy.get("#btn-buy-now-hms").click()
  })

  // proclass
  it("proses login", () => {
    cy.viewport(1920, 1080)
    cy.visit("https://www.harisenin.net/")
    cy.get("#password").type("gwsudahbahagia{enter}")
    cy.get("#btn-login").click()

    cy.get('input[type="text"]')
      .type("vanessa.angelica@student.umn.ac.id")
      .should("have.value", "vanessa.angelica@student.umn.ac.id")
    cy.get('input[type="password"]').type("vannes88")
    cy.get("#btn-login-modal").click()
  })

  it("proses checkout Human Resources Development", () => {
    cy.viewport(1920, 1080)
    cy.visit("https://www.harisenin.net/school/")
    cy.get("#password").type("gwsudahbahagia{enter}")
    cy.contains("Human Resources Development").click()
    cy.contains("Daftar Sekarang").click({ force: true })
  })

  it("isi form checkout", () => {
    cy.viewport(1920, 1080)
    cy.get("body").then(($body) => {
      if ($body.find("#btn-login").length > 0) {
        cy.get('input[type="text"]')
          .type("vanessa.angelica@student.umn.ac.id")
          .should("have.value", "vanessa.angelica@student.umn.ac.id")
        cy.get('input[type="password"]').type("vannes88")
        cy.get("#btn-login-modal").click()
        cy.get("#password").type("gwsudahbahagia{enter}")
        cy.contains("Daftar Sekarang").click({ force: true })
      }
    })
    cy.get("#name-field").clear().type("Vanessa Angelica")
    cy.get("#phone-number-field").clear().type("081212255555")
    cy.contains("Karyawan").click().type("Belum Bekerja{enter}")
    cy.get("#job-sector-field")
      .clear()
      .type("Information Technology{enter}")
      .type("Human Resource Manager")
    cy.get("#link-linkedin-field")
      .clear()
      .type("https://www.linkedin.com/in/vanessa-angelica-7662a6162/")
  })

  it("TC_PEMBAYARAN_001", () => {
    cy.viewport(1920, 1080)
    cy.get("#btn-bca-hms").click()
    cy.get("#hms-promo-field").should("exist")
    cy.get("#btn-buy-now-hms").click()
  })

  it("TC_PEMBAYARAN_002", () => {
    cy.viewport(1920, 1080)
    cy.get("#btn-bni-hms").click()
    cy.get("#hms-promo-field").should("exist")
    cy.get("#btn-buy-now-hms").click()
  })

  it("TC_PEMBAYARAN_003", () => {
    cy.viewport(1920, 1080)
    cy.get("#btn-bri-hms").click()
    cy.get("#hms-promo-field").should("exist")
    cy.get("#btn-buy-now-hms").click()
  })

  it("TC_PEMBAYARAN_004", () => {
    cy.viewport(1920, 1080)
    cy.get("#btn-mandiri-hms").click()
    cy.get("#hms-promo-field").should("exist")
    cy.get("#btn-buy-now-hms").click()
  })

  // it("TC_PEMBAYARAN_005", () => {
  //   cy.viewport(1920, 1080)
  //   cy.get("#btn-id_ovo-hms").click()
  //   cy.get("#hms-promo-field").should("exist")
  //   cy.get("#btn-buy-now-hms").click()
  // })

  // it("TC_PEMBAYARAN_006", () => {
  //   cy.viewport(1920, 1080)
  //   cy.get("#btn-id_dana-hms").click()
  //   cy.get("#hms-promo-field").should("exist")
  //   cy.get("#btn-buy-now-hms").click()
  // })

  // it("TC_PEMBAYARAN_007", () => {
  //   cy.viewport(1920, 1080)
  //   cy.get("#btn-id_linkaja-hms").click()
  //   cy.get("#hms-promo-field").should("exist")
  //   cy.get("#btn-buy-now-hms").click()
  // })

  // it("TC_PEMBAYARAN_008", () => {
  //   cy.viewport(1920, 1080)
  //   cy.get("#btn-id_shopeepay-hms").click()
  //   cy.get("#hms-promo-field").should("exist")
  //   cy.get("#btn-buy-now-hms").click()
  // })

  it("TC_PEMBAYARAN_009", () => {
    cy.viewport(1920, 1080)
    cy.get("#btn-cc-hms").click()
    cy.get("#hms-promo-field").should("exist")
    cy.get("#btn-buy-now-hms").click()
  })
})
