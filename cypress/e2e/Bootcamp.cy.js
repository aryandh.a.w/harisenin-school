describe("Bootcamp", () => {
  it("validasi komponen", () => {
    cy.viewport(1920, 1080)
    cy.visit("https://www.harisenin.net/school/bootcamp")
    cy.get("#password").type("gwsudahbahagia{enter}").wait(500)
    cy.contains("Program").should("exist")
    cy.contains("Menjadi Tutor").should("exist")
    cy.get("#btn-regist").should("exist")
    cy.get("#btn-login").should("exist")

    cy.get("#hms-search-field").should("exist")
    cy.get("#btn-search-hms").should("exist")

    cy.get("button").contains("BOOTCAMP").should("exist")
    cy.get("button").contains("PROCLASS").should("exist")
    cy.get("#program-card-bootcamp-0").should("exist")
    cy.get("#program-card-bootcamp-1").should("exist")
    cy.get("#program-card-bootcamp-2").should("exist")
    cy.get("#program-card-bootcamp-3").should("exist")
    cy.get("#program-card-bootcamp-4").should("exist")
    cy.get("#program-card-bootcamp-5").should("exist")
    cy.get("button").contains("Muat Lainnya").should("exist").click()
    cy.get("#program-card-bootcamp-6").should("exist")
    cy.get("#program-card-bootcamp-7").should("exist")

    cy.get("footer").should("exist")
    cy.contains("PT.Karya Kaya Bahagia. 2020").should("exist")
  })

  it("proses login", () => {
    cy.viewport(1920, 1080)
    cy.visit("https://www.harisenin.net/")
    cy.get("#password").type("gwsudahbahagia{enter}").wait(500)
    cy.get("#btn-login").click()

    cy.get('input[type="text"]')
      .type("vanessa.angelica@student.umn.ac.id")
      .should("have.value", "vanessa.angelica@student.umn.ac.id")
    cy.get('input[type="password"]').type("vannes88")
    cy.get("#btn-login-modal").click()
  })

  it("masuk ke bootcamp", () => {
    cy.viewport(1920, 1080)
    cy.visit("https://www.harisenin.net/")
    cy.get("#password").type("gwsudahbahagia{enter}").wait(500)
    cy.contains("Temukan Bootcamp Yang Tepat Untukmu").click().wait(1000)
    cy.get("#btn-all-program-bootcamp").click()
  })

  it("pencarian bootcamp", () => {
    cy.viewport(1920, 1080)
    cy.visit("https://www.harisenin.net/school/bootcamp")
    cy.get("#password").type("gwsudahbahagia{enter}").wait(500)
    cy.get("#hms-search-field").click().type("Digital")
    cy.get("#btn-search-hms").click()
    cy.get("#program-card-bootcamp-0").click()

    // tidak ditemukan bootcamp
    cy.visit("https://www.harisenin.net/school/bootcamp")
    cy.get("#hms-search-field").click().type("abcde")
    cy.get("#btn-search-hms").click()
  })

  it("PROCLASS", () => {
    cy.viewport(1920, 1080)
    cy.visit("https://www.harisenin.net/school/bootcamp")
    cy.get("#password").type("gwsudahbahagia{enter}").wait(500)
    cy.get("button").contains("PROCLASS").click()
  })

  it("testing bootcamp Human Resource", () => {
    cy.viewport(1920, 1080)
    cy.visit("https://www.harisenin.net/school/bootcamp")
    cy.get("#password").type("gwsudahbahagia{enter}").wait(500)
    cy.get("#program-card-bootcamp-0").click()
    cy.get("#btn-regist-class-banner").click()

    cy.get("#btn-share-hms").click()
    cy.get(".program_program_page__share__row__YB7SB").should("exist")
    cy.get(".program_program_page__share__link__XRxzJ").should("exist")
    cy.visit("https://www.harisenin.net/school/bootcamp/human-resources-iw6pf")

    // Kurikulum
    cy.get("#curriculum-0").click().wait(1000)
    cy.get("#curriculum-1").click()
    cy.get("#curriculum-2").click()
    cy.get("#curriculum-3").click()
    cy.get("#curriculum-4").click()
    cy.get("#curriculum-5").click()
    cy.get("#curriculum-6").click()
    cy.get("#curriculum-7").click()
    cy.get("#curriculum-8").click()
    cy.get("#curriculum-9").click()

    // download silabus
    cy.get("#btn-login").click()
    cy.get('input[type="text"]')
      .type("vanessa.angelica@student.umn.ac.id")
      .should("have.value", "vanessa.angelica@student.umn.ac.id")
    cy.get('input[type="password"]').type("vannes88")
    cy.get("#btn-login-modal").click().wait(1000)
    cy.get("#btn-download-syllabus").click()
    cy.get('input[type="text"]').eq(1).click().clear().type("angelica")
    cy.get("textarea").type("testing download silabus bootcamp harisenin")
    cy.get("button").contains("Download Silabus").click({ force: true })

    // slider tutor
    cy.visit("https://www.harisenin.net/school/bootcamp/human-resources-iw6pf")
    cy.get(".slider_custom_carousel_button__next-arrow__78WKM").first().click()
    cy.get(".slider_custom_carousel_button__prev-arrow__vpINq").first().click()

    // pilihan kelas
    cy.get("#investment-card-0").click({ force: true })
    cy.visit("https://www.harisenin.net/school/bootcamp/human-resources-iw6pf")
    cy.get("#investment-card-1").click({ force: true })
    cy.visit("https://www.harisenin.net/school/bootcamp/human-resources-iw6pf")
    cy.get("#investment-card-2").click({ force: true })
    cy.visit("https://www.harisenin.net/school/bootcamp/human-resources-iw6pf")
    cy.get("#investment-card-3").click({ force: true })
    cy.visit("https://www.harisenin.net/school/bootcamp/human-resources-iw6pf")
    cy.get("#investment-card-4").click({ force: true })
    cy.visit("https://www.harisenin.net/school/bootcamp/human-resources-iw6pf")

    // slider alumni
    cy.get(".slider_custom_carousel_button__next-arrow__78WKM").eq(1).click()
    cy.get(".slider_custom_carousel_button__prev-arrow__vpINq").eq(1).click()

    // faq
    cy.get(".faq-home_chevron").click({ multiple: true })
    cy.get(".program_program_page__related__card__j40K0").should("exist")

    // slider program relevan
    cy.get(".slider_custom_carousel_button__next-arrow__78WKM").eq(2).click()
    cy.get(".slider_custom_carousel_button__prev-arrow__vpINq").eq(2).click()

    cy.contains("Hubungi Kami").should("exist")
    cy.get("footer").should("exist")
    cy.contains("PT.Karya Kaya Bahagia. 2020").should("exist")
  })

  it("testing bootcamp Digital Marketer", () => {
    cy.viewport(1920, 1080)
    cy.visit("https://www.harisenin.net/school/bootcamp")
    cy.get("#password").type("gwsudahbahagia{enter}").wait(500)
    cy.get("#program-card-bootcamp-1").click()
    cy.get("#btn-regist-class-banner").click()

    cy.get("#btn-share-hms").click()
    cy.get(".program_program_page__share__row__YB7SB").should("exist")
    cy.get(".program_program_page__share__link__XRxzJ").should("exist")
    cy.visit("https://www.harisenin.net/school/bootcamp/digital-marketer")

    // Kurikulum
    cy.get("#curriculum-0").click().wait(1000)
    cy.get("#curriculum-1").click()
    cy.get("#curriculum-2").click()
    cy.get("#curriculum-3").click()
    cy.get("#curriculum-4").click()
    cy.get("#curriculum-5").click()
    cy.get("#curriculum-6").click()
    cy.get("#curriculum-7").click()
    cy.get("#curriculum-8").click()

    // download silabus
    cy.get("#btn-login").click()
    cy.get('input[type="text"]')
      .type("vanessa.angelica@student.umn.ac.id")
      .should("have.value", "vanessa.angelica@student.umn.ac.id")
    cy.get('input[type="password"]').type("vannes88")
    cy.get("#btn-login-modal").click().wait(1000)
    cy.get("#btn-download-syllabus").click()
    cy.get('input[type="text"]').eq(1).click().clear().type("angelica")
    cy.get("textarea").type("testing download silabus bootcamp harisenin")
    cy.get("button").contains("Download Silabus").click({ force: true })

    // slider tutor
    cy.visit("https://www.harisenin.net/school/bootcamp/digital-marketer")
    cy.get(".slider_custom_carousel_button__next-arrow__78WKM").first().click()
    cy.get(".slider_custom_carousel_button__prev-arrow__vpINq").first().click()

    // pilihan kelas
    cy.get("#investment-card-0").click({ force: true })
    cy.visit("https://www.harisenin.net/school/bootcamp/digital-marketer")
    cy.get("#investment-card-1").click({ force: true })
    cy.visit("https://www.harisenin.net/school/bootcamp/digital-marketer")
    cy.get("#investment-card-2").click({ force: true })
    cy.visit("https://www.harisenin.net/school/bootcamp/digital-marketer")
    cy.get("#investment-card-3").click({ force: true })
    cy.visit("https://www.harisenin.net/school/bootcamp/digital-marketer")
    cy.get("#investment-card-4").click({ force: true })
    cy.visit("https://www.harisenin.net/school/bootcamp/digital-marketer")

    cy.get(".slider_custom_carousel_button__next-arrow__78WKM").eq(1).click({ force: true })
    cy.get(".slider_custom_carousel_button__prev-arrow__vpINq").eq(1).click({ force: true })

    cy.get(".faq-home_chevron").click({ multiple: true })
    cy.get(".program_program_page__related__card__j40K0").should("exist")

    cy.get(".slider_custom_carousel_button__next-arrow__78WKM").eq(2).click()
    cy.get(".slider_custom_carousel_button__prev-arrow__vpINq").eq(2).click()

    cy.contains("Hubungi Kami").should("exist")
    cy.get("footer").should("exist")
    cy.contains("PT.Karya Kaya Bahagia. 2020").should("exist")
  })
})
