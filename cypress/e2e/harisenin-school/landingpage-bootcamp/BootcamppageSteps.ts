import { Given, When, Then } from "@badeball/cypress-cucumber-preprocessor";
import { Bootcamppage } from "../../../support/pages/Bootcamppage";

let bootcamppage = new Bootcamppage();

//@HMS-69
Given(/^User on Landingpage school$/, () => {
  bootcamppage.visitLandingpageHMS();
})
When(/^click button "Lihat Semua Program" for visit Bootcamp page$/, () => {
  bootcamppage.clickBtnAllBootcamp();
})
Then(/^System showing wording on banner correctly$/, () => {
  bootcamppage.verifyLandingpageBootcamp();
})

// @HMS-70
Given(/^User success visit landingpage school$/, () => {
  bootcamppage.visitLandingpageHMS();
})
When(/^click button "Lihat Semua Program"$/, () => {
  bootcamppage.clickBtnAllBootcamp();
})
When(/^click button CTA "Pilih Bootcamp"$/, () => {
  bootcamppage.clickButtonCTAChooseBootcamp();
})
Then(/^System direct scrool into view section "Program Bootcamp" and show all course bootcamp$/, () => {
  bootcamppage.verifyCardBootcampDisplayed();
})

// @HMS-73
Given(/^User on bootcamp page$/, () => {
  bootcamppage.visitBootcamppage();
})
When(/^scrool into next section$/, () => {
  bootcamppage.scroolIntoSectionStatisticData();
})
Then(/^System show section presentase statistic data alumni, student, and company$/, () => {
  bootcamppage.verifyDataVisible();
})

// @HMS-74
Given(/^User visit bootcamp page$/, () => {
  bootcamppage.visitBootcamppage();
})
When(/^scrool into section capture class bootcamp$/, () => {
  bootcamppage.scroolIntoSectionCaptureClass();
})
Then(/^System show capture class bootcamp from 2021 and wording$/, () => {
  bootcamppage.verifyCaptureClassAndWordingVisible();
})

// @HMS-78
Given(/^User on bootcamppage harisenin$/, () => {
  bootcamppage.visitBootcamppage();
})
When(/^scrool into section consultation banner$/, () => {
  bootcamppage.scroolIntoSectionConsultataion();
})
Then(/^System showing button consult via WhatApps$/, () => {
  bootcamppage.verifyBtnConsultationOnBootcamppage();
})