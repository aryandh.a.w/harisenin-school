Feature: Bootcamp page
  Users can view and visit each active bootcamp
@HMS-69
  Scenario: Verify landingpage bootcamp containing title "Upgrade Skill Untuk Karier yang Lebih Baik"
    Given User on Landingpage school
    When click button "Lihat Semua Program" for visit Bootcamp page
    Then System showing wording on banner correctly
  
@HMS-70
  Scenario: Verify that bootcamp program card are displayed
    Given User success visit landingpage school
    When click button "Lihat Semua Program"
    When click button CTA "Pilih Bootcamp"
    Then System direct scrool into view section "Program Bootcamp" and show all course bootcamp

@HMS-73
  Scenario: Verify user can view presentase statistic data alumni, student and company
    Given User on bootcamp page
    When scrool into next section
    Then System show section presentase statistic data alumni, student, and company

@HMS-74
  Scenario: Verify user can view capture class bootcamp from 2021
    Given User visit bootcamp page
    When scrool into section capture class bootcamp
    Then System show capture class bootcamp from 2021 and wording

@HMS-78
  Scenario: Verify user can view section consult via whatApps
    Given User on bootcamppage harisenin
    When scrool into section consultation banner
    Then System showing button consult via WhatApps