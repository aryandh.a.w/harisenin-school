import { Given, When, Then } from "@badeball/cypress-cucumber-preprocessor";
import { DetailUIUXProductManagement } from './../../../support/pages/detail-bootcamp/DetailUIUXProductManagement';

let detailUIUX = new DetailUIUXProductManagement();

// @HMS-1221
Given(/^User visit UIUX Product Management bootcamp$/, () => {
    detailUIUX.visitUIUXProductManagementBootcamp();
})
Then(/^The title banner 'The title banner 'Siap Kerja Menjadi Auditor & Financial Analyst dalam 17 Minggu' is visible' is visible$/, () => {
    detailUIUX.verifyTitleBannerUIUXBootcamp();
})
// @HMS-1222
Given(/^User access UIUX Product Management bootcamp$/, () => {
    detailUIUX.visitUIUXProductManagementBootcamp();
})
Then(/^The subtitle banner on UIUX Product Management bootcamp is visible$/, () => {
    detailUIUX.verifySubtitleBannerUIUX();
})

// @HMS-1223
Given(/^User success visit UIUX Product Management Bootcamp$/, () => {
    detailUIUX.visitUIUXProductManagementBootcamp();
})
When(/^click button "Daftar Sekarang" on UIUX Product Management Bootcamp$/, () => {
    detailUIUX.clickBtnRegisterNow();
})
Then(/^System View Title Section: "Biaya dan Pendaftaran" on UIUX Product Management Bootcamp$/, () => {
    detailUIUX.verifySectionFeeAndRegister();
})

// @HMS-1224
Given(/^User on banner UIUX Product Management Bootcamp$/, () => {
    detailUIUX.visitUIUXProductManagementBootcamp();
})
Then(/^The system properly displays images on the banner UIUX Product Management Bootcamp$/, () => {
    detailUIUX.verifyImageOnBannerUIUXBootcampDisplayed();
})