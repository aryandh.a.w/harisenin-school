import { Given, When, Then } from "@badeball/cypress-cucumber-preprocessor";
import { DetailHumanResourcepage } from "../../../support/pages/detail-bootcamp/DetailHumanresourcepage";

let detailHumanresource = new DetailHumanResourcepage();

//@HMS-1213
Given(/^User visit Humanresource bootcamp$/, () => {
  detailHumanresource.visitHumaresourceBootcamp();
})
Then(/^Title Banner "Siap Kerja Menjadi HR Profesional dalam 18 Minggu" is visible$/, () => {
  detailHumanresource.verifyTitleTextOnBannerHRbootcamp();
})

// @HMS-1214
Given(/^User visit detail program Humanresource bootcamp$/, () => {
  detailHumanresource.visitHumaresourceBootcamp();
})
Then(/^Subtitle banner on detail progam HR bootcamp is visible$/, () => {
  detailHumanresource.verifySubtitleBannerHResourceBootcamp();
})

// @HMS-1215
Given(/^User on banner HR Bootcamp$/, () => {
  detailHumanresource.visitHumaresourceBootcamp();
})
When(/^click button "Daftar Sekarang"$/, () => {
  detailHumanresource.clickButtonRegisterNow();
})
Then(/^System view section "Biaya and Pendaftaran"$/, () => {
  detailHumanresource.verifySectionFeeAndRegistration();
})

// @HMS-1216
Given(/^User access Humanresource bootcamp$/, () => {
  detailHumanresource.visitHumaresourceBootcamp();
})
Then(/^Image on banner success visible$/, () => {
  detailHumanresource.verifyImageHRBootcampOnBanner();
})