Feature: Detail Program and Checkout Auditor Financial Analyst Bootcamp
  User wants to get detailed information about the bootcamp and checkout

@HMS-1217
  Scenario: Containing title text "Siap Kerja Menjadi Auditor & Financial Analyst dalam 17 Minggu"
    Given User visit Auditor & Financial Analyst bootcamp
    Then The title banner 'Siap Kerja Menjadi Auditor & Financial Analyst dalam 17 Minggu' is visible

@HMS-1218
  Scenario: Containing subtitle banner on Auditor and Financial bootcamp detail page
    Given User access Auditor & Financial Analyst bootcamp
    Then The subtitle banner is visible

@HMS-1219
  Scenario: Verify user can click CTA Button "Daftar Sekarang"
    Given User success visit Audtitor Financial Analyst Bootcamp
    When click button 'Daftar Sekarang' on the banner Auditor Financial Analyst Bootcamp
    Then System View Title Section: "Kategori & Biaya Program"

@HMS-1220
  Scenario: Verify the image on the banner is displayed properly
    Given User on banner Auditor Financial Analyst Bootcamp
    Then The system properly displays images on the banner