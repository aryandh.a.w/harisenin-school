import { Given, When, Then } from "@badeball/cypress-cucumber-preprocessor";
import { DetailFullstackWebpage } from "../../../support/pages/detail-bootcamp/DetailFullstackWebpage";

let detailFullstackWebBootcamp = new DetailFullstackWebpage();

// @HMS-203
Given(/^User visit detail class Fullstack Web Developer$/, () => {
    detailFullstackWebBootcamp.visitDetailFullstackWebPage();
})
Then(/^System displays the Title banner according to the selected bootcamp$/, () => {
    detailFullstackWebBootcamp.verifyTitleBannerFSDBootcamp();
})

// @HMS-204
Given(/^User visits the web developer bootcamp details page$/, () => {
    detailFullstackWebBootcamp.visitDetailFullstackWebPage();
})
Then(/^System displays the subtitles correctly$/, () => {
    detailFullstackWebBootcamp.verifySubtitleOnBannerBootcamppage();
})

// @HMS-205
Given(/^User visit FSD Bootcamp detail page$/, () => {
    detailFullstackWebBootcamp.visitDetailFullstackWebPage();
})
When(/^click button "Daftar Sekarang" on Banner$/, () => {
    detailFullstackWebBootcamp.clickCTABtnRegisterNow();
})
Then(/^System displays "Kategori & Biaya Program" section$/, () => {
    detailFullstackWebBootcamp.verifySectionFeeAndRegister();
})

// @HMS-733
Given(/^User selects the web developer bootcamp and visits the details page$/, () => {
    detailFullstackWebBootcamp.visitDetailFullstackWebPage();
})
Then(/^System successfully displays the image on the banner$/, () => {
    detailFullstackWebBootcamp.verifyImageFullstackDsiplayedOnBanner();
})