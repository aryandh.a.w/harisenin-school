Feature: Detail Program and Checkout Bootcamp
  User wants to get detailed information about the bootcamp and checkout
  @HMS-1213
  Scenario: Containing title text "Siap Kerja Menjadi HR Profesional dalam 18 Minggu"
    Given User visit Humanresource bootcamp
    Then Title Banner "Siap Kerja Menjadi HR Profesional dalam 18 Minggu" is visible
  
  @HMS-1214
  Scenario: Containing subtitle banner on Human Resource bootcamp detail page
    Given User visit detail program Humanresource bootcamp
    Then Subtitle banner on detail progam HR bootcamp is visible

  @HMS-1215
  Scenario: Verify user can click CTA Button "Daftar Sekarang"
    Given User on banner HR Bootcamp
    When click button "Daftar Sekarang"
    Then System view section "Biaya and Pendaftaran"

  @HMS-1216
  Scenario: Verify the image on the banner is displayed properly
    Given User access Humanresource bootcamp
    Then Image on banner success visible