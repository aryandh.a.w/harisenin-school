Feature: Bootcamp detail and checkout page
  User wants to get detailed information about the bootcamp and checkout

@HMS-203
  Scenario: Containing title text "Siap Kerja Menjadi FullStack Web Developer dalam 19 Minggu"
    Given User visit detail class Fullstack Web Developer
    Then System displays the Title banner according to the selected bootcamp

@HMS-204
  Scenario: Containing subtitle banner on Fullstackweb bootcamp detail page
    Given User visits the web developer bootcamp details page
    Then System displays the subtitles correctly

@HMS-205
  Scenario: Verify user can click CTA Button "Daftar Sekarang"
    Given User visit FSD Bootcamp detail page
    When click button "Daftar Sekarang" on Banner
    Then System displays "Kategori & Biaya Program" section

@HMS-733
  Scenario: Verify the image on the banner is displayed properly
    Given User selects the web developer bootcamp and visits the details page
    Then System successfully displays the image on the banner