Feature: Detail Program and Checkout UIUX Product Management Bootcamp
  User wants to get detailed information about the bootcamp and checkout

@HMS-1221
  Scenario: Containing title text "Siap Kerja Menjadi UI/UX Designer & Product Manager dalam 16 Minggu"
    Given User visit UIUX Product Management bootcamp
    Then The title banner 'The title banner 'Siap Kerja Menjadi Auditor & Financial Analyst dalam 17 Minggu' is visible

@HMS-1222
  Scenario: Containing subtitle banner on UI/UX Design & Product Management bootcamp detail page
    Given User access UIUX Product Management bootcamp
    Then The subtitle banner on UIUX Product Management bootcamp is visible

@HMS-1223
  Scenario: Verify user can click CTA Button "Daftar Sekarang"
    Given User success visit UIUX Product Management Bootcamp
    When click button "Daftar Sekarang" on UIUX Product Management Bootcamp
    Then System View Title Section: "Biaya dan Pendaftaran" on UIUX Product Management Bootcamp

@HMS-1224
  Scenario: Verify the image on the banner is displayed properly
    Given User on banner UIUX Product Management Bootcamp
    Then The system properly displays images on the banner UIUX Product Management Bootcamp
