import { Given, When, Then } from "@badeball/cypress-cucumber-preprocessor";
import { DetailAuditorFinancialpage } from "../../../support/pages/detail-bootcamp/DetailAuditorFinancialpage";

let detailAuditor = new DetailAuditorFinancialpage();

// @HMS-1217
Given(/^User visit Auditor & Financial Analyst bootcamp$/, () => {
    detailAuditor.visitAuditorAnalystBootcamp();
})
Then(/^The title banner 'Siap Kerja Menjadi Auditor & Financial Analyst dalam 17 Minggu' is visible$/, () => {
    detailAuditor.verifyTitleBannerAuditorBootcamp();
})

// @HMS-1218
Given(/^User access Auditor & Financial Analyst bootcamp$/, () => {
    detailAuditor.visitAuditorAnalystBootcamp();
})
Then(/^The subtitle banner is visible$/, () => {
    detailAuditor.verifySubtitleBannerAuditorBootcamp();
})

// @HMS-1219
Given(/^User success visit Audtitor Financial Analyst Bootcamp$/, () => {
    detailAuditor.visitAuditorAnalystBootcamp();
})
When(/^click button 'Daftar Sekarang' on the banner Auditor Financial Analyst Bootcamp$/, () => {
    detailAuditor.clickButtonRegisterNow();
})
Then(/^System View Title Section: "Kategori & Biaya Program"$/, () => {
    detailAuditor.verifySectionFeeAndRegisterOnAuditorBootcamp();
})

// @HMS-1220
Given(/^User on banner Auditor Financial Analyst Bootcamp$/, () => {
    detailAuditor.visitAuditorAnalystBootcamp();
})
Then(/^The system properly displays images on the banner$/, () => {
    detailAuditor.verifyImageOnBannerAuditor();
})