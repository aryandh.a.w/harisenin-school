import { Given, When, Then } from "@badeball/cypress-cucumber-preprocessor";
import { DetailDigitalMarketingpage } from "../../../support/pages/detail-bootcamp/DetailDigitalMarketingpage";
import SuiteSteps from "../SuiteSteps";

let suite = new SuiteSteps();
let detailDigmar = new DetailDigitalMarketingpage();

// @HMS-1225
Given(/^User visit Digital Marketer bootcamp$/, () => {
    detailDigmar.visitDigmarBootcamp();
})
Then(/^The title banner 'Siap Kerja Menjadi Digital Marketing dalam 12 Minggu' is visible$/, () => {
    detailDigmar.verifyTitleBannerOnDigmarBootcamp();
})

// @HMS-1226
Given(/^User access Digital Marketing bootcamp$/, () => {
    detailDigmar.visitDigmarBootcamp();
})
Then(/^The subtitle banner on Digital Marketing bootcamp is visible$/, () => {
    detailDigmar.verifySubtitleDigmarBootcamp();
})

// @HMS-1227
Given(/^User success visit Digital Marketer Bootcamp$/, () => {
    detailDigmar.visitDigmarBootcamp();
})
When(/^click button "Daftar Sekarang" on Digital Marketer Bootcamp$/, () => {
    detailDigmar.clickBtnRegisterNow();
})
Then(/^System View Title Section: "Biaya dan Pendaftaran" on Digital Marketer Bootcamp$/, () => {
    detailDigmar.verifySectionFeeAndRegister();
})

// @HMS-1228
Given(/^User on banner Digital Marketing Bootcamp$/, () => {
    detailDigmar.visitDigmarBootcamp();
})
Then(/^The system properly displays images on the banner Digital Marketing Bootcamp$/, () => {
    detailDigmar.verifyImageDigmarBootcamp();
})

// @HMS-796
Given(/^User visit detail bootcamp Digital Marketing$/, () => {
    detailDigmar.visitDigmarBootcamp();
})
Then(/^System displays 12 menu that must be shown on the sidebar menu$/, () => {
    detailDigmar.verifySidebarMenuDigmar();
})

// @HMS-1229
Given(/^Users want to view upcoming batches for the Digital Marketing Bootcamp$/, () => {
    detailDigmar.visitDigmarBootcamp();
})
When(/^Scroll down to find the title "Upcoming Batches"$/, () => {
    detailDigmar.scrollDownUntilUpcomingBatch();
})
Then(/^System displays three available batches$/, () => {
    detailDigmar.verifyThreeBatchDigmar();
})

// @HMS-1230
Given(/^Users wish to see outcomes for the Digital Marketing Bootcamp$/, () => {
    detailDigmar.sidebarOutcomesDigmar();
})
When(/^click "Outcomes" on sidebarmenu$/, () => {
    detailDigmar.viewOutcomesOnSidebar();
})
Then(/^System will scroll up the page until the title "Outcomes" is displayed$/, () => {
    detailDigmar.verifySectionOutcomesDigmar();
})

// @HMS-797
Given(/^Users wish to see benefit for the Digital Marketing Bootcamp$/,() => {
    detailDigmar.sidebarBenefitDigmar();
})
When(/^click "Benefit" on sidebarmenu$/,() => {
    detailDigmar.viewBenefitSidebar();
})
Then(/^System will scroll up the page until the title "Benefit" is displayed$/,() => {
    detailDigmar.verifySectionBenfitDigmar();
})

// @HMS-820
Given(/^Users wish to see curiculum for the Digital Marketing Bootcamp$/, () => {
    detailDigmar.sidebarBenefitDigmar();
})
When(/^click "curiculum" on sidebarmenu$/, () => {
    detailDigmar.viewCuriculumSidebar();
})
Then(/^System will scroll up the page until the title "curiculum" is displayed$/, () => {
    detailDigmar.viewSectionCuriculum();
})

// @HMS-799
Given(/^Users wish to see Tools for the Digital Marketing Bootcamp$/, () => {
    detailDigmar.sidebarBenefitDigmar();
})
When(/^click "Tools" on sidebarmenu$/, () => {
    detailDigmar.viewToolsSidebar();
})
Then(/^System will scroll up the page until the title "Tools" is displayed$/, () => {
    detailDigmar.viewSectionTools();
})

// @HMS-812
Given(/^Users wish to see Tutor for the Digital Marketing Bootcamp$/, () => {
    detailDigmar.sidebarBenefitDigmar();
})
When(/^click "Tutor" on sidebarmenu$/, () => {
    detailDigmar.viewTutorSidebar();
})
Then(/^System will scroll up the page until the title "Tutor" is displayed$/, () => {
    detailDigmar.viewSectionTutor();
})

// @HMS-798
Given(/^Users wish to see "Learning Experience" for the Digital Marketing Bootcamp$/, () => {
    detailDigmar.visitDigmarBootcamp();
})
When(/^click "Learning Experience" on sidebarmenu$/, () => {
    detailDigmar.viewLearningExperienceSidebar();
})
Then(/^System will scroll up the page until the title "Learning Experience" is displayed$/, () => {
    detailDigmar.viewSectionLearningExperience();
})

// @HMS-805
Given(/^Users wish to see "Testimoni" for the Digital Marketing Bootcamp$/, () => {
    detailDigmar.visitDigmarBootcamp();
})
When(/^click "Testimoni" on sidebarmenu$/, () => {
    detailDigmar.viewTestimoniSidebar();
})
Then(/^System will scroll up the page until the title "Testimoni" is displayed$/, () => {
    detailDigmar.viewSectionTestimoni();
})

// @HMS-804
Given(/^Users wish to see "Portfolio Alumni" for the Digital Marketing Bootcamp$/, () => {
    detailDigmar.visitDigmarBootcamp();
})
When(/^click "Portfolio Alumni" on sidebarmenu$/, () => {
    detailDigmar.viewPortfolioSidebar();
})
Then(/^System will scroll up the page until the title "Portfolio Alumni" is displayed$/, () => {
    detailDigmar.viewSectionPortfolio();
})

// @HMS-801
Given(/^Users wish to see "Jadwal Bootcamp" for the Digital Marketing Bootcamp$/, () => {
    detailDigmar.visitDigmarBootcamp();
})
When(/^click "Jadwal Bootcamp" on sidebarmenu$/, () => {
    detailDigmar.viewScheduleBootcampSidebar();
})
Then(/^System will scroll up the page until the title "Jadwal Bootcamp" is displayed$/, () => {
    detailDigmar.viewSectionScheduleBootcamp();
})

// @HMS-1231
Given(/^Users wish to see "Kategori Program" for the Digital Marketing Bootcamp$/, () => {
    detailDigmar.visitDigmarBootcamp();
})
When(/^click "Kategori Program" on sidebarmenu$/, () => {
    detailDigmar.viewCategoryProgramSidebar();
})
Then(/^System will scroll up the page until the title "Kategori Program" is displayed$/, () => {
    detailDigmar.viewSectionCategoryProgram();
})

// @HMS-821
Given(/^Users wish to see "Metode Pembayaran" for the Digital Marketing Bootcamp$/, () => {
    detailDigmar.visitDigmarBootcamp();
})
When(/^click "Metode Pembayaran" on sidebarmenu$/, () => {
    detailDigmar.viewPaymentMethodSidebar();
})
Then(/^System will scroll up the page until the title "Metode Pembayaran" is displayed$/, () => {
    detailDigmar.viewSectionPaymentMethod();
})

// @HMS-806
Given(/^Users wish to see "FAQ" for the Digital Marketing Bootcamp$/, () => {
    detailDigmar.visitDigmarBootcamp();
})
When(/^click "FAQ" on sidebarmenu$/, () => {
    detailDigmar.viewFAQSidebar();
})
Then(/^System will scroll up the page until the title "FAQ" is displayed$/, () => {
    detailDigmar.viewSectionFAQ();
})

// @HMS-816
Given(/^Users can access details of the Digital Marketing Bootcamp$/, () => {
    detailDigmar.visitDigmarBootcamp();
})
When(/^Scroll into the Other Bootcamp section$/, () => {
    detailDigmar.viewOtherBootcampSection();
})
Then(/^System displays 5 bootcamp$/, () => {
    detailDigmar.viewAllBootcampOnSlidebar();
})

// @HMS-852
Given(/^User visits the 'Digital Marketing' bootcamp$/, () => {
    detailDigmar.visitDigmarBootcamp();
})
When(/^Scroll down into the Curriculum section$/, () => {
    detailDigmar.scrollIntoViewCurriculumSection();
})
When(/^Click the 'Download Kurikulum' button in the curriculum section of the digital marketing bootcamp$/, () => {
    detailDigmar.clickBtnDownloadCurriculumDigmarBootcamp();
})
Then(/^Then the system displays a login modal box$/, () => {
    detailDigmar.verifyDisplayingModalboxLogin();
})

// @HMS-853
Given(/^Users want to view the curriculum section of the digital marketing bootcamp$/, () => {
    detailDigmar.visitDigmarBootcamp();
    detailDigmar.scrollIntoViewCurriculumSection();
})
When(/^Click the 'Download Curriculum' button to download the syllabus$/, () => {
    detailDigmar.clickBtnDownloadCurriculumDigmarBootcamp();
})
When(/^In the Login modal box, input your username and password$/, () => {
    detailDigmar.InputUsernameAndPasswordInTheCurriculumModalbox();
})
When(/^The user successfully log in and clicks the 'Download Kurikulum' button$/, () => {
    detailDigmar.clickBtnDownloadCurriculumDigmarBootcamp();
})
Then(/^System displays a modal box for user data entry$/, () => {
    detailDigmar.verifyModalBoxForDownloadSylabus();
})

// @HMS-854
Given(/^User successfully logs in and views the 'Download Kurikulum' button on the detail page of the Digital Marketing Bootcamp$/, () => {
    suite.successLogin();
    detailDigmar.visitDigmarBootcamp();
    detailDigmar.scrollIntoViewCurriculumSection();
})
When(/^Click the 'Download Kurikulum' button in the Curriculum section$/, () => {
    detailDigmar.clickBtnDownloadCurriculumDigmarBootcamp();
})
When(/^Click the 'Download Kurikulum' button located within the popup$/, () => {
    detailDigmar.clickBtnDownloadWithoutInputRequiredField();
})
Then(/^System displays an error message 'Wajib diisi' in all required fields$/, () => {
    detailDigmar.verifyErrorMessageRequiredFields();
})

// @HMS-860
Given(/^User accesses the 'Download Kurikulum' popup after successfully logging into the system$/, () => {
    detailDigmar.accessesDownloadSylabusPopup();
})
When(/^Input all field on form within popup download sylabus$/, () => {
    detailDigmar.inputDataInThePopupDownloadSylabus();
})
Then(/^System should not display an error message and should successfully open the syllabus document in a new tab$/, () => {
    detailDigmar.verifySuccessfullyDownload();
})

// @HMS-861
Given(/^Users are visiting the digital marketing bootcamp$/, () => {
    suite.successLogin();
    detailDigmar.visitDigmarBootcamp();
})
When(/^Click the 'Download Kurikulum' button when will cancel download$/, () => {
    detailDigmar.clickBtnDownloadCurriculumDigmarBootcamp();
})
When(/^Click the 'X' icon to close the 'Download Kurikulum'$/, () => {
    detailDigmar.clickCloseIconOnDownloadPopup();
})
Then(/^System successfully closed the 'Download Kurikulum' popup$/, () => {
    detailDigmar.verifySystemClosingDownloadPopup();
})