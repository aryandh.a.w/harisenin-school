Feature: Detail Program and Checkout Digital Marketing Bootcamp
  User wants to get detailed information about the bootcamp and checkout

@HMS-1225
  Scenario: Siap Kerja Menjadi Digital Marketer Professional dalam 12 Minggu
		Given User visit Digital Marketer bootcamp
		Then The title banner 'Siap Kerja Menjadi Digital Marketing dalam 12 Minggu' is visible

@HMS-1226
	Scenario: Containing subtitle banner on Digital Marketer bootcamp detail page
		Given User access Digital Marketing bootcamp
		Then The subtitle banner on Digital Marketing bootcamp is visible

@HMS-1227
	Scenario: Verify user can click CTA Button "Daftar Sekarang"
		Given User success visit Digital Marketer Bootcamp
		When click button "Daftar Sekarang" on Digital Marketer Bootcamp
		Then System View Title Section: "Biaya dan Pendaftaran" on Digital Marketer Bootcamp

@HMS-1228
	Scenario: Verify the image on the banner is displayed properly
		Given User on banner Digital Marketing Bootcamp
		Then The system properly displays images on the banner Digital Marketing Bootcamp

@HMS-796
	Scenario: Verify that when the user scrolls up in each section, the background-color change following the position
    Given User visit detail bootcamp Digital Marketing
    Then System displays 12 menu that must be shown on the sidebar menu

@HMS-1229
	Scenario: Verify that clicking on 'Batch Terdekat' redirects the user to the 'Batch Terdekat' section
    Given Users want to view upcoming batches for the Digital Marketing Bootcamp
    When Scroll down to find the title "Upcoming Batches"
    Then System displays three available batches

@HMS-1230
  Scenario: Verify that clicking on 'Outcomes' redirects the user to the 'Outcomes' section
    Given Users wish to see outcomes for the Digital Marketing Bootcamp
    When click "Outcomes" on sidebarmenu
    Then System will scroll up the page until the title "Outcomes" is displayed

@HMS-797
	Scenario: Verify that clicking on 'Benefit' redirects the user to the 'Benefit' section
		Given Users wish to see benefit for the Digital Marketing Bootcamp
		When click "Benefit" on sidebarmenu
		Then System will scroll up the page until the title "Benefit" is displayed

@HMS-820
  Scenario: Verify that clicking on 'Kurikulum' redirects the user to the 'Kurikulum' section
    Given Users wish to see curiculum for the Digital Marketing Bootcamp
    When click "curiculum" on sidebarmenu
    Then System will scroll up the page until the title "curiculum" is displayed

@HMS-799
  Scenario: Verify that clicking on 'Tools' redirects the user to the 'Tools' section
    Given Users wish to see Tools for the Digital Marketing Bootcamp
    When click "Tools" on sidebarmenu
    Then System will scroll up the page until the title "Tools" is displayed

@HMS-812
  Scenario: Verify that clicking on 'Tutor' redirects the user to the 'Tutor' section
    Given Users wish to see Tutor for the Digital Marketing Bootcamp
    When click "Tutor" on sidebarmenu
    Then System will scroll up the page until the title "Tutor" is displayed

@HMS-798
  Scenario: Verify that clicking on 'Learning Experience' redirects the user to the 'Learning Experience' section
    Given Users wish to see "Learning Experience" for the Digital Marketing Bootcamp
    When click "Learning Experience" on sidebarmenu
    Then System will scroll up the page until the title "Learning Experience" is displayed

@HMS-805
  Scenario: Verify that clicking on 'Testimoni' redirects the user to the 'Testimoni' section
    Given Users wish to see "Testimoni" for the Digital Marketing Bootcamp
    When click "Testimoni" on sidebarmenu
    Then System will scroll up the page until the title "Testimoni" is displayed

@HMS-804
  Scenario: Verify that clicking on 'Portfolio Alumni' redirects the user to the 'Portfolio Alumni' section
    Given Users wish to see "Portfolio Alumni" for the Digital Marketing Bootcamp
    When click "Portfolio Alumni" on sidebarmenu
    Then System will scroll up the page until the title "Portfolio Alumni" is displayed

@HMS-801
  Scenario: Verify that clicking on 'Jadwal Bootcamp' redirects the user to the 'Jadwal Bootcamp' section
    Given Users wish to see "Jadwal Bootcamp" for the Digital Marketing Bootcamp
    When click "Jadwal Bootcamp" on sidebarmenu
    Then System will scroll up the page until the title "Jadwal Bootcamp" is displayed

@HMS-1231
  Scenario: Verify that clicking on 'Kategori Program' redirects the user to the 'Kategori Program' section
    Given Users wish to see "Kategori Program" for the Digital Marketing Bootcamp
    When click "Kategori Program" on sidebarmenu
    Then System will scroll up the page until the title "Kategori Program" is displayed

@HMS-821
  Scenario: Verify that clicking on 'Metode Pembayaran' redirects the user to the 'Metode Pembayaran Program' section
    Given Users wish to see "Metode Pembayaran" for the Digital Marketing Bootcamp
    When click "Metode Pembayaran" on sidebarmenu
    Then System will scroll up the page until the title "Metode Pembayaran" is displayed

@HMS-806
  Scenario: Verify that clicking on 'FAQ' redirects the user to the 'FAQ' section
    Given Users wish to see "FAQ" for the Digital Marketing Bootcamp
    When click "FAQ" on sidebarmenu
    Then System will scroll up the page until the title "FAQ" is displayed

@HMS-816
  Scenario: Verify that the Other Bootcamp section displays 5 bootcamps on the card
    Given Users can access details of the Digital Marketing Bootcamp
    When Scroll into the Other Bootcamp section
    Then System displays 5 bootcamp

@HMS-852
  Scenario: Verify that you can click 'Download Kurikulum' without the need to log in or register for an account
    Given User visits the 'Digital Marketing' bootcamp
    When Scroll down into the Curriculum section
    When Click the 'Download Kurikulum' button in the curriculum section of the digital marketing bootcamp
    Then Then the system displays a login modal box

@HMS-853
  Scenario: Verify that you can click 'Download Kurikulum' after logging into an account
    Given Users want to view the curriculum section of the digital marketing bootcamp
    When Click the 'Download Curriculum' button to download the syllabus
    When In the Login modal box, input your username and password
    When The user successfully log in and clicks the 'Download Kurikulum' button
    Then System displays a modal box for user data entry

@HMS-854
  Scenario: Verify that the user will fail to download the syllabus if they do not fill in all the required fields
    Given User successfully logs in and views the 'Download Kurikulum' button on the detail page of the Digital Marketing Bootcamp
    When Click the 'Download Kurikulum' button in the Curriculum section
    When Click the 'Download Kurikulum' button located within the popup
    Then System displays an error message 'Wajib diisi' in all required fields

@HMS-860
  Scenario: Verify that users can click the 'Download Kurikulum' button after filling out the data
    Given User accesses the 'Download Kurikulum' popup after successfully logging into the system
    When Input all field on form within popup download sylabus
    Then System should not display an error message and should successfully open the syllabus document in a new tab

@HMS-861
  Scenario: Verify that the user is able to click icon 'X'
    Given Users are visiting the digital marketing bootcamp
    When Click the 'Download Kurikulum' button when will cancel download
    When Click the 'X' icon to close the 'Download Kurikulum'
    Then System successfully closed the 'Download Kurikulum' popup