import { Given, When, Then } from "@badeball/cypress-cucumber-preprocessor";
import { DetailSeospecialistpage } from "../../../support/pages/detail-proclass/DetailSeospecialistpage";

let detailSeospecialistpage = new DetailSeospecialistpage();

Given(/^User success login account$/, () => {
  detailSeospecialistpage.userSuccessLoginAccount();
})
When(/^click button All Proclass$/, () => {
  detailSeospecialistpage.clickAllProclass();
})
When(/^click Seo Specialist Proclass$/, () => {
  detailSeospecialistpage.clickSeoSpecialistProclass();
})
When(/^click button "Daftar Kelas" on detail Seospecialist page$/, () => {
  detailSeospecialistpage.clickBtnRegistBanner();
})
Then(/^User view button SOLD OUT on Early Bird Promo investment$/, () => {
  detailSeospecialistpage.verifyBtnSoldoutEarlybirdpromo();
})