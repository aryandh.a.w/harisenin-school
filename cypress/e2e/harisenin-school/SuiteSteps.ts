import LandingpageData from "../../support/dataset/LandingpageData";
import LandingpageLocator from "../../support/locator/LandingpageLocator";
import LoginLocator from "../../support/locator/login/LoginLocator";
import LoginData from "../../support/dataset/login/LoginData";

export default class SuiteSteps {
    visit() {
        cy.visit("");
        cy.viewport(1400, 1200);
        cy.wait(6000);
    }

    exceptionClickBtn() {
        Cypress.on('uncaught:exception', (err, runnable) => {
            return false;
        });
    }

    clickButtonLogin() {
        this.visit();
        this.exceptionClickBtn();
        cy.get(LoginLocator.login.btn_login).click({force: true});
    }

    successLogin() {
        this.visit();
        cy.get(LoginLocator.login.btn_login).click({force: true});
        cy.get(LoginLocator.login.login_email_field).should("be.visible").type(LoginData.auth.login_email);
        cy.get(LoginLocator.login.login_password_field).should("be.visible").type(LoginData.auth.login_password);
        cy.get(LoginLocator.login.btn_login_modal).click({force: true});
        cy.wait(8000);
        cy.get(LoginLocator.login.username, {timeout: 8000}).should("be.visible");
    }

    visitHMSLandingpage() {
        cy.visit(LandingpageData.data_url.harisenin_school);
        cy.viewport(1400, 1200);
        cy.wait(6000);
    }

    visitSchoolBootcamppage() {
        cy.visit(LandingpageData.data_url.scholl_bootcamp);
        cy.viewport(1400, 1200);
        cy.wait(8000);
    }
}