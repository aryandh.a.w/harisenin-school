import { Given, When, Then } from "@badeball/cypress-cucumber-preprocessor";
import {Landingpage} from "../../../support/pages/Landingpage"

let landingpage = new Landingpage();

// @HMS-2
Given(/^User success login on landingpage$/, () => {
  landingpage.visitHariseninCom();
})
When(/^visit landingpage harisenin school$/, () => {
  landingpage.onHMSLandingpage();
})
Then(/^System can direct User to Harisenin School page$/, () => {
  landingpage.verifySuccessLoadHMSLandingpge();
})

// @HMS-1210
Given(/^User on landingpage HMS$/, () => {
  landingpage.onHMSLandingpage();
})
When(/^scrool in to view section Jelajahi Program Bootcamp$/, () => {
  landingpage.scroolInToSectionListBootcamp();
})
When(/^click button next until last course bootcamp$/, () => {
  landingpage.clickBtnNextOnListBootcamp();
})
Then(/^System view all course bootcamp$/, () => {
  landingpage.verifyViewAllCourse();
})

// @HMS-1211
Given(/^User on landingpage school$/, () => {
  landingpage.onHMSLandingpage();
})
When(/^click button "Lihat Semua Program" Bootcamp$/, () => {
  landingpage.clickButtonViewAllProgram();
})
Then(/^System success load bootcamp page and show all course bootcamp$/, () => {
  landingpage.verifyBootcamppage();
})