Feature: Landingpage harisenin-school
  User success load landingpage harisenin-school site

@HMS-2
  Scenario: User want access landingpage Harisenin School
    Given User success login on landingpage
    When visit landingpage harisenin school
    Then System can direct User to Harisenin School page

@HMS-1210
  Scenario: User can view Course Bootcamp on Section Slider
    Given User on landingpage HMS
    When scrool in to view section Jelajahi Program Bootcamp
    When click button next until last course bootcamp
    Then System view all course bootcamp

@HMS-1211
  Scenario: User can view "Lihat Semua Program" Bootcamp
    Given User on landingpage school
    When click button "Lihat Semua Program" Bootcamp
    Then System success load bootcamp page and show all course bootcamp