import { Given, When, Then } from "@badeball/cypress-cucumber-preprocessor";
import { Proclasspage } from "../../../support/pages/Proclasspage";

let proclasspage = new Proclasspage();

//@HMS-027
Given(/^User success load proclass page$/, () => {
  proclasspage.goToProclasspage();
})
When(/^type keyword valid input$/, () => {
  proclasspage.typeValidSearchProclas();
})
When(/^click button "Cari" Proclass$/, () => {
  proclasspage.clickBtnCariProclass();
})
Then(/^System show proclass accordance keyword searching$/, () => {
  proclasspage.verifyResultSearchingProclass();
})

//@HMS-028
Given(/^User visit Proclass page$/, () => {
  proclasspage.visitProclasspage();
})
When(/^type keyword invalid input$/, () => {
  proclasspage.typeInvalidSearchProclass();
})
When(/^click button "Cari" invalid proclass$/, () => {
  proclasspage.clickBtnSearchInvalidProclass();
})
Then(/^System show message Not Found Proclass$/, () => {
  proclasspage.verifyMsgNotFoundProclass();
})

//HMS-029
Given(/^User on proclass page$/, () => {
  proclasspage.onProclasspage();
})
When(/^click one of proclass program$/, () => {
  proclasspage.clickOneOfProclass();
})
When(/^click button "Daftar Kelas" on Proclass page$/, () => {
  proclasspage.clickBtnRegistClassBannerProclasspage();
})
Then(/^System show information "Biaya Kelas"$/, () => {
  proclasspage.verifyInfoBiayaKelas();
})

//HMS-030
Given(/^User open detail information proclass page$/, () => {
  proclasspage.onProclassPage();
})
When(/^scroll inTo section Benefit on Proclass page$/, () => {
  proclasspage.scrollToInfoBenefit();
})
Given(/^System show information Benefit on Proclass page$/, () => {
  proclasspage.verifySidebarActiveBenefit();
})