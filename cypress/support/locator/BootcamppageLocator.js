module.exports = {
  banner: {
    buttonChooseBootcampBanner: "button[id='btn-regist-class-banner']",
    titleSection: "h2[class='sm:text-[40px] text-2xl font-semibold mb-3']",
  },
  sectionStatisic: {
    container: "section[class='bg-[#F7F9FA] sm:py-[30px] sm:mt-20 py-6 px-5']",
    dataNumeric: "div[class='sm:text-[32px] font-semibold text-[28px] sm-only:text-center']",
    label: "div[class='text-[#484848] sm-only:text-center']",
  },
  sectionCaptureClass: {
    container: "section[class='mx-auto lg:w-[90%] px-4 w-full sm:my-[60px] flex sm:gap-[80px] sm:items-center sm-only:flex-col gap-[18px] my-9']",
    titleWording: "h2[class='sm:text-[48px] text-2xl font-medium mb-4']",
    description: "div[class='sm:text-xl text-[#484848] text-sm']",
    detailDesc: "Gak terasa harisenin.com telah hadir hingga saat ini. Namun, perjalanan kami gak berhenti di sini. Kami akan terus berkomitmen untuk memberikan solusi yang terbaik atas kebutuhan kariermu. Karena kami percaya, kamu pun bisa #JadiYangKamuMau",
  },
  sectionProgramBootcamp: {
    container: "div[class='flex flex-wrap -mx-4 sm-only:gap-[18px]']",
  },
  sectionConsultation: {
    container: "section[class='bg-[#001D43] relative w-full sm:py-[90px] py-[72px] flex items-center justify-center']",
    buttonConsultationAdmision: "a[class='relative flex items-center justify-center rounded font-medium hover:shadow-[0_12px_32px_0px_rgba(0,0,0,0.2)] text-white bg-green hover:text-white sm:py-[14px] sm:px-[50px] py-4 px-6 mx-auto text-sm lg:text-base w-fit rounded-none']",
  },
}