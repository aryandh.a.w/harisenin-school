module.exports = {
  login: {
    username: "[class='name']",
    btn_login: "[id='btn-login']",
    login_email_field: "[id='login-email-field']",
    login_password_field: "[id='login-email-password']",
    btn_login_modal: "[id='btn-login-modal']",
    btn_forgot_password: "[id='btn-forget-password-modal']",
    btn_regist_modal: "[id='btn-regist-modal']",
    validation_emailMandatory: "[class='text-red absolute sm:-bottom-4 -bottom-3 left-0']",
    validation_passwordMandatory: "[class='text-red']",
    validation_emailUnregistered: "[class='mt-2.5 text-red']",
    validation_passwordIncorrect: "[class='mt-2.5 text-red']",
    validation_emailInvalid: "[class='text-red absolute sm:-bottom-4 -bottom-3 left-0']",
  },  
}