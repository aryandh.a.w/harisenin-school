module.exports = {
  sectionListBootcamp: {
    title: "h2[class='sm:text-2xl font-semibold text-secondary-black text-center mb-2.5 text-lg']",
    buttonPrev: "button[class='left-[-10px] w-[50px] h-[50px] absolute top-[42%] flex items-center justify-center bg-white rounded-full shadow-[0_2px_5px_-0px_rgba(0,0,0,0.3)] absolute  top-[45%] w-[42px] h-[42px] -left-[0.6vw]']",
    buttonNext: "button[class='right-[-10px] w-[50px] h-[50px] absolute top-[42%] flex items-center justify-center bg-white rounded-full shadow-[0_2px_5px_-0px_rgba(0,0,0,0.3)] absolute  top-[45%] w-[42px] h-[42px] -right-[0.8vw]']",
    programCardBootcampFSD: "h5[id='program-title-bootcamp-0']",
    programCardBootcampHR: "h5[id='program-title-bootcamp-1']",
    programCardBootcampAudit: "h5[id='program-title-bootcamp-2']",
    programCardBootcampDigmar: "h5[id='program-title-bootcamp-3']",
    programCardBootcampUXR: "h5[id='program-title-bootcamp-4']",

    btnBootcampAllProgram: "div a[id='btn-all-program-bootcamp']",
    imageBanner: "div img[alt='bootcamp banner']",

  },
  sectionListProclass: {
    btnAllProclass: "a[id='btn-all-program-proclass']",
  },
  sectionConsultation: {
    btnAdminSupport: "a[class='relative flex items-center justify-center rounded font-medium hover:shadow-[0_12px_32px_0px_rgba(0,0,0,0.2)] text-white bg-green hover:text-white sm:py-[14px] sm:px-[50px] py-2.5 px-5 text-sm lg:text-base sm:text-md mx-0 text-xs sm-only:mx-auto']",    
  }
}