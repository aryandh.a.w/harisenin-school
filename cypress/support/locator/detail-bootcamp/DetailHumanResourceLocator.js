module.exports = {
  banner: {
    registerNow: "button[id='btn-regist-class-banner']",
  },
  titleSection: "h2[class='sm:text-lg sm:mb-0 font-medium text-green uppercase tracking-widest mb-1']",
  sectionTutor: {
    containerTutor: "div[id='tutor1']",
    tutorName: "div[class='font-semibold sm:mb-2.5 mb-1 sm:text-xl']",
    tutorCompany: "div[class='text-light-grey sm:mb-4 sm-only:text-sm h-[3em] line-clamp-2 mb-2.5']",
},
}