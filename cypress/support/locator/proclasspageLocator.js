module.exports = {
  selector: {
    navigationProclass_active: "[class='button_btn_solid__EydVE undefined programlist_program_list__program__head__menu__item__qTpEW programlist_program_list__program__head__menu__item__active__HIIya']",
    tag_proclass: "[class='card_program_card__type__N1AJq']",
    titleCard_seoSpecialist: "h5[class='card_program_card__title__MoLD4']",
  },
  listOfProclass: {
    img_proclassNotFound: "img[alt='Empty Product']",
    card_seoSpecialist: "[id='program-card-proclass-0']",
  }
}