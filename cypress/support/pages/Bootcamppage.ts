import BootcamppageData from "../dataset/BootcamppageData"
import LandingpageData from "../dataset/LandingpageData";
import BootcamppageLocator from "../locator/BootcamppageLocator";
import LandingpageLocator from "../locator/LandingpageLocator";
import SuiteSteps from "../../e2e/harisenin-school/SuiteSteps";

let suite = new SuiteSteps();

export class Bootcamppage {

  // @HMS-69
  async visitLandingpageHMS() {
    suite.visitHMSLandingpage();
  }
  async clickBtnAllBootcamp() {
    cy.get(LandingpageLocator.sectionListBootcamp.btnBootcampAllProgram).click({force: true});
  }
  async verifyLandingpageBootcamp() {
    cy.wait(5000);
    cy.contains("Upgrade Skill Untuk Karier yang Lebih Baik");
    cy.request(BootcamppageData.assetImage.imageBanner).then((response) => {
      expect(response.status).to.eq(200);
    })
  }

  // @HMS-70
  async clickButtonCTAChooseBootcamp() {
    cy.wait(5000);
    cy.get(BootcamppageLocator.banner.buttonChooseBootcampBanner).click({force: true});
  }
  async verifyCardBootcampDisplayed() {
    cy.get(BootcamppageLocator.banner.titleSection).contains("Program Bootcamp").should("be.visible");
    cy.get(LandingpageLocator.sectionListBootcamp.programCardBootcampFSD).contains("Full-stack Web Developer").should("be.visible");
  }

  // @HMS-73
  async visitBootcamppage() {
    this.visitLandingpageHMS();
    this.clickBtnAllBootcamp();
  }
  async scroolIntoSectionStatisticData() {
    cy.wait(5000);
    cy.get(BootcamppageLocator.sectionStatisic.container).scrollIntoView();
  }
  async verifyDataVisible() {
    cy.get(BootcamppageLocator.sectionStatisic.dataNumeric).contains("99.98%").should("be.visible");
    cy.get(BootcamppageLocator.sectionStatisic.label).contains("Alumni sudah berhasil bekerja");
    cy.get(BootcamppageLocator.sectionStatisic.dataNumeric).contains("3000+ Orang").should("be.visible");
    cy.get(BootcamppageLocator.sectionStatisic.label).contains("Memilih belajar di harisenin.com");
    cy.get(BootcamppageLocator.sectionStatisic.dataNumeric).contains("500+ Company").should("be.visible");
    cy.get(BootcamppageLocator.sectionStatisic.label).contains("Mempercayai dan merekrut alumni kami");
  }

  // @HMS-74
  async scroolIntoSectionCaptureClass() {
    cy.wait(5000);
    cy.get(BootcamppageLocator.sectionCaptureClass.container).scrollIntoView();
  }
  async verifyCaptureClassAndWordingVisible() {
    cy.request(BootcamppageData.assetImage.captureClass).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(BootcamppageLocator.sectionCaptureClass.titleWording).contains("Dari 2021, Hingga Sekarang").should("be.visible")
    cy.get(BootcamppageLocator.sectionCaptureClass.description).contains(BootcamppageLocator.sectionCaptureClass.detailDesc).should("be.visible")
  }

  // @HMS-78
  async scroolIntoSectionConsultataion() {
    cy.get(BootcamppageLocator.sectionConsultation.container).scrollIntoView();
  }
  async verifyBtnConsultationOnBootcamppage() {
    cy.get(BootcamppageLocator.sectionConsultation.buttonConsultationAdmision).contains("Klik untuk Konsultasi. Gratis!").should("be.visible");
  }
}