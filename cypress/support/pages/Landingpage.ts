import LandingpageData from "../dataset/LandingpageData";
import LandingpageLocator from "../locator/LandingpageLocator";
import SuiteSteps from "../../e2e/harisenin-school/SuiteSteps";

let suite = new SuiteSteps();

export class Landingpage {

  // @HMS-2
  async visitHariseninCom() {
    suite.successLogin();
  }
  async onHMSLandingpage() {
    suite.visitHMSLandingpage();
  }
  async verifySuccessLoadHMSLandingpge() {
    cy.wait(5000);
    cy.url().should("eq","https://www.harisenin.com/school");
    cy.contains("Sekolah online yang membantumu menjadi Top 7% Talenta Indonesia").should("be.visible");
    cy.contains("Jelajahi Program Bootcamp").should("be.visible");
    cy.contains("Mentor kami berpengalaman di startup teratas & perusahaan multinasional").should("be.visible");
    cy.contains("Pertanyaan yang Sering Ditanyakan").should("be.visible");
  }

  // @HMS-1210
  async scroolInToSectionListBootcamp() {
    cy.get(LandingpageLocator.sectionListBootcamp.title).contains("Jelajahi Program Bootcamp").scrollIntoView();
    cy.get(LandingpageLocator.sectionListBootcamp.programCardBootcampFSD).contains("Full-stack Web Developer").should("be.visible");
    cy.get(LandingpageLocator.sectionListBootcamp.programCardBootcampHR).contains("Human Resources").should("be.visible");
    cy.get(LandingpageLocator.sectionListBootcamp.programCardBootcampAudit).contains("Big 4 Auditor & Financial Analyst").should("be.visible");
  }
  async clickBtnNextOnListBootcamp() {
    cy.get(LandingpageLocator.sectionListBootcamp.buttonNext).click({force: true});
    cy.get(LandingpageLocator.sectionListBootcamp.programCardBootcampDigmar).contains("Digital Marketer").should("be.visible");
    cy.wait(5000);
    cy.get(LandingpageLocator.sectionListBootcamp.buttonNext).click({force: true});
    cy.get(LandingpageLocator.sectionListBootcamp.programCardBootcampUXR).contains("UI/UX Design & Product Management").should("be.visible");
    cy.get(LandingpageLocator.sectionListBootcamp.buttonPrev).click({force: true}).click({force: true});
  }
  async verifyViewAllCourse() {
    cy.wait(5000);
    cy.get(LandingpageLocator.sectionListBootcamp.buttonPrev).click({force: true}).click({force: true});
    cy.get(LandingpageLocator.sectionListBootcamp.programCardBootcampFSD).contains("Full-stack Web Developer").should("be.visible");
  }

  // @HMS-1211
  async clickButtonViewAllProgram() {
    cy.wait(5000);
    suite.exceptionClickBtn();
    cy.get(LandingpageLocator.sectionListBootcamp.btnBootcampAllProgram).scrollIntoView().click({force: true});
  }
  async verifyBootcamppage() {
    cy.wait(5000);
    cy.get(LandingpageLocator.sectionListBootcamp.imageBanner).should("be.visible");
  }
}
