import SuiteSteps from "../../e2e/harisenin-school/SuiteSteps";
import LandingpageData from "../dataset/LandingpageData";
import BootcamppageLocator from "../locator/BootcamppageLocator";
import LandingpageLocator from "../locator/LandingpageLocator";

let suite = new SuiteSteps();

export class Proclasspage {

  visitProclasspage() {
    cy.visit("");
    cy.viewport(1289, 720);

    suite.exceptionClickBtn();
    cy.get(LandingpageLocator.sectionListProclass.btnAllProclass).click();
    cy.url().should("eq", `${LandingpageData.data_url.school_proclass}`);
  }

  //@HMS-027
  async goToProclasspage() {
    this.visitProclasspage();
  }

  async typeValidSearchProclas() {
    suite.exceptionClickBtn();
  }

  async clickBtnCariProclass() {
  }

  async verifyResultSearchingProclass() {
  }

  //@HMS-028
  async visitSiteProclasspage() {
    this.visitProclasspage();
  }

  async typeInvalidSearchProclass() {
    suite.exceptionClickBtn();
    cy.get(BootcamppageLocator.selector.input_search).click()
    .type("Invalid");
  }

  async clickBtnSearchInvalidProclass() {
  }

  async verifyMsgNotFoundProclass() {
  }

  //@HMS-029
  async onProclasspage() {
  }

  async clickOneOfProclass() {
  }

  async clickBtnRegistClassBannerProclasspage() {
  }

  async verifyInfoBiayaKelas() {
  }

  //HMS-030
  async onProclassPage() {
    this.visitProclasspage();
  }

  async scrollToInfoBenefit() {
  }

  async verifySidebarActiveBenefit() {
    cy.get(BootcamppageLocator.detailBootcampPage.sidebar_active).contains("Benefit");
  }
}