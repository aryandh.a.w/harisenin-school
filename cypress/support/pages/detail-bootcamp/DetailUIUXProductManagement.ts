import { should } from "chai";
import SuiteSteps from "../../../e2e/harisenin-school/SuiteSteps";
import DetailUIUXProductManagementData from "../../dataset/detail-bootcamp/DetailUIUXProductManagementData";
import DetailUIUXProductManagementLocator from "../../locator/detail-bootcamp/DetailUIUXProductManagementLocator";
import LandingpageLocator from "../../locator/LandingpageLocator";

let suite = new SuiteSteps();

export class DetailUIUXProductManagement {
    // @HMS-1221
    async visitUIUXProductManagementBootcamp() {
        suite.visitSchoolBootcamppage();
        suite.exceptionClickBtn();
        cy.get(LandingpageLocator.sectionListBootcamp.programCardBootcampUXR).click({force: true});
    }
    async verifyTitleBannerUIUXBootcamp() {
        cy.contains(DetailUIUXProductManagementData.banner.title);
    }

    // @HMS-1222
    async verifySubtitleBannerUIUX() {
        cy.contains(DetailUIUXProductManagementData.banner.subtitle).should("be.visible");
    }

    // @HMS-1223
    async clickBtnRegisterNow() {
        cy.get(DetailUIUXProductManagementLocator.banner.btnRegister).click({force: true});
    }
    async verifySectionFeeAndRegister() {
        cy.get(DetailUIUXProductManagementLocator.titleSection).contains("Kategori & Biaya Program").should("be.visible");
    }

    // @HMS-1224
    async verifyImageOnBannerUIUXBootcampDisplayed() {
        cy.request(DetailUIUXProductManagementData.banner.imageUrl).then((response) => {
            expect(response.status).to.eq(200);
        })
    }
}