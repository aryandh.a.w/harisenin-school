import LandingpageLocator from "../../locator/LandingpageLocator";
import DetailDigitalMarketingLocator from "../../locator/detail-bootcamp/DetailDigitalMarketingLocator";
import DetailDigitalMarketingData from "../../dataset/detail-bootcamp/DetailDigitalMarketingData";
import SuiteSteps from "../../../e2e/harisenin-school/SuiteSteps";
import { response } from "express";

let suite = new SuiteSteps();

export class DetailDigitalMarketingpage {
  // @HMS-1225
  async visitDigmarBootcamp() {
    suite.visitHMSLandingpage();
    suite.exceptionClickBtn();
    cy.get(LandingpageLocator.sectionListBootcamp.programCardBootcampDigmar).click({force: true});
  }
  async verifyTitleBannerOnDigmarBootcamp() {
    cy.contains(DetailDigitalMarketingData.banner.title).should("be.visible");
  }

  // @HMS-1226
  async verifySubtitleDigmarBootcamp() {
    cy.contains(DetailDigitalMarketingData.banner.subtitle).should("be.visible");
  }

  // @HMS-1227
  async clickBtnRegisterNow() {
    suite.exceptionClickBtn();
    cy.get(LandingpageLocator.sectionListBootcamp.programCardBootcampDigmar).click({force: true});
  }
  async verifySectionFeeAndRegister() {
    cy.get(DetailDigitalMarketingLocator.titleSection).contains("Kategori & Biaya").should("be.visible");
  }

  // @HMS-1228
  async verifyImageDigmarBootcamp() {
    cy.request(DetailDigitalMarketingData.banner.imageUrl).then((response) => {
      expect(response.status).to.eq(200);
    })
  }

  // @HMS-796
  async verifySidebarMenuDigmar() {
    cy.get(DetailDigitalMarketingLocator.sidebar_container).should("be.visible", "Batch Terdekat");
  }

  // @HMS-1229
  async scrollDownUntilUpcomingBatch() {
    cy.get(DetailDigitalMarketingLocator.sectionUpcomingBatch.containerBatchSoon).scrollIntoView();
  }

  async verifyThreeBatchDigmar() {
    //verify icon calender
    cy.request(DetailDigitalMarketingData.sectionUpcommingBatch.urlIconCalender).then((response) => {
      expect(response.status).to.eq(200);
    })

    // verify count element link Daftar equal 3
    cy.get(DetailDigitalMarketingLocator.sectionUpcomingBatch.linkRedirectRegist).should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionUpcomingBatch.cardUpcommingBatch).then((card) => {
      expect(card.length = 3);
    })
  }

  // @HMS-1230
  async sidebarOutcomesDigmar() {
    this.visitDigmarBootcamp();
    cy.get(DetailDigitalMarketingLocator.sidebar_container).scrollIntoView();
  }

  async viewOutcomesOnSidebar() {
    cy.wait(5000);
    suite.exceptionClickBtn();
    cy.get(DetailDigitalMarketingLocator.titleSection).contains("Outcomes").scrollIntoView();
  }

  async verifySectionOutcomesDigmar() {
    cy.wait(5000);
    cy.get(DetailDigitalMarketingLocator.sidebarmenuIsActive).contains("Outcomes").should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionOutcomes.containerOutcomes).contains("Seperti Apa Output yang Kami Berikan?").should("be.visible");

    //expect displays 4 data
    cy.get(DetailDigitalMarketingLocator.sectionOutcomes.dataStatsOutcomes).should("have.length", 4);
    cy.get(DetailDigitalMarketingLocator.sectionOutcomes.dataOutcomes).contains("3000+Alumni telah bekerja").should("be.visible");
  }

  // @HMS-797
  async sidebarBenefitDigmar() {
    this.visitDigmarBootcamp();
  }

  async viewBenefitSidebar() {
    cy.get(DetailDigitalMarketingLocator.titleSection).contains("Benefit").scrollIntoView();
  }

  async verifySectionBenfitDigmar() {
    //contains 6 container benefit
    cy.get(DetailDigitalMarketingLocator.sectionBenefit.containerBenefit).should("have.length", 6);

    //6 icon displayed properly
    cy.request(DetailDigitalMarketingData.sectionBenefit.urlIconRealProject).then((response) => {
      expect(response.status).to.eq(200);
    })

    cy.request(DetailDigitalMarketingData.sectionBenefit.urlIconTopTalent).then((response) => {
      expect(response.status).to.eq(200);
    })

    cy.request(DetailDigitalMarketingData.sectionBenefit.urlIconAprentice).then((response) => {
      expect(response.status).to.eq(200);
    })

    cy.request(DetailDigitalMarketingData.sectionBenefit.urlIconCareerProgram).then((response) => {
      expect(response.status).to.eq(200);
    })

    cy.request(DetailDigitalMarketingData.sectionBenefit.urlIconProject).then((response) => {
      expect(response.status).to.eq(200);
    })

    cy.request(DetailDigitalMarketingData.sectionBenefit.urlIconGetJob).then((response) => {
      expect(response.status).to.eq(200);
    })

    //contains title and detail benefit
    cy.get(DetailDigitalMarketingLocator.sectionBenefit.titleBenefit).contains(DetailDigitalMarketingData.sectionBenefit.portfolioBenefit).should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionBenefit.titleBenefit).contains(DetailDigitalMarketingData.sectionBenefit.topTalentBenefit).should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionBenefit.titleBenefit).contains(DetailDigitalMarketingData.sectionBenefit.apprenticeBenefit).should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionBenefit.titleBenefit).contains(DetailDigitalMarketingData.sectionBenefit.careerprogramBenefit).should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionBenefit.titleBenefit).contains(DetailDigitalMarketingData.sectionBenefit.projectBenefit).should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionBenefit.titleBenefit).contains(DetailDigitalMarketingData.sectionBenefit.getJobBenefit).should("be.visible");
    
    cy.get(DetailDigitalMarketingLocator.sectionBenefit.descBenefit).contains(DetailDigitalMarketingData.sectionBenefit.subBenefitPortfolio).should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionBenefit.descBenefit).contains(DetailDigitalMarketingData.sectionBenefit.subBenefitTopTalent).should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionBenefit.descBenefit).contains(DetailDigitalMarketingData.sectionBenefit.subBenefitApprentice).should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionBenefit.descBenefit).contains(DetailDigitalMarketingData.sectionBenefit.subBenefitCareerprogram).should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionBenefit.descBenefit).contains(DetailDigitalMarketingData.sectionBenefit.subBenefitProject).should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionBenefit.descBenefit).contains(DetailDigitalMarketingData.sectionBenefit.subBenefitGetJob).should("be.visible");
  }

  // @HMS-820
  async viewCuriculumSidebar() {
    cy.get(DetailDigitalMarketingLocator.titleSection).contains("Kurikulum").scrollIntoView();
  }

  async viewSectionCuriculum() {
    cy.get(DetailDigitalMarketingLocator.sectionCurriculum.containerListCuriculum).should("be.visible");
  }

  // @HMS-799
  async viewToolsSidebar() {
    cy.get(DetailDigitalMarketingLocator.titleSection).contains("Tools").scrollIntoView();
    cy.get(DetailDigitalMarketingLocator.subTitleSection).contains("Seperti Apa Output yang Kami Berikan?").should("be.visible");
  }

  async viewSectionTools() {
    cy.request(DetailDigitalMarketingData.sectionTools.urlIconHris).then((response) => {
      expect(response.status).to.eq(200);
    })

    cy.request(DetailDigitalMarketingData.sectionTools.urlIconExcel).then((response) => {
      expect(response.status).to.eq(200);
    })
  }

  // @HMS-812
  async viewTutorSidebar() {
    cy.get(DetailDigitalMarketingLocator.titleSection).contains("Tutor").scrollIntoView();
    cy.get(DetailDigitalMarketingLocator.subTitleSection).contains("Belajar Bareng Tutor yang Ahli di Bidangnya").should("be.visible");
  }

  async viewSectionTutor() {
    //tutor iqbalzakky
    cy.request(DetailDigitalMarketingData.sectionTutor.pototutorIqbalZakky).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTutor.tutorName).contains("Iqbal Zakky").should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionTutor.tutorCompany).contains("Senior Digital Account Manager").should("be.visible");

    cy.request(DetailDigitalMarketingData.sectionTutor.companyInnocean).then((response) => {
      expect(response.status).to.eq(200);
    });


    //tutor RenicaCristyanignrum
    cy.request(DetailDigitalMarketingData.sectionTutor.pototutorRenicaCristiyaningrum).then((response) => {
      expect(response.status).to.eq(200);
    })
    cy.get(DetailDigitalMarketingLocator.sectionTutor.tutorName).contains("Renica Cristyaningrum").should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionTutor.tutorCompany).contains("Executive Marketing Assistant").should("be.visible");
    cy.request(DetailDigitalMarketingData.sectionTutor.companyFulco).then((response) => {
      expect(response.status).to.eq(200);
    })

    //tutor Deny Faedhurrahman
    cy.request(DetailDigitalMarketingData.sectionTutor.pototutorDenyFaedhurahman).then((response) => {
      expect(response.status).to.eq(200);
    })
    cy.get(DetailDigitalMarketingLocator.sectionTutor.tutorName).contains("Deny Faedhurrahman").should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionTutor.tutorCompany).contains("Performance Marketing Manager").should("be.visible");
    cy.request(DetailDigitalMarketingData.sectionTutor.companyBareksa).then((response) => {
      expect(response.status).to.eq(200);
    })
    // click next
    suite.exceptionClickBtn();
    cy.get(DetailDigitalMarketingLocator.sectionTutor.buttonNext, {timeout: 5000}).click({force: true});

    //tutor vanessa asdtari
    cy.request(DetailDigitalMarketingData.sectionTutor.pototutorVanesaAstari).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTutor.tutorName).contains("Vanessa Astari").should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionTutor.tutorCompany).contains("Marketing Specialist").should("be.visible");
    cy.request(DetailDigitalMarketingData.sectionTutor.companyCMLabs).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTutor.buttonNext, {timeout: 5000}).click({force: true});

    //tutor angelika fridora
    cy.request(DetailDigitalMarketingData.sectionTutor.pototutorAngelikaFridora).then((response) => {
      expect(response.status).to.eq(200);
    })
    cy.get(DetailDigitalMarketingLocator.sectionTutor.tutorName).contains("Angelika Fridora").should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionTutor.tutorCompany).contains("Social Media Manager").should("be.visible");
    cy.request(DetailDigitalMarketingData.sectionTutor.companyHaloka).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTutor.buttonNext, {timeout: 5000}).click({force: true});

    //tutor Auliya Damayanti
    cy.request(DetailDigitalMarketingData.sectionTutor.pototutorAuliyaDamayanti).then((response) => {
      expect(response.status).to.eq(200);
    })
    cy.get(DetailDigitalMarketingLocator.sectionTutor.tutorName).contains("Aulia Damayanti").should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionTutor.tutorCompany).contains("Performance Marketing");
    cy.request(DetailDigitalMarketingData.sectionTutor.companyRukita).then((response) => {
      expect(response.status).to.eq(200);
    })

  }

  // @HMS-798
  async viewLearningExperienceSidebar() {
    cy.get(DetailDigitalMarketingLocator.titleSection).contains("Learning Experience").scrollIntoView();
    cy.get(DetailDigitalMarketingLocator.subTitleSection).contains("Gimana Sistem Belajar di Bootcamp Harisenin.com?").should("be.visible");
  }

  async viewSectionLearningExperience() {
    cy.get(DetailDigitalMarketingLocator.sectionLearningExperience.containerRiseLearnAndRiseUp).should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionLearningExperience.areaRiseLearnAndPlay).contains(DetailDigitalMarketingData.sectionLearningExperience.areaRiseLearnAndPlay).should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionLearningExperience.areaRiseUp).contains(DetailDigitalMarketingData.sectionLearningExperience.areaRiseUp).should("be.visible");

    cy.get(DetailDigitalMarketingLocator.sectionLearningExperience.buttonBoxOnBoarding).click({force: true});
    cy.get(DetailDigitalMarketingLocator.sectionLearningExperience.titleGreenOnBoxDesc).contains("Onboarding").should("be.visible");

    cy.get(DetailDigitalMarketingLocator.sectionLearningExperience.buttonBoxSkillAcquisition).click({force: true});
    cy.get(DetailDigitalMarketingLocator.sectionLearningExperience.titleGreenOnBoxDesc).contains("Skill Acquisition & Mission").should("be.visible");

    cy.get(DetailDigitalMarketingLocator.sectionLearningExperience.buttonBoxBigMission).click({force: true});
    cy.get(DetailDigitalMarketingLocator.sectionLearningExperience.titleGreenOnBoxDesc).contains("Big Mission Presentation").should("be.visible");

    cy.get(DetailDigitalMarketingLocator.sectionLearningExperience.buttonBoxGraduation).click({force: true});
    cy.get(DetailDigitalMarketingLocator.sectionLearningExperience.titleGreenOnBoxDesc).contains("Graduation").should("be.visible");

    cy.get(DetailDigitalMarketingLocator.sectionLearningExperience.buttonBoxCareerCoaching).click({force: true});
    cy.get(DetailDigitalMarketingLocator.sectionLearningExperience.titleBlueOnBoxDesc).contains("Career Coaching & Job Connect").should("be.visible");

    cy.get(DetailDigitalMarketingLocator.sectionLearningExperience.buttonBoxRealProject).click({force: true});
    cy.get(DetailDigitalMarketingLocator.sectionLearningExperience.titleBlueOnBoxDesc).contains("Real Project/ Apprenticeship").should("be.visible");

    // Metode Belajar
    cy.contains(DetailDigitalMarketingData.sectionLearningExperience.metodeLearning);
    // interactive learning
    cy.get(DetailDigitalMarketingLocator.sectionLearningExperience.titleMetodeLearning).contains(DetailDigitalMarketingData.sectionLearningExperience.titleInteractiveLearning).should("be.visible"),
    cy.get(DetailDigitalMarketingLocator.sectionLearningExperience.subtitleMetodeLearning).contains(DetailDigitalMarketingData.sectionLearningExperience.descInteractiveLearning).should("be.visible"),
    cy.request(DetailDigitalMarketingData.sectionLearningExperience.bgImageInteractiveLearning).then((response) => {
      expect(response.status).to.eq(200);
    });
    // MissionChalenge
    cy.get(DetailDigitalMarketingLocator.sectionLearningExperience.titleMetodeLearning).contains(DetailDigitalMarketingData.sectionLearningExperience.titleMissionChalenges).should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionLearningExperience.subtitleMetodeLearning).contains(DetailDigitalMarketingData.sectionLearningExperience.descMissionChalenges).should("be.visible");
    cy.request(DetailDigitalMarketingData.sectionLearningExperience.bgImageMissionChalenge).then((response) => {
      expect(response.status).to.eq(200);
    });
    // Team Buddy
    cy.get(DetailDigitalMarketingLocator.sectionLearningExperience.titleMetodeLearning).contains(DetailDigitalMarketingData.sectionLearningExperience.titleTeamBuddy).should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionLearningExperience.subtitleMetodeLearning).contains(DetailDigitalMarketingData.sectionLearningExperience.descTeamBuddy).should("be.visible");
    cy.request(DetailDigitalMarketingData.sectionLearningExperience.bgImageTeamBuddy).then((response) => {
      expect(response.status).to.eq(200);
    });
    // career support
    cy.get(DetailDigitalMarketingLocator.sectionLearningExperience.titleMetodeLearning).contains(DetailDigitalMarketingData.sectionLearningExperience.titleCareerSuport).should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionLearningExperience.subtitleMetodeLearning).contains(DetailDigitalMarketingData.sectionLearningExperience.descCareerSuport).should("be.visible");
    cy.request(DetailDigitalMarketingData.sectionLearningExperience.bgImageCareerSuport).then((response) => {
      expect(response.status).to.eq(200);
    });
  }

  // @HMS-805
  async viewTestimoniSidebar() {
    cy.get(DetailDigitalMarketingLocator.titleSection).contains("Testimoni").scrollIntoView();
    cy.contains("Apa Kata Alumni Harisenin.com?").should("be.visible");
  }

  async viewSectionTestimoni() {
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.areaTestimoni).should("have.length", 23);

    // Glorianna Riadhy
    cy.request(DetailDigitalMarketingData.sectionTestimoni.imageTestimoniGloriana).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.testimoniAlumniName).contains("Glorianna Riadhy").should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.testimooniAlumniCompany).contains("Marketing Staff at");
    cy.request(DetailDigitalMarketingData.sectionTestimoni.companyJNT).then((response) => {
      expect(response.status).to.eq(200);
    });

    // Abiyyu Fakhri
    cy.request(DetailDigitalMarketingData.sectionTestimoni.imageTestimoniAbiyyu).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.testimoniAlumniName).contains("Abiyyu Fakhri").should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.testimooniAlumniCompany).contains("Social Media Officer at");
    cy.request(DetailDigitalMarketingData.sectionTestimoni.companyXXI).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.buttonNextTestimoni).click({multiple: true, force: true});

    // Syafira Audianty
    cy.request(DetailDigitalMarketingData.sectionTestimoni.imageTestimoniSyafira).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.testimoniAlumniName).contains("Syafira Audianty").should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.testimooniAlumniCompany).contains("Brand Marketing Support at");
    cy.request(DetailDigitalMarketingData.sectionTestimoni.companyFlip).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.buttonNextTestimoni).click({multiple: true, force: true});

    // Ridhol Ikhwani
    cy.request(DetailDigitalMarketingData.sectionTestimoni.imageTestimoniRidhol).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.testimoniAlumniName).contains("Ridhol Ikhwani").should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.testimooniAlumniCompany).contains("SEO Specialist at");
    cy.request(DetailDigitalMarketingData.sectionTestimoni.companyArkademy).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.buttonNextTestimoni).click({multiple: true, force: true});

    // Cecelia Tiana
    cy.request(DetailDigitalMarketingData.sectionTestimoni.imageTestimoniCecelia).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.testimoniAlumniName).contains("Cecelia Tiana Noverani").should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.testimooniAlumniCompany).contains("Retail Engagement at");
    cy.request(DetailDigitalMarketingData.sectionTestimoni.companyHMSampoerna).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.buttonNextTestimoni).click({multiple: true, force: true});

    // Donny Dwi Haryanto
    cy.request(DetailDigitalMarketingData.sectionTestimoni.imageTestimoniDony).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.testimoniAlumniName).contains("Donny Dwi Haryanto").should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.testimooniAlumniCompany).contains("Marketing Specialist at");
    cy.request(DetailDigitalMarketingData.sectionTestimoni.companyCoulavaDigital).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.buttonNextTestimoni).click({multiple: true, force: true});

    // Dini Oktaviani
    cy.request(DetailDigitalMarketingData.sectionTestimoni.imageTestimoniDiniOktaviani).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.testimoniAlumniName).contains("Dini Oktaviani").should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.testimooniAlumniCompany).contains("Digital Marketing at");
    cy.request(DetailDigitalMarketingData.sectionTestimoni.companyKitaBisa).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.buttonNextTestimoni).click({multiple: true, force: true});

    // Galih Akmal Latif
    cy.request(DetailDigitalMarketingData.sectionTestimoni.imageTestimoniGalih).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.testimoniAlumniName).contains("Galih Akmal Latif").should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.testimooniAlumniCompany).contains("Employer Branding at");
    cy.request(DetailDigitalMarketingData.sectionTestimoni.companyTokopedia, {setTimeout: 5000}).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.buttonNextTestimoni).click({multiple: true, force: true});

    // Hanifah Hasna Makarim
    cy.request(DetailDigitalMarketingData.sectionTestimoni.imageTestimoniHanifah).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.testimoniAlumniName).contains("Hanifah Hasna Makarim").should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.testimooniAlumniCompany).contains("Social Media Specialist at");
    cy.request(DetailDigitalMarketingData.sectionTestimoni.companyXiaomi).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.buttonNextTestimoni).click({multiple: true, force: true});

    // Vebrian Angga Raditya
    cy.request(DetailDigitalMarketingData.sectionTestimoni.imageTestimoniVebrian).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.testimoniAlumniName).contains("Vebrian Angga Raditya").should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.testimooniAlumniCompany).contains("Sales Representative at");
    cy.request(DetailDigitalMarketingData.sectionTestimoni.companyRevcomm).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.buttonNextTestimoni).click({multiple: true, force: true});

    // Syafiqa Amanda
    cy.request(DetailDigitalMarketingData.sectionTestimoni.imageTestimoniSyafiqa).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.testimoniAlumniName).contains("Syafiqa Amanda").should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.testimooniAlumniCompany).contains("Sales & Marketing at");
    cy.request(DetailDigitalMarketingData.sectionTestimoni.companyTokopedia, {setTimeout: 5000}).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.buttonNextTestimoni).click({multiple: true, force: true});

    // Putri Aisha
    cy.request(DetailDigitalMarketingData.sectionTestimoni.imageTestimoniPutriAisha).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.testimoniAlumniName).contains("Putri Aisha").should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.testimooniAlumniCompany).contains("Jr. Content Writer at");
    cy.request(DetailDigitalMarketingData.sectionTestimoni.companySpaceAndShapes).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.buttonNextTestimoni).click({multiple: true, force: true});

    // Gregorius Alvin
    cy.request(DetailDigitalMarketingData.sectionTestimoni.imageTestimoniGregorius).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.testimoniAlumniName).contains("Gregorius Alvin").should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.testimooniAlumniCompany).contains("Marketing Staff at");
    cy.request(DetailDigitalMarketingData.sectionTestimoni.companyKiatAnandaGroup).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.buttonNextTestimoni).click({multiple: true, force: true});

    // Gregorius Fendi
    cy.request(DetailDigitalMarketingData.sectionTestimoni.imageTestimoniGregorius).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.testimoniAlumniName).contains("Gregorius Alvin").should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.testimooniAlumniCompany).contains("Strategic Marketing at");
    cy.request(DetailDigitalMarketingData.sectionTestimoni.companyMetroTV).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.buttonNextTestimoni).click({multiple: true, force: true});
    
    // Daffa Noor Salmandika
    cy.request(DetailDigitalMarketingData.sectionTestimoni.imageTestimoniDafaNoor).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.testimoniAlumniName).contains("Daffa Noor Salmandika").should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.testimooniAlumniCompany).contains("Video Content Creative at");
    cy.request(DetailDigitalMarketingData.sectionTestimoni.companyGrab).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.buttonNextTestimoni).click({multiple: true, force: true});

    // Athiyyah Aryaza Putri
    cy.request(DetailDigitalMarketingData.sectionTestimoni.imageTestimoniAthiyya).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.testimoniAlumniName).contains("Athiyyah Aryaza Putri").should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.testimooniAlumniCompany).contains("Academic Operation at");
    cy.request(DetailDigitalMarketingData.sectionTestimoni.companySchoters).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.buttonNextTestimoni).click({multiple: true, force: true});

    // Alif Muhammad Firdaus
    cy.request(DetailDigitalMarketingData.sectionTestimoni.imageTestimoniAlif).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.testimoniAlumniName).contains("Alif Muhammad Firdaus").should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.testimooniAlumniCompany).contains("Marketing Support at");
    cy.request(DetailDigitalMarketingData.sectionTestimoni.companyOfficeSecuritySolution).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.buttonNextTestimoni).click({multiple: true, force: true});

    // Jeremi Napoli Siregar
    cy.request(DetailDigitalMarketingData.sectionTestimoni.imageTestimoniJermyNapoli).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.testimoniAlumniName).contains("Jeremi Napoli Siregar").should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.testimooniAlumniCompany).contains("Digital Marketing at");
    cy.request(DetailDigitalMarketingData.sectionTestimoni.companyKlinikUtama).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.buttonNextTestimoni).click({multiple: true, force: true});

    // Nanda Fitri Supriani
    cy.request(DetailDigitalMarketingData.sectionTestimoni.imageTestimoniNandaFitri).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.testimoniAlumniName).contains("Nanda Fitri Supriani").should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.testimooniAlumniCompany).contains("SEO Content Writer at");
    cy.request(DetailDigitalMarketingData.sectionTestimoni.companyPowerCommerceAsia).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.buttonNextTestimoni).click({multiple: true, force: true});

    // Dimas Prabantio Wahyu Wibowo
    cy.request(DetailDigitalMarketingData.sectionTestimoni.imageTestimoniDimasPrabantio).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.testimoniAlumniName).contains("Dimas Prabantio Wahyu Wibowo").should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.testimooniAlumniCompany).contains("Sales & Marketing at");
    cy.request(DetailDigitalMarketingData.sectionTestimoni.companyPoetiMountain).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.buttonNextTestimoni).click({multiple: true, force: true});

    // Fendik Dana Iswara
    cy.request(DetailDigitalMarketingData.sectionTestimoni.imageTestimoniFendikDana).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.testimoniAlumniName).contains("Fendik Dana Iswara").should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.testimooniAlumniCompany).contains("Digital Marketing at");
    cy.request(DetailDigitalMarketingData.sectionTestimoni.companyEximdo).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.buttonNextTestimoni).click({multiple: true, force: true});

    // Hudan Dardiri
    cy.request(DetailDigitalMarketingData.sectionTestimoni.imageTestimoniHudanDardiri).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.testimoniAlumniName).contains("Hudan Dardiri").should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionTestimoni.testimooniAlumniCompany).contains("Creative Marketing at");
    cy.request(DetailDigitalMarketingData.sectionTestimoni.companyKubuku).then((response) => {
      expect(response.status).to.eq(200);
    });
  }

  // @HMS-804
  async viewPortfolioSidebar() {
    cy.get(DetailDigitalMarketingLocator.titleSection).contains("Portofolio Alumni").scrollIntoView();
    cy.contains("Yuk, Intip Hasil Karya dari Para Alumni!").should("be.visible");
  }

  async viewSectionPortfolio() {
    cy.get(DetailDigitalMarketingLocator.sectionPortFolioAlumni.titlePortfolio).should("have.length", 21);
    cy.get(DetailDigitalMarketingLocator.sectionPortFolioAlumni.descPortfolio).should("have.length", 21);

    // portfolio_0
    cy.request(DetailDigitalMarketingData.sectionPortfolioAlumni.imagePortfolio_0).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionPortFolioAlumni.linkPortofolio_0).should("be.visible");

    // portfolio_1
    cy.request(DetailDigitalMarketingData.sectionPortfolioAlumni.imagePortfolio_1).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionPortFolioAlumni.linkPortofolio_1).should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionPortFolioAlumni.buttonNextPortfolio).click({multiple: true, force: true});

    // portfolio_2
    cy.request(DetailDigitalMarketingData.sectionPortfolioAlumni.imagePortfolio_2).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionPortFolioAlumni.linkPortofolio_2).should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionPortFolioAlumni.buttonNextPortfolio).click({multiple: true, force: true});

    // portfolio_3
    cy.request(DetailDigitalMarketingData.sectionPortfolioAlumni.imagePortfolio_3).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionPortFolioAlumni.linkPortofolio_3).should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionPortFolioAlumni.buttonNextPortfolio).click({multiple: true, force: true});

    // portfolio_4
    cy.request(DetailDigitalMarketingData.sectionPortfolioAlumni.imagePortfolio_4).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionPortFolioAlumni.linkPortofolio_4).should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionPortFolioAlumni.buttonNextPortfolio).click({multiple: true, force: true});

    // portfolio_5
    cy.request(DetailDigitalMarketingData.sectionPortfolioAlumni.imagePortfolio_5).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionPortFolioAlumni.linkPortofolio_5).should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionPortFolioAlumni.buttonNextPortfolio).click({multiple: true, force: true});

    // portfolio_6
    cy.request(DetailDigitalMarketingData.sectionPortfolioAlumni.imagePortfolio_6).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionPortFolioAlumni.linkPortofolio_6).should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionPortFolioAlumni.buttonNextPortfolio).click({multiple: true, force: true});

    // portfolio_7
    cy.request(DetailDigitalMarketingData.sectionPortfolioAlumni.imagePortfolio_7).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionPortFolioAlumni.linkPortofolio_7).should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionPortFolioAlumni.buttonNextPortfolio).click({multiple: true, force: true});

    // portfolio_8
    cy.request(DetailDigitalMarketingData.sectionPortfolioAlumni.imagePortfolio_8).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionPortFolioAlumni.linkPortofolio_8).should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionPortFolioAlumni.buttonNextPortfolio).click({multiple: true, force: true});

    // portfolio_9
    cy.request(DetailDigitalMarketingData.sectionPortfolioAlumni.imagePortfolio_9).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionPortFolioAlumni.linkPortofolio_9).should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionPortFolioAlumni.buttonNextPortfolio).click({multiple: true, force: true});

    // portfolio_10
    cy.request(DetailDigitalMarketingData.sectionPortfolioAlumni.imagePortfolio_10).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionPortFolioAlumni.linkPortofolio_10).should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionPortFolioAlumni.buttonNextPortfolio).click({multiple: true, force: true});

    // portfolio_11
    cy.request(DetailDigitalMarketingData.sectionPortfolioAlumni.imagePortfolio_11).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionPortFolioAlumni.linkPortofolio_11).should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionPortFolioAlumni.buttonNextPortfolio).click({multiple: true, force: true});

    // portfolio_12
    cy.request(DetailDigitalMarketingData.sectionPortfolioAlumni.imagePortfolio_12).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionPortFolioAlumni.linkPortofolio_12).should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionPortFolioAlumni.buttonNextPortfolio).click({multiple: true, force: true});

    // portfolio_13
    cy.request(DetailDigitalMarketingData.sectionPortfolioAlumni.imagePortfolio_13).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionPortFolioAlumni.linkPortofolio_13).should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionPortFolioAlumni.buttonNextPortfolio).click({multiple: true, force: true});

    // portfolio_14
    cy.request(DetailDigitalMarketingData.sectionPortfolioAlumni.imagePortfolio_14).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionPortFolioAlumni.linkPortofolio_14).should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionPortFolioAlumni.buttonNextPortfolio).click({multiple: true, force: true});

    // portfolio_15
    cy.request(DetailDigitalMarketingData.sectionPortfolioAlumni.imagePortfolio_15).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionPortFolioAlumni.linkPortofolio_15).should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionPortFolioAlumni.buttonNextPortfolio).click({multiple: true, force: true});

    // portfolio_16
    cy.request(DetailDigitalMarketingData.sectionPortfolioAlumni.imagePortfolio_16).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionPortFolioAlumni.linkPortofolio_16).should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionPortFolioAlumni.buttonNextPortfolio).click({multiple: true, force: true});

  };

  // @HMS-801
  async viewScheduleBootcampSidebar() {
    cy.get(DetailDigitalMarketingLocator.titleSection).contains("Jadwal Bootcamp").should("be.visible");
    cy.contains("Hanya dengan 3 - 5 Bulan Menggali Potensi Kamu").should("be.visible");
  }

  async viewSectionScheduleBootcamp() {
    cy.get(DetailDigitalMarketingLocator.sectionScheduleBootcamp.headerTableScheduleBootcamp).should("be.visible");
  }

  // @HMS-1231
  async viewCategoryProgramSidebar() {
    cy.wait(6000);
    cy.get(DetailDigitalMarketingLocator.sectionCategoryProgram.containerCategoryProgram).scrollIntoView();
    cy.get(DetailDigitalMarketingLocator.titleSection).contains("Kategori & Biaya").should("be.visible");
    cy.contains("Apa Saja Program dan Benefit yang Ditawarkan?").should("be.visible");
  }

  async viewSectionCategoryProgram() {
    cy.get(DetailDigitalMarketingLocator.sectionCategoryProgram.iconChecklistOnCategoryProgram).should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionCategoryProgram.iconFalseOnCategoryProgram).should("be.visible");

    cy.get(DetailDigitalMarketingLocator.sectionCategoryProgram.tabCreditProgramInvest).then($tabCicilan => {
      if ($tabCicilan.is(':visible')) {
        cy.get(DetailDigitalMarketingLocator.sectionCategoryProgram.tabCreditProgramInvest).click({force: true});
        cy.get(DetailDigitalMarketingLocator.sectionCategoryProgram.titleCicilan).contains("cicilan").should("be.visible");
        cy.get(DetailDigitalMarketingLocator.sectionCategoryProgram.linkDocs).should("be.visible");
        cy.get(DetailDigitalMarketingLocator.sectionCategoryProgram.buttonContactAdminProgramInvest).contains("Daftar Dengan Cicilan").should("be.visible");
      }
    });

  }

  // @HMS-821
  async viewPaymentMethodSidebar() {
    cy.get(DetailDigitalMarketingLocator.titleSection).contains("Metode Pembayaran").scrollIntoView();
    cy.contains("Bayar Jadi Mudah & Bisa Dicicil").should("be.visible");
  }

  async viewSectionPaymentMethod() {
    // upfront
    cy.request(DetailDigitalMarketingData.sectionPaymentMethod.urlIconCashUpFront).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionPaymentMethod.titlePaymentMethod).contains("Cash 100% Upfront").should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionPaymentMethod.descPaymentMethod).should("be.visible");

    // cicilan
    cy.request(DetailDigitalMarketingData.sectionPaymentMethod.urlIconCicilan).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionPaymentMethod.titlePaymentMethod).contains("Cicilan").should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionPaymentMethod.descPaymentMethod).should("be.visible");
  }

  // @HMS-806
  async viewFAQSidebar() {
    cy.get(DetailDigitalMarketingLocator.titleSection).contains("FAQ").scrollIntoView();
    cy.contains("Pertanyaan Seputar Bootcamp").should("be.visible");
  }

  async viewSectionFAQ() {
    cy.get(DetailDigitalMarketingLocator.sectionFAQ.containerFAQ).should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionFAQ.questionsList).click({multiple: true});
    cy.get(DetailDigitalMarketingLocator.sectionFAQ.areaAnswer).should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionFAQ.questionsList).click({multiple: true});
  }

  // @HMS-816
  async viewOtherBootcampSection() {
    cy.get(DetailDigitalMarketingLocator.sectionOtherBootcamp.titleOtherBootcamp).contains("Bootcamp Lainnya").scrollIntoView();
    cy.contains("Tertarik belajar bidang lain?").should("be.visible");
  }

  async viewAllBootcampOnSlidebar() {
    cy.get(DetailDigitalMarketingLocator.sectionOtherBootcamp.tagBootcamp).should("have.length", 5);
    cy.get(DetailDigitalMarketingLocator.sectionOtherBootcamp.areaDurationAndSchedule).should("be.visible");

    // Fullstackweb
    cy.request(DetailDigitalMarketingData.sectionOtherBootcamp.imagecardFullstackWeb).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionOtherBootcamp.titleFullstackWebOnCard).contains("Full-stack Web Developer").should("be.visible");

    // Humanresource
    cy.request(DetailDigitalMarketingData.sectionOtherBootcamp.imagecardHumanresource).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionOtherBootcamp.titleHumanresourceOnCard).contains("Human Resources").should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionOtherBootcamp.btnNextCardBootcamp).click({force: true});

    // Big4Auditor
    cy.request(DetailDigitalMarketingData.sectionOtherBootcamp.imageCardBig4Auditor).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionOtherBootcamp.titleBig4AuditorOnCard).contains("Big 4 Auditor & Financial Analyst").should("be.visible");

    // Digital Marketer
    cy.request(DetailDigitalMarketingData.sectionOtherBootcamp.imageCardDigmar).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionOtherBootcamp.titleDigmarOnCard).contains("Digital Marketer").should("be.visible");

    // UXR
    cy.request(DetailDigitalMarketingData.sectionOtherBootcamp.imageCardUXR).then((response) => {
      expect(response.status).to.eq(200);
    });
    cy.get(DetailDigitalMarketingLocator.sectionOtherBootcamp.titleUXROnCard).contains("UI/UX Design & Product Management").should("be.visible");
  }

  // @HMS-852
  async scrollIntoViewCurriculumSection() {
    cy.get(DetailDigitalMarketingLocator.sectionCurriculum.containerCuriculum).scrollIntoView();
  }

  async clickBtnDownloadCurriculumDigmarBootcamp() {
    cy.get(DetailDigitalMarketingLocator.sectionCurriculum.btnDownload, {timeout:5000}).contains("Download Kurikulum").click({force: true});
    cy.wait(6000);
  }

  async verifyDisplayingModalboxLogin() {
    cy.get(DetailDigitalMarketingLocator.sectionCurriculum.containerModalLogin).contains("Login").should("be.visible");
  }

  // @HMS-853
  async InputUsernameAndPasswordInTheCurriculumModalbox() {
    cy.get(DetailDigitalMarketingLocator.sectionCurriculum.inputLoginEmail).type(DetailDigitalMarketingData.sectionCurriculum.emaiLogin);
    cy.get(DetailDigitalMarketingLocator.sectionCurriculum.inputLoginPassword).type(DetailDigitalMarketingData.sectionCurriculum.passwordLogin);
    cy.get(DetailDigitalMarketingLocator.sectionCurriculum.buttonLogin).contains("Masuk").click({force: true});
    cy.wait(6000);
  }

  async verifyModalBoxForDownloadSylabus() {
    cy.wait(7000);
    cy.get(DetailDigitalMarketingLocator.sectionCurriculum.titleFormDataForDownloadSylabus).contains("Yuk, Isi Data Dirimu untuk Akses Materinya!").should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionCurriculum.inputUserFullname).should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionCurriculum.inputUserEmail).should("have.value", DetailDigitalMarketingData.sectionCurriculum.emaiLogin);
    cy.get(DetailDigitalMarketingLocator.sectionCurriculum.inputUserPhonenumber).type("85377745621");
    cy.get(DetailDigitalMarketingLocator.sectionCurriculum.inputReasonDownload, {timeout: 5000}).type("memahami kurikulum bootcamp dengan mendalam");
    cy.get(DetailDigitalMarketingLocator.sectionCurriculum.btnDownloadCurriculum).contains("Download Kurikulum").click({force: true});
    cy.get(DetailDigitalMarketingLocator.sectionCurriculum.buttonClosePopupDownload).click({force: true});
  }

  // @HMS-854
  async clickBtnDownloadWithoutInputRequiredField() {
    cy.wait(500);
    cy.get(DetailDigitalMarketingLocator.sectionCurriculum.btnDownloadCurriculum).click({force: true});
  }
  async verifyErrorMessageRequiredFields() {
    cy.wait(5000);
    cy.get(DetailDigitalMarketingLocator.sectionCurriculum.errorMessageRequiredFields).contains("*Wajib diisi").should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionCurriculum.errorMessageOnReasonArea).contains("Minimal 20 Karakter (0/300)").should("be.visible");
  }
  // @HMS-860
  async accessesDownloadSylabusPopup() {
    suite.successLogin();
    this.visitDigmarBootcamp();
    this.clickBtnDownloadCurriculumDigmarBootcamp();
    cy.wait(6000);
  }

  async inputDataInThePopupDownloadSylabus() {
    cy.get(DetailDigitalMarketingLocator.sectionCurriculum.titleFormDataForDownloadSylabus).contains("Yuk, Isi Data Dirimu untuk Akses Materinya!").should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionCurriculum.inputUserFullname).should("be.visible");
    cy.get(DetailDigitalMarketingLocator.sectionCurriculum.inputUserEmail).should("have.value", DetailDigitalMarketingData.sectionCurriculum.emaiLogin);
    cy.get(DetailDigitalMarketingLocator.sectionCurriculum.inputUserPhonenumber).type("85377745621");
    cy.get(DetailDigitalMarketingLocator.sectionCurriculum.inputReasonDownload, {timeout: 5000}).type("memahami kurikulum bootcamp dengan mendalam");
  }

  async verifySuccessfullyDownload() {
    this.clickBtnDownloadCurriculumDigmarBootcamp();
    cy.wait(5000);
    this.clickBtnDownloadCurriculumDigmarBootcamp();
    cy.wait(5000);

    cy.get(DetailDigitalMarketingLocator.sectionCurriculum.inputUserPhonenumber).type("85377745621");
    cy.get(DetailDigitalMarketingLocator.sectionCurriculum.inputReasonDownload, {timeout: 5000}).type("memahami kurikulum bootcamp dengan mendalam");
    cy.get(DetailDigitalMarketingLocator.sectionCurriculum.btnDownloadCurriculum).contains("Download Kurikulum").click({force: true});
    cy.get(DetailDigitalMarketingLocator.sectionCurriculum.buttonClosePopupDownload).click({force: true});
    cy.wait(7000);
    cy.request(DetailDigitalMarketingData.sectionCurriculum.sylabus).then((res) => {
      expect(res.status).to.eq(200);
    })
  }

  // @HMS-861
  async clickCloseIconOnDownloadPopup() {
    cy.wait(6000);
    cy.get(DetailDigitalMarketingLocator.sectionCurriculum.buttonClosePopupDownload).click({force: true});
  }

  async verifySystemClosingDownloadPopup() {
    cy.get(DetailDigitalMarketingLocator.sectionCurriculum.btnDownload).should("be.visible");
  }

}