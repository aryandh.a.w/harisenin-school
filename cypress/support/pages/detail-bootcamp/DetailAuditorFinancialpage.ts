import DetailAuditorFinancialLocator from "../../locator/detail-bootcamp/DetailAuditorFinancialLocator";
import DetailAuditorFinancialData from "../../dataset/detail-bootcamp/DetailAuditorFinancialData";
import LandingpageLocator from "../../locator/LandingpageLocator";
import SuiteSteps from "../../../e2e/harisenin-school/SuiteSteps";

let suite = new SuiteSteps();

export class DetailAuditorFinancialpage {

    // @HMS-1217
    async visitAuditorAnalystBootcamp() {
        suite.visitSchoolBootcamppage();
        suite.exceptionClickBtn();
        cy.get(LandingpageLocator.sectionListBootcamp.programCardBootcampAudit).click({force: true});
        cy.wait(6000);
    }

    async verifyTitleBannerAuditorBootcamp() {
        cy.contains(DetailAuditorFinancialData.banner.title).should("be.visible");
    }

    // @HMS-1218
    async verifySubtitleBannerAuditorBootcamp() {
        cy.contains(DetailAuditorFinancialData.banner.subtitle).should("be.visible");
    }

    // @HMS-1219
    async clickButtonRegisterNow() {
        cy.wait(5000);
        suite.exceptionClickBtn();
        cy.get(DetailAuditorFinancialLocator.banner.btnRegister, {timeout: 5000}).click({force: true});
    }

    async verifySectionFeeAndRegisterOnAuditorBootcamp() {
        cy.get(DetailAuditorFinancialLocator.titleSection, {timeout: 6000}).contains("Kategori & Biaya").should("be.visible");
    }

    // @HMS-1220
    async verifyImageOnBannerAuditor() {
        cy.request(DetailAuditorFinancialData.banner.imageUrl).then((response) => {
            expect(response.status).to.eq(200);
        })
    }

}