import SuiteSteps from "../../../e2e/harisenin-school/SuiteSteps";
import LandingpageLocator from "../../locator/LandingpageLocator";
import DetailFullstackWebData from "../../dataset/detail-bootcamp/DetailFullstackWebData";
import DetailFullstackWebLocator from "../../locator/detail-bootcamp/DetailFullstackWebLocator";
import BootcamppageData from "../../dataset/BootcamppageData";

let suite = new SuiteSteps();

export class DetailFullstackWebpage {
    // @HMS-203
    async visitDetailFullstackWebPage() {
        suite.visitSchoolBootcamppage();
        cy.wait(5000);
        cy.get(LandingpageLocator.sectionListBootcamp.programCardBootcampFSD).scrollIntoView();
        suite.exceptionClickBtn();
        cy.get(LandingpageLocator.sectionListBootcamp.programCardBootcampFSD).click({force: true});
    }
    async verifyTitleBannerFSDBootcamp() {
        cy.contains(DetailFullstackWebData.banner.title);
    }

    // @HMS-204
    async verifySubtitleOnBannerBootcamppage() {
        cy.contains(DetailFullstackWebData.banner.subtitle);
    }

    // @HMS-205
    async clickCTABtnRegisterNow() {
        cy.wait(6000);
        cy.get(DetailFullstackWebLocator.banner.registerNow).click({force: true}).click({force: true});
        cy.wait(6000);
    }
    async verifySectionFeeAndRegister() {
        cy.wait(6000);
        cy.get(DetailFullstackWebLocator.titleSection).contains("Kategori & Biaya").should("be.visible");
    }

    // @HMS-733
    async verifyImageFullstackDsiplayedOnBanner() {
        cy.request(DetailFullstackWebData.banner.urlImage).then((response) => {
            expect(response.status).to.eq(200);
        })
    }
}