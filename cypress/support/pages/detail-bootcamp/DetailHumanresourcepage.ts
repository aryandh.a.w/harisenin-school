import SuiteSteps from "../../../e2e/harisenin-school/SuiteSteps";
import LandingpageLocator from "../../locator/LandingpageLocator";
import LandingpageData from "../../dataset/LandingpageData";
import DetailHumanresourceData from "../../dataset/detail-bootcamp/DetailHumanresourceData";
import DetailHumanResourceLocator from "../../locator/detail-bootcamp/DetailHumanResourceLocator";

let suite = new SuiteSteps();

export class DetailHumanResourcepage {

  // @HMS-1213
  async visitHumaresourceBootcamp() {
    suite.visitSchoolBootcamppage();
    suite.exceptionClickBtn();
    cy.get(LandingpageLocator.sectionListBootcamp.programCardBootcampHR).click({force: true});
  }

  async verifyTitleTextOnBannerHRbootcamp() {
    cy.contains("Siap Kerja Menjadi HR Profesional dalam 18 Minggu");
  }

  // @HMS-1214
  async verifySubtitleBannerHResourceBootcamp() {
    cy.contains(DetailHumanresourceData.banner.subtitle);
  }

  // @HMS-1215
  async clickButtonRegisterNow() {
    suite.exceptionClickBtn();
    cy.get(DetailHumanResourceLocator.banner.registerNow).click({force: true});
  }
  async verifySectionFeeAndRegistration() {
    cy.get(DetailHumanResourceLocator.titleSection).contains("Kategori & Biaya").should("be.visible");
  }

  // @HMS-1216
  async verifyImageHRBootcampOnBanner() {
    cy.request(DetailHumanresourceData.banner.urlImage).then((response) => {
      expect(response.status).to.eq(200);
    })
  }
}