import SuiteSteps from "../../../e2e/harisenin-school/SuiteSteps";
import BootcamppageLocator from "../../locator/BootcamppageLocator";

let suite = new SuiteSteps();

export class DetailSeospecialistpage{
  async userSuccessLoginAccount() {
    suite.successLogin();
  }

  async clickAllProclass() {
  }

  async clickSeoSpecialistProclass() {
  }

  async clickBtnRegistBanner() {
  }

  async verifyBtnSoldoutEarlybirdpromo() {
    cy.get(BootcamppageLocator.detailBootcampPage.investment_card0).contains("Sold Out").should("be.visible");
  }
}