module.exports = {
    banner: {
        title: "Siap Kerja Menjadi Auditor & Financial Analyst dalam 17 Minggu",
        subtitle: "Belajar Audit dan Financial Analysis dengan Job Guarantee Program paling terjangkau di Indonesia. Apabila setelah lulus kamu masih menganggur dalam 365 hari dapatkan refund 110%.",
        imageUrl: "https://www.harisenin.com/cdn-cgi/image/width=1920/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fhms%2F90608c74ac32135c585b33202cc7f21f.webp",
    }
}