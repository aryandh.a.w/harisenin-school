module.exports = {
    banner: {
        title: "Siap Kerja Menjadi UI/UX Designer & Product Manager dalam 16 Minggu",
        subtitle: "Belajar UI/UX sekaligus Product Management jadi lebih hemat plus Job Guarantee program. Apabila setelah lulus kamu masih menganggur dalam 365 hari, dapatkan refund 110%.",
        imageUrl: "https://www.harisenin.com/cdn-cgi/image/width=1920/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fhms%2F35549194265d5d205424d13aca525c43.webp",
    }
}