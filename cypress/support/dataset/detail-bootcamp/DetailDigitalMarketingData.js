module.exports = {
    banner: {
        title: "Siap Kerja Menjadi Digital Marketer Professional dalam 12 Minggu",
        subtitle: "Belajar Digital Marketing dengan Job Guarantee Program paling terjangkau di Indonesia. Apabila setelah lulus kamu masih menganggur dalam 365 hari dapatkan refund 110%.",
        imageUrl: "https://www.harisenin.com/cdn-cgi/image/width=3840/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fhms%2F174cc707388090b283040a443296e524.webp",
    },
    sectionUpcommingBatch: {
        urlIconCalender: "https://www.harisenin.com/cdn-cgi/image/width=48/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fpublic%2Fassets%2Ficon%2Ficon_calendar.png",
    },
    sectionBenefit: {
        urlIconRealProject: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fhms%2Fc5b87ba9033f9c96f064fd7e78c66540.webp",
        urlIconTopTalent: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fhms%2Fb6c49c09c8fa2c497b36c7dda24e1798.webp",
        urlIconAprentice: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fhms%2F6543f9ffb7c25bfbf390f6561ddc572d.webp",
        urlIconCareerProgram: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fhms%2F686c93bea636388b5c7e4e240de04d59.webp",
        urlIconProject: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fhms%2F0e7a7feb7ba35aa95ba6dbcd010b0cd5.webp",
        urlIconGetJob: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fhms%2Fd5cd13e961846f2b905f8a9e326662a8.webp",

        portfolioBenefit: "Build Real Project Portfolio",
        subBenefitPortfolio: "Bangun portofolio untuk penunjang career based on real project yang diawasi & dibimbing oleh tutor top company.",
        topTalentBenefit: "Be A Top Talent",
        subBenefitTopTalent: "Menjadi talenta yang kredibel dan mumpuni dimana perusahaan siap merekrutmu.",
        apprenticeBenefit: "Apprenticeship Setelah Lulus",
        subBenefitApprentice: "Dapatkan pengalaman kerja nyata melalui program apprentice dari hiring partner setelah lulus. (*khusus Get A Job & Refund Guarantee program)",
        careerprogramBenefit: "Career Preparation Program",
        subBenefitCareerprogram: "Konsultasikan rencana karier masa depanmu bersama career specialist harisenin.com kapan saja. (*khusus Get A Job & Refund Guarantee program)",
        projectBenefit: "Project for Alumni Only!",
        subBenefitProject: "Dapatkan uang tambahan melalui freelance dan part time project dari hiring partner harisenin.com, khusus untuk alumni.",
        getJobBenefit: "Get A Job or Refund 110%",
        subBenefitGetJob: "Bisa refund s.d. 110% jika masih menganggur 360 hari setelah lulus. Syarat dan ketentuan berlaku. (*khusus Refund Guarantee program)",
    },
    sectionCurriculum: {
        emaiLogin: "bashar.mucad@feerock.com",
        passwordLogin: "harisenin1",
        sylabus: "https://nos.jkt-1.neo.id/harisenin-storage/hms/portfolio/7d6ab5acfeed49720463ddbb3409cb29.pdf",
    },
    sectionTools: {
        urlIconHris: "https://www.harisenin.com/cdn-cgi/image/width=2048/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fhms%2Fschool-requirement-tools%2Fb5707d41b674346568f89c273b19ca39.webp",
        urlIconExcel: "https://www.harisenin.com/cdn-cgi/image/width=2048/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fhms%2Fschool-requirement-tools%2F3a0359c9e6c78da09b6692b997b15e16.webp",
    },
    sectionTutor: {
        pototutorIqbalZakky: "https://www.harisenin.com/cdn-cgi/image/width=2048/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Ftutor%2Fharisenin-1635241931.jfif",
        pototutorRenicaCristiyaningrum: "https://www.harisenin.com/cdn-cgi/image/width=2048/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Ftutor%2F7a155b42bf25ded2fb54d90fae77887a.webp",
        pototutorDenyFaedhurahman: "https://www.harisenin.com/cdn-cgi/image/width=2048/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Ftutor%2Fad3146a949c7d5f934403eb74c4e4c6a.webp",
        pototutorVanesaAstari: "https://www.harisenin.com/cdn-cgi/image/width=2048/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Ftutor%2F1f6e06bcb72f6795819a9831bc5c745b.webp",
        pototutorAngelikaFridora: "https://www.harisenin.com/cdn-cgi/image/width=2048/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Ftutor%2F1468123e7cb317104a165ed7f0237ea2.webp",
        pototutorAuliyaDamayanti: "https://www.harisenin.com/cdn-cgi/image/width=2048/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Ftutor%2Ff018496a47e58395814a05e73edc574e.webp",

        companyInnocean: "https://www.harisenin.com/cdn-cgi/image/width=1080/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fcompany%2Fd30933c55958d94239efd56c3b60a53c.webp",
        companyFulco: "https://www.harisenin.com/cdn-cgi/image/width=1080/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fcompany%2F942a46263306272c7f8fa26d3281e96a.webp",
        companyBareksa: "https://www.harisenin.com/cdn-cgi/image/width=1080/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fcompany%2F211019c0339b4b6265a3a0ac21089e3e.webp",
        companyCMLabs: "https://www.harisenin.com/cdn-cgi/image/width=1080/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fcompany%2Fb7157e43d609d0dd196790fb1e5970a1.webp",
        companyHaloka: "https://www.harisenin.com/cdn-cgi/image/width=1080/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fcompany%2F3636ebf2919fd9271e20a88e52455a43.webp",
        companyRukita: "https://www.harisenin.com/cdn-cgi/image/width=1080/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fcompany%2Ff399232d19c95f889e17fe0d35df3599.webp",
    },
    sectionLearningExperience: {
        areaRiseLearnAndPlay: "Rise Learn & Play",
        areaRiseUp: "Riseup+",
        descOnBoarding: "ONBOARDING Di sesi ini, kamu akan mengikuti dua kali onboarding untuk lebih mengenal program yang kamu pilih dan juga Team Buddy kamu selama mengikuti program bootcamp. Khusus untuk Get A Job Program, akan ada satu sesi lagi, yaitu Student Success Session.",
        descSkillAcquisition: "SKILL ACQUISITION & MISSION Di sesi ini, kamu dibimbing untuk mengenal dasar digital marketing, mengidentifikasi target audiens dan market, dan cara menggunakan tools dan strategi pemasaran yang sesuai. Pembelajaran juga didukung live mission berbasis studi kasus tertentu.",
        metodeLearning: "Dapatkan Akses Terhadap Metode Belajar Terbaik",

        titleInteractiveLearning: "Interactive Learning",
        descInteractiveLearning: "Gak hanya materi, di kelas juga akan ada sesi diskusi biar proses belajarmu makin hidup.",
        bgImageInteractiveLearning: "https://www.harisenin.com/cdn-cgi/image/width=1080/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fpublic%2Fassets%2Fpage-assets%2Fcourse_method-1.png",

        titleMissionChalenges: "Missions & Challenges",
        descMissionChalenges: "Belajar jadi paham dan makin seru karena ada misi yang perlu diselesaikan untuk bantu improve skill-mu.",
        bgImageMissionChalenge: "https://www.harisenin.com/cdn-cgi/image/width=1080/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fpublic%2Fassets%2Fpage-assets%2Fcourse_method-2.png",

        titleTeamBuddy: "Team Buddy",
        descTeamBuddy: "Kamu bisa bertanya dan akan dibimbing oleh Team Buddy saat di dalam maupun di luar kelas.",
        bgImageTeamBuddy: "https://www.harisenin.com/cdn-cgi/image/width=1080/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fpublic%2Fassets%2Fpage-assets%2Fcourse_method-3.png",

        titleCareerSuport: "Career Support",
        descCareerSuport: "Kamu akan mendapatkan akses terhadap konsultasi karier seperti pembuatan CV hingga persiapan interview.",
        bgImageCareerSuport: "https://www.harisenin.com/cdn-cgi/image/width=1080/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fpublic%2Fassets%2Fpage-assets%2Fcourse_method-4.png",
    },
    sectionTestimoni: {
        imageTestimoniGloriana: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Ftestimony%2F98208e43a4a8cdc47439c6d2cbae0f09.webp",
        companyJNT: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fcompany%2F8d0b519242225f4caf9338bc455adf71.webp",

        imageTestimoniAbiyyu: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Ftestimony%2Fe65f67d4f642a97881c106c3e0fe454e.webp",
        companyXXI: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fcompany%2F693c9f1f6b4788e951a600bb851ee8d8.webp",

        imageTestimoniSyafira: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Ftestimony%2F73ed1c5775b34c7b2e78f19623f1ea3b.webp",
        companyFlip: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fcompany%2F888980ca87b9d5e82b275e15d61ae4f5.webp",

        imageTestimoniRidhol: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Ftestimony%2Fdb1b8b03ad45ada879f13dea0a0f50f7.webp",
        companyArkademy: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fcompany%2F2403c3fcb6a155a5b0b35e1e8ae04c4a.webp",

        imageTestimoniCecelia: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Ftestimony%2F1c1dbaddb75a55a9cdc389a756dcb4c5.webp",
        companyHMSampoerna: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fcompany%2Fadb0ed2e238c49912e1d6ab57f64482b.webp",

        imageTestimoniDony: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Ftestimony%2F40b8774fccdeb2be09e07ea9fb571669.webp",
        companyCoulavaDigital: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fcompany%2F2d550b4f9f2bfa95bf906b25b7449307.webp",

        imageTestimoniDiniOktaviani: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Ftestimony%2Fc33522631c1e47bf1c81ed3d854e5ea5.webp",
        companyKitaBisa: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fcompany%2F5a0d81be003a5a17b02e89be9024e999.webp",

        imageTestimoniGalih: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Ftestimony%2F94742bce7f30cdef4da10041a878aefa.webp",
        companyTokopedia: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fcompany%2F92bf16ef8e4aab03ab4eefa0c7acede6.webp",

        imageTestimoniHanifah: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Ftestimony%2F345b661bdc3f0fc77853f9528a7efd5e.webp",
        companyXiaomi: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fcompany%2F8dadb66e78aed2d73b8b92bd49772a6e.webp",

        imageTestimoniVebrian: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Ftestimony%2Fac4d5c3bde8674205e385a4633352fa7.webp",
        companyRevcomm: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fcompany%2Fdcbd4a7f50955226b133782b4b081b06.webp",

        imageTestimoniSyafiqa: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Ftestimony%2F761eb104590e91eece1fff35a64cdb4b.webp",
        
        imageTestimoniPutriAisha: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Ftestimony%2F7f32e9be74d23d5bd8088632d40cd15b.webp",
        companySpaceAndShapes: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fcompany%2Ffecfc1ba3b6fc81ed8e3d551f13869c6.webp",

        imageTestimoniGregorius: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Ftestimony%2Fa987f111f4d68f0e3ac39617ab6eb1c5.webp",
        companyKiatAnandaGroup: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fcompany%2Fcec3b15cd2e91567a781f1d3267aace9.webp",

        imageGreogoriusEfendy: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Ftestimony%2F6ddb02b39373df3f1fe19f6f89ff1e0d.webp",
        companyMetroTV: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fcompany%2Fc34bf960ad1958364a10c9aa9b77a1c3.webp",

        imageTestimoniDafaNoor: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Ftestimony%2F7706a61eedbc5bfa266535b0e16b3a3e.webp",
        companyGrab: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fcompany%2F2c124e47d1cf348fd0c54cedd52c1b2f.webp",

        imageTestimoniAthiyya: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Ftestimony%2Fc4d5a94d97d53d2d612590d3ef811bef.webp",
        companySchoters: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fcompany%2F15f0463c42d3ef30c5491444562d8689.webp",

        imageTestimoniAlif: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Ftestimony%2F0b5f216d507e513307a57e091971f27a.webp",
        companyOfficeSecuritySolution: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fcompany%2F3df3868b2be4c892f53b428513729691.webp",

        imageTestimoniJermyNapoli: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Ftestimony%2Ffe4f72866bcea9a7b37ff6f271da4300.webp",
        companyKlinikUtama: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fcompany%2F71dc85809b8c7d8e0ad789594df442ac.webp",

        imageTestimoniNandaFitri: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Ftestimony%2F1436e8a7053f530c2287894912f40227.webp",
        companyPowerCommerceAsia: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fcompany%2Ff7a5017e233db3f83fba1f80c2408cb4.webp",

        imageTestimoniDimasPrabantio: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Ftestimony%2Fe6f4b7c71e0d1e7da8d5468717aa7111.webp",
        companyPoetiMountain: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fcompany%2F3cbf37b6dec47ac24ede5bd727d0ee49.webp",

        imageTestimoniFendikDana: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Ftestimony%2Fda9c22e27b6b2955a36676df06a26e95.webp",
        companyEximdo: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fcompany%2F048a14e91062fa8e82d614974965d1c9.webp",

        imageTestimoniHudanDardiri: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Ftestimony%2Fb0a5d0c5752dfe5f5b0f92d201cf59b5.webp",
        companyKubuku: "https://www.harisenin.com/cdn-cgi/image/width=128/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fcompany%2Fe5c7ac212c15d40405d03eec5b82a1d7.webp",
    },
    sectionPortfolioAlumni: {
        imagePortfolio_0: "https://www.harisenin.com/cdn-cgi/image/width=1920/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fhms%2Fthumbnail%2F16fed6478932bf5d1dcd86aefcfa8aaa.webp",
        imagePortfolio_1: "https://www.harisenin.com/cdn-cgi/image/width=1920/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fhms%2Fthumbnail%2F0c2ad0952a93b21d96a6f03e27c133ce.webp",
        imagePortfolio_2: "https://www.harisenin.com/cdn-cgi/image/width=1920/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fhms%2Fthumbnail%2Fd1a6171b347b73a69139979e3359c51e.webp",
        imagePortfolio_3: "https://www.harisenin.com/cdn-cgi/image/width=1920/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fhms%2Fthumbnail%2F78d7965962763ed48133d3b8b5ddc87e.webp",
        imagePortfolio_4: "https://www.harisenin.com/cdn-cgi/image/width=1920/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fhms%2Fthumbnail%2F3ccbd5448a8e83db6028d8f0cd3aedf0.webp",
        imagePortfolio_5: "https://www.harisenin.com/cdn-cgi/image/width=1920/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fhms%2Fthumbnail%2Fd05b9d4182d28a4a9f49a9bdd0d6dfbd.webp",
        imagePortfolio_6: "https://www.harisenin.com/cdn-cgi/image/width=1920/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fhms%2Fthumbnail%2Fb41e8458396dfb12c9060799f01b8371.webp",
        imagePortfolio_7: "https://www.harisenin.com/cdn-cgi/image/width=1920/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fhms%2Fthumbnail%2F1c1fb0f95009ebf9714083b66c2ff254.webp",
        imagePortfolio_8: "https://www.harisenin.com/cdn-cgi/image/width=1920/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fhms%2Fthumbnail%2F778287cfc412206b620acd1d439a3fb3.webp",
        imagePortfolio_9: "https://www.harisenin.com/cdn-cgi/image/width=1920/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fhms%2Fthumbnail%2Fe52119ee3ff47ec888514b280456f03d.webp",
        imagePortfolio_10: "https://www.harisenin.com/cdn-cgi/image/width=1920/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fhms%2Fthumbnail%2F6f6c6255abb70b4ff8fbd884b55ee2b7.webp",
        imagePortfolio_11: "https://www.harisenin.com/cdn-cgi/image/width=1920/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fhms%2Fthumbnail%2F2bd3a89557768b5269142cfdcc79f17c.webp",
        imagePortfolio_12: "https://www.harisenin.com/cdn-cgi/image/width=1920/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fhms%2Fthumbnail%2F5fab92a9446826116cf9924bb6890f46.webp",
        imagePortfolio_13: "https://www.harisenin.com/cdn-cgi/image/width=1920/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fhms%2Fthumbnail%2Fa01bf77c9c266d84b6908fdc6f0889e5.webp",
        imagePortfolio_14: "https://www.harisenin.com/cdn-cgi/image/width=1920/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fhms%2Fthumbnail%2F0834a74eee1afcbe20cdb92619a15061.webp",
        imagePortfolio_15: "https://www.harisenin.com/cdn-cgi/image/width=1920/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fhms%2Fthumbnail%2Ff1e95707930c38ef2bc8256b9894592a.webp",
        imagePortfolio_16: "https://www.harisenin.com/cdn-cgi/image/width=1920/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fhms%2Fthumbnail%2F3c65ea6d874b1e7b756a0194293667c5.webp",
        imagePortfolio_17: "https://www.harisenin.com/cdn-cgi/image/width=1920/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fhms%2Fthumbnail%2Fc3f9109bcde569786e88d632f4d83ae3.webp",
    },
    sectionCategoryProgram: {
        logoDanaCita: "https://www.harisenin.com/cdn-cgi/image/width=1080/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fhms%2Fschool-program%2Fprogram-type-logo%2Fc020c0c989a7bde9f8639625082e65b8.svg",
    },
    sectionPaymentMethod: {
        urlIconCashUpFront: "https://www.harisenin.com/cdn-cgi/image/width=96/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fpublic%2Fassets%2Ficon%2Ficon_register-4.png",
        urlIconCicilan: "https://www.harisenin.com/cdn-cgi/image/width=96/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fpublic%2Fassets%2Ficon%2Ficon_register-5.png",
    },
    sectionOtherBootcamp: {
        imagecardFullstackWeb: "https://www.harisenin.com/cdn-cgi/image/width=1200/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fhms%2Ffullstack-image.jpeg",
        imagecardHumanresource: "https://www.harisenin.com/cdn-cgi/image/width=1200/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fhms%2Faset%20web%20(5).png",
        imageCardBig4Auditor: "https://www.harisenin.com/cdn-cgi/image/width=1200/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fhms%2Faudit-image.png",
        imageCardDigmar: "https://www.harisenin.com/cdn-cgi/image/width=1200/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fhms%2Fdigmar-image.png",
        imageCardUXR: "https://www.harisenin.com/cdn-cgi/image/width=1200/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fhms%2Fuiux-image.jpeg",
    },
}