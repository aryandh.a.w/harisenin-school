module.exports = {
    banner: {
        subtitle: "Tingkatkan skill menjadi Human Resource profesional dengan Job Guarantee Program paling terjangkau di Indonesia. Dapatkan unlimited career support sampai kamu diterima bekerja.",
        urlImage: "https://www.harisenin.com/cdn-cgi/image/width=3840/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fhms%2Fb5f61979f0af3253346238665c6ec588.webp",
    },
    sectionTutor: {
        pototutorMariaGiovani: "https://www.harisenin.com/cdn-cgi/image/width=2048/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Ftutor%2Fharisenin-1632835925.jpg",
        pototutorRetnoPratiwi: "https://www.harisenin.com/cdn-cgi/image/width=2048/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Ftutor%2Fc9988a8657aedf25d211f92c71efc8c3.webp",
        pototutorSyahadatanBeva: "https://www.harisenin.com/cdn-cgi/image/width=2048/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Ftutor%2Fa3746ab872019f5897fd6ede8bc65331.webp",
        pototutorKievasCahyadi: "https://www.harisenin.com/cdn-cgi/image/width=2048/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Ftutor%2Fb5bcc43fc591af4f199ad8a61b0771df.webp",
        pototutorDioRizkiAnamia: "https://www.harisenin.com/cdn-cgi/image/width=2048/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Ftutor%2Fff74f2b51317ccdb2bfba78e6641cd0e.webp",

        urlCompanyWings: "https://www.harisenin.com/cdn-cgi/image/width=1080/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fcompany%2F8b199ee6acd715c6b75e2cef033d73ed.webp",
        urlCompanySBElectric: "https://www.harisenin.com/cdn-cgi/image/width=1080/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fcompany%2Fd86eedc3c0d4cd96b58e7286b6b0e3f0.webp",
        urlCompanyLemonilo: "https://www.harisenin.com/cdn-cgi/image/width=1080/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fcompany%2F4d33ea7a3e47f7607c5c4929634d21ab.webp",
        urlCompanyAltechOmega: "https://www.harisenin.com/cdn-cgi/image/width=1080/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fcompany%2F9c9b9a6fa47893db527e315eb9d4c939.webp",
        urlCompanyAksel: "https://www.harisenin.com/cdn-cgi/image/width=1080/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fcompany%2F3af53edb5f3541762d9abff875937bb6.webp",
    },
}