module.exports = {
  data_url: {
    harisenin_web: "https://www.harisenin.com/",
    harisenin_school: "https://www.harisenin.com/school",
    scholl_bootcamp: "https://www.harisenin.com/school/bootcamp",
    school_proclass: "https://www.harisenin.com/school/proclass",
  },
  data_auth: {
    name: "QA Tester",
    email: "lamel.lamontae@foundtoo.com",
    phone: "085377745621",
    password_valid: "Pa$$w0rd!",
    password_invalid: "harisenin",
    login_email: "lamel.lamontae@foundtoo.com",
    login_password: "Pa$$w0rd!",
  }
}