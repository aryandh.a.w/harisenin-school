module.exports = {
  url: {
    schoolBootcamp: ""
  },
  assetImage: {
    imageBanner: "https://www.harisenin.com/cdn-cgi/image/width=3840/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fpublic%2Fassets%2Fpage-assets%2Fbootcamp_banner.png",
    captureClass: "https://www.harisenin.com/cdn-cgi/image/width=1920/https%3A%2F%2Fnos.jkt-1.neo.id%2Fharisenin-storage%2Fpublic%2Fassets%2Fpage-assets%2Fbootcamp_asset_1.png",
  },
  bootcamp: {
    fullstackwebdev: "Full-stack Web Developer",
  }
}