// Require:
var postmark = require("postmark");
var json = require("./package.json")

require('dotenv').config();


// Send an email:
var client = new postmark.ServerClient("8f3b0287-44ba-44c8-bbc0-f86679b642c1");

client.sendEmailWithTemplate({
  "From": "deployment@kkbahagia.com",
  "To": "developer@harisenin.com",
  "TemplateAlias": "deployment",
  "TemplateModel": {
    "service_name": json.name,
    "version": json.version,
    "environtment": process.env.NODE_ENV,
    "deployment_time": Date(Date.now()).toString(),
  },
  "MessageStream": "outbound"
});