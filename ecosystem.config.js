module.exports = {
  apps: [
    {
      name: "harisenin-school",
      namespace: "harisenin",
      script: "./dist/index.js",
      watch: true,
      env: {
        PORT: 3001,
        NODE_ENV: "production",
      },
      env_development: {
        NODE_ENV: "development",
      },
    },
  ],
}
